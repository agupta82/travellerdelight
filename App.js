/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Image, View, BackHandler, ToastAndroid, StatusBar } from "react-native"
import { createAppContainer, createSwitchNavigator, } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { store, persistor } from './src/Redux/Store/Store'
import NavigationServices from './src/Helper Classes/NavigationServices'
import AsyncStorage from '@react-native-community/async-storage'

//Importing Third Parties

import Icon from "react-native-vector-icons/Entypo";
import { Provider } from "react-redux";
import { PersistGate } from 'redux-persist/integration/react'
//Importing Classes

import HomeScreen from "./src/components/Home";
import BookingList from "./src/components/BookingList";
import Message from "./src/components/Message";
import Profile from "./src/components/Profile";
import FlightSearch from "./src/components/Flight/FlightSearch/FlightSearch";
import FlightList from "./src/components/Flight/FlightList/FlightList";
import PassengerDetails from "./src/components/Flight/PassengerDetails/PassengerDetails";
import MakePayment from "./src/components/MakePayment";
import SearchAirport from "./src/components/Flight/SearchAirport";
import Calender from "./src/components/Calender";
import HotelSearch from "./src/components/Hotel/HotelSearch/HotelSearch";
import HotelList from "./src/components/Hotel/HotelList/HotelList";
import HotelOverview from "./src/components/Hotel/HotelOverview/HotelOverview";
import RoomsList from "./src/components/Hotel/RoomList/RoomsList";
import BookingConfirmation from "./src/components/BookingConfirmation";
import BookingDetails from "./src/components/Flight/BookingDetails";
import FlightDetails from "./src/components/Flight/FlightDetails/FlightDetails";
import EditProfile from "./src/components/EditProfile";
import GuestDetails from "./src/components/Hotel/GuestDetails/GuestDetails";
import ReviewBooking from "./src/components/Hotel/ReviewBooking/ReviewBooking";
import HotelBookingDetails from "./src/components/Hotel/HotelBookingDetail/HotelBookingDetails";
//import ReviewFlightBooking1 from "./src/components/Flight/ReviewBooking/ReiewBooking1";
import ReviewFlightBooking from "./src/components/Flight/ReviewBooking/ReviewBooking";
import ForgotPassword from "./src/components/ForgotPassword";
import { colors } from "./src/Helper Classes/Colors";
import Images from "./src/Helper Classes/Images";
import Tutorials from "./src/components/Tutorials";
import Authentication from "./src/components/Authentication";
import PaymentScreen from "./src/components/Payment/PaymentScreen";
import BookingSuccess from "./src/components/Flight/BookingSuccess";
import PaypalWebView from "./src/components/Payment/PaypalWebView"
//imporct { AuthProvider} from "./src/Context/AuthContext";

// const store = configureStore();


class TabIcon extends Component {
  render() {
    // console.log("props are:=" + JSON.stringify(this.props));
    return (
      <View
        style={[
          styles.iconStyle,
          {
            borderTopWidth: this.props.focused ? 1.5 : 0,
            borderTopColor: this.props.tintColor
          }
        ]}
      >
        <Image source={this.props.imageIcon} style={{ width: 18, height: 18, resizeMode: "contain" }}></Image>
      </View>
    );
  }
}
const ProfileStack = createStackNavigator({
  Profile: {
    screen: Profile
  },
  Authentication: {
    screen: Authentication
  },
  ForgotPassword: {
    screen: ForgotPassword
  }
}, {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

const TabNavigator = createBottomTabNavigator({
  Home: {
    screen: HomeScreen,
    // screen: ReviewFlightBooking,
    navigationOptions: () => ({
      tabBarIcon: ({ focused, tintColor }) => (
        <TabIcon focused={focused}
          tintColor={tintColor}
          imageIcon={focused ? Images.imghomeactive : Images.imghomedeactive}>
        </TabIcon>
      )
    })
  },
  "My Trips": {
    screen: BookingList,
    navigationOptions: () => ({
      tabBarIcon: ({ focused, tintColor }) => (
        <TabIcon focused={focused}
          tintColor={tintColor}
          imageIcon={focused ? Images.imgtripsactive : Images.imgtripsdeactive}>
        </TabIcon>
      ),
      // tabBarLabel: <View />
    })
  },
  Accounts: {
    screen: ProfileStack,
    navigationOptions: () => ({
      tabBarIcon: ({ focused, tintColor }) => (
        <View style={{
          position: "relative",
          height: '100%',
          width: '100%',
          justifyContent: "center",
          alignItems: "center"
        }}>
          <View style={{
            position: "relative",
            zIndex: 99,
            top: -15,
            shadowColor: colors.colorBlack,
            shadowOpacity: .5,
            shadowRadius: 5,
            shadowOffset: { height: 4, width: 1 },
            elevation: 5,
            borderRadius: 27,
          }}>
            <Image source={Images.imgaccount} style={{
              width: 45,
              height: 45,
              resizeMode: "contain",
            }}></Image>
          </View>
        </View>
      ),
    })
  },
  Wallet: {
    screen: Message,
    navigationOptions: () => ({
      tabBarIcon: ({ focused, tintColor }) => (
        <TabIcon focused={focused}
          tintColor={tintColor}
          imageIcon={focused ? Images.imgwalletactive : Images.imgwalletdeactive}>
        </TabIcon>
      ),
    })
  },
  Help: {
    screen: Message,
    navigationOptions: () => ({
      tabBarIcon: ({ focused, tintColor }) => (
        <TabIcon focused={focused}
          tintColor={tintColor}
          imageIcon={focused ? Images.imghelpactive : Images.imghelpdeactive}>
        </TabIcon>
      ),
    })
  }
}, {
  //initialRouteName: "Wallet",
  tabBarOptions: {
    keyboardHidesTabBar: true,
    showLabel: true,
    inactiveTintColor: colors.colorBlack,
    activeTintColor: colors.colorBlue
  },
  lazy: true,
});

const DashboardStack = createStackNavigator({
  HomeTabStack: {
    screen: TabNavigator
  },
  FlightSearch: {
    screen: FlightSearch
  },
  FlightList: {
    // screen: FlightListPrevious1,
    screen: FlightList,
  },
  PassengerDetails: {
    screen: PassengerDetails
  },
  MakePayment: {
    screen: MakePayment
  },
  SearchAirport: {
    screen: SearchAirport
  },
  HotelSearch: {
    screen: HotelSearch
  },
  HotelList: {
    screen: HotelList
  },
  HotelOverview: {
    screen: HotelOverview
  },
  // BookingConfirmation: {
  //   screen: BookingConfirmation
  // },
  ReviewFlightBooking: {
    screen: ReviewFlightBooking
  },
  BookingDetails: {
    screen: BookingDetails
  },
  RoomsList: {
    screen: RoomsList
  },
  EditProfile: {
    screen: EditProfile
  },
  Calender: {
    screen: Calender
  },
  FlightDetails: {
    screen: FlightDetails
  },
  GuestDetails: {
    screen: GuestDetails
  },
  ReviewBooking: {
    screen: ReviewBooking
  },
  HotelBookingDetails: {
    screen: HotelBookingDetails
  },
  PaymentScreen: {
    screen: PaymentScreen
  },
  PaypalWebView: {
    screen: PaypalWebView
  }
  // FlightList: {
  //   screen: FlightList
  // },
  // HotelBookingDetails: {
  //   screen: HotelBookingDetails
  // },

}, {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

const TutorialsStack = createStackNavigator({
  Tutorials: {
    screen: Tutorials
  },
}, {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

class App extends Component {
  constructor(props) {
    super(props)
    this.lastBackButtonPress = null;
    this.state = {
      isTutorial: null
    }
  }

  componentDidMount() {
    AsyncStorage.getItem("isTutorial", (error, res) => {
      if (error) {
        isTutorial = false
      } else if (res) {
        isTutorial = JSON.parse(res)
      }
      else isTutorial = false
      this.setState({
        isTutorial: isTutorial
      })
    })
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {

      if (this.lastBackButtonPress + 2000 >= new Date().getTime()) {
        BackHandler.exitApp();
        return true;
      }

      else {
        ToastAndroid.show("Press back again to exit", ToastAndroid.SHORT);
        this.lastBackButtonPress = new Date().getTime();
        return true;
      }


    });
  }

  componentWillUnmount() {
    this.backHandler.remove()
  }

  render() {
    if (this.state.isTutorial === null) { return null }
    const SwitchNavigator = createAppContainer(
      createSwitchNavigator({
        DashboardStack: DashboardStack,
        TutorialsStack: TutorialsStack,
        BookingSuccess: BookingSuccess
      }, {
        initialRouteName: this.state.isTutorial ? "DashboardStack" : "TutorialsStack"
      })
    );

    console.disableYellowBox = true;
    return (
      // <AuthProvider>
      <Provider store={store}>

        <PersistGate loading={null} persistor={persistor}>
          <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} />
          <SwitchNavigator ref={navigatorRef => {
            NavigationServices.setTopLevelNavigator(navigatorRef);
          }} />
        </PersistGate>
      </Provider>
      // </AuthProvider>
    );
  }
}

const styles = StyleSheet.create({
  tabBar: {
    borderTopWidth: 0.5,
    borderColor: "gray",
    backgroundColor: "#f5f5f5",
    opacity: 1
  },
  navTitle: {
    color: "white" // changing navbar title color
  },
  iconStyle: {
    // alignItems: "center",
    // alignSelf: "center",
    // justifyContent: "center",
    // height: "90%",
    // width: "50%",
    // marginBottom: 5
    borderWidth: 2, height: '100%',
    width: '100%', borderColor: 'white',
    justifyContent: 'center', alignItems: 'center'
  }
});

export default App