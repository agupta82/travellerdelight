import React, { Component } from "react";
import { Text, View, Dimensions, StatusBar, StyleSheet, Image, TouchableHighlight, ScrollView, DeviceEventEmitter, SafeAreaView, Modal, TouchableOpacity, TextInput } from "react-native";
import NavigationServices from '../../Helper Classes/NavigationServices'
import Images from "../../Helper Classes/Images";
import { colors } from "../../Helper Classes/Colors";

import moment from 'moment'
import Fonts from "../../Helper Classes/Fonts";
const screenWidth = Dimensions.get('window').width;
import Spinner from "react-native-loading-spinner-overlay";
import StringConstants from '../../Helper Classes/StringConstants';
import { connect } from "react-redux";
import { getAirportListAction, searchCityAction } from '../../Redux/Actions'

class SelectAirPortModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      airports: [
        {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }, {
          name: "Mumbai",
          code: "BOM",
          fullName: "Chhatrapati Shivaji International Airport"
        }
      ],
      searchText: ""
    }
  }

  componentDidMount = () => {
    DeviceEventEmitter.addListener(StringConstants.SEARCH_AIRPORT_EVENT, this.searchAirports)
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.SEARCH_AIRPORT_EVENT, this.searchAirports);
    // DeviceEventEmitter.removeListener(StringConstants.SEARCH_CITY_EVENT, this.updateCityDataFromApi);
  }

  searchAirports = (name) => {
    this.props.getAirportListAction();
  }

  render() {
    let { airports, searchText } = this.state;
    let { closeModal, isLoading, airPortListData } = this.props;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}
      >
        <Spinner
          visible={isLoading}
          textContent={"Loading..."}
          textStyle={{ color: colors.colorWhite }}
        />
        <View style={styles.mainContainer}>
          <View style={styles.header}>
            <TouchableOpacity style={styles.headerBtn} onPress={() => closeModal()}>
              <Image source={Images.imgClose} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
            </TouchableOpacity>
            <TextInput style={{
              width: screenWidth - 110,
              height: 50
            }}
              placeholder="Enter City/Airport Name"
              placeholderTextColor={colors.colorBlack}
              value={searchText}
              onChangeText={(text) => {
                this.setState({ searchText: text }, () => {
                  // this.props.searchAirports();
                });
              }}
            ></TextInput>
            <TouchableOpacity style={styles.headerBtn}>
              <Text style={{
                fontSize: 14,
                fontFamily: Fonts.medium,
                color: colors.colorBlue
              }} onPress={() => this.setState({ searchText: "" })}>Clear</Text>
            </TouchableOpacity>
          </View>
          <ScrollView showsVerticalScrollIndicator={false}>
            {
              airPortListData.map((airport, index) => (
                <View>
                  <TouchableOpacity style={styles.item}>
                    <View style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}>
                      <Text style={styles.name}>{airport.name}</Text>
                      <Text style={styles.code}>{airport.code}</Text>
                    </View>
                    <Text style={styles.fullName}>{airport.fullName}</Text>
                  </TouchableOpacity>
                  <View style={styles.border}></View>
                </View>
              ))
            }
          </ScrollView>
        </View>
      </Modal>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
    airPortListData: state.airportListDataReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAirportListAction: () => dispatch(getAirportListAction()),
    searchCityAction: (data) => dispatch(searchCityAction(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectAirPortModal)

const styles = StyleSheet.create({
  border: {
    backgroundColor: "lightgrey",
    height: 1,
    marginLeft: 15,
    marginRight: 15
  },
  fullName: {
    color: colors.colorBlack,
    fontFamily: Fonts.regular,
    fontSize: 12,
    marginTop: 5
  },
  code: {
    color: colors.colorBlack,
    fontFamily: Fonts.regular,
    fontSize: 14
  },
  name: {
    color: colors.colorBlack,
    fontFamily: Fonts.semiBold,
    fontSize: 14
  },
  item: {
    height: 60,
    width: screenWidth,
    paddingLeft: 15,
    paddingRight: 15,
    padding: 10
  },
  header: {
    height: 50,
    width: "100%",
    backgroundColor: "rgb(246,245,246)",
    flexDirection: "row",
    alignItems: "center"
  },
  mainContainer: {
    marginTop: 50,
    flex: 1,
    backgroundColor: colors.colorWhite
  },
  headerBtn: {
    width: 50,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "red"
  }
})