import React, { Component } from "react";
import { Text, View, Dimensions, StatusBar, StyleSheet, Image, TouchableHighlight, ScrollView, DeviceEventEmitter, SafeAreaView, Modal, TouchableOpacity, TextInput } from "react-native";
import NavigationServices from '../../Helper Classes/NavigationServices'
import Images from "../../Helper Classes/Images";
import Dash from "react-native-dash";
import NavigationBar from "../../Helper Classes/NavigationBar";
import Constants from "../../Helper Classes/Constants";
import SelectTraveller from '../../components/Flight/SelectTraveller'
import SelectFlightClass from '../../components/Flight/SelectFlightClass'
import { colors } from "../../Helper Classes/Colors";
import { commonstyle } from "../../Helper Classes/Commonstyle";
import { connect } from "react-redux"
import { saveFlightSearchParams } from "../../Redux/Actions/Actions";
import { getStatusBarHeight } from 'react-native-iphone-x-helper';

import moment from 'moment'
import Fonts from "../../Helper Classes/Fonts";
import StringConstants from "../../Helper Classes/StringConstants";
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class SelectTravellerModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numberOfAdults: this.props.numberOfAdults,
      numberOfChilds: this.props.numberOfChilds,
      numberOfInfant: this.props.numberOfInfant,
    }
  }

  renderClassView = () => {
    return (
      <View>
        <View style={{
          alignItems: "center",
          padding: 15
        }}>
          <Text style={styles.text}>Stop</Text>
        </View>
        <View style={styles.classContainer}>
          <TouchableOpacity style={[styles.classButton, {
            backgroundColor: stopType == 1 ? colors.colorBlue : colors.lightBlue,
          }]} onPress={() => this.setState({ stopType: 1 })}>
            <Text style={[styles.btnText, {
              color: stopType == 1 ? colors.colorWhite : colors.colorBlack
            }]}>NONSTOP</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.classButton, {
            marginHorizontal: 13,
            backgroundColor: stopType == 2 ? colors.colorBlue : colors.lightBlue,
          }]} onPress={() => this.setState({ stopType: 2 })}>
            <Text style={[styles.btnText, {
              color: stopType == 2 ? colors.colorWhite : colors.colorBlack
            }]}>1 STOP</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.classButton, {
            backgroundColor: stopType == 3 ? colors.colorBlue : colors.lightBlue,
          }]} onPress={() => this.setState({ stopType: 3 })}>
            <Text style={[styles.btnText, {
              color: stopType == 3 ? colors.colorWhite : colors.colorBlack
            }]}>2+ STOP</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  validateAdults(adultNum, childNum, infantNum) {
    if (adultNum == 0) {
      alert("Please add atleast 1 adult.");
      return;
    }

    let dict = {
      adultsNum: adultNum,
      childNum: childNum,
      infantNum: infantNum
    };

    DeviceEventEmitter.emit(StringConstants.TRAVELLER_SELECT, dict);
  }

  render() {
    let { child, infant, adult, numberOfAdults, numberOfChilds, numberOfInfant } = this.state;
    let { closeModal } = this.props;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}
      >
        <View style={styles.mainContainer}>
          <View style={styles.header}>
            <TouchableOpacity style={styles.headerBtn} onPress={() => closeModal()}>
              <Image source={Images.imgClose} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
            </TouchableOpacity>
            <View style={styles.headerTitle}>
              <Text style={styles.titleText}>Select Travellers & Stop</Text>
            </View>
            <TouchableOpacity style={styles.headerBtn} onPress={() => {
              console.log("numberOfAdults:-" + numberOfAdults)
              if (numberOfAdults == 0) {
                alert("Please add atleast 1 adult.");
                return;
              }

              let dict = {
                adultsNum: numberOfAdults,
                childNum: numberOfChilds,
                infantNum: numberOfInfant
              };

              closeModal();
              DeviceEventEmitter.emit(StringConstants.TRAVELLER_SELECT, dict);
            }}>
              <Text style={{
                fontSize: 14,
                fontFamily: Fonts.medium,
                color: colors.colorBlue
              }}>Done</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.mainView}>
            <View style={styles.row}>
              <Text style={styles.text}>Adults
                <Text style={{
                  fontFamily: Fonts.regular,
                  fontSize: 12
                }}> (Above 12 Yrs)</Text>
              </Text>
              <View style={styles.buttonView}>
                <TouchableOpacity onPress={() => {
                  this.setState({
                    numberOfAdults: numberOfAdults + 1
                  });
                  this.forceUpdate();
                }}>
                  <Image source={Images.imgplus} style={styles.btnImg} ></Image>
                </TouchableOpacity>
                <View style={styles.inputStyle}>
                  <Text style={{ textAlign: "center" }}>{numberOfAdults}</Text>
                </View>
                <TouchableOpacity onPress={() => {
                  if (numberOfAdults > 0) {
                    this.setState({
                      numberOfAdults: numberOfAdults - 1
                    });
                  }
                }}>
                  <Image source={Images.imgminus} style={styles.btnImg} ></Image>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.row}>
              <Text style={styles.text}>Children
                <Text style={{
                  fontFamily: Fonts.regular,
                  fontSize: 12
                }}> (2-12 Yrs)</Text>
              </Text>
              <View style={styles.buttonView}>
                <TouchableOpacity onPress={() => {
                  this.setState({
                    numberOfChilds: numberOfChilds + 1
                  });
                  this.forceUpdate();
                }}>
                  <Image source={Images.imgplus} style={styles.btnImg} ></Image>
                </TouchableOpacity>
                <View style={styles.inputStyle}>
                  <Text style={{ textAlign: "center" }}>{numberOfChilds}</Text>
                </View>
                <TouchableOpacity onPress={() => {
                  if (numberOfChilds > 0) {
                    this.setState({
                      numberOfChilds: numberOfChilds - 1
                    });
                  }
                }}>
                  <Image source={Images.imgminus} style={styles.btnImg} ></Image>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.row}>
              <Text style={styles.text}>Infants
                <Text style={{
                  fontFamily: Fonts.regular,
                  fontSize: 12
                }}> (Under 2 Yrs)</Text>
              </Text>
              <View style={styles.buttonView}>
                <TouchableOpacity onPress={() => {
                  this.setState({
                    numberOfInfant: numberOfInfant + 1
                  });
                  this.forceUpdate();
                }}>
                  <Image source={Images.imgplus} style={styles.btnImg} ></Image>
                </TouchableOpacity>
                <View style={styles.inputStyle}>
                  <Text style={{ textAlign: "center" }}>{numberOfInfant}</Text>
                </View>
                <TouchableOpacity onPress={() => {
                  if (numberOfInfant > 0) {
                    this.setState({
                      numberOfInfant: numberOfInfant - 1
                    });
                  }
                }}>
                  <Image source={Images.imgminus} style={styles.btnImg} ></Image>
                </TouchableOpacity>
              </View>
            </View>
          </View>

        </View>
      </Modal >
    )
  }
}

const styles = StyleSheet.create({
  btnText: {
    fontFamily: Fonts.medium,
    fontSize: 14, letterSpacing: .5,
  },
  inputStyle: {
    width: 50, height: 30, borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: "lightgrey",
    justifyContent: "center",
    alignItems: "center"
  },
  header: {
    height: 50,
    width: "100%",
    backgroundColor: "rgb(246,245,246)",
    flexDirection: "row",
    alignItems: "center"
  },
  mainContainer: {
    marginTop: 50,
    flex: 1,
    backgroundColor: colors.colorWhite
  },
  headerBtn: {
    width: 50,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "red"
  },
  headerTitle: {
    width: screenWidth - 110,
    height: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  titleText: {
    fontSize: 14,
    fontFamily: Fonts.medium,
    color: colors.colorBlack
  },
  mainView: {
    width: screenWidth,
    height: 180,
    borderBottomColor: "lightgrey",
    borderBottomWidth: 1,
    padding: 12
  },
  row: {
    width: "100%",
    height: 50,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  buttonView: {
    width: 110,
    height: 30,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    overflow: "hidden",
  },
  btnImg: { width: 30, height: 30, resizeMode: "contain" },
  text: {
    fontSize: 14,
    color: colors.colorBlack,
    fontFamily: Fonts.bold
  },
  classButton: {
    backgroundColor: colors.colorBlue,
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },
  classContainer: {
    width: "100%",
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
})