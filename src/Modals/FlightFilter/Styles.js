import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../Helper Classes/Colors";
import Fonts from "../../Helper Classes/Fonts";
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default styles = StyleSheet.create({
  bottomBottonText: {
    color: colors.colorWhite,
    fontSize: 20, letterSpacing: .7,
    fontFamily: Fonts.bold
  },
  bottomBotton: {
    backgroundColor: colors.colorBlue,
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  checkImg: { width: 20, height: 20, resizeMode: "contain" },
  checkBox: {
    width: "15%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  airlineImg: {
    width: 35, height: 35,
    resizeMode: "contain", marginRight: 10
  },
  checkViewContent: {
    width: "85%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center"
  },
  checkView: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 40,
  },
  timeBtn: {
    // backgroundColor: "red",
    height: 58, width: 50,
    margin: 6
  },
  textStyle: {
    fontFamily: Fonts.medium,
    fontSize: 14, letterSpacing: .7
  },
  btnStyle: {
    width: 110, margin: 6,
    paddingLeft: 15,
    paddingRight: 15,
    padding: 10,
    borderRadius: 10,
    alignItems: "center"
  },
  grayStrip: {
    width: "100%",
    backgroundColor: colors.gray,
    height: 40,
    justifyContent: "center",
    paddingLeft: 12
  },
  dividerText: {
    color: colors.colorBlack,
    fontSize: 14, fontFamily: Fonts.semiBold
  },
  contentView: {
    width: "100%",
    flexDirection: "row",
    padding: 6,
    alignItems: "center"
  },
  header: {
    height: 50,
    width: "100%",
    backgroundColor: "rgb(246,245,246)",
    flexDirection: "row",
    alignItems: "center"
  },
  headerTitle: {
    width: screenWidth - 110,
    height: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  titleText: {
    fontSize: 14,
    fontFamily: Fonts.medium,
    color: colors.colorBlack
  },
  mainContainer: {
    flex: 1,
    backgroundColor: colors.colorWhite
  },
  headerBtn: {
    width: 50,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "red"
  }
})