import React, { Component } from "react";
import { Text, View, Dimensions, StatusBar, StyleSheet, Image, TouchableHighlight, ScrollView, DeviceEventEmitter, Modal, TouchableOpacity, TextInput } from "react-native";
import { SafeAreaView } from "react-navigation";

import NavigationServices from '../../Helper Classes/NavigationServices';
import Images from "../../Helper Classes/Images";
import { colors } from "../../Helper Classes/Colors";
import Dash from "react-native-dash";
import NavigationBar from "../../Helper Classes/NavigationBar";
import Constants from "../../Helper Classes/Constants";
import SelectTraveller from '../../components/Flight/SelectTraveller';
import SelectFlightClass from '../../components/Flight/SelectFlightClass';
import { commonstyle } from "../../Helper Classes/Commonstyle";
import { connect } from "react-redux";
import { saveFlightSearchParams } from "../../Redux/Actions/Actions";
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import styles from "./Styles";
import moment from 'moment';
import Fonts from "../../Helper Classes/Fonts";
import { ThemeButton } from "../../Helper Classes/Button";
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class FlightFilterModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stop: 0,
      time: 1,
      airlines: [
        {
          imgUrl: "../../Assets/images/jet.png",
          name: "Jet Airways 304",
          id: 1,
          checked: true
        },
        {
          imgUrl: "../../Assets/images/jet.png",
          name: "Go Air",
          id: 2,
          checked: false
        }, {
          imgUrl: "../../Assets/images/jet.png",
          name: "Vistara ",
          id: 3,
          checked: false
        }, {
          imgUrl: "../../Assets/images/jet.png",
          name: "Air Asia",
          id: 4,
          checked: false
        }
      ],
      amenities: [
        {
          imgUrl: "../../Assets/images/meal.png",
          name: "Meals",
          id: 1,
          checked: true
        },
        {
          imgUrl: "../../Assets/images/jet.png",
          name: "Wifi",
          id: 2,
          checked: false
        }
      ]
    }
  }

  render() {
    let { stop, time, amenities, airlines } = this.state;
    let { closeModal } = this.props;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}
      >
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.colorBlue }}>
          <View style={styles.mainContainer}>
            <View style={styles.header}>
              <TouchableOpacity style={styles.headerBtn} onPress={() => closeModal()}>
                <Image source={Images.imgClose} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
              </TouchableOpacity>
              <View style={styles.headerTitle}>
                <Text style={styles.titleText}>Filters</Text>
              </View>
              <TouchableOpacity style={styles.headerBtn} onPress={() => closeModal()}>
                <Text style={{
                  fontSize: 14,
                  fontFamily: Fonts.medium,
                  color: colors.colorBlue
                }}>Done</Text>
              </TouchableOpacity>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={{ height: 5 }}></View>
              <View style={styles.grayStrip}>
                <Text style={styles.dividerText}>Preferred Departure Time From New Delhi</Text>
              </View>
              <View style={styles.contentView}>
                <TouchableOpacity style={styles.timeBtn} onPress={() => this.setState({ time: 1 })}>
                  <Image source={time == 1 ? Images.imgtime1active : Images.imgtime1deactive} style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
                </TouchableOpacity>
                <TouchableOpacity style={styles.timeBtn} onPress={() => this.setState({ time: 2 })}>
                  <Image source={time == 2 ? Images.imgtime2active : Images.imgtime2deactive} style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
                </TouchableOpacity>
                <TouchableOpacity style={styles.timeBtn} onPress={() => this.setState({ time: 3 })}>
                  <Image source={time == 3 ? Images.imgtime3active : Images.imgtime3deactive} style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
                </TouchableOpacity>
                <TouchableOpacity style={styles.timeBtn} onPress={() => this.setState({ time: 4 })}>
                  <Image source={time == 4 ? Images.imgtime4active : Images.imgtime4deactive} style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
                </TouchableOpacity>
              </View>
              <View style={styles.grayStrip}>
                <Text style={styles.dividerText}>Stops</Text>
              </View>
              <View style={[styles.contentView]}>
                <TouchableOpacity style={[styles.btnStyle, {
                  backgroundColor: stop == 0 ? colors.colorBlue : colors.lightBlue,
                }]} onPress={() => this.setState({ stop: 0 })}>
                  <Text style={[styles.textStyle, {
                    color: stop == 0 ? colors.colorWhite : colors.colorBlack,
                  }]}>NONSTOP</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.btnStyle, {
                  backgroundColor: stop == 1 ? colors.colorBlue : colors.lightBlue,
                }]} onPress={() => this.setState({ stop: 1 })}>
                  <Text style={[styles.textStyle, {
                    color: stop == 1 ? colors.colorWhite : colors.colorBlack,
                  }]}>1 STOP</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.btnStyle, {
                  backgroundColor: stop == 2 ? colors.colorBlue : colors.lightBlue,
                }]} onPress={() => this.setState({ stop: 2 })}>
                  <Text style={[styles.textStyle, {
                    color: stop == 2 ? colors.colorWhite : colors.colorBlack,
                  }]}>2+ STOP</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.grayStrip}>
                <Text style={styles.dividerText}>Preffer Airlines</Text>
              </View>
              <View style={[styles.contentView, { padding: 12, flexDirection: "column" }]}>
                {
                  airlines.map((airline, index) => (
                    <TouchableOpacity key={index} activeOpacity={0.8} style={styles.checkView} onPress={() => {
                      let airlines1 = airlines;
                      airlines1[index].checked = !airlines1[index].checked;
                      this.setState({ airlines: airlines1 });
                    }}>
                      <View style={styles.checkViewContent}>
                        <Image source={require("../../Assets/images/jet.png")} style={styles.airlineImg}></Image>
                        <Text style={styles.textStyle}>{airline.name}</Text>
                      </View>
                      <View style={styles.checkBox}>
                        <Image source={airline.checked ? Images.imgcheck : Images.imguncheck} style={styles.checkImg}></Image>
                      </View>
                    </TouchableOpacity>
                  ))
                }
              </View>
              <View style={styles.grayStrip}>
                <Text style={styles.dividerText}>Amenities</Text>
              </View>
              <View style={[styles.contentView, { padding: 12, flexDirection: "column" }]}>
                {
                  amenities.map((amenity, index) => (
                    <TouchableOpacity key={index} activeOpacity={0.8}
                      style={styles.checkView} onPress={() => {
                        let amenities1 = amenities;
                        amenities1[index].checked = !amenities1[index].checked;
                        this.setState({ amenities: amenities1 });
                      }}>
                      <View style={styles.checkViewContent}>
                        <Image source={require("../../Assets/images/meal.png")} style={styles.airlineImg}></Image>
                        <Text style={styles.textStyle}>{amenity.name}</Text>
                      </View>
                      <View style={styles.checkBox}>
                        <Image source={amenity.checked ? Images.imgcheck : Images.imguncheck} style={styles.checkImg}></Image>
                      </View>
                    </TouchableOpacity>
                  ))
                }
              </View>
            </ScrollView>
            <TouchableOpacity style={styles.bottomBotton} onPress={() => closeModal()}>
              <Text style={styles.bottomBottonText}>APPLY FILTERS</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </Modal>
    )
  }
}