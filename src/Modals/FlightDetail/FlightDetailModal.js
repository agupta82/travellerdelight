import React, { Component } from "react";
import { Text, View, Dimensions, StatusBar, StyleSheet, Image, TouchableHighlight, ScrollView, DeviceEventEmitter, Modal, TouchableOpacity, TextInput, Platform } from "react-native";
import NavigationServices from '../../Helper Classes/NavigationServices'
import Images from "../../Helper Classes/Images";
import Dash from "react-native-dash";
import NavigationBar from "../../Helper Classes/NavigationBar";
import Constants from "../../Helper Classes/Constants";
import SelectTraveller from '../../components/Flight/SelectTraveller'
import SelectFlightClass from '../../components/Flight/SelectFlightClass'
import { colors } from "../../Helper Classes/Colors";
import { commonstyle } from "../../Helper Classes/Commonstyle";
import { connect } from "react-redux"
import { saveFlightSearchParams } from "../../Redux/Actions/Actions";
import styles from "./Styles";
import moment from 'moment'
import { SafeAreaView } from "react-navigation";
import BottomStrip from "../../Helper Classes/BottomStrip";

export default class FlightDetailModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adult: "2",
      child: "2",
      infant: "2",
      stopType: 1
    }
  }

  checkout() {
    alert("Checking out");
  }

  render() {
    let { child, infant, adult, stopType } = this.state;
    let { closeModal } = this.props;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}
      >
        <View style={styles.mainContainer}>
          <SafeAreaView forceInset={{ bottom: 'always' }} style={{ flex: 1, backgroundColor: colors.colorBlue }}>
            <View style={{ backgroundColor: colors.colorWhite, flex: 1 }}>
              <View style={styles.header}>
                <TouchableOpacity style={styles.headerBtn} onPress={() => closeModal()}>
                  <Image source={Images.imgClose} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
                </TouchableOpacity>
                <View style={styles.headerTitle}>
                  <Text style={styles.titleText}>Flight Detail</Text>
                </View>
              </View>
              <View style={styles.flightView}>
                <View style={styles.flightRow1}>
                  <View style={styles.flightRow1Col}>
                    <Image source={require("../../Assets/images/jet.png")}
                      style={{ width: 30, height: 30, resizeMode: "contain" }}></Image>
                    <Text style={styles.flightName}>Vistara 9836</Text>
                  </View>
                  <View style={[styles.flightRow1Col, {
                    justifyContent: "flex-end"
                  }]}>
                    <Image source={Images.imgfood} style={styles.flightAmenityImg}></Image>
                    <Image source={Images.imgflightinfo} style={styles.flightAmenityImg}></Image>
                    <Image source={Images.imgseat} style={styles.flightAmenityImg}></Image>
                  </View>
                </View>
                <View style={styles.flightRow2}>
                  <View style={styles.cardContent}>
                    <View style={styles.cardContentRow1}>
                      <View style={{ width: "25%" }}>
                        <Text style={styles.estimate}>20:15</Text>
                        <Text style={styles.stnName}>New Delhi</Text>
                      </View>
                      <View style={styles.estimateView}>
                        <View style={{
                          width: "45%",
                          alignItems: "center"
                        }}>
                          <View style={styles.dotView}>
                            <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
                            <Dash style={{
                              width: "75%",
                              height: 1,
                            }} dashColor={colors.gray} dashLength={5}
                            />
                          </View>
                        </View>
                        <View style={{
                          width: "10%",
                          height: 20,
                          marginTop: 5
                        }}>
                          <Image source={Images.imgplaneblue} style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
                        </View>
                        <View style={{
                          width: "45%",
                          alignItems: "center"
                        }}>
                          <View style={styles.dotView}>
                            <Dash style={{
                              width: "75%",
                              height: 1,
                            }} dashColor={colors.gray} dashLength={5}
                            />
                            <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
                          </View>
                        </View>
                      </View>
                      <View style={{ width: "25%", alignItems: "flex-end" }}>
                        <Text style={styles.estimate}>20:15</Text>
                        <Text style={styles.stnName}>Nagpur</Text>
                      </View>
                    </View>
                    <View style={styles.cardContentRow2}>
                      <View style={{ width: "40%" }}>
                        <Text numberOfLines={2} style={styles.airportName}>Indira Gandhi International Airport</Text>
                      </View>
                      <View style={{ width: "20%", alignItems: "center" }}>
                        <Text style={[styles.stnName, { position: "absolute", top: -25 }]}>1h 15m</Text>
                      </View>
                      <View style={{
                        width: "40%",
                        alignItems: "flex-end"
                      }}>
                        <Text numberOfLines={2} style={[styles.airportName, { textAlign: "right" }]}>Dr Babasaheb Ambedkar Airport</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.grayStrip}>
                <Text style={styles.stnName}>3 Hour 40 Minutes Layover In Nagpur</Text>
              </View>
              <View style={styles.flightView}>
                <View style={styles.flightRow1}>
                  <View style={styles.flightRow1Col}>
                    <Image source={require("../../Assets/images/jet.png")}
                      style={{ width: 30, height: 30, resizeMode: "contain" }}></Image>
                    <Text style={styles.flightName}>Vistara 9836</Text>
                  </View>
                  <View style={[styles.flightRow1Col, {
                    justifyContent: "flex-end"
                  }]}>
                    <Image source={Images.imgfood} style={styles.flightAmenityImg}></Image>
                    <Image source={Images.imgflightinfo} style={styles.flightAmenityImg}></Image>
                    <Image source={Images.imgseat} style={styles.flightAmenityImg}></Image>
                  </View>
                </View>
                <View style={styles.flightRow2}>
                  <View style={styles.cardContent}>
                    <View style={styles.cardContentRow1}>
                      <View style={{ width: "25%" }}>
                        <Text style={styles.estimate}>20:15</Text>
                        <Text style={styles.stnName}>New Delhi</Text>
                      </View>
                      <View style={styles.estimateView}>
                        <View style={{
                          width: "45%",
                          alignItems: "center"
                        }}>
                          <View style={styles.dotView}>
                            <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
                            <Dash style={{
                              width: "75%",
                              height: 1,
                            }} dashColor={colors.gray} dashLength={5}
                            />
                          </View>
                        </View>
                        <View style={{
                          width: "10%",
                          height: 20,
                          marginTop: 5
                        }}>
                          <Image source={Images.imgplaneblue} style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
                        </View>
                        <View style={{
                          width: "45%",
                          alignItems: "center"
                        }}>
                          <View style={styles.dotView}>
                            <Dash style={{
                              width: "75%",
                              height: 1,
                            }} dashColor={colors.gray} dashLength={5}
                            />
                            <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
                          </View>
                        </View>
                      </View>
                      <View style={{ width: "25%", alignItems: "flex-end" }}>
                        <Text style={styles.estimate}>20:15</Text>
                        <Text style={styles.stnName}>Nagpur</Text>
                      </View>
                    </View>
                    <View style={styles.cardContentRow2}>
                      <View style={{ width: "40%" }}>
                        <Text numberOfLines={2} style={styles.airportName}>Indira Gandhi International Airport</Text>
                      </View>
                      <View style={{ width: "20%", alignItems: "center" }}>
                        <Text style={[styles.stnName, { position: "absolute", top: -25 }]}>1h 15m</Text>
                      </View>
                      <View style={{
                        width: "40%",
                        alignItems: "flex-end"
                      }}>
                        <Text numberOfLines={2} style={[styles.airportName, { textAlign: "right" }]}>Dr Babasaheb Ambedkar Airport</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <BottomStrip fireEvent={this.checkout} />
            </View>
          </SafeAreaView>
        </View>
      </Modal>
    )
  }
}