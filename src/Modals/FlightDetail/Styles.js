import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import { Dimensions, StyleSheet, Platform } from "react-native";
import Fonts from "../../Helper Classes/Fonts";
import { colors } from "../../Helper Classes/Colors";
const screenWidth = Dimensions.get('window').width;

export default styles = StyleSheet.create({
  grayStrip: {
    height: 40,
    width: "100%",
    backgroundColor: colors.backgroundColor,
    justifyContent: "center",
    alignItems: "center"
  },
  airportName: {
    fontSize: 12,
    fontFamily: Fonts.regular,
    color: colors.colorBlack
  },
  estimateView: {
    width: "50%",
    height: 15,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  cardHeaderCol1: {
    width: "70%",
    height: "100%",
    flexDirection: "row"
  },
  stnName: {
    fontFamily: Fonts.medium,
    color: colors.colorBlack,
    fontSize: 14
  },
  dotView: {
    height: 10,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  estimate: {
    fontFamily: Fonts.bold,
    color: colors.colorBlack,
    fontSize: 17
  },
  priceImg: {
    width: 12, height: 12, resizeMode: "contain"
  },
  priceText: {
    flexDirection: "row",
    color: colors.colorBlack,
    fontFamily: Fonts.bold,
    fontSize: 14
  },
  cardContentRow2: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  cardContentRow1: {
    width: "100%", paddingRight: 7,
    height: "100%",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  cardContent: {
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    height: 50
    // backgroundColor: "red"
  },
  flightName: {
    fontSize: 14,
    fontFamily: Fonts.semiBold,
    color: colors.colorBlack,
    marginLeft: 5
  },
  flightRow1Col: {
    width: "50%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center"
  },
  flightRow2: {
    width: "100%", paddingBottom: 15,
    padding: 12, paddingTop: 15,
  },
  flightRow1: {
    height: 50, width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomColor: colors.lightgrey,
    borderBottomWidth: 1,
    paddingLeft: 12,
    paddingRight: 12
  },
  flightAmenityImg: {
    width: 20, height: 20,
    resizeMode: "contain",
    margin: 5
  },
  flightView: {
    width: screenWidth,
    height: 180,
    // borderBottomColor: "lightgrey",
    // borderBottomWidth: 1,
  },
  priceText: {
    flexDirection: "row",
    color: colors.colorWhite,
    fontFamily: Fonts.bold,
    fontSize: 22,
    lineHeight: 30
  },
  priceImg: {
    width: 20, height: 20, resizeMode: "contain"
  },
  btnText: {
    fontFamily: Fonts.medium,
    fontSize: 14, letterSpacing: .5,
  },
  header: {
    height: 50,
    width: "100%",
    backgroundColor: colors.backgroundColor,
    flexDirection: "row",
    alignItems: "center"
  },
  mainContainer: {
    marginTop: 75 + (Platform.OS == "ios" ? getStatusBarHeight() : 0),
    flex: 1,
    backgroundColor: colors.colorWhite
  },
  headerBtn: {
    width: 50,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "red"
  },
  headerTitle: {
    width: screenWidth - 110,
    height: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  titleText: {
    fontSize: 14,
    fontFamily: Fonts.medium,
    color: colors.colorBlack
  },
})