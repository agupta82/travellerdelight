import React, { Component } from "react";
import {
  Animated,
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList,
  Image,
  TouchableHighlight,
  Dimensions,
  TouchableOpacity,
  DeviceEventEmitter,
  InteractionManager,
  NativeModules,
  LayoutAnimation, Modal
} from "react-native";
import Dash from "react-native-dash";

import { CalendarList } from "react-native-calendars";
import { ifIphoneX, getStatusBarHeight } from "react-native-iphone-x-helper";
import { Button } from "../../Helper Classes/Button";
import Spinner from "react-native-loading-spinner-overlay";
import moment from "moment"
import { colors } from "../../Helper Classes/Colors";
import { commonstyle } from "../../Helper Classes/Commonstyle";


// var dateFormat = require("dateformat");




export default class CalenderModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      isOnewayActive: null,
      selectedReturnDate: null,
      isSelectDepartDate: true,
      selectedDepartDate: null,
      interactionFinished: false,
      spinner: false,
      isForHotel: null,
      forReturnDate: null
    };
    this.springValue = new Animated.Value(0.3)
    this._renderSelectedDateView = this._renderSelectedDateView.bind(this);
  }

  componentDidMount() {
    // const { navigation } = this.props;
    // const forReturnDate = navigation.getParam('forReturnDate', null);
    // const departDate = navigation.getParam('departDate', departDate);
    // const isForHotel = navigation.getParam('isForHotel', null);
    // const isOnewayActive = navigation.getParam('isOnewayActive', null);
    // const returnDate = navigation.getParam('returndate', null);
    // this.setState({
    //   forReturnDate: forReturnDate,
    //   selectedDepartDate: departDate,
    //   isForHotel: isForHotel,
    //   isOneWayActive: isOnewayActive,
    //   selectedReturnDate: returnDate
    // })

    // InteractionManager.runAfterInteractions(() => {

    //   if (forReturnDate) {
    //     if (returnDate != null) {
    //       this.setState({
    //         selectedIndex: 1,
    //         selectedReturnDate: moment(returnDate).format('YYYY-MM-DD')
    //       })
    //     } else {
    //       this.setState({
    //         selectedIndex: 1,
    //         selectedReturnDate: moment(departDate).format('YYYY-MM-DD'),
    //       })
    //     }

    //   }


    //   this.setState({
    //     interactionFinished: true,
    //     selectedDepartDate: moment(this.state.selectedDepartDate).format('YYYY-MM-DD'),
    //     // selectedReturnDate: dateFormat(this.state.selectedReturnDate, 'YYYY-MM-DD')
    //   })
    // });


  }

  getDate(date) {
    return moment(date).format("ddd, MMM Do");
  }



  _renderSelectedDateView() {
    return (
      <View
        style={{
          flexDirection: "row",
          width: "100%",
          height: 60,
          backgroundColor: "white"
        }}
      >
        <TouchableHighlight
          style={{ flex: 1 }}
          underlayColor="transparent"
          onPress={() => {
            this.setState({ selectedIndex: 0 });
          }}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              borderBottomColor: "rgba(25,89,189,1)",
              borderBottomWidth: this.state.selectedIndex == 0 ? 2 : 0,
              opacity: this.state.selectedIndex == 0 ? 1 : 0.5
            }}
          >
            <Text style={[commonstyle.greyText, { fontSize: 11 }]}>Depart</Text>
            <Text style={[commonstyle.blackSemiBold, { fontSize: 14 }]}>
              {this.getDate(this.state.selectedDepartDate)}
            </Text>
          </View>
        </TouchableHighlight>

        <Dash
          style={{
            height: "98.5%",
            width: 1,
            flexDirection: "column"
          }}
          dashColor="gray"
        />

        <TouchableHighlight
          style={{ flex: 1 }}
          underlayColor="transparent"
          onPress={() => {
            let departdate = new Date(this.state.selectedDepartDate);
            let returndate;
            returndate = moment(departdate).add(1, 'days').format("YYYY-MM-DD");
            this.setState({
              selectedIndex: 1,
              spinner: true,
              selectedReturnDate: returndate,
              isOnewayActive: false
            })
            setTimeout(() => { this.setState({ spinner: false }) }, 500)
          }}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              borderBottomColor: "rgba(25,89,189,1)",
              borderBottomWidth: this.state.selectedIndex == 1 ? 2 : 0,
              opacity: this.state.selectedIndex == 1 ? 1 : 0.5
            }}
          >
            <Text style={[commonstyle.greyText, { fontSize: 11 }]}>Return</Text>
            <Text
              style={{
                width: "60%",
                marginTop: 5,
                fontSize: 11,
                color: "rgba(25,89,189,1)",
                fontFamily: "Montserrat-SemiBold"
              }}
            >
              Book Round Trip to Save extra
            </Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }

  render() {
    let { oneWayActive, departDate, isForHotel, forReturnDate, closeModal } = this.props;
    let selectedDate = (this.state.selectedIndex == 0) ? this.state.selectedDepartDate : (this.state.selectedReturnDate) ? this.state.selectedReturnDate : ''
    // let selectedDate = (this.state.selectedIndex == 0 )? this.state.selectedDepartDate:this.state.selectedReturnDate 

    let markingDates = {};

    markingDates[selectedDate] = { selected: true, selectedColor: colors.colorBlue }


    if (this.state.selectedIndex == 1) {
      if (!this.state.isOnewayActive) {
        markingDates[this.state.selectedDepartDate] = { selected: true, selectedColor: "#9ECEFF" }
        markingDates[this.state.selectedReturnDate] = { selected: true, selectedColor: colors.colorBlue }
      }
    }

    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}
      >
        <View style={styles.container}
        >
          <View style={styles.mainContainer}>
            <View style={styles.header}>
              <TouchableOpacity style={styles.headerBtn} onPress={() => closeModal()}>
                <Image source={Images.imgClose} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
              </TouchableOpacity>
              <Text>Select Travel Dates</Text>
            </View>

            <Spinner
              visible={this.state.spinner}
              textContent={"Loading..."}
              textStyle={{ color: "white" }}
            />
            <View
              style={{
                margin: 20,
                justifyContent: "space-between",
              }}
            >
              {this._renderSelectedDateView()}
            </View>
            <View style={styles.styleCalendarSuperView}>

              <View style={styles.styleCalendarSuperView}>
                {this.state.interactionFinished && <CalendarList
                  pastScrollRange={0}
                  minDate={this.state.selectedIndex == 0 ? new Date() : this.state.selectedDepartDate}
                  onDayPress={(date) => {
                    if (this.state.selectedIndex == 0) {
                      if (!this.state.isOnewayActive) {
                        console.log("date is " + date)
                        let departdate = new Date(date.dateString);
                        let retundate = new Date();
                        retundate = moment(departdate).add(1, 'days').format("YYYY-MM-DD");
                        this.setState({
                          selectedDepartDate: date.dateString,
                          selectedIndex: 1,
                          spinner: true,
                          selectedReturnDate: retundate
                        })
                        setTimeout(() => { this.setState({ spinner: false }) }, 500)
                      } else {
                        this.setState({
                          selectedDepartDate: date.dateString,
                        })
                      }

                    } else {
                      this.setState({
                        selectedReturnDate: date.dateString
                      })
                    }
                  }}
                  markedDates={markingDates}
                  theme={{
                    backgroundColor: "transparent",
                    calendarBackground: "transparent",
                  }}
                />}
              </View>
            </View>
            <View
              style={{ margin: 10 }}>
              <Button
                width={"95%"}
                height={45}
                text={"Ok"}

                onPress={() => {
                  let departDate = Date.parse(this.state.selectedDepartDate)
                  let returnDate = Date.parse(this.state.selectedReturnDate)
                  // console.log('return Date   '+this.state.selectedReturnDate)


                  let dict = {
                    departDate: this.state.selectedDepartDate,
                    returnDate: this.state.selectedReturnDate
                  }

                  console.log('date select for flight  ' + JSON.stringify(dict))
                  DeviceEventEmitter.emit('dateSelect', dict)
                  NavigationServices.goBack();
                }}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(244,248,254,1)"
  },
  styleCalendarSuperView: {
    flex: 1,
    marginTop: 15,
    backgroundColor: "white",
    margin: 5,
  },
  header: {
    height: 50,
    width: "100%",
    backgroundColor: "rgb(246,245,246)",
    flexDirection: "row",
    alignItems: "center"
  },
  mainContainer: {
    marginTop: 50,
    flex: 1,
    backgroundColor: colors.colorWhite
  },
  headerBtn: {
    width: 50,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "red"
  }
});
