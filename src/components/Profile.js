import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  DeviceEventEmitter,
  TouchableHighlight,
  Share,
  ScrollView,
  Platform,
  TouchableOpacity,
  SafeAreaView
} from "react-native";
// akash.kulshreshtha@enukesoftware.com
// 123456
import NavigationServices from './../Helper Classes/NavigationServices'
import NavigationBar from "../Helper Classes/NavigationBar";
import Images from "../Helper Classes/Images";
import Constants from "../Helper Classes/Constants";
import Spinner from "react-native-loading-spinner-overlay";
import StringConstants from "../Helper Classes/StringConstants";
const screenSize = Dimensions.get("window");
import { connect } from "react-redux";
import { doLogoutAction, userDataAction, isLoginAction, userTokenAction } from '../Redux/Actions'
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import { colors } from "../Helper Classes/Colors";
import Fonts from "../Helper Classes/Fonts";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import CommonHeader from '../Helper Classes/CommonHeader'
import CardView from "react-native-cardview";
import Dash from "react-native-dash";
import { imageBaseUrl } from '../Helper Classes/APIManager/APIConstants'


const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

class profile extends Component {
  constructor(props) {
    if (!props.isLogin) {
      NavigationServices.replace("Authentication")
    }
    super(props);
    this.state = {
      name: "username",
      email: "useremail",
      phone: "userphone",
      userData: null,
      isLogin: false,
      spinner: false
    };
  }

  componentDidMount() {

    // if (this.props.userData && this.props.isLogin) {
    //   this.setState({
    //     isLogin: true,
    //     name: this.props.userData.full_name,
    //     email: this.props.userData.email,
    //     phone: this.props.userData.address.phone,
    //     userData: this.props.userData
    //   });
    // }

    DeviceEventEmitter.addListener(StringConstants.IS_LOGOUTEVENT, success => {
      if (success) {
        this.setState({
          isLogin: false,
          userData: null
        })
        // Actions.refresh();
      } else {
        setTimeout(() => {
          alert("Unable to Logout");
        }, 100)
      }
    })
  }

  onProfileUpdate() {
    NavigationServices.goBack();
    // this.setState({
    //   isLogin: true,
    //   name: Constants.userData.full_name,
    //   email: Constants.userData.email,
    //   phone: Constants.userData.address.phone,
    //   userData: Constants.userData
    // });
    //Actions.refresh()
  }

  onLoginSuccessHandler() {
    // Actions.pop()
    console.log("in login success handler")
    this.setState({
      // isLogin: true,
      // name: Constants.userData.full_name,
      // email: Constants.userData.email,
      // phone: Constants.userData.address.phone,
      // userData: Constants.userData
    });
  }



  otherInfoItemRow(itemname, icon) {
    return (
      <TouchableHighlight
        style={{
          padding: 12,
        }}
        underlayColor="transparent"
        onPress={() => {
          if (itemname == 'Share') {
            Share.share({
              message: this.state.text + 'http://kokotala.com',
              url: 'http://kokotala.com',
              title: 'Wow, did you see that?'
            }, {
              // Android only:
              dialogTitle: 'Share Invitation using',
              // iOS only:
              excludedActivityTypes: [
                'com.apple.UIKit.activity.PostToTwitter'
              ]
            })
          }
          else {
            alert(itemname);
          }

        }}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Image style={{ height: 15, width: 15, tintColor: colors.colorBlack }} source={icon} />
          <Text style={{ marginLeft: 15, fontSize: 12 }}>{itemname}</Text>
        </View>

      </TouchableHighlight>
    );
  }

  otherInfoComponent() {
    return (
      <View>
        <CardView style={styles.card}
          cardElevation={2}
          cardMaxElevation={3}
          cornerRadius={10}>
          <View style={{ width: '100%', borderRadius: 10, overflow: 'hidden' }}>
            <View style={styles.cardheader}>
              <Image source={Images.img_other_info} style={{ width: 30, height: 30, resizeMode: "contain", }}></Image>
              <Text style={styles.cardheaderText}>Other Information</Text>
            </View>
            <View style={styles.cardCOntent, { backgroundColor: colors.colorWhite, paddingHorizontal: 0, }}>
              {this.otherInfoItemRow("Help", Images.imgquestionmark)}
              <Dash dashColor={colors.lightgrey} dashLength={1} style={styles.dash} />
              {this.otherInfoItemRow("Share", Images.imgshare)}
              <Dash dashColor={colors.lightgrey} dashLength={1} style={styles.dash} />
              {this.otherInfoItemRow("Rate our app", Images.imgstar)}
              <Dash dashColor={colors.lightgrey} dashLength={1} style={styles.dash} />
              {this.otherInfoItemRow("Settings", Images.img_settings)}
            </View>
          </View>
        </CardView>
      </View>

    );
  }

  profileComponent() {
    return (
      <View
        style={{
          width: screenSize.width,
          height: 150,
          alignItems: "center",
          justifyContent: "center",
          marginTop: 20,
        }}
      >
        <Image
          style={{ height: 100, width: 100,borderRadius: 50, }}
          source={(this.props.userData && this.props.userData.image && this.props.userData.image.path) ? { uri: imageBaseUrl + this.props.userData.image.path } : Images.imguserprofile}
        />
        {/* <Image
          style={{ height: '100%', width: '100%', borderRadius: 50, }}
          source={(this.state.selectedUserImgPath && this.state.selectedUserImgPath.uri) ? { uri: this.state.selectedUserImgPath.uri } : Images.imgprofiledefault}
          resizeMode="cover"
        /> */}
        {this.props.isLogin && this.props.userData ? (
          <View>
            <Text style={{ fontFamily: Fonts.medium, fontSize: 14, marginTop: 15, textAlign: 'center' }}>
              {this.props.userData.first_name}
            </Text>
            {/* <Text style={{ fontFamily:Fonts.semiBold,marginTop: 10, fontSize: 13, textAlign: 'center' }}>
              {this.props.userData.email}
            </Text> */}
            {/* <Text style={{fontFamily:Fonts.semiBold, marginTop: 5, fontSize: 13, textAlign: 'center' }}>
              {this.props.userData.address.phone}
            </Text> */}
          </View>
        ) : (
            <Text style={{ marginTop: 15, textAlign: 'center' }}>You are not logged in</Text>
          )}

      </View>
    );
  }


  addressComponent() {
    const address = this.props.userData.address
    return (
      <View>
        <CardView style={styles.card}
          cardElevation={2}
          cardMaxElevation={3}
          cornerRadius={10}>
          <View style={{ width: '100%', borderRadius: 10, overflow: 'hidden' }}>
            <View style={styles.cardheader}>
              <Image source={Images.img_address_detail} style={{ width: 30, height: 30, resizeMode: "contain" }}></Image>
              <Text style={styles.cardheaderText}>Address Details</Text>
            </View>
            <View style={styles.cardCOntent, { backgroundColor: colors.colorWhite, paddingHorizontal: 0, }}>
              <View style={{ padding: 12 }}>
                <Text style={styles.textHeader}>
                  Address Line 1
                </Text>
                <Text style={[styles.textValue, { marginTop: 5 }]}>
                  {address && address.address1? address.address1:"NA"}
                </Text>
              </View>
              <Dash dashColor={colors.lightgrey} dashLength={1} style={styles.dash} />
              <View style={{ padding: 12 }}>
                <Text style={styles.textHeader}>
                  Address Line 2
                </Text>
                <Text style={[styles.textValue, { marginTop: 5 }]}>
                  {address && address.address2? address.address2:"NA"}
                </Text>
              </View>
              <Dash dashColor={colors.lightgrey} dashLength={1} style={styles.dash} />
              <View style={{ padding: 12 }}>
                <Text style={styles.textHeader}>
                  City Name
                </Text>
                <Text style={[styles.textValue, { marginTop: 5 }]}>
                  {address && address.city? address.city:"NA"}
                </Text>
              </View>
              <Dash dashColor={colors.lightgrey} dashLength={1} style={styles.dash} />
            </View>
          </View>
        </CardView>
      </View>
    )
  }


  _renderBackgroundImage() {
    return (
      <Image source={Images.imghomebg}
        style={{
          height: screenHeight / 2.5,
          width: screenWidth,
          resizeMode: "stretch"
        }}>
      </Image>
    )
  }


  render() {

    let loginText = "Login";
    let logoutText = "LOGOUT";

    console.log("profileUserData:", this.props.userData)
    return (
      <View style={styles.container}>
        <Spinner
          visible={this.props.isLoading}
          textContent={'Loading...'}
          textStyle={{ color: 'white' }}
        />
        {/* {Platform.OS == 'Android'
          ? < SafeAreaView backgroundColor={colors.colorBlue} ></SafeAreaView>
          : null
        } */}

        {this._renderBackgroundImage()}
        <View style={styles.mainContainer}>
          <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} />
          <CommonHeader title={"My Profile"} noBack={true} isTransparent={true}
            fireEvent={() => {
              NavigationServices.navigate("EditProfile")
            }}
            rightImg={Images.img_edit} />
          {this.profileComponent()}
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ width: '100%', flex: 1 }}>
            {
              this.props.isLogin && this.props.userData
                ? <CardView style={styles.card}
                  cardElevation={2}
                  cardMaxElevation={3}
                  cornerRadius={10}>
                  <View style={{ width: '100%', borderRadius: 10, overflow: 'hidden' }}>
                    <View style={styles.cardheader}>
                      <Image source={Images.img_primary_contact} style={{ width: 30, height: 30, resizeMode: "contain" }}></Image>
                      <Text style={styles.cardheaderText}>Primary Contact</Text>
                    </View>
                    <View style={styles.cardCOntent}>
                      <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textHeader}>Email  :-  </Text>
                        <Text style={styles.textValue}>{this.props.userData && this.props.userData.email?this.props.userData.email:"NA"}</Text>
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 5 }}>
                        <Text style={styles.textHeader}>Phone  :-  </Text>
                        <Text style={styles.textValue}>{ this.props.userData && this.props.userData.address && this.props.userData.address.phone ? this.props.userData.address.phone : "NA"}</Text>
                      </View>
                    </View>
                  </View>
                </CardView>
                : null
            }

            {this.props.isLogin && this.props.userData && this.props.userData.address ? this.addressComponent() : <View />}

            {this.otherInfoComponent()}
            <TouchableOpacity
              style={styles.login_button}
              // underlayColor='transparent'
              onPress={() => {
                if (this.props.isLogin && this.props.userData) {
                  // AuthManagement.APILogout()
                  this.props.doLogoutAction()
                }
                else {
                  NavigationServices.navigate('Authentication')
                }
              }}>

              <Text style={{ color: 'white', fontSize: 16, fontFamily: Fonts.semiBold }}>
                {this.props.isLogin && this.props.userData ? logoutText : loginText}
              </Text>
            </TouchableOpacity>
          </KeyboardAwareScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
    userData: state.userDataReducer,
    isLogin: state.isLoginReducer,
    userToken: state.userTokenReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userDataAction: (data) => dispatch(userDataAction(data)),
    isLoginAction: (data) => dispatch(isLoginAction(data)),
    userTokenAction: (data) => dispatch(userTokenAction(data)),
    doLogoutAction: () => dispatch(doLogoutAction())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(profile)

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  mainContainer: {
    position: "absolute",
    top: Platform.OS == "ios" ? (getStatusBarHeight() + 5) : 0,
    width: screenWidth,
    height: screenHeight,
    flex: 1,
    left: 0,
    alignItems: "center",
    // backgroundColor: "red"
  },
  card: {
    backgroundColor: colors.colorWhite,
    marginHorizontal: 20,
    marginTop: 15,
  },
  cardheaderText: {
    color: colors.colorBlack,
    fontFamily: Fonts.medium,
    fontSize: 13, marginLeft: 12
  },
  cardheader: {
    height: 40, width: "100%",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "rgb(245,245,245)",
    padding: 10,
  },
  cardCOntent: {
    backgroundColor: colors.colorWhite,
    paddingVertical: 16,
    paddingHorizontal: 12,
  },
  textHeader: {
    fontFamily: Fonts.medium,
    fontSize: 11
  },
  textValue: {
    fontFamily: Fonts.regular,
    fontSize: 11
  },
  dash: {
    width: "100%",
    height: 0.2,
  },
  buttoncontainer: {
    backgroundColor: "rgba(244,248,254,1)",
    flexDirection: "row",
    margin: 20,
    padding: 20,
    height: 100,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: 2 },
    elevation: 10
    // backgroundColor:'white',
  },
  login_button: {
    height: 40,
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: colors.colorBlue,
    borderRadius: 5,
    marginTop: 15,
    elevation: 4,
    marginBottom: 150,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    shadowRadius: 4,
  },
  signup_button: {
    width: "50%",
    height: 40,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10
  },
  itemContainer: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    shadowRadius: 4,
    elevation: 6
  }
});
