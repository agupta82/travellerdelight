import React, { Component } from 'react';
import {
  Animated,
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList,
  Image,
  TouchableHighlight,
  DeviceEventEmitter,
  Dimensions,
  Platform,
  TouchableOpacity
} from 'react-native';
import NavigationBar from '../Helper Classes/NavigationBar';
import Images from '../Helper Classes/Images'
import NavigationServices from './../Helper Classes/NavigationServices'
import APIManager from '../Helper Classes/APIManager/APIManagerUpdated';
import Constants from '../Helper Classes/Constants';
import StringConstants from '../Helper Classes/StringConstants';
import Spinner from "react-native-loading-spinner-overlay"
import { connect } from "react-redux";
import { getHotelTicketListAction, doLogoutAction, flightBookedListAction } from '../Redux/Actions'
import moment from "moment";
import CommonHeader from '../Helper Classes/CommonHeader'
import { colors } from "../Helper Classes/Colors";
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import Fonts from '../Helper Classes/Fonts'


const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const Page = "Booking List Page"

class BookingList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hotelFlightSelectedIndex: 1,

      flightListData: [],
      selectedIndex: 0,
      hotelListData: [],

      hotelCompletedBookingData: [],
      hotelUpcomingBookingData: [],
      hotelCancelledBookingData: [],

      flightCompletedBookingData: [],
      flightUpcomingBookingData: [],
      flightCancelledBookingData: [],
      spinner: false
    }


    // this.loginEventHandler = this.loginEventHandler.bind(this);
    this.logoutEventHandler = this.logoutEventHandler.bind(this);
    this.loginEventHandler = this.loginEventHandler.bind(this);
    this.renderHotelList = this.renderHotelList.bind(this);
    this.filterHoteldata = this.filterHoteldata.bind(this);
    this.sessionExpire = this.sessionExpire.bind(this);
    this.filterFlightData = this.filterFlightData.bind(this);
  }

  componentDidMount() {
    console.log("componenet did mount")

    if (this.props.isLogin) {
      this.getHotelBookedList()
      this.getFlightBookedList()
    }
    DeviceEventEmitter.addListener(StringConstants.IS_LOGINEVENT, this.loginEventHandler)
    DeviceEventEmitter.addListener(StringConstants.IS_LOGOUTEVENT, this.logoutEventHandler)
    DeviceEventEmitter.addListener(StringConstants.SESSION_EXPIRE_EVENT, this.sessionExpire)
    DeviceEventEmitter.addListener(StringConstants.HOTEL_BOOKING_LIST_EVENT, this.filterHoteldata)
    DeviceEventEmitter.addListener(StringConstants.FLIGHT_BOOKED_LIST_EVENT, this.filterFlightData)
  }

  componentWillUnmount() {

    DeviceEventEmitter.removeListener(StringConstants.IS_LOGINEVENT, this.loginEventHandler)
    DeviceEventEmitter.removeListener(StringConstants.IS_LOGOUTEVENT, this.logoutEventHandler)
    DeviceEventEmitter.removeListener(StringConstants.SESSION_EXPIRE_EVENT, this.sessionExpire)
    DeviceEventEmitter.removeListener(StringConstants.HOTEL_BOOKING_LIST_EVENT, this.filterHoteldata)
    DeviceEventEmitter.removeListener(StringConstants.FLIGHT_BOOKED_LIST_EVENT, this.filterFlightData)
  }

  filterHoteldata(data) {

    var upcomingBookings = [];
    var todayDate = new Date();
    var cancalledData = [];

    todayDate = moment(todayDate).format("YYYY-MM-DD")

    var completedBooking = data && data.length > 0 && data.filter(item => {
      itemdate = moment(item.check_in).format("YYYY-MM-DD")
      // return itemdate < todayDate
      if (itemdate < todayDate) {
        return item;
      }
      else {
        upcomingBookings.push(item);
      }
    })

    // var completedBooking2 = data.filter(item => {
    //   itemdate = dateFormat(item.check_in, "YYYY-MM-DD")
    //   // return itemdate < todayDate
    //   if (itemdate < todayDate) {
    //     return item;
    //   }
    //   else {
    //     upcomingBookings.push(item);
    //   }
    // })

    // const completedBooking = [...completedBooking1, ...completedBooking2];


    this.setState({
      hotelUpcomingBookingData: upcomingBookings,
      hotelCompletedBookingData: completedBooking,
      hotelCancelledBookingData: cancalledData
    })
    console.log("past bookings " + JSON.stringify(completedBooking));
    console.log("upcoming bookings" + JSON.stringify(upcomingBookings));
  }

  sessionExpire() {
    this.props.doLogoutAction()
  }

  filterFlightData(data) {
    console.log("FLIGHT_RES:" + JSON.stringify(data))
    var upcomingBookings = [];
    var todayDate = new Date();
    var cancelledData = [];

    todayDate = moment(todayDate).format("YYYY-MM-DD")

    var completedBooking = data && data.length > 0 && data.filter(item => {
      itemdate = moment(item.flights[0].depart_at).format("YYYY-MM-DD")
      // return itemdate < todayDate
      if (item.cancelled == 1) {
        cancelledData.push(item)
      }
      else if (itemdate < todayDate) {
        return item;
      }
      else {
        upcomingBookings.push(item);
      }
    })

    // var completedBooking2 = data.filter(item => {
    //   itemdate = dateFormat(item.flights[0].depart_at, "YYYY-MM-DD")
    //   // return itemdate < todayDate
    //   if (itemdate < todayDate) {
    //     return item;
    //   }
    //   else {
    //     upcomingBookings.push(item);
    //   }
    // })

    // const completedBooking = [...completedBooking1, ...completedBooking2];

    this.setState({
      flightUpcomingBookingData: upcomingBookings,
      flightCompletedBookingData: completedBooking,
      flightCancelledBookingData: cancelledData
    })
  }

  logoutEventHandler(islogout) {
    console.log(Page + " in hander logout event handler ")
    if (islogout) {
      console.log(Page + "logging out")
      this.setState({
        isLogin: false
      })
      // Actions.refresh()
    } else {
      setTimeout(() => {
        alert("Unable to logout user")
      }, 100)
    }
  }

  loginEventHandler(isLogin) {
    console.log("in hander login event handler ")
    if (isLogin) {
      this.setState({
        isLogin: true
      })
      this.getHotelBookedList()
      this.getFlightBookedList()
    } else {
      this.setState({
        isLogin: false
      })
    }
  }

  hotelBookingdata = {
    "booking": [
      {
        "id": 78,
        "email": "akash.kulshreshtha@enukesoftware.com",
        "mobile": "8285574730",
        "hotel_id": 0,
        "hotel_name": "Zostel Delhi Hostel",
        "package_id": 0,
        "night": 3,
        "room": 1,
        "check_in": "2019-01-19 00:00:00",
        "check_out": "2019-01-22 00:00:00",
        "total_price": "1026",
        "booking_id": 1429954,
        "quote_id": "e26f7a1b-ccc0-441d-a352-56802a56f63b",
        "user_id": 0,
        "booking_by": 78,
        "status": 1,
        "created_at": "2019-01-17 05:41:36",
        "updated_at": null,
        "deleted_at": null,
        "user": null,
        "rooms_prices": [
          {
            "id": 57,
            "hotel_booking_id": 78,
            "room": 49973,
            "night": 3,
            "price": "1026",
            "created_at": "2019-01-17 05:41:36",
            "updated_at": null
          }
        ],
        "rooms": [
          {
            "id": 57,
            "hotel_booking_id": 78,
            "room_number": 49973,
            "total_price": "1026",
            "agent_commission": 0,
            "nights": 3,
            "room_type": "6362018|cd",
            "room_type_code": "6362018|cd",
            "meal_type_code": "",
            "meal_type_text": "",
            "updated_at": "2019-01-17 05:41:36",
            "deleted_at": null
          }
        ],
        "messages": [
          {
            "id": 67,
            "hotel_booking_id": 78,
            "room": 49973,
            "type": "CancellationPolicy",
            "message": "Bed In 6-Bed Dormitory#^#100.00% of total amount will be charged, If cancelled between 17-Jan-2019 00:00:00 and 22-Jan-2019 23:59:59.|#!#",
            "created_at": "2019-01-17 05:41:36",
            "updated_at": "2019-01-17 05:41:36"
          }
        ],
        "guests": [
          {
            "id": 96,
            "hotel_booking_id": 78,
            "room": 49973,
            "guest_type": 1,
            "reference_id": 70132,
            "title": "Mr",
            "first_name": "Akash",
            "middle_name": "",
            "last_name": "KUl",
            "age": 0,
            "price": "0"
          }
        ],
        "amounts": [
          {
            "id": 40,
            "hotel_booking_id": 78,
            "payment_type": "",
            "currency": "INR",
            "amount": 1026,
            "enuke_commission": 0,
            "agent_commission": 0,
            "payment_date": "2019-01-17 05:41:37",
            "status": 1
          }
        ]
      },
      {
        "id": 87,
        "email": "akash.kulshreshtha@enukesoftware.com",
        "mobile": "8285574730",
        "hotel_id": 0,
        "hotel_name": "Zostel Delhi Hostel",
        "package_id": 0,
        "night": 1,
        "room": 1,
        "check_in": "2019-01-21 00:00:00",
        "check_out": "2019-01-22 00:00:00",
        "total_price": "341.24",
        "booking_id": 1430853,
        "quote_id": "416fab26-3927-475f-98c6-ee551f959171",
        "user_id": 0,
        "booking_by": 78,
        "status": 1,
        "created_at": "2019-01-21 06:09:15",
        "updated_at": null,
        "deleted_at": null,
        "user": null,
        "rooms_prices": [
          {
            "id": 66,
            "hotel_booking_id": 87,
            "room": 50124,
            "night": 1,
            "price": "341.24",
            "created_at": "2019-01-21 06:09:15",
            "updated_at": null
          }
        ],
        "rooms": [
          {
            "id": 66,
            "hotel_booking_id": 87,
            "room_number": 50124,
            "total_price": "341.24",
            "agent_commission": 0,
            "nights": 1,
            "room_type": "6362018|cd",
            "room_type_code": "6362018|cd",
            "meal_type_code": "",
            "meal_type_text": "",
            "updated_at": "2019-01-21 06:09:15",
            "deleted_at": null
          }
        ],
        "messages": [
          {
            "id": 76,
            "hotel_booking_id": 87,
            "room": 50124,
            "type": "CancellationPolicy",
            "message": "Bed In 6-Bed Dormitory#^#100.00% of total amount will be charged, If cancelled between 21-Jan-2019 00:00:00 and 22-Jan-2019 23:59:59.|#!#",
            "created_at": "2019-01-21 06:09:15",
            "updated_at": "2019-01-21 06:09:15"
          }
        ],
        "guests": [
          {
            "id": 105,
            "hotel_booking_id": 87,
            "room": 50124,
            "guest_type": 1,
            "reference_id": 70334,
            "title": "Mr",
            "first_name": "Akash",
            "middle_name": "",
            "last_name": "kul",
            "age": 0,
            "price": "0"
          }
        ],
        "amounts": [
          {
            "id": 49,
            "hotel_booking_id": 87,
            "payment_type": "",
            "currency": "INR",
            "amount": 341.24,
            "enuke_commission": 341.24,
            "agent_commission": 341.24,
            "payment_date": "2019-01-21 06:09:15",
            "status": 1
          }
        ]
      },
      {
        "id": 87,
        "email": "akash.kulshreshtha@enukesoftware.com",
        "mobile": "8285574730",
        "hotel_id": 0,
        "hotel_name": "Zostel Delhi Hostel",
        "package_id": 0,
        "night": 1,
        "room": 1,
        "check_in": "2019-01-21 00:00:00",
        "check_out": "2019-01-22 00:00:00",
        "total_price": "341.24",
        "booking_id": 1430853,
        "quote_id": "416fab26-3927-475f-98c6-ee551f959171",
        "user_id": 0,
        "booking_by": 78,
        "status": 1,
        "created_at": "2019-01-21 06:09:15",
        "updated_at": null,
        "deleted_at": null,
        "user": null,
        "rooms_prices": [
          {
            "id": 66,
            "hotel_booking_id": 87,
            "room": 50124,
            "night": 1,
            "price": "341.24",
            "created_at": "2019-01-21 06:09:15",
            "updated_at": null
          }
        ],
        "rooms": [
          {
            "id": 66,
            "hotel_booking_id": 87,
            "room_number": 50124,
            "total_price": "341.24",
            "agent_commission": 0,
            "nights": 1,
            "room_type": "6362018|cd",
            "room_type_code": "6362018|cd",
            "meal_type_code": "",
            "meal_type_text": "",
            "updated_at": "2019-01-21 06:09:15",
            "deleted_at": null
          }
        ],
        "messages": [
          {
            "id": 76,
            "hotel_booking_id": 87,
            "room": 50124,
            "type": "CancellationPolicy",
            "message": "Bed In 6-Bed Dormitory#^#100.00% of total amount will be charged, If cancelled between 21-Jan-2019 00:00:00 and 22-Jan-2019 23:59:59.|#!#",
            "created_at": "2019-01-21 06:09:15",
            "updated_at": "2019-01-21 06:09:15"
          }
        ],
        "guests": [
          {
            "id": 105,
            "hotel_booking_id": 87,
            "room": 50124,
            "guest_type": 1,
            "reference_id": 70334,
            "title": "Mr",
            "first_name": "Akash",
            "middle_name": "",
            "last_name": "kul",
            "age": 0,
            "price": "0"
          }
        ],
        "amounts": [
          {
            "id": 49,
            "hotel_booking_id": 87,
            "payment_type": "",
            "currency": "INR",
            "amount": 341.24,
            "enuke_commission": 341.24,
            "agent_commission": 341.24,
            "payment_date": "2019-01-21 06:09:15",
            "status": 1
          }
        ]
      }
    ]
  }

  getFlightBookedList() {
    this.props.flightBookedListAction()
    // console.log("Calling Flight booking List api")
    // this.setState({
    //   spinner: true
    // })
    // APIManager.getFlightsBookedListApi((success, response) => {
    //   console.log(Page + " Flight data " + JSON.stringify(response))
    //   this.setState({
    //     spinner: false
    //   })
    //   if (success) {
    //     if (response.code.code === 200) {

    //       { this.filterFlightData(response.data) }

    //     } else if (response.code.code === 500) {
    //       Alert.alert(
    //         "Alert",
    //         StringConstants.Session_Expired,
    //         [{ text: "Ok", onPress: () => { AuthManagement.APILogout() } }],
    //         { cancelable: false }
    //       )
    //     }

    //   } else {
    //     console.log("error " + response)
    //   }
    // })
  }

  getHotelBookedList() {
    console.log("calling hotel booking list  api")
    this.props.getHotelTicketListAction()
    //  APIManager.getHotelBookedListApi((success,response)=>{
    //    if(success){

    //       console.log("success data is "+ JSON.stringify(response))
    //        if(response.code.code == 200){

    //           console.log("hotel data " + JSON.stringify(response))

    //           {this.filterHoteldata(response.data)}

    //       }else if(response.code.code == 500){

    //          setTimeout(()=>{
    //           Alert.alert(
    //               "Alert",
    //               "Please Login Again",
    //               [ {text:"Ok",onPress:()=>{AuthManagement.APILogout()} } ],
    //               {cancelable:false}
    //             )
    //            },100) 
    //           }else{

    //               setTimeout(() => {
    //               alert(StringConstants.ERROR_OCCURED)
    //             }, 100);

    //        }
    //    }else{
    //        console.log("error is " +response)
    //        setTimeout(() => {
    //         alert(JSON.stringify(response));
    //       }, 100);
    //    }
    //  })
  }

  noBookingItem() {
    let text = ""
    if (this.props.isLogin) {
      text = "You do not have any booking with us"
    } else {
      text = "Please login to see your trips"
    }
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
        <Image
          style={{ height: 100, width: 105, margin: 10 }}
          source={Images.imgnervous} />
        <Text>
          {text}
        </Text>
      </View>
    )
  }

  _renderHotelItemList({ item }) {
    return (
      <TouchableOpacity onPress={() => NavigationServices.navigate("HotelBookingDetails", { BookingDetails: item })}>
        <View style={styles.hotelitemlist}>
          <View style={{ padding: 15, borderBottomWidth: 0.2 }} >
            <View style={{ flexDirection: "row", marginTop: 10 }}>
              <Image
                source={Images.imgBed}
                style={{ height: 15, width: 22, tintColor: colors.colorBlue }} />
              <Text style={{ fontWeight: "600", marginLeft: 10 }}>
                {item.hotel_name}
              </Text>
            </View>
            <View style={{ marginTop: 10 }}>
              <View style={{ marginTop: 5, flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={{ fontSize: 12, color: "rgb(75,75,75)" }}>
                  Booking ID
             </Text>
                <Text style={{ fontSize: 12, color: colors.colorBlue }}>
                  {item.booking_id}
                </Text>
              </View>
              <View style={{ marginTop: 5, flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={{ fontSize: 12, color: "rgb(75,75,75)" }}>
                  Check-In
      </Text>
                <Text style={{ fontSize: 12, color: colors.colorBlue }}>
                  {moment(item.check_in).format("ddd, DD MMM YYYY")}
                </Text>
              </View>
              <View style={{ marginTop: 5, flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={{ fontSize: 12, color: "rgb(75,75,75)" }}>
                  Check-Out
      </Text>
                <Text style={{ fontSize: 12, color: colors.colorBlue }}>
                  {moment(item.check_out).format("ddd, DD MMM YYYY")}
                </Text>
              </View>
            </View>

          </View>

          <View style={{ padding: 15, flexDirection: "row", justifyContent: "flex-end" }}>
            <View style={styles.hotellistbtn}>
              <Text style={{ color: "white", fontWeight: "500" }}>
                {item.room} Room
            </Text>
            </View>

            <View style={styles.hotellistbtn}>
              <Text style={{ color: "white", fontWeight: "500" }}>
                {item.amounts[0].currency + " "}{item.amounts[0].amount}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>

    )
  }

  renderHotelList() {
    let hotelData = [];
    if (this.state.selectedIndex == 0) {
      hotelData = this.state.hotelUpcomingBookingData
    } else if (this.state.selectedIndex == 1) {
      hotelData = this.state.hotelCompletedBookingData
    }
    else {
      //   hotelData = this.state.hotelCancelledBookingData
    }

    return (
      <View style={{ margin: 10, marginTop: 20, flex: 1 }}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={hotelData}
          renderItem={this._renderHotelItemList}
        />
      </View>
    )
  }

  renderHotelFlightTab() {
    return (
      <View style={{ width: '100%', alignItems: 'center' }}>
        <View style={styles.tabs}>
          <TouchableOpacity activeOpacity={0.9} style={[styles.tabButton,
          { backgroundColor: this.state.hotelFlightSelectedIndex == 0 ? colors.colorGold : colors.colorWhite, }
          ]}
            onPress={() =>
              this.setState({
                hotelFlightSelectedIndex: 0
              })
            }>
            <Text style={[styles.tabText, {
              color: this.state.hotelFlightSelectedIndex == 0 ? colors.colorWhite : colors.colorBlack
            }]}>Flight</Text>
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={0.9} style={[styles.tabButton, {
            backgroundColor: this.state.hotelFlightSelectedIndex == 1 ? colors.colorGold : colors.colorWhite,
          }]} onPress={() =>
            this.setState({
              hotelFlightSelectedIndex: 1
            })
          }>
            <Text style={[styles.tabText, {
              color: this.state.hotelFlightSelectedIndex == 1 ? colors.colorWhite : colors.colorBlack
            }]}>Hotel</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderHotelFlightTab2() {
    return (
      <View style={styles.hotelflighttabcontainer}>
        <View style={styles.hotelflighttabstyle}>
          <TouchableHighlight
            underlayColor="transparent"
            onPress={() => {
              // this.getFlightBookedList()
              this.setState({
                hotelFlightSelectedIndex: 0
              });
            }}
            style={
              this.state.hotelFlightSelectedIndex == 0
                ? styles.hotelflightselectedTab
                : styles.hotelflightnotSelectedTab
            }
          >
            <Text
              style={
                this.state.hotelFlightSelectedIndex == 0
                  ? styles.selectedTabText
                  : styles.notSelectedTabText
              }
            >
              Flight
              </Text>
          </TouchableHighlight>
          <TouchableHighlight
            underlayColor="transparent"
            onPress={() => {
              this.setState({
                hotelFlightSelectedIndex: 1
              });
            }}
            style={
              this.state.hotelFlightSelectedIndex == 1
                ? styles.hotelflightselectedTab
                : styles.hotelflightnotSelectedTab
            }
          >
            <Text
              style={
                this.state.hotelFlightSelectedIndex == 1
                  ? styles.selectedTabText
                  : styles.notSelectedTabText
              }
            >
              Hotel
              </Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }

  upcomingCompletetab() {
    return (
      <View style={styles.upcomingTabContainer}>
        <TouchableOpacity
          activeOpacity={0.9}
          style={[this.state.selectedIndex == 0 ? styles.selectedTab : styles.unSelectedTab, { borderBottomLeftRadius: 20, borderTopLeftRadius: 20 }]}
          underlayColor={'transparent'}
          onPress={() => this.setState({ selectedIndex: 0 })}
        >
          <Text style={this.state.selectedIndex == 0 ? styles.selectedTabText : styles.unSelectedTabText}>
            Upcoming
        </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.9}
          style={[this.state.selectedIndex == 1 ? styles.selectedTab : styles.unSelectedTab, { borderLeftWidth: 1, borderLeftColor: 'grey', borderRightColor: 'grey', borderRightWidth: 1 }]}
          underlayColor={'transparent'}
          onPress={() => this.setState({ selectedIndex: 1 })}
        >
          <Text style={this.state.selectedIndex == 1 ? styles.selectedTabText : styles.unSelectedTabText}>
            Completed
        </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.9}
          style={[this.state.selectedIndex == 2 ? styles.selectedTab : styles.unSelectedTab, { borderBottomRightRadius: 20, borderTopRightRadius: 20 }]}
          underlayColor={'transparent'}
          onPress={() => this.setState({ selectedIndex: 2 })}
        >
          <Text style={this.state.selectedIndex == 2 ? styles.selectedTabText : styles.unSelectedTabText}>
            Cancelled
        </Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderFlightList() {

    let flightData = [];
    if (this.state.selectedIndex == 0) {
      flightData = this.state.flightUpcomingBookingData
    } else if (this.state.selectedIndex == 1) {
      flightData = this.state.flightCompletedBookingData
    }
    else {
      //   hotelData = this.state.hotelCancelledBookingData
    }
    return (
      <View style={{ margin: 10, marginTop: 20, flex: 1 }}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={flightData}
          renderItem={(item) => this.renderFlightListItem(item)}
        />
      </View>
    )
  }

  showBookingItems() {
    return (
      <View style={{ flex: 1 }}>
        {this.renderHotelFlightTab()}
        {this.upcomingCompletetab()}
        {(this.state.hotelFlightSelectedIndex == 1 ? this.renderHotelList() : this.renderFlightList())}
      </View>
    )
  }

  flightListItemRow(text1, text2) {
    return (
      <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 10 }}>
        <Text>
          {text1}
        </Text>
        <Text style={{ color: colors.colorBlue }}>
          {text2}
        </Text>
      </View>
    )
  }
  //   flightDummy=[  
  //     {  
  //        "id":479,
  //        "phone":"8978798787",
  //        "email":"akash.kulshreshtha@enukesoftware.com",
  //        "cancelled":0,
  //        "user_id":0,
  //        "pnr":"N9QS8Y",
  //        "booking_id":"1434713",
  //        "traceId":null,
  //        "parent_id":479,
  //        "refundable":1,
  //        "applied_code":null,
  //        "booking_by":78,
  //        "mailed":0,
  //        "created_at":"2019-02-05 11:38:12",
  //        "updated_at":null,
  //        "doj":null,
  //        "cancelled_at":null,
  //        "deleted_at":null,
  //        "base_fare":"2280.00",
  //        "airline_fuel_surcharges":"0.00",
  //        "total":"3161.99",
  //        "user":null,
  //        "price":{  },
  //        "booking_flight":[  
  //           {  
  //              "id":425,
  //              "booking_id":479,
  //              "airline":"6E",
  //              "flight_id":155,
  //              "equipment":"320",
  //              "origin":"DEL",
  //              "origin_terminal":"1",
  //              "destination":"BOM",
  //              "destination_terminal":"1",
  //              "direction":1,
  //              "depart_at":"2019-02-05 22:25:00",
  //              "arrive_at":"2019-02-06 00:45:00",
  //              "travel_time":"140",
  //              "created_at":"2019-02-05 11:38:12",
  //              "updated_at":null
  //           }
  //        ]
  //     }
  //  ]
  renderFlightListItem({ item }) {

    return (
      <TouchableOpacity onPress={() => NavigationServices.navigate("BookingDetails", { bookingDataItem: item })}>
        <View style={styles.flightlistitem}>
          <View style={{ padding: 15 }}>
            <View style={{ flexDirection: "row" }}>
              <Image
                source={Images.imgPlane}
                style={{ height: 25, width: 30, tintColor: colors.colorBlue }}
              />
              <Text style={{ color: colors.colorBlue, marginLeft: 20 }}>

                {item.flights[0].origin} -  {item.flights[0].destination}
              </Text>
            </View>
            {this.flightListItemRow("Pnr No.", item.pnr)}
            {this.flightListItemRow("Booking Id", item.booking_id)}
            <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 10 }}>
              <Text>
                Dep. Time
                    </Text>
              <Text style={{ color: colors.colorBlue }}>
                {moment(item.flights[0].depart_at).format("ddd, DD MMM YY")} | {moment(item.flights
                [0].depart_at).format("hh:mm a")}
              </Text>
            </View>
            {/* {this.flightListItemRow("Dep Time",item.booking_flight[0].depart_at)} */}
            {this.flightListItemRow("Total Price", item.total)}
            <View>
            </View>
          </View>
          <View style={{ backgroundColor: "rgb(235,235,235)", padding: 10, paddingLeft: 20, flexDirection: "row" }}>
            <View style={{ width: "15%", justifyContent: "center" }}>
              <Image
                source={Images.imgflightglobe}
                style={{ height: 27, width: 27 }} />

            </View>
            <View style={{ width: "85%" }}>
              <Text style={{ fontWeight: "500" }}>
                {item.flights[0].airline}
              </Text>
              <Text style={{ marginTop: 5, color: "rgb(100,100,100)" }}>
                Airbus A320-100/200 (Economy)
                </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  _renderBackgroundImage() {
    return (
      <Image source={Images.imghomebg}
        style={{
          height: screenHeight / 2.5,
          width: screenWidth,
          resizeMode: "stretch"
        }}>
      </Image>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <Spinner
          visible={this.props.isLoading}
          textContent={"Loading..."}
          textStyle={{ color: "white" }} />
        {this._renderBackgroundImage()}
        <View style={styles.mainContainer}>
          <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} />
          <CommonHeader title={"My Trips"} noBack={true} isTransparent={true} />
          <View style={{ flex: 1, position: 'relative' }}>
            {(this.props.isLogin ? this.showBookingItems() : this.noBookingItem())}
          </View>
        </View>
      </View>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
    isLogin: state.isLoginReducer,
    userData: state.userDataReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getHotelTicketListAction: () => dispatch(getHotelTicketListAction()),
    flightBookedListAction: () => dispatch(flightBookedListAction()),
    doLogoutAction: () => dispatch(doLogoutAction())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BookingList)

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainContainer: {
    position: "absolute",
    top: Platform.OS == "ios" ? (getStatusBarHeight() + 5) : 0,
    width: '100%',
    height: '100%',
    flex: 1,
    left: 0,
    // alignItems: "center",
    // backgroundColor: "red"
  },
  tabs: {
    height: 40,
    width: screenWidth - 24,
    borderRadius: 10,
    flexDirection: "row",
    overflow: "hidden",
    borderWidth: .5,
    borderColor: "lightgrey",
    marginTop: 20,
  },
  tabButton: {
    height: "100%",
    width: (screenWidth - 24) / 2,
    alignItems: "center",
    justifyContent: "center"
  },
  tabText: {
    color: colors.colorBlack,
    fontSize: 14,
    fontFamily: Fonts.medium
  },
  upcomingTabContainer: {
    width: 300,
    marginTop: 30,
    height: 40,
    alignSelf: 'center',
    flexDirection: 'row',
    borderWidth: .5,
    borderColor: 'grey',
    borderRadius: 20,
    shadowColor: "gray",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 1 },
    shadowRadius: 5,
    elevation: 4


  },
  selectedTab: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.colorBlue,

  },
  unSelectedTab: {
    flex: 1,
    backgroundColor: colors.colorWhite,
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectedTabText: {
    fontFamily: Fonts.medium,
    color: 'white',
    fontSize: 13,
  },
  unSelectedTabText: {
    color: 'grey',
    fontFamily: Fonts.medium,
    fontSize: 13,
  },
  hotelflighttabcontainer: {
    shadowColor: "gray",
    shadowOpacity: 0.2,
    elevation: 4,
    shadowOffset: { width: 0, height: 5 }
  },
  hotelflighttabstyle: {
    height: 50,
    backgroundColor: "white",
    borderRadius: 25,
    margin: 30,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "gray",
    shadowOpacity: 0.4,
    elevation: 6,
    shadowOffset: { width: 0, height: 3 }
  },
  hotelflightnotSelectedTab: {
    backgroundColor: "white",
    height: 50,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25
  },
  hotelflightselectedTab: {
    height: 50,
    flex: 1,
    backgroundColor: "rgb(0,114,198)",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    shadowOpacity: 0.4,
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 1
  },
  selectedTabText: {
    color: "white",
    fontWeight: "bold",
    fontSize: 14
  },
  notSelectedTabText: {
    color: "rgb(0,114,198)",
    fontSize: 14,
    fontWeight: "bold"
  },
  hotellistbtn: {
    marginLeft: 10,
    backgroundColor: colors.colorBlue,
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
    borderRadius: 4
  },
  hotelitemlist: {
    margin: 10,
    backgroundColor: "white",
    borderRadius: 5,
    shadowColor: "gray",
    shadowOpacity: 0.5,
    shadowOffset: { width: 1, height: 1 },
    shadowRadius: 5,
    elevation: 6
  },
  flightlistitem: {
    margin: 10,
    backgroundColor: "white",
    borderRadius: 5,
    shadowColor: "gray",
    shadowOpacity: 0.5,
    shadowOffset: { width: 1, height: 1 },
    shadowRadius: 5,
    elevation: 6
  },
});
