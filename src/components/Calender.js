import React, { Component } from "react";
import {
  Animated,
  Text,
  View,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  Image,
  TouchableHighlight,
  Dimensions,
  DeviceEventEmitter,
  InteractionManager,
  NativeModules,
  LayoutAnimation,
} from "react-native";

import NavigationBar from "../Helper Classes/NavigationBar";
import Dash from "react-native-dash";
import NavigationServices from './../Helper Classes/NavigationServices'
import { CalendarList } from "react-native-calendars";
import { ifIphoneX, getStatusBarHeight } from "react-native-iphone-x-helper";
import { Button } from "../Helper Classes/Button";
import Spinner from "react-native-loading-spinner-overlay";
import moment from "moment"
import { colors } from "../Helper Classes/Colors";
import { commonstyle } from "../Helper Classes/Commonstyle";
import Images from "../Helper Classes/Images";
import { SafeAreaView } from "react-navigation";
import Fonts from "../Helper Classes/Fonts";
import StringConstants from "../Helper Classes/StringConstants";
const screenWidth = Dimensions.get('window').width;

export default class Calender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      isOnewayActive: null,
      selectedReturnDate: null,
      isSelectDepartDate: true,
      selectedDepartDate: null,
      interactionFinished: false,
      spinner: false,
      isForHotel: null,
      forReturnDate: null
    };
    this.springValue = new Animated.Value(0.3)
    this._renderSelectedDateView = this._renderSelectedDateView.bind(this);
  }

  componentDidMount() {
    const { navigation } = this.props;
    const forReturnDate = navigation.getParam('forReturnDate', null);
    const departDate = navigation.getParam('departDate', departDate);
    const isForHotel = navigation.getParam('isForHotel', null);
    const isOnewayActive = navigation.getParam('isOneWayActive', null);
    const returnDate = navigation.getParam('returndate', null);
    this.setState({
      forReturnDate: forReturnDate,
      selectedDepartDate: departDate,
      isForHotel: isForHotel,
      isOneWayActive: isOnewayActive,
      selectedReturnDate: returnDate
    }, () => {
      console.log("isOnewayActive is:--" + this.state.isOneWayActive);
      console.log("selectedIndex is:--" + this.state.selectedIndex);
    })

    InteractionManager.runAfterInteractions(() => {
      console.log("forReturnDate:==" + forReturnDate);
      if (forReturnDate) {
        if (returnDate != null) {
          this.setState({
            selectedIndex: 1,
            selectedReturnDate: moment(returnDate).format('YYYY-MM-DD')
          })
        } else {
          this.setState({
            selectedIndex: 1,
            selectedReturnDate: moment(departDate).format('YYYY-MM-DD'),
          })
        }

      }


      this.setState({
        interactionFinished: true,
        selectedDepartDate: moment(this.state.selectedDepartDate).format('YYYY-MM-DD'),
        // selectedReturnDate: dateFormat(this.state.selectedReturnDate, 'YYYY-MM-DD')
      })
    });


  }

  getDate(date) {
    return moment(date).format("ddd, MMM Do");
  }

  _renderSelectedDateView() {
    return (
      <View
        style={{
          flexDirection: "row",
          width: "100%",
          height: 60,
          backgroundColor: "white"
        }}
      >
        <TouchableOpacity
          style={{
            flex: 1,
            justifyContent: "center",
            // alignItems: "center",
          }}
          underlayColor="transparent"
          onPress={() => {
            this.setState({ selectedIndex: 0 });
          }}
        >
          <Text style={styles.normalText}>Departure</Text>
          <Text style={styles.boldText}>
            {this.getDate(this.state.selectedDepartDate)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconButton} disabled={true}>
          <Image source={Images.imgCalender}
            style={styles.icon}></Image>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "flex-end",
          }}
          underlayColor="transparent"
          onPress={() => {
            let departdate = new Date(this.state.selectedDepartDate);
            let returndate;
            returndate = moment(departdate).add(1, 'days').format("YYYY-MM-DD");
            this.setState({
              selectedIndex: 1,
              spinner: true,
              selectedReturnDate: returndate,
              isOnewayActive: false
            })
            setTimeout(() => { this.setState({ spinner: false }) }, 500)
          }}
        >
          <Text style={styles.normalText}>Return</Text>
          <Text style={[styles.boldText, { color: colors.lightgrey }]}>Select Date</Text>
        </TouchableOpacity>
      </View>
    );
  }

  _renderNavBar() {
    return (
      <View style={styles.header}>
        <TouchableOpacity style={styles.headerBtn} onPress={() => {
          NavigationServices.goBack();
        }}>
          <Image source={Images.imgClose} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
        </TouchableOpacity>
        <View style={{
          width: screenWidth - 110,
          height: 50, alignItems: "center", justifyContent: "center"
        }}>
          <Text style={{ color: colors.colorBlack, fontSize: 14, fontFamily: Fonts.semiBold }}>Select Travel Dates</Text>
        </View>
      </View>
    );
  }

  render() {

    let selectedDate = (this.state.selectedIndex == 0) ? this.state.selectedDepartDate : (this.state.selectedReturnDate) ? this.state.selectedReturnDate : ''
    // let selectedDate = (this.state.selectedIndex == 0 )? this.state.selectedDepartDate:this.state.selectedReturnDate 

    let markingDates = {}

    markingDates[selectedDate] = { selected: true, selectedColor: colors.colorBlue }


    if (this.state.selectedIndex == 1) {
      if (!this.state.isOnewayActive) {
        markingDates[this.state.selectedDepartDate] = { selected: true, selectedColor: "#9ECEFF" }
        markingDates[this.state.selectedReturnDate] = { selected: true, selectedColor: colors.colorBlue }
      }
    }

    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.colorBlue }}
      >
        <StatusBar barStyle="light-content" />

        <Spinner
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={{ color: "white" }}
        />
        <View style={styles.container}>
          {this._renderNavBar()}
          <View
            style={{
              padding: 14,
              justifyContent: "space-between",
              borderBottomColor: colors.lightgrey,
              borderBottomWidth: 1
              // height: screenHeight - (getStatusBarHeight(true) + 84)
            }}
          >
            {this._renderSelectedDateView()}

          </View>
          <View style={styles.styleCalendarSuperView}>

            <View style={styles.styleCalendarSuperView}>
              {this.state.interactionFinished && <CalendarList
                pastScrollRange={0}
                minDate={this.state.selectedIndex == 0 ? new Date() : this.state.selectedDepartDate}
                onDayPress={(date) => {
                  console.log("isOnewayActive is:--" + this.state.isOneWayActive);
                  console.log("selectedIndex is:--" + this.state.selectedIndex);
                  if (this.state.selectedIndex == 0) {
                    if (!this.state.isOnewayActive) {

                      console.log("date is " + date)

                      let departdate = new Date(date.dateString);
                      let retundate = new Date();
                      retundate = moment(departdate).add(1, 'days').format("YYYY-MM-DD");

                      // retundate.setDate(departdate.getDate() + 1);
                      // retundate = moment(retundate).format("YYYY-MM-DD");

                      console.log(retundate)

                      this.setState({
                        selectedDepartDate: date.dateString,
                        selectedIndex: 1,
                        spinner: true,
                        selectedReturnDate: retundate
                      })

                      setTimeout(() => { this.setState({ spinner: false }) }, 500)
                      // Actions.refresh()

                    } else {
                      this.setState({
                        selectedDepartDate: date.dateString,
                      }, () => {
                        console.log("selectedDepartDate is:--" + this.state.selectedDepartDate);
                        console.log("selectedReturnDate is:--" + this.state.selectedReturnDate);
                      })
                    }

                  } else {
                    this.setState({
                      selectedReturnDate: date.dateString
                    }, () => {
                      console.log("selectedReturnDate 1 is:--" + this.state.selectedReturnDate);
                    })
                  }
                }}
                markedDates={markingDates}
                // markingType='period'
                theme={{
                  backgroundColor: "transparent",
                  calendarBackground: "transparent",
                }}

              />}
            </View>
          </View>
          <View
            style={{ margin: 10 }}>
            <Button
              width={"95%"}
              height={45}
              text={"Ok"}

              onPress={() => {
                let departDate = Date.parse(this.state.selectedDepartDate)
                let returnDate = Date.parse(this.state.selectedReturnDate)
                // console.log('return Date   '+this.state.selectedReturnDate)


                let dict = {
                  departDate: this.state.selectedDepartDate,
                  returnDate: this.state.selectedReturnDate
                }

                console.log('date select for flight  ' + JSON.stringify(dict))
                DeviceEventEmitter.emit(StringConstants.DATE_SELECT, dict)
                NavigationServices.goBack();
              }}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  iconButton: {
    width: 50,
    height: "100%",
    paddingTop: 5,
    alignItems: "center",
  }, icon: {
    width: 30, height: 30,
    resizeMode: "contain",
    marginTop: 7
  },
  boldText: {
    fontFamily: Fonts.bold,
    fontSize: 14,
    color: colors.colorBlack,
    letterSpacing: .5
  },
  normalText: {
    fontFamily: Fonts.regular,
    fontSize: 12,
    color: colors.colorBlack,
    marginBottom: 5
  },
  header: {
    height: 50,
    width: "100%",
    backgroundColor: "rgb(246,245,246)",
    flexDirection: "row",
    alignItems: "center"
  },
  mainContainer: {
    marginTop: 50,
    flex: 1,
    backgroundColor: colors.colorWhite
  },
  headerBtn: {
    width: 50,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "red"
  },
  container: {
    flex: 1,
    backgroundColor: colors.colorWhite
  },
  styleCalendarSuperView: {
    flex: 1,
    marginTop: 15,
    backgroundColor: "white",
    margin: 5,
  },

});
