import React, { Component } from "react";
import { View, Image, FlatList, ScrollView, TextInput, SafeAreaView, Text, StyleSheet, TouchableOpacity, Modal, StatusBar } from 'react-native';
import NavigationBar from '../../../Helper Classes/NavigationBar'
import { connect } from "react-redux";
import Images from '../../../Helper Classes/Images';
import RatinBar from "../../../Helper Classes/RatingBar";
import Dash from "react-native-dash"
import { styles } from "./Style"
import NavigationServices from '../../../Helper Classes/NavigationServices'
import CommomHeader from "../../../Helper Classes/CommonHeader"
import { commonstyle } from "../../../Helper Classes/Commonstyle"
import moment from "moment";
import { colors } from "../../../Helper Classes/Colors";

let SOURCES = null
class ReviewBooking extends Component {
  constructor(props) {
    super(props)
    this.state = {
      roomIndex: null,
      phone: null,
      guestDetails: null,
      email: null
      //    hotelSearchParam : [],
      //    hotelDict:{},
      //    hotelDetails:{},
      //    email:"",
      //    phone:"",
      //    guestCount:0,
      //    roomCount:0,
      //    fullAddress:"",
      //    cancellationPolicy:"",
      //    roomDetails:{}

    }

  }

  componentWillMount() {

  }

  componentDidMount() {
    //alert("DJ")
    let { navigation } = this.props;
    let roomIndex = navigation.getParam('roomIndex', null)
    let guestDetails = navigation.getParam('guestDetails', null)
    let email = navigation.getParam('email', null)
    let phone = navigation.getParam('phone', null)
    SOURCES = navigation.getParam('sources', null)
    this.setState({
      roomIndex: roomIndex,
      phone: phone,
      guestDetails: guestDetails,
      email: email
    }, () => {
      //console.log("roomIndex", JSON.stringify(this.state))
      ///alert(JSON.stringify(navigation))
    })
  }

  _renderTarrifDetail() {

    let roomDetails = this.props.savedHotelDetails.rooms.roomlist[this.state.roomIndex];

    otherCharge = roomDetails.Price.OtherCharges.toFixed(2);

    return (

      <View style={[commonstyle.itemBackground, { padding: 0 }]}>
        <View style={{ padding: 15 }}>
          <Text style={{ fontSize: 15, color: 'rgb(76,76,76', fontFamily: "Montserrat-Medium" }}>
            Tarrif Details
            </Text>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
            <Text style={{ color: colors.colorBlue }}>
              Base Fare
                </Text>
            <Text style={{ color: colors.colorBlue, fontFamily: "Montserrat-Medium" }}>
              {roomDetails.Price.RoomPrice}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
            <Text style={{ color: colors.colorBlue }}>
              Tax
                </Text>
            <Text style={{ color: colors.colorBlue, fontFamily: "Montserrat-Medium" }}>
              {roomDetails.Price.Tax}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
            <Text style={{ color: colors.colorBlue }}>
              Convenience Fees & Taxes
                </Text>
            <Text style={{ color: colors.colorBlue, fontFamily: "Montserrat-Medium" }}>
              {otherCharge}
            </Text>
          </View>
        </View>


        <Dash
          style={{
            width: "98.7%",
            flexDirection: "row",
            alignSelf: 'center'
          }}
          dashColor="gray"
          dashLength={7.5}
        />

        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 15 }}>
          <Text style={{ fontFamily: "Montserrat-Medium" }}>
            You Pay
                </Text>
          <Text style={{ fontFamily: "Montserrat-Medium" }} >
            {roomDetails.Price.AgencyFare}
          </Text>
        </View>

      </View>
    )

  }

  cancellationPolicyItem() {
    return (
      <TouchableOpacity
        underlayColor="transparent"
        onPress={() => {
          var Policy = this.props.savedHotelDetails.rooms.roomlist[this.state.roomIndex].CancellationPolicy
          alert(Policy)
        }}>
        <View style={[commonstyle.itemBackground, { flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5 }]}>
          <View>
            <Text style={{ color: colors.colorBlue, fontSize: 15, fontFamily: "Montserrat-SemiBold" }}>
              Cancellation Policy
                   </Text>
            <Text style={{ marginTop: 5, fontFamily: "Montserrat-Medium" }}>
              Rules and Policy
                   </Text>
          </View>
          <Image
            style={{ height: 15, width: 10, alignSelf: 'flex-start' }}
            source={Images.imgrightarrow}
          />
        </View>
      </TouchableOpacity>
    )
  }

  _renderConfirmButton() {
    return (
      <TouchableOpacity
        style={styles.confirmButtonStyle}
        underlayColor={"transparent"}
        onPress={() => {
          this.continueToPayment()
        }}>
        <Text
          style={{ color: 'white' }}>
          Continue
                     </Text>
      </TouchableOpacity>
    )

  }
  continueToPayment() {
    // console.log("Props:", JSON.stringify(this.props))
    // console.log("State:", JSON.stringify(this.state))
    const { phone, guestDetails, email } = this.state
    const { hotel, rooms } = this.props.savedHotelDetails
    const savedHotelRoom = this.props.savedHotelRoom
    let finalPrice = Math.ceil(savedHotelRoom.Price.AgencyFare);

    let GuestDetailsArray = []
    guestDetails.forEach((item, index) => {
      newDataArray = []
      item.data.forEach((d, i) => {
        newDataArray.push({
          title: d.title,
          first_name: d.fName,
          middle_name: d.mName,
          last_name: d.lName,
          type: d.type
        })
      })
      GuestDetailsArray.push(newDataArray)
    })

    let SelectedRoomArray = []
    SelectedRoomArray.push(savedHotelRoom)



    let hotelObject = {
      id: hotel.id,
      name: hotel.name,
      index: hotel.index,
      address: hotel.address,
      city: hotel.city,
      country: hotel.country,
      roomCombination: rooms.combination.RoomCombination,
      selectedRoom: SelectedRoomArray,
    }




    let hotelBookingObject = {
      guestDetail: GuestDetailsArray,
      check_in: moment(Date.parse(this.props.hotelSearchParam.checkIn)).format("YYYY-MM-DD"),
      check_out: moment(Date.parse(this.props.hotelSearchParam.checkOut)).format("YYYY-MM-DD"),
      hotel: hotelObject,
      description: hotel.description,
      room: SelectedRoomArray,
      source: SOURCES.source,
      sourceDetail: SOURCES.sourceDetail,
      ResultIndex: hotel.index,
      hotelRooms: rooms.roomlist,
      email: this.state.email,
      mobile: this.state.phone,
      orderID: "",
      captureID: ""
    }
    console.log("hotelBookingObject:", JSON.stringify(hotelBookingObject))
    NavigationServices.navigate("PaymentScreen", { data: hotelBookingObject, isFlight: false, price: finalPrice, currency: "INR" })
  }

  _renderCheckInItem() {

    let { checkIn, checkOut } = this.props.hotelSearchParam;

    checkIn = moment(checkIn).format("ddd, D MMM YYYY");
    checkOut = moment(checkOut).format("ddd, D MMM YYYY");
    return (
      <View style={[commonstyle.itemBackground, { backgroundColor: colors.colorBlue, flexDirection: 'row', marginBottom: 5 }]}>
        <View style={{ width: "50%", padding: 5 }}>
          <Text style={{ color: 'white', fontSize: 13, fontFamily: "Montserrat-Medium" }}>
            CHECK-IN
                </Text>
          <Text style={{ color: 'white', fontWeight: '600', marginTop: 10, fontFamily: "Montserrat-Medium" }}>
            {checkIn}
            {/* {this.state.hotelSearchParam.checkIn} */}
          </Text>
        </View>
        <View style={{ width: 1, height: 42, alignSelf: 'center', backgroundColor: 'white' }}>
          {/* {checkOut} */}
          {/* {this.state.hotelSearchParam.checkOut} */}
        </View>
        <View style={{ width: "50%", padding: 5, alignItems: 'flex-end' }}>
          <Text style={{ color: 'white', fontSize: 13, fontFamily: "Montserrat-Medium" }}>
            CHECK-OUT
                </Text>
          <Text style={{ color: 'white', fontWeight: "500", marginTop: 10, fontFamily: "Montserrat-Medium" }}>
            {checkOut}
          </Text>
        </View>
      </View>
    )
  }

  _renderRoomItem() {
    const room = this.props.savedHotelDetails.rooms.roomlist[this.state.roomIndex];
    if (this.state.roomIndex)
      return (
        <View style={[commonstyle.itemBackground, { marginBottom: 5 }]}>
          <Text style={{ fontSize: 15, fontFamily: "Montserrat-SemiBold", color: 'rgb(76,76,76)' }}>
            {room.name}
          </Text>

          {room.amenities.length > 0 ? <Text style={{ color: 'rgb(76,76,76)', fontSize: 12, marginTop: 10 }}>{room.amenities[0]}</Text> : null}

          <Text style={{ marginTop: 5, color: 'rgb(76,76,76)', fontFamily: "Montserrat-Medium" }}>

            {this.props.hotelSearchParam.numberOfRooms} Room  {this.props.hotelSearchParam.numberOfGuest} Guest
            </Text>
        </View>
      )
  }

  _renderHotelItem() {
    const hotel = this.props.savedHotelDetails.hotel;
    const { name, address, star } = hotel;
    const imageurl = hotel.images[0]
    return (
      <View style={[commonstyle.itemBackground, { marginBottom: 5 }]}>
        <View style={{ flexDirection: 'row' }}>
          <View style={styles.imageContainer}>
            <Image
              style={{
                height: 100,
                width: 120,
              }}
              source={{ uri: imageurl }}
              resizeMode="contain"
            />
          </View>
          <View style={{ flex: 1, marginLeft: 15 }}>

            <Text style={{ fontSize: 15, fontWeight: '600', color: colors.colorBlue, fontFamily: "Montserrat-SemiBold" }}>
              {name}
              {/* {this.state.hotelDetails.name} */}
            </Text>
            <View style={{ marginTop: 5 }}>
              <Text style={{ color: 'rgb(76,76,76)', fontSize: 12, fontFamily: "Montserrat-Medium" }}>
                {address}
                {/* {this.state.hotelDetails.address} */}
              </Text>
              <View style={{ width: 90, marginTop: 10 }}>
                <RatinBar
                  count={star}
                />
              </View>

            </View>
          </View>
        </View>
      </View>
    )
  }

  _renderContactDetails() {

    // addressDict: {
    //     addLine1: "",
    //     addLine2: "",
    //     city: "",
    //     countryCode: "",
    //     postalCode: ""
    // }

    return (
      <View style={styles.itemContainer}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={{ fontSize: 15, fontWeight: '600', color: 'rgb(76,76,76)' }}>
            Contact Details
                </Text>
          <TouchableOpacity
            style={{ height: 15, width: 30, alignItems: "flex-end" }}
            underlayColor={'transparent'}
            onPress={() => {
              NavigationServices.goBack();
            }}>
            <Image
              style={{ height: 15, width: 15, tintColor: colors.colorBlue }}
              source={Images.imgpencil}
            />
          </TouchableOpacity>

        </View>
        <View style={{ flexDirection: 'row', marginTop: 10 }}>
          <Image
            style={{ height: 15, width: 15 }}
            source={Images.imgusericon} />
          <Text style={{ marginLeft: 15, fontSize: 12 }}>
            John Deo
                 </Text>
        </View>
        <View style={{ flexDirection: 'row', marginTop: 8 }}>
          <View style={{ justifyContent: 'center' }}>
            <Image
              style={{ height: 10, width: 15, tintColor: colors.colorBlue }}
              source={Images.imgmail} />
          </View>
          <Text style={{ marginLeft: 15, fontSize: 12 }}>
            {this.state.hotelDict.email}
          </Text>
        </View>
        <View style={{ flexDirection: 'row', marginTop: 8 }}>
          <Image
            style={{ height: 15, width: 15, tintColor: colors.colorBlue }}
            source={Images.imgphone} />
          <Text style={{ marginLeft: 15, fontSize: 12 }}>
            {this.state.hotelDict.phoneNumber}
          </Text>
        </View>
        <View style={{ flexDirection: 'row', marginTop: 8 }}>
          <Image
            style={{ height: 15, width: 15 }}
            source={Images.imgGps}
          />
          <View style={{ marginLeft: 15 }}>
            <Text
              numberOfLines={0}>
              {this.state.fullAddress}
            </Text>
            {/* <Text style={{marginTop:8}}>
                  Gugaon, Haryana, India  231234
                 </Text> */}
          </View>

        </View>
      </View>
    )
  }


  passengerDetailItem() {
    const firstPassenger = this.state.guestDetails[0].data[0];
    return (
      <View style={[commonstyle.itemBackground]}>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Image
            style={{ height: 15, width: 15 }}
            source={Images.imgusericon} />
          <Text style={{ marginLeft: 10, fontFamily: "Montserrat-Medium", flex: 1 }}>
            {firstPassenger.title} {firstPassenger.fName} {firstPassenger.mName} {firstPassenger.lName}
          </Text>
          <TouchableOpacity
            style={{ padding: 5 }}
            underlayColor={"transparent"}
            onPress={() => this.setState({ isModalVisible: true })}>
            <Text style={{ color: colors.colorBlue, fontFamily: "Montserrat-Medium", fontSize: 12 }}>
              {/* View All */}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: "row", marginTop: 10 }}>
          <View style={{ flex: 1, alignItems: "center" }}>
            <Image
              style={{ height: 15, width: 15 }}
              source={Images.imgemail}
            />
            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 11, marginTop: 10 }}>
              {this.state.email}
            </Text>
          </View>
          <View style={{ width: 1, backgroundColor: "#848383" }} />


          <View style={{ flex: 1, alignItems: "center" }}>
            <Image
              style={{ height: 15, width: 10 }}
              source={Images.imgphone}
            />
            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 11, marginTop: 10 }}>
              {this.state.phone}
            </Text>
          </View>
        </View>
      </View>
    )

  }


  render() {
    if (this.state.roomIndex == null) {
      return <View style={styles.container}>
        <SafeAreaView style={{ backgroundColor: colors.colorBlue }} />
        <CommomHeader title={"Review Booking"} /></View>
    }


    return (
      <View style={styles.container}>
        <SafeAreaView style={{ backgroundColor: colors.colorBlue }} />
        <CommomHeader title={"Review Booking"} />
        <StatusBar barStyle='light-content' />
        <ScrollView >
          <View style={{ paddingBottom: 40 }}>
            {this._renderHotelItem()}
            {this._renderCheckInItem()}
            {this._renderRoomItem()}

            {/* {this._renderContactDetails()} */}
            {this.cancellationPolicyItem()}
            {this.passengerDetailItem()}
            {this._renderTarrifDetail()}

          </View>
        </ScrollView>
        {this._renderConfirmButton()}
      </View>
    )
  }

}

const mapStateToProps = state => {
  //console.log("reduxState:" + JSON.stringify(state))
  return {
    hotelSearchParam: state.HotelListreducer.hotelSearchParam,
    savedHotelDetails: state.HotelListreducer.selectedHotelItem,
    savedHotelRoom: state.HotelListreducer.selectedRoomData
  }
}

export default connect(mapStateToProps, {})(ReviewBooking)
