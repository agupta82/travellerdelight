import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Text,
  FlatList,
  Image,
  TextInput,
  Modal,
  SafeAreaView,
  Platform,
  Dimensions,
  DeviceEventEmitter,
  StatusBar
} from "react-native";
import { ifIphoneX, getStatusBarHeight } from "react-native-iphone-x-helper";
import RatinBar from "../../../Helper Classes/RatingBar";
import CommomHeader from '../../../Helper Classes/CommonHeader'
import Images from "../../../Helper Classes/Images";
import Spinner from "react-native-loading-spinner-overlay";
import NavigationServices from '../../../Helper Classes/NavigationServices'
import styles from "./Style";
import { getHotelListApi } from "../../../Helper Classes/APIManager/ApiProvider";
import { connect } from "react-redux";
import HotelItemElement from './HotelItemElement'
import { saveHotelList } from "../../../Redux/Actions/Actions"
//import hotelListJson from "../../../json/hotelList.json"
import MultiSlider from "@ptomasroos/react-native-multi-slider"
import moment from 'moment'
import { filterAmenities } from '../../../Utility'
//import RangeSlider from 'react-native-range-slider'

const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").height;
const screen = Dimensions.get("window");

import MapView from "react-native-maps";
import { Marker, Callout } from "react-native-maps";
import { commonstyle } from "../../../Helper Classes/Commonstyle";
import { colors } from "../../../Helper Classes/Colors";
import { getHotelListAction } from "../../../Redux/Actions"
import StringConstants from '../../../Helper Classes/StringConstants';
import Fonts from "../../../Helper Classes/Fonts";
import HotelListData from "../../../json/HotelListData"


const ASPECT_RATIO = screen.width / screen.height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


class HotelList extends Component {
  constructor(props) {
    super(props);
    this.mapViewRef = null;
    this.state = {
      // selectedIndex: 0,
      hotelsListData: [],
      hotelsListData1: [],
      hotelsListData2: [],
      hotelsListData3: [],
      hotelsListData4: [],
      sortingStatus: 0,
      searchList: [],
      isSearchActive: false,
      spinner: false,
      loading: false,
      selectedDict: null,
      searchStr: "",
      is2starSelected: false,
      is3starSelected: false,
      is4starSelected: false,
      is5starSelected: false,
      isFilterActive: false,
      filterData: [],
      showFilter: false,
      selectedMinPrice: 1000,
      selectedMaxPrice: 3000,
      minPrice: 0,
      maxPrice: 5000,
      source: {
        "source": "",
        "traceId": ""
      }
      // hotelSearchParam:[],
      // mapRegion: {
      //   latitude: 37.78825,
      //   longitude: -122.4324,
      //   latitudeDelta: LATITUDE_DELTA,
      //   longitudeDelta: LONGITUDE_DELTA
      // }
    };
    this.minimumselPrice = 0;
    this.maximumselPrice = 0;
    this.getHotelsList = this.getHotelsList.bind(this);
    this.hotelsList = this.hotelsList.bind(this);
    this.updateHotelListFromApi = this.updateHotelListFromApi.bind(this);
  }

  // --------------- component Life cycle methods ------------

  componentDidMount() {



    DeviceEventEmitter.addListener(StringConstants.HOTEL_LIST_EVENT, this.updateHotelListFromApi)
    this.getHotelsList();
    // this.updateHotelListFromApi(HotelListData)
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.HOTEL_LIST_EVENT, this.updateHotelListFromApi)
  }

  updateHotelListFromApi(res) {
    let min, max;
    if (res && res.results) {
      res.results.forEach((item, index) => {
        if (index == 0) {
          min = item.priceDetail.agencyFare
          max = item.priceDetail.agencyFare
        } else {
          if (item.priceDetail.agencyFare < min) {
            min = item.priceDetail.agencyFare
          }
          if (item.priceDetail.agencyFare > max) {
            max = item.priceDetail.agencyFare
          }
        }
      });
    }

    //  console.log("1121"+JSON.stringify(res.results))
    //  console.log("1121"+JSON.stringify(sort1))
    //  console.log("1121"+JSON.stringify(sort2))

    this.setState(
      {
        hotelsListData: res.results,
        source: res.source,
        minPrice: min,
        maxPrice: max,
        selectedMinPrice: min,
        selectedMaxPrice: max,
      })
  }

  //--------------- API Calling Methods ----------------
  getHotelsList() {
    let dict = this.props.hotelSearchParams;

    if (dict != null && dict.roomData != null) {
      console.log("get hotel list room data " + dict.roomData);

      // this.setState({
      //   hotelSearchParam: dict
      // });

      let param = {
        keyword: dict.name + ", " + dict.country,
        type_id: dict.id,
        type: "city",
        check_in: this.getDate(dict.checkIn),
        check_out: this.getDate(dict.checkOut),
        room: dict.roomData
      };

      this.props.getHotelListAction(param)

    } else {
      console.log("wrong props receive " + JSON.stringify(dict));
    }
  }



  onFilterSelection = () => {

    const {
      is2starSelected,
      is3starSelected,
      is4starSelected,
      is5starSelected,
      selectedMinPrice,
      selectedMaxPrice,
      minPrice,
      maxPrice,
      sortingStatus,
    } = this.state;

    this.setState({ showFilter: false })
    if (is2starSelected || is3starSelected || is4starSelected || is5starSelected || selectedMinPrice > minPrice || selectedMaxPrice < maxPrice || sortingStatus != 0) {
      this.setState({ isFilterActive: true })
      // filter data Item 
      let data = this.state.hotelsListData;
      if (is2starSelected || is3starSelected || is4starSelected || is5starSelected) {
        data = this.state.hotelsListData.filter((item, index) => {
          if (is2starSelected) {
            if (item.star <= 2) {
              return true;
            }
          }

          if (is3starSelected) {
            // console.log("3 STAR ITEM1:"+JSON.stringify(item))
            if (item.star == 3) {
              // console.log("3 STAR ITEM2:"+JSON.stringify(item))
              return true;
            }
          }

          if (is4starSelected) {
            if (item.star == 4) {
              return true;
            }
          }

          if (is5starSelected) {
            if (item.star == 5) {
              return true;
            }

          }

        })

      }

      data = data.filter((item, index) => {
        // console.log("PRICE_ITEM1:"+JSON.stringify(item))
        if (item.priceDetail.agencyFare >= selectedMinPrice && item.priceDetail.agencyFare <= selectedMaxPrice) {
          // console.log("PRICE_ITEM2:"+JSON.stringify(item))
          return true;
        }
      })

      switch (sortingStatus) {
        case 0:
          data.sort(function (a, b) {
            return a.priceDetail.agencyFare - b.priceDetail.agencyFare
          })
          break;
        case 1:
          data.sort(function (a, b) {
            return a.priceDetail.agencyFare - b.priceDetail.agencyFare
          })
          break;
        case 2:
          data.sort(function (a, b) {
            return b.priceDetail.agencyFare - a.priceDetail.agencyFare
          })
          break;
        case 3:
          data.sort(function (a, b) {
            return b.star - a.star
          })
          break;
        case 4:
          data.sort(function (a, b) {
            return b.star - a.star
          })
          break;
      }


      this.setState({
        filterData: data,
        loading: !this.state.loading
      })
      console.log(data);
    } else {
      this.setState({ isFilterActive: false })
    }
  }

  getDate(date) {
    //return dateFormat(date, "yyyy-mm-dd");
    return moment(date).format("YYYY-MM-DD");
  }
  getDate2(date) {
    //return dateFormat(date, "yyyy-mm-dd");
    return moment(date).format("DD MMM");
  }

  hotelsList() {
    return (
      <View style={{ flex: 1, marginTop: 0 }}>
        {/* {this.searchComponent()} */}
        <FlatList
          style={{ flex: 1 }}
          data={this.state.isSearchActive
            ? this.state.searchList
            : this.state.isFilterActive ? this.state.filterData : this.state.hotelsListData}
          renderItem={({ item }) => <HotelItemElement hotel={item} fullDetail={false} onPress={() => { this.openHotel(item) }} />}
          keyExtractor={(item) => item.index}
          extraData={this.state.loading}
        />
        {/* <FlatList
          style={{ flex: 1 }}
          data={
            this.state.isSearchActive
              ? this.state.searchList
              : this.state.isFilterActive ? this.state.filterData : this.state.hotelsListData
          }
          extraData={this.state.loading}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => { index.toString() }}
        /> */}
      </View>
    );
  }
  _renderItem = ({ item }) => (
    <TouchableOpacity
      activeOpacity={0.6}
      onPress={() => {


        // alert(JSON.stringify(item))


        // let dict = {
        //   id: item.code,
        //   index: item.index,
        //   source: this.state.source.source,
        //   traceId: this.state.source.traceId,
        //   currencycode: item.priceDetail.currencyCode,
        //   price: item.priceDetail.agencyFare
        // }

        let dict = {
          code: item.code,
          id: item.index.toString(),
          source: this.state.source.source,
          traceId: this.state.source.traceId,
          currencycode: item.priceDetail.currencyCode,
          price: item.priceDetail.agencyFare,
          rooms: this.props.hotelSearchParams.numberOfRooms
        }

        NavigationServices.navigate('HotelOverview', { jsonDict: dict });
      }}
    >
      <View>
        <Image
          source={{ uri: item.photo }}
          style={styles.imageStyle}
          resizeMode="cover"
        />
        <View style={styles.imageOverlayContent}>
          <View style={[styles.imageInnerContent, { width: "70%" }]}>
            <Text style={styles.HotelTitleText}>{item.name}</Text>

            <View style={{ height: "100%" }}>
              <View style={styles.ratinBarStyle}>
                <RatinBar count={item.star} starsize={12} />
              </View>
              <Text
                numberOfLines={3}
                ellipsizeMode={"tail"}
                style={styles.addressStyle} >
                {item.address}
              </Text>
            </View>

          </View>
          <View style={styles.priceBtnContainer}>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
              <Text style={{ fontFamily: Fonts.semiBold, color: colors.colorBlue, fontSize: 18 }}>{hotel.priceDetail && hotel.priceDetail.CurrencyCode == "INR" ? "₹ " : null}</Text>
              <Text style={{ fontFamily: Fonts.semiBold, color: colors.black, fontSize: 18 }}>{hotel.priceDetail && hotel.priceDetail.agencyFare ? hotel.priceDetail.agencyFare : null}</Text>
            </View>
            {/* <Text style={styles.hotelPricedesc} adjustsFontSizeToFit={true}>
                {item.hotel.description}
              </Text> */}
          </View>

        </View>

      </View>
    </TouchableOpacity>
  );


  searchHotel() {

    if (String(this.state.searchStr).trim().length == 0) {

      return;
    }


    let data;
    if (this.state.isFilterActive) {
      data = this.state.filterData.filter(dict => {
        let name = String(dict.name);
        return name.toLocaleLowerCase().startsWith(this.state.searchStr.toLocaleLowerCase());
      });
    } else {
      data = this.state.hotelsListData.filter(dict => {
        let name = String(dict.name);
        return name.toLocaleLowerCase().startsWith(this.state.searchStr.toLocaleLowerCase());
      });
    }

    this.setState({
      searchList: data,
      // searchStr: str,
      isSearchActive: true,
      loading: !this.state.loading
    });


  }


  // onChangeText(str) {
  //   if (String(str).trim().length == 0) {
  //     this.setState({
  //       searchStr: str
  //     });
  //     return;
  //   }

  //   let data;
  //   if(this.state.isFilterActive){
  //     data = this.state.filterData.filter(dict => {
  //       let name = String(dict.hotel.name);
  //       return name.toLocaleLowerCase().startsWith(str.toLocaleLowerCase());
  //     });
  //   }else{
  //     data = this.state.hotelsListData.filter(dict => {
  //       let name = String(dict.hotel.name);
  //       return name.toLocaleLowerCase().startsWith(str.toLocaleLowerCase());
  //     });
  //   }


  //   this.setState({
  //     searchList: data,
  //     searchStr: str,
  //     isSearchActive: true,
  //     loading: !this.state.loading
  //   });
  // }

  searchComponent() {
    return (
      <View style={styles.hotelSearchContainer}>
        <View style={styles.hotelSearch}>
          <View
            style={{
              width: "20%",
              justifyContent: "center",
              alignItems: "center",

            }}
          >
            <Image
              style={{ height: 30, width: 30 }}
              source={Images.imgSearchIcon}
              resizeMode={"contain"}
            />
          </View>

          <TextInput
            placeholder={"Hotel Name"}
            placeholderTextColor={"rgb(112,128,144)"}
            autoCorrect={false}
            style={{ fontSize: 16 }}
            value={this.state.searchStr}
            onChangeText={text => this.setState({ searchStr: text })}
            onEndEditing={() => {
              if (this.state.searchStr.trim().length == 0) {
                this.setState({
                  isSearchActive: false,
                  searchList: [],
                  loading: !this.state.loading
                });
              }
            }}
          />
        </View>

        {/* <View style={{ width: "15%" }}> */}
        <TouchableOpacity
          style={styles.goButton}
          underlayColor="transparent"
          onPress={() => {
            this.searchHotel();
            //  Actions.CityHotel()
          }}
        >
          <Text style={{
            color: "white",
            fontSize: 16,
            fontFamily: "Montserrat-SemiBold"
          }}>
            Go
            </Text>
        </TouchableOpacity>
        {/* </View> */}
      </View>
    );
  }

  // searchComponentzForMap() {
  //   return (
  //     <View
  //       style={[
  //         styles.hotelSearchContainer,
  //         {
  //           backgroundColor: "transparent",
  //           position: "absolute",
  //           width: "100%",
  //           marginTop: 20
  //         }
  //       ]}
  //     >
  //       <View style={[styles.hotelSearch, { marginLeft: 10 }]}>
  //         <View
  //           style={{
  //             width: "20%",
  //             justifyContent: "center",
  //             alignItems: "center"
  //           }}
  //         >
  //           <Image
  //             style={{ height: 30, width: 30 }}
  //             source={Images.imgSearchIcon}
  //             resizeMode={"contain"}
  //           />
  //         </View>
  //         <View style={{ width: "80%" }}>
  //           <TextInput
  //             placeholder={"Hotel Name"}
  //             placeholderTextColor={"rgb(112,128,144)"}
  //             autoCorrect={false}
  //             style={{ fontSize: 16 }}
  //             value={this.state.searchStr}
  //             onChangeText={text => this.setState({searchStr:text})}
  //             onEndEditing={() => {
  //               if (this.state.searchStr.trim().length == 0) {
  //                 this.setState({
  //                   isSearchActive: false,
  //                   searchList: [],
  //                   loading: !this.state.loading
  //                 });
  //               }
  //             }}
  //           />
  //         </View>
  //       </View>

  //       <View style={{ width: "15%" }}>
  //         <TouchableOpacity
  //           style={styles.goButton}
  //           underlayColor="transparent"
  //           onPress={() => {
  //             this.searchHotel()
  //             //  Actions.CityHotel()
  //           }}
  //         >
  //           <Text style={{ color: "white", fontSize: 16, fontWeight: "500" }}>
  //             Go
  //           </Text>
  //         </TouchableOpacity>
  //       </View>
  //     </View>
  //   );
  // }

  _renderNavigationBar = () => {
    // const {destination ,origin,return_date,departure_date} = this.props.flightSearchParams; 
    return (
      <View style={styles.styleNavBar}>
        <View style={styles.styleNavBarSubView}>
          <TouchableOpacity
            style={styles.styleLeftBtn}
            underlayColor="transparent"
            onPress={() => {
              NavigationServices.goBack();
            }}
          >
            <Image
              style={{ height: 25, width: 25 }}
              resizeMode="contain"
              source={Images.imgback}
            />
          </TouchableOpacity>
          {/* <View style={{ width: "25%" }}>
          <View style={{ flexDirection: "row", alignSelf: "center", alignItems: "center", justifyContent: "space-between" }}>
            <Text style={styles.styleNavTitle}>
              {origin}
            </Text>
            <View>
              <Image
                style={{ height: 5, width: 20, alignItems: "center", justifyContent: "center" }}
                resizeMode={"contain"}
                source={Images.imgflightrightarrow}
              />
              {return_date && <Image
                style={{ height: 5, width: 20, alignItems: "center", justifyContent: "center", transform: [{ rotate: '180deg' }] }}
                resizeMode={"contain"}
                source={Images.imgflightrightarrow}
              />}
            </View>
            <Text style={styles.styleNavTitle}>
              {destination}
            </Text>
          </View>
          <View style={{ flexDirection: "row", alignSelf: "center", marginTop: -5 }}>
            <Text style={[styles.listItemGreyText, { textAlign: "center" }]}>
              {moment(departure_date).format("D MMM")}
            </Text>
            {return_date
              ?
              <Text style={[styles.listItemGreyText, { textAlign: "center" }]}>
                - {moment(return_date).format("D MMM")}
              </Text>
              : null}

          </View>
        </View>
 */}


          <View style={[styles.styleNavBarRightView]}>
            <TouchableOpacity
              style={{
                width: 30,
                marginRight: 10
              }}
              underlayColor="transparent"
              onPress={() => {
                this.setState({
                  showFilter: true,

                })
              }
              }
            >

              <Image
                style={{ height: 20, width: 20, marginLeft: 5 }}
                source={Images.imgfilter}
                resizeMode="contain"
              />

            </TouchableOpacity>


          </View>
        </View>
      </View>
    )
  }
  openHotel(item) {
    let dict = {
      code: item.code,
      id: item.index.toString(),
      source: this.state.source.source.toUpperCase(),
      traceId: this.state.source.traceId,
      currencycode: item.priceDetail.currencyCode,
      price: item.priceDetail.agencyFare,
      rooms: this.props.hotelSearchParams.numberOfRooms
    }
    // alert(JSON.stringify(dict))
    console.log(JSON.stringify(dict))

    NavigationServices.navigate('HotelOverview', { jsonDict: dict });

  }
  renderMiddle() {
    let param = this.props.hotelSearchParams
    if (param)
      // alert(JSON.stringify(param))
      return (
        <View style={styles.headerContent}>
          <View style={styles.headerContentRow}>
            <Text style={styles.headerStnName}>{param.name}</Text>
          </View>
          <View style={styles.headerContentRow}>
            <Text style={styles.headerContentRowText}>{this.getDate2(Date.parse(param.checkIn)) + "-" + this.getDate2(Date.parse(param.checkOut))}</Text>
            <View style={styles.headerRowDevider}></View>
            <Text style={styles.headerContentRowText}>{param.numberOfRooms + (param.numberOfRooms > 1 ? " Rooms" : " Room")}</Text>
            <View style={styles.headerRowDevider}></View>
            <Text style={styles.headerContentRowText}>{param.numberOfGuest + (param.numberOfGuest > 1 ? " Guests" : " Guest")} </Text>
          </View>
        </View>

      )
  }
  renderFilterModel() {
    return (
      < Modal
        animationType="slide"
        transparent={false}
        visible={this.state.showFilter}
      >
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.colorBlue }}>
          <StatusBar barStyle={"light-content"} />
          <View style={{ flex: 1, marginTop: 10, backgroundColor: "rgba(244,248,254,1)" }}>
            <View style={{ height: 50, width: "100%", backgroundColor: colors.colorBlue, flexDirection: "row", alignItems: "center", padding: 10 }}>
              <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20, color: "white", flex: 1, textAlign: "center" }}>Filter</Text>
              <TouchableOpacity
                underlayColor={"transparent"}
                onPress={this.onFilterSelection}>
                <Image
                  style={{ height: 20, width: 20, tintColor: "white" }}
                  source={Images.imgClose} />
              </TouchableOpacity>
            </View>

            <View style={[commonstyle.itemBackground, { borderRadius: 2, marginTop: 15 }]}>
              <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={commonstyle.blackSemiBold}>  Price   </Text>
                <TouchableOpacity
                  underlayColor={"transparent"}
                  onPress={() => {
                    this.minimumselPrice = this.state.minPrice;
                    this.maximumselPrice = this.state.maxPrice;
                    this.setState({ selectedMinPrice: this.state.minPrice, selectedMaxPrice: this.state.maxPrice })
                  }}>
                  <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 15, color: colors.colorBlue }}>Reset  </Text>
                </TouchableOpacity>
              </View>

              <View style={{ flexDirection: "row", justifyContent: "space-between", padding: 5 }}>
                <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 12, color: "grey" }}>Min: {parseInt(this.state.selectedMinPrice)}   </Text>

                <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 12, color: "grey" }}>Max: {parseInt(this.state.selectedMaxPrice)}   </Text>


              </View>
              <View style={{ marginTop: 40, justifyContent: "center", alignItems: "center" }}>

                <MultiSlider
                  isMarkersSeparated={true}
                  customMarkerLeft={(e) => {
                    return (
                      <View style={{ height: 75, alignItems: "center" }}>
                        <Text style={{ marginBottom: 4, textAlign: "center", fontSize: 12 }} >
                          {e.pressed ? parseInt(e.currentValue) : null}
                        </Text>
                        <View style={{ height: 30, backgroundColor: "white", borderColor: "black", borderWidth: 1, width: 30, borderRadius: 15, slipDisplacement: 200 }}
                          currentValue={parseInt(e.currentValue)}>
                        </View>
                      </View>)
                  }}


                  customMarkerRight={(e) => {

                    return (
                      <View style={{ height: 75, alignItems: "center" }}>
                        <Text style={{ marginBottom: 4, textAlign: "center", fontSize: 12 }} >
                          {e.pressed ? parseInt(e.currentValue) : null}
                        </Text>
                        <View style={{ height: 30, backgroundColor: "white", borderColor: "black", borderWidth: 1, width: 30, borderRadius: 15, slipDisplacement: 200 }}
                          currentValue={parseInt(e.currentValue)}>

                        </View>
                      </View>)
                  }}
                  onValuesChangeFinish={(values) => { this.setState({ selectedMinPrice: values[0], selectedMaxPrice: values[1] }) }}

                  values={[this.state.selectedMinPrice, this.state.selectedMaxPrice]}
                  min={this.state.minPrice}
                  max={this.state.maxPrice}
                  step={1}
                />

              </View>



            </View>

            <View style={[commonstyle.itemBackground, { borderRadius: 2, marginTop: 5 }]}>

              <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={[commonstyle.blackText, { fontSize: 15 }]}>Rating</Text>
                <TouchableOpacity
                  underlayColor={"transparent"}
                  onPress={() => {
                    this.setState({
                      is2starSelected: false,
                      is3starSelected: false,
                      is4starSelected: false,
                      is5starSelected: false,
                    })
                  }}>

                  <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 15, color: colors.colorBlue }}>  Reset </Text>
                </TouchableOpacity>
              </View>

              <View style={{ marginTop: 25, flexDirection: "row", justifyContent: "space-evenly", marginBottom: 10 }}>

                <TouchableOpacity
                  onPress={() => this.setState({ is2starSelected: !this.state.is2starSelected })}
                  underlayColor={"transparent"}>
                  <View style={{ height: 50, alignItems: 'center', width: 50, borderWidth: 0.8, borderColor: this.state.is2starSelected ? colors.colorBlue : colors.colorBlack62, borderRadius: 4 }}>
                    <View style={{ height: 24, padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{ width: 15, height: 15, tintColor: this.state.is2starSelected ? colors.colorBlue : colors.colorBlack62 }} source={this.state.is2starSelected ? Images.star : Images.icon_holo_star} resizeMode={'contain'} />
                    </View>
                    <View style={{ height: 1, backgroundColor: this.state.is2starSelected ? colors.colorBlue : colors.colorBlack62, width: 40, }}>

                    </View>
                    <View style={{ height: 25, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: this.state.is2starSelected ? colors.colorBlue : colors.colorBlack62 }}>{"<2 Star"}</Text>
                    </View>
                  </View>


                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.setState({ is3starSelected: !this.state.is3starSelected })}
                  underlayColor={"transparent"}>
                  <View style={{ height: 50, alignItems: 'center', width: 50, borderWidth: 0.8, borderColor: this.state.is3starSelected ? colors.colorBlue : colors.colorBlack62, borderRadius: 4 }}>
                    <View style={{ height: 24, padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{ width: 15, height: 15, tintColor: this.state.is3starSelected ? colors.colorBlue : colors.colorBlack62 }} source={this.state.is3starSelected ? Images.star : Images.icon_holo_star} resizeMode={'contain'} />
                    </View>
                    <View style={{ height: 1, backgroundColor: this.state.is3starSelected ? colors.colorBlue : colors.colorBlack62, width: 40, }}>

                    </View>
                    <View style={{ height: 25, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: this.state.is3starSelected ? colors.colorBlue : colors.colorBlack62 }}>{"3 Star"}</Text>
                    </View>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.setState({ is4starSelected: !this.state.is4starSelected })}
                  underlayColor={"transparent"}>
                  <View style={{ height: 50, alignItems: 'center', width: 50, borderWidth: 0.8, borderColor: this.state.is4starSelected ? colors.colorBlue : colors.colorBlack62, borderRadius: 4 }}>
                    <View style={{ height: 24, padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{ width: 15, height: 15, tintColor: this.state.is4starSelected ? colors.colorBlue : colors.colorBlack62 }} source={this.state.is4starSelected ? Images.star : Images.icon_holo_star} resizeMode={'contain'} />
                    </View>
                    <View style={{ height: 1, backgroundColor: this.state.is4starSelected ? colors.colorBlue : colors.colorBlack62, width: 40, }}>

                    </View>
                    <View style={{ height: 25, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: this.state.is4starSelected ? colors.colorBlue : colors.colorBlack62 }}>{"4 Star"}</Text>
                    </View>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.setState({ is5starSelected: !this.state.is5starSelected })}
                  underlayColor={"transparent"}>
                  <View style={{ height: 50, alignItems: 'center', width: 50, borderWidth: 0.8, borderColor: this.state.is5starSelected ? colors.colorBlue : colors.colorBlack62, borderRadius: 4 }}>
                    <View style={{ height: 24, padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{ width: 15, height: 15, tintColor: this.state.is5starSelected ? colors.colorBlue : colors.colorBlack62 }} source={this.state.is5starSelected ? Images.star : Images.icon_holo_star} resizeMode={'contain'} />
                    </View>
                    <View style={{ height: 1, backgroundColor: this.state.is5starSelected ? colors.colorBlue : colors.colorBlack62, width: 40, }}>

                    </View>
                    <View style={{ height: 25, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: this.state.is5starSelected ? colors.colorBlue : colors.colorBlack62 }}>{"5 Star"}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <View style={[commonstyle.itemBackground, { borderRadius: 2, marginTop: 5 }]}>

              <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={[commonstyle.blackText, { fontSize: 15 }]}>Sorting</Text>
                <TouchableOpacity
                  underlayColor={"transparent"}
                  onPress={() => {
                    this.setState({
                      sortingStatus: 0,
                    })
                  }}>
                  <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 15, color: colors.colorBlue }}>  Reset </Text>
                </TouchableOpacity>
              </View>
              <View style={{ marginTop: 25, flexDirection: "row", justifyContent: "space-evenly", marginBottom: 10 }}>
                <TouchableOpacity
                  underlayColor={"transparent"}
                  onPress={() => {
                    this.setState({
                      sortingStatus: 1,
                    })
                  }}>
                  <View style={{ alignItems: 'center' }}>
                    <View style={{ padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{ width: 18, height: 18, tintColor: this.state.sortingStatus == 1 ? colors.colorBlue : "#626262" }} source={Images.icon_low} resizeMode={'contain'} />
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: Fonts.regular, textAlign: 'center', fontSize: 10, color: this.state.sortingStatus == 1 ? colors.colorBlue : "#626262" }}>{"Price\nLow To High"}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <View style={{ width: 1, height: 40, backgroundColor: 'grey' }} />

                <TouchableOpacity
                  underlayColor={"transparent"}
                  onPress={() => {
                    this.setState({
                      sortingStatus: 2,
                    })
                  }}>
                  <View style={{ alignItems: 'center' }}>
                    <View style={{ padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{ width: 18, height: 18, tintColor: this.state.sortingStatus == 2 ? colors.colorBlue : "#626262" }} source={Images.icon_high} resizeMode={'contain'} />
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: Fonts.regular, textAlign: 'center', fontSize: 10, color: this.state.sortingStatus == 2 ? colors.colorBlue : "#626262" }}>{"Price\nHigh To Low"}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <View style={{ width: 1, height: 40, backgroundColor: 'grey' }} />

                <TouchableOpacity
                  underlayColor={"transparent"}
                  onPress={() => {
                    this.setState({
                      sortingStatus: 3,
                    })
                  }}>
                  <View style={{ alignItems: 'center' }}>
                    <View style={{ padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{ width: 18, height: 18, tintColor: this.state.sortingStatus == 3 ? colors.colorBlue : "#626262" }} source={Images.icon_rating} resizeMode={'contain'} />
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: Fonts.regular, textAlign: 'center', fontSize: 10, color: this.state.sortingStatus == 3 ? colors.colorBlue : "#626262" }}>{"User\nRating"}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <View style={{ width: 1, height: 40, backgroundColor: 'grey' }} />

                <TouchableOpacity
                  underlayColor={"transparent"}
                  onPress={() => {
                    this.setState({
                      sortingStatus: 4,
                    })
                  }}>
                  <View style={{ alignItems: 'center' }}>
                    <View style={{ padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{ width: 18, height: 18, tintColor: this.state.sortingStatus == 4 ? colors.colorBlue : "#626262" }} source={Images.star} resizeMode={'contain'} />
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: Fonts.regular, textAlign: 'center', fontSize: 10, color: this.state.sortingStatus == 4 ? colors.colorBlue : "#626262" }}>{"Hotel\nPopularity"}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>

          </View>
        </SafeAreaView>
      </Modal >
    )
  }



  render() {
    return (
      <View style={styles.container}>
        {this.renderFilterModel()}


        <SafeAreaView backgroundColor={colors.colorBlue} ></SafeAreaView>
        <StatusBar backgroundColor={colors.colorBlue} />

        <CommomHeader titleView={() => this.renderMiddle()} fireEvent={() => {
          this.setState({
            showFilter: true,
          })
        }} rightImg={Images.imgfilter} />
        <Spinner
          style={{ flex: 1 }}
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={{ color: colors.colorWhite }}
        />

        <View style={{
          flex: 1,
          backgroundColor: colors.colorWhite
        }}>
          <View style={{ padding: 18, paddingHorizontal: 20, flex: 1 }}>
            <Text style={{ fontSize: 14, color: colors.colorBlack, fontFamily: Fonts.bold, marginBottom: 5 }}>Recommended For You</Text>
            {this.hotelsList()}
          </View>
        </View></View >);
  }
}


const mapDisatchToProps = dispatch => {
  return {
    getHotelListAction: data => dispatch(getHotelListAction(data))
  };
}


const mapStateToProps = state => {

  console.log("redux state is " + state)
  return {
    isLoading: state.isLoadingReducer,
    hotelSearchParams: state.HotelListreducer.hotelSearchParam,
    hotelList: state.HotelListreducer.HotellistData
  }
}

export default connect(mapStateToProps, mapDisatchToProps)(HotelList)
