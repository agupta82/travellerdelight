import { Dimensions, StyleSheet } from "react-native";
import Images from "../../../Helper Classes/Images";
import { colors } from "../../../Helper Classes/Colors";
import Fonts from '../../../Helper Classes/Fonts';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default styles = StyleSheet.create({
  flightAmenityImg: {
    width: 20, height: 20,
    resizeMode: "contain",
  },
  cardHeaderAmenties: {
    width: "40%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around"
  },
  cardheaderFlightImg: {
    width: 20, height: 20,
    resizeMode: "contain", marginRight: 5
  },
  cardheaderFlightView: {
    height: "100%",
    width: "60%",
    flexDirection: "row",
    alignItems: "center"
  },
  bookBtnText: {
    color: colors.colorWhite,
    fontSize: 12,
    fontFamily: Fonts.semiBold
  },
  bookButton: {
    backgroundColor: colors.colorBlue,
    borderRadius: 7,
    padding: 7
  },
  cardHeaderCol2: {
    width: "30%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  cardHeaderCol1: {
    width: "70%",
    height: "100%",
    flexDirection: "row"
  },
  stnName: {
    fontFamily: Fonts.regular,
    color: colors.colorBlack,
    fontSize: 14
  },
  dotView: {
    height: 10,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  estimate: {
    fontFamily: Fonts.semiBold,
    color: colors.colorBlack,
    fontSize: 14
  },
  priceImg: {
    width: 12, height: 12, resizeMode: "contain"
  },
  priceText: {
    flexDirection: "row",
    color: colors.colorBlack,
    fontFamily: Fonts.bold,
    fontSize: 14
  },
  cardContentCol2: {
    width: "20%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  cardContentCol1: {
    width: "70%",
    height: "100%",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  cardContent: {
    height: 85, width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 7
  },
  cardHeader: {
    height: 35,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: colors.gray,
    paddingLeft: 7
  },
  card: {
    borderWidth: 3,
    borderColor: colors.gray,
    borderRadius: 10,
    height: 120,
    width: "100%",
    overflow: "hidden",
    marginBottom: 20
  },
  mainView: {
    width: screenWidth,
    height: screenHeight - 180,
    padding: 12,
    paddingTop: 15
  },
  price: {
    color: colors.colorBlue,
    fontFamily: Fonts.medium,
    fontSize: 14
  },
  priceBtn: {
    width: 120,
    height: "100%",

    justifyContent: "center",
    alignItems: "center",
  },
  priceDevider: {
    width: .5,
    height: 40,
    backgroundColor: colors.colorBlack
  },
  priceView: {
    height: 70,
    width: 120,
    flexDirection: "row",
    alignItems: "center",
    borderTopWidth: 1.5,
  },
  pricesView: {
    width: screenWidth,
    height: 70,
    backgroundColor: colors.gray,
    position: "absolute",
    bottom: 0
  },
  tabs: {
    height: 40,
    width: "100%",
    flexDirection: "row",
    backgroundColor: colors.gray,
    flexDirection: "row"
  },
  tabText: {
    color: colors.colorBlack,
    fontSize: 14,
    fontFamily: Fonts.semiBold
  },
  tab: {
    width: screenWidth / 3,
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  headerRowDevider: {
    height: 12, width: 1.5,
    backgroundColor: colors.colorWhite,
    marginLeft: 7,
    marginRight: 7,
    marginTop: 5
  },
  headerContentRowText: {
    color: colors.colorWhite,
    fontSize: 12,
    fontFamily: Fonts.medium,
    lineHeight: 20
  },
  headerContentIcon: {
    width: 20, height: 20,
    resizeMode: "contain",
    marginLeft: 10,
    marginRight: 10
  },
  headerStnName: {
    color: colors.colorWhite,
    fontSize: 14,
    fontFamily: Fonts.semiBold
  },
  headerContentRow: { width: "100%", flexDirection: "row", justifyContent: "center" },
  headerContent: {
    height: "100%",
    width: screenWidth - 120,
    padding: 8,
    justifyContent: "center"
  },
  backBtnText: {
    color: colors.colorWhite,
    fontFamily: Fonts.semiBold
  },
  container: {
    flex: 1,
    backgroundColor: "#fdfdfd"
  },
  header: {
    height: 70,
    width: screenWidth,
    backgroundColor: colors.colorBlue,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 12,
    paddingRight: 12
  },
  headerBackButton: {
    width: 60,
    // backgroundColor: "red",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  headerButton: {
    height: "100%",
    width: 40,
    // backgroundColor: "blue",
    justifyContent: "center",
    alignItems: "flex-end"
  },
})