import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import CardView from 'react-native-cardview'

import ImageSliderz from "react-native-image-slideshow";
import Images from '../../../Helper Classes/Images'
import Fonts from "../../../Helper Classes/Fonts";
import { colors } from '../../../Helper Classes/Colors';
import { filterAmenities } from '../../../Utility'

class HotelItemElement extends Component {
  constructor(props) {
    super(props)
    this.state = {
      amenities: null,
      hotel: {

      }

    }
  }

  componentDidMount() {
    let newAmenities = this.props.hotel.amenities && this.props.hotel.amenities.length > 0 ? filterAmenities(this.props.hotel.amenities) : null
    // alert(JSON.stringify(newAmenities))
    this.setState({
      amenities: newAmenities
    })


  }
  render() {
    let { hotel, onPress } = this.props;
    return (
      <CardView style={{
        //overflow: 'hidden',
        backgroundColor: colors.colorWhite,
        marginVertical: 10,
        marginHorizontal: 2
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10, overflow: "hidden" }}>
          <Image source={{ uri: hotel.photo }} style={{ width: '100%', height: 170, resizeMode: 'cover' }} />

          {/* <ImageSliderz
            height={170}
            width={'100%'}
            containerStyle={{}}
            dataSource={this.props.hotel.images}
            indicatorSelectedColor="white"
            indicatorSize={7}
            position={this.state.position}
            arrowSize={0}
            //onPositionChanged={position => this.setState({ position })}
            titleViewBackgroundColor='rgba(0,0,0,0.5)'
            imageBorderRadius={10}
            indicatorAlignment='flex-end'
          /> */}
          <TouchableOpacity style={{ borderRadius: 20, position: 'absolute', backgroundColor: 'white', height: 35, width: 35, right: 10, top: 10, alignItems: 'center', justifyContent: 'center' }}>
            <Image source={Images.favourite_inactive} style={{ width: 30, height: 30, resizeMode: 'contain' }} />

          </TouchableOpacity>
          <TouchableOpacity onPress={onPress} style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 10, paddingVertical: 12 }}>
            <View style={{ width: '70%' }}>
              <Text style={{ fontFamily: Fonts.semiBold, color: colors.black, fontSize: 15 }}>{this.props.hotel.name ? this.props.hotel.name : null}</Text>
              {!this.props.fullDetails ?
                <View>
                  <View style={{ flexDirection: 'row', marginTop: 4, alignItems: 'center', }}>
                    <Text numberOfLines={2} style={{ fontFamily: Fonts.semiBold, color: '#626262', fontSize: 12 }}>{this.props.hotel.address ? this.props.hotel.address : null}</Text>
                  </View>
                  {this.state.amenities ?
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Image source={this.state.amenities.length > 1 && this.state.amenities[0].icon} style={{ width: 12, height: 12, resizeMode: 'center' }} />
                      <Text style={{ fontFamily: Fonts.semiBold, color: '#4d4d4d', fontSize: 13 }}>{this.state.amenities.length > 0 && this.state.amenities[0].amenity ? this.state.amenities[0].amenity : null}</Text>
                      <Image source={this.state.amenities.length > 1 && this.state.amenities[1].icon} style={{ width: 12, height: 12, resizeMode: 'center' }} />
                      <Text style={{ fontFamily: Fonts.semiBold, color: '#4d4d4d', fontSize: 13 }}>{this.state.amenities.length > 1 && this.state.amenities[1].amenity ? this.state.amenities[1].amenity : null}</Text>

                    </View> : null}
                </View>
                :
                <Text style={{ fontFamily: Fonts.semiBold, color: "#626262", fontSize: 13, paddingTop: 4, paddingBottom: 4 }}>{hotel.address ? hotel.address : null}</Text>

              }
            </View>
            {!this.props.fullDetails ?
              <View style={{ flex: 1, alignItems: 'flex-end' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                  <Text style={{ fontFamily: Fonts.semiBold, color: colors.colorBlue, fontSize: 18 }}>{hotel.priceDetail && hotel.priceDetail.CurrencyCode == "INR" ? "₹ " : null}</Text>
                  <Text style={{ fontFamily: Fonts.semiBold, color: colors.black, fontSize: 18 }}>{hotel.priceDetail && hotel.priceDetail.OfferedPriceRoundedOff ? hotel.priceDetail.OfferedPriceRoundedOff : null}</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', paddingTop: 3 }}>
                  <Text style={{ fontFamily: Fonts.semiBold, color: "grey", fontSize: 10 }}>{hotel.priceDetail ? "Per Night" : null}</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 4, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.star} style={{ width: 12, height: 12, resizeMode: 'contain' }} />
                  <Text style={{ fontFamily: Fonts.semiBold, color: '#4d4d4d', fontSize: 13, paddingLeft: 4 }}>{hotel.star ? hotel.star : null}</Text>
                </View>
              </View> : null
            }
          </TouchableOpacity>
        </View>
      </CardView>
    )
  }
}

export default HotelItemElement
