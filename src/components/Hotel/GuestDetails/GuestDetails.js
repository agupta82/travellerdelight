import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  DeviceEventEmitter,
  SectionList,
  FlatList,
  SafeAreaView,
  TextInput
} from "react-native";


import { Dropdown } from 'react-native-material-dropdown';
import NavigationBar from '../../../Helper Classes/NavigationBar'
import NavigationServices from './../../../Helper Classes/NavigationServices'
import CustomTextInput from "../../../Helper Classes/Custom TextFiel/CustomTextInput";
import ActionSheet from "react-native-actionsheet";
import Spinner from "react-native-loading-spinner-overlay";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Images from "../../../Helper Classes/Images";
import { styles } from "./Styles";
import { connect } from "react-redux";
import { colors } from "../../../Helper Classes/Colors";
import CommonHeader from "../../../Helper Classes/CommonHeader";
import Titles from "../../../json/Titles"
import Fonts from "../../../Helper Classes/Fonts";


const guestType = {
  Adult: "Adult",
  child: "Children",
  Infant: "Infant"
}
let SOURCES = null;

let sectionItemIndex = -1;
let listItemIndex = -1;
class GuestDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      phoneNumber: "",
      email: "",
      roomIndex: null,
      adultTitleActionSheet: null,
      childTitleActionSheet: null,
      sectionData: [],
      email: 'dj@email.com', phoneNumber: "9588558818"

    }


    this._renderAdultView = this._renderAdultView.bind(this);
    this._renderChildView = this._renderChildView.bind(this);

    this._renderAdultOrChildView = this._renderAdultOrChildView.bind(this);
    this.adultTitlesArr = ["Mr.", "Mrs.", "Ms.", "Cancel"]
    this.childTitlesArr = ["Master", "Miss", "Cancel"]
    //         // this._rendercheckInItem = this._rendercheckInItem.bind(this);
    //         // this._handleDatePicked = this._handleDatePicked.bind(this);
  }


  componentDidMount() {
    let { navigation } = this.props;
    let roomIndex = navigation.getParam('roomIndex', null)
    SOURCES = navigation.getParam('sources', null)
    this.setState({
      roomIndex: roomIndex
    })

    DeviceEventEmitter.addListener("citySelected", dict => {
      let addDict = this.state.addressDict;

      (addDict.city = dict.name), (addDict.countryCode = dict.country_code);
      console.log(JSON.stringify(addDict));
      this.setState({
        addressDict: addDict
      });

    });

    let roomData = this.props.hotelSearchParams.roomData;
    console.log("RoomData:=>" + JSON.stringify(roomData))
    if (roomData) {

      const guestArray = [];
      const guestType = {
        Adult: "Adult",
        child: "Child",
      };
      if (roomData != null) {
        let adultCount;
        let childCount;
        let index = 0;
        roomData.forEach((element) => {
          const roomGuestArray = [];
          adultCount = element.adult;
          childCount = element.children;



          console.log("index " + index + "   " + "ChildCount " + childCount + "AdultCount " + adultCount);
          while (adultCount) {
            roomGuestArray.push({
              type: guestType.Adult,
              title: "",
              fName: "",
              mName: "",
              lName: "",
            })
            adultCount--;
          }
          while (childCount) {
            roomGuestArray.push({
              type: guestType.child,
              title: "",
              fName: "",
              mName: "",
              lName: "",
              age: ""
            })
            childCount--;
          }
          console.log("LOOP ROOMGUEST ARRAY" + JSON.stringify(roomGuestArray))
          const room = index + 1
          const dict = {
            data: roomGuestArray,
            key: "Room " + room,
            index: index
          }
          guestArray.push(dict);
          //   guestArray.push(roomGuestArray)
          console.log("LOOP GUEST ARRAY" + JSON.stringify(guestArray))
          index++;
          // roomCount = roomCount+1
        });
        this.setState({
          sectionData: [...guestArray]
        })
        //this.sectionData = ;

        console.log("guest array " + JSON.stringify(guestArray));
      } else {
        console.log("guest count null")
      }
    } else {
      console.log("no props data" + JSON.stringify(this.props.hotelSearchParam) + "   " + JSON.stringify(this.props.roomDetails))
    }


    // this.calculatePassengers(dict.adult, dict.child, dict.infant);

  }

  ApiHotelBooking() {
    console.log("calling Hotel Booking Api")
  }


  validateEntryFields() {

    let arr = this.state.sectionData;

    let isDetailsValid = true;

    arr.every((sectionItem, index) => {
      const dataArray = sectionItem.data;
      dataArray.every((item, index) => {

        if (item.title.trim().length == 0) {
          alert("Please Select Title.");
          isDetailsValid = false;
          return false;
        }
        if (String(item.fName).trim().length == 0) {
          alert("Please Enter First Name.");
          isDetailsValid = false;
          return false;
        }

        if (String(item.lName).trim().length == 0) {
          alert("Please Enter Last Name.");
          isDetailsValid = false;
          return false;
        }

        if (item.type === "child") {
          if (String(item.age).trim().length == 0) {
            alert("Please Enter the child's age");
            isDetailsValid = false;
            return false;
          }
          if (item.age <= 0) {
            alert("Child's age must be greater than 0");
            isDetailsValid = false;
            return false;
          }
          if (item.age >= 12) {
            alert("Child's age must be below 12");
            isDetailsValid = false;
            return false;
          }
        }
        // isDetailsValid = true;
      })

      if (!isDetailsValid) {
        return false;
      }
    }

    )
    if (!this.isEmailValid(this.state.email)) {
      alert("Invalid Email");
      isDetailsValid = false;
      return false;
    }
    if (this.state.phoneNumber.length < 1 && this.state.phoneNumber.length > 10) {
      alert("Invalid Phone");
      isDetailsValid = false;
      return false;
    }

    if (isDetailsValid) {
      // let a = [{ "data": [{ "type": "Adult", "title": "Mr.", "fName": "as", "mName": "safa", "lName": "sfa" }], "key": "Room 1", "index": 0 }]

      NavigationServices.navigate('ReviewBooking', { roomIndex: this.state.roomIndex, guestDetails: this.state.sectionData, email: this.state.email, phone: this.state.phoneNumber, sources: SOURCES });

      console.log()
      // alert("data valid");
      //NavigationServices.navigate('ReviewBooking', { roomIndex: this.state.roomIndex, guestDetails: this.state.sectionData, email: this.state.email, phone: this.state.phoneNumber });
    }




  }


  calculatePassengers(numOfAdult, numOfChild) {
    let arr = [];

    if (numOfAdult > 0) {
      for (i = 0; i < numOfAdult; i++) {
        arr.push(this.state.adultDict);
      }
    }

    if (numOfChild > 0) {
      for (i = 0; i < numOfChild; i++) {
        arr.push(this.state.childDict);
      }
    }

    console.log("Passenger Details" + JSON.stringify(arr));
    this.setState({
      passengersArray: arr,
      loading: !this.state.loading
    });
  }

  isEmailValid(text) {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      return false;
    } else {
      console.log("Email is Correct");
      return true;
    }
  }

  showAdultTitleActionSheet() {

    this.adultTitleActionSheet.show();
  }

  showChildTitleActionSheet() {

    this.childTitleActionSheet.show();
  }

  onChangeText(itemIndex, text, key, section, item) {



    if (section != null && section.index != null) {
      console.log("section " + JSON.stringify(section) + " index",
        itemIndex, "item data is " + JSON.stringify(item),
        "text is " + text, "key is " + key);
      const data = [...this.state.sectionData]
      if (key === "title") {
        data[section.index].data[itemIndex].title = text;
      } else
        if (key === "fName") {
          data[section.index].data[itemIndex].fName = text;
        } else if (key === "mName") {
          data[section.index].data[itemIndex].mName = text;
        } else if (key === "lName") {
          data[section.index].data[itemIndex].lName = text;
        } else if (key === "age") {
          data[section.index].data[itemIndex].age = text;
        }

      this.setState({
        loading: !this.state.loading
      })

      this.setState({
        sectionData: [...data]
      })

    }


  }

  _renderContactDetailsView() {
    return (
      <View style={{ padding: 10 }}>
        <View style={styles.styleAdultOrChildViewContainer}>
          <Text style={{ fontFamily: "Montserrat-Bold" }}>
            Contact Details
                   </Text>
          <CustomTextInput
            placeholder="Email"
            value={this.state.email}
            tintColor={colors.colorBlue}
            isEditable={true}
            icon={Images.imgmail}
            contentType="postalCode"
            onChangeText={text => {
              this.setState({
                email: text
              });
            }}
          />

          <CustomTextInput
            placeholder="Phone Number"
            isEditable={true}
            value={this.state.phoneNumber}
            tintColor={colors.colorBlue}
            keyboardType="number-pad"
            contentType="telephoneNumber"
            maxLength={10}
            icon={Images.imgphone}
            onChangeText={text => {
              this.setState({
                phoneNumber: text
              });
            }}
          />
        </View>
      </View>
    );
  }

  _renderChildView(item, index, section) {
    console.log("sectionList item " + JSON.stringify(item))
    console.log("section is " + JSON.stringify(section))
    return (
      <View style={styles.styleAdultOrChildViewContainer}>
        <View style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ width: '50%', padding: 5 }}>
            <Dropdown placeholder={"Title"}
              placeholderTextColor={"grey"}
              data={Titles.child}
              fontSize={13}
              fontFamily={Fonts.regular}
              value={item.title}
              containerStyle={{
                width: "100%",
              }}
              inputContainerStyle={{
                borderBottomColor: 'grey',
                borderBottomWidth: 1,
                padding: 4,
                height: 34,
                margin: 4,
              }}
              pickerStyle={{
                width: "50%",
                marginLeft: 10,
                marginTop: 40,
              }}
              itemTextStyle={{ color: "grey", fontSize: 13, height: 34, fontFamily: Fonts.regular }}
              itemColor={"grey"}
              dropdownOffset={{ top: 10, left: 0 }}
              onChangeText={text => this.onChangeText(index, text, "title", section, item)}
            />
          </View>
          <View style={{ width: '50%', padding: 5 }}>
            <TextInput
              style={{
                height: 34,
                margin: 4,
                fontSize: 13,
                padding: 4,
                fontFamily: Fonts.regular,
                borderBottomColor: 'grey',
                borderBottomWidth: 1
              }}
              value={item.type}
              onChangeText={text => this.onChangeText(index, text, "type", section, item)}
              isEditable={false}
              autoCapitalize={"none"}
              autoCorrect={false}
            />
          </View>
        </View>


        <View style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ width: '50%', padding: 5 }}>
            <TextInput
              style={{
                height: 34,
                margin: 4,
                fontSize: 13,
                padding: 4,
                fontFamily: Fonts.regular,
                borderBottomColor: 'grey',
                borderBottomWidth: 1
              }}
              value={item.fName}
              onChangeText={text => this.onChangeText(index, text, "fName", section, item)}
              placeholder={'First Name'}
              autoCapitalize={"none"}
              autoCorrect={false}
              placeholderTextColor={'grey'}
            />
          </View>
          <View style={{ width: '50%', padding: 5 }}>
            <TextInput
              style={{
                height: 34,
                margin: 4,
                fontSize: 13,
                padding: 4,
                fontFamily: Fonts.regular,
                borderBottomColor: 'grey',
                borderBottomWidth: 1
              }}
              value={item.lName}
              onChangeText={text => this.onChangeText(index, text, "lName", section, item)}
              placeholder={'Last Name'}
              autoCapitalize={"none"}
              autoCorrect={false}
              placeholderTextColor={'grey'}
            />
          </View>
        </View>



        <View style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ width: '50%', padding: 5 }}>
            <TextInput
              style={{
                height: 34,
                margin: 4,
                fontSize: 13,
                padding: 4,
                fontFamily: Fonts.regular,
                borderBottomColor: 'grey',
                borderBottomWidth: 1
              }}
              value={item.dob}
              onChangeText={text => this.onChangeText(index, text, "age", section, item)}
              placeholder={'Age (below 12)'}
              autoCapitalize={"none"}
              autoCorrect={false}
              placeholderTextColor={'grey'}
            />
          </View>

        </View>




      </View>
    );
  }

  _renderAdultView(item, index, section) {
    console.log("_renderAdultView" + JSON.stringify(item) + " " + index + " " + " " + JSON.stringify(section))
    return (
      <View style={styles.styleAdultOrChildViewContainer}>

        <View style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ width: '50%', padding: 5 }}>
            <Dropdown placeholder={"Title"}
              placeholderTextColor={"grey"}
              data={Titles.adult}
              fontSize={13}
              fontFamily={Fonts.regular}
              value={item.title}
              containerStyle={{
                width: "100%",
              }}
              inputContainerStyle={{
                borderBottomColor: 'grey',
                borderBottomWidth: 1,
                padding: 4,
                height: 34,
                margin: 4,
              }}
              pickerStyle={{
                width: "50%",
                marginLeft: 10,
                marginTop: 40,
              }}
              itemTextStyle={{ color: "grey", fontSize: 13, height: 34, fontFamily: Fonts.regular }}
              itemColor={"grey"}
              dropdownOffset={{ top: 10, left: 0 }}
              onChangeText={text => this.onChangeText(index, text, "title", section, item)}
            />
          </View>
          <View style={{ width: '50%', padding: 5 }}>
            <TextInput
              style={{
                height: 34,
                margin: 4,
                fontSize: 13,
                padding: 4,
                fontFamily: Fonts.regular,
                borderBottomColor: 'grey',
                borderBottomWidth: 1
              }}
              value={item.type}
              onChangeText={text => this.onChangeText(index, text, "type", section, item)}
              isEditable={false}
              autoCapitalize={"none"}
              autoCorrect={false}
            />
          </View>
        </View>


        <View style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ width: '50%', padding: 5 }}>
            <TextInput
              style={{
                height: 34,
                margin: 4,
                fontSize: 13,
                padding: 4,
                fontFamily: Fonts.regular,
                borderBottomColor: 'grey',
                borderBottomWidth: 1
              }}
              value={item.fName}
              onChangeText={text => this.onChangeText(index, text, "fName", section, item)}
              placeholder={'First Name'}
              autoCapitalize={"none"}
              autoCorrect={false}
              placeholderTextColor={'grey'}
            />
          </View>
          <View style={{ width: '50%', padding: 5 }}>
            <TextInput
              style={{
                height: 34,
                margin: 4,
                fontSize: 13,
                padding: 4,
                fontFamily: Fonts.regular,
                borderBottomColor: 'grey',
                borderBottomWidth: 1
              }}
              value={item.lName}
              onChangeText={text => this.onChangeText(index, text, "lName", section, item)}
              placeholder={'Last Name'}
              autoCapitalize={"none"}
              autoCorrect={false}
              placeholderTextColor={'grey'}
            />
          </View>
        </View>

      </View>

    )
  }

  _renderAdultOrChildView({ item, index, section }) {
    console.log("sectionLisf item " + JSON.stringify(item))
    return (
      <View style={{ margin: 10 }}>
        {item.type === "Adult" ? this._renderAdultView(item, index, section) : this._renderChildView(item, index, section)}
      </View>
    );
  }


  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ backgroundColor: colors.colorBlue }} />
        <StatusBar barStyle='light-content' />
        <CommonHeader title="Guest Details" />
        <Spinner
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={{ color: "white" }}
        />
        <KeyboardAwareScrollView
          style={{ marginBottom: 20, padding: 10 }}>
          <SectionList
            renderItem={this._renderAdultOrChildView}
            renderSectionHeader={({ section: { key } }) => (
              <View style={{ height: 50, backgroundColor: colors.colorBlue, margin: 10, marginTop: 10, marginBottom: 5, justifyContent: "center", borderRadius: 10 }}>
                <Text style={{ color: "white", marginLeft: 10, fontWeight: "700", fontFamily: Fonts.regular, fontSize: 15 }}>{key}</Text>
              </View>

            )}
            sections={this.state.sectionData}
            keyExtractor={(item, index) => item + index}
          />

          <ActionSheet
            ref={o => (this.adultTitleActionSheet = o)}
            title={"Select Title"}
            options={this.adultTitlesArr}
            cancelButtonIndex={3}
            destructiveButtonIndex={1}
            onPress={index => {
              if (index != 3) {

                let title = this.adultTitlesArr[index];
                let data = [...this.state.sectionData]

                console.log("in adult title")
                console.log(JSON.stringify(data) + " section item " + sectionItemIndex + " listitem" + listItemIndex)
                data[sectionItemIndex].data[listItemIndex].title = title
                console.log(JSON.stringify(data[sectionItemIndex].data[listItemIndex].title) + " section item " + sectionItemIndex + " listitem" + listItemIndex)
                this.forceUpdate()

              }
            }}
          />

          <ActionSheet
            ref={k => (this.childTitleActionSheet = k)}
            title={"Select Title"}
            options={this.childTitlesArr}
            cancelButtonIndex={2}
            //   destructiveButtonIndex={1}
            onPress={index => {
              if (index != 2) {

                let title = this.childTitlesArr[index];
                let data = [...this.state.sectionData];
                console.log("section data " + JSON.stringify(data[sectionItemIndex].data[listItemIndex].title))
                data[sectionItemIndex].data[listItemIndex].title = title
                console.log("section data " + JSON.stringify(data[sectionItemIndex].data[listItemIndex].title))
                //  this.setState({
                //   sectionData:data
                //  })

                this.forceUpdate();



              }
            }}
          />


          {this._renderContactDetailsView()}
        </KeyboardAwareScrollView>
        <TouchableOpacity
          style={styles.continueButtonStyle}
          underlayColor={"transparent"}
          onPress={() => {
            this.validateEntryFields()
          }}>
          <Text
            style={{ color: 'white' }}>
            Continue
                     </Text>
        </TouchableOpacity>
        {/* {this._renderAdultOrChildView()} */}
        <SafeAreaView />
      </View>
    )
  }
}
const mapStateToProps = state => {
  return {
    hotelSearchParams: state.HotelListreducer.hotelSearchParam
  }
}

const mapDispatchToProps = dispatch => {
  return {
    savePassengerData: (data) => dispatch()
  }
}

export default connect(mapStateToProps, {})(GuestDetails)
