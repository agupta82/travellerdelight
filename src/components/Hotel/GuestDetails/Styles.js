import {StyleSheet,Dimensions} from "react-native";
import { colors } from "../../../Helper Classes/Colors";

const screenSize = Dimensions.get("window");

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "rgba(244,248,254,1)",
        height: screenSize.height
    },
    styleAdultOrChildViewContainer: {
        width: "100%",
        padding: 15,
        shadowColor: "gray",
        shadowOpacity: 0.5,
        shadowOffset: { width: 1, height: 1 },
        shadowRadius: 5,
        backgroundColor: "white",
        borderRadius: 10,
        marginBottom: 5
    },
    
    styleTitleViewContainer: {
        height: 30,
        width: "100%",
        justifyContent: "center",
        marginLeft: 5
    },
 
    styleBaggageDropView: {
        height: "100%",
        width: "100%",
        borderRadius: 5,
        borderColor: "gray",
        borderWidth: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingLeft: 5,
        paddingRight: 5
    },
  
    addButtonStyle:{
        height: 40, width: 80, 
        backgroundColor: colors.colorBlue,
        justifyContent: 'center',
        alignItems: 'center', 
        alignSelf: 'flex-end', 
        marginRight: 20, 
        borderRadius: 7
    },
    continueButtonStyle:{
        height: 50, width: "95%", 
        backgroundColor: 'rgb(0,104,208)',
        alignSelf: 'center', 
        justifyContent: 'center',
        alignItems: 'center', 
        borderRadius: 7,
        bottom:15
    },
    
});

