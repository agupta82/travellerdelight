import React, { Component } from "react";
import {
  View,
  Image,
  TextInput,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  SafeAreaView,
  DeviceEventEmitter,
  Platform,
  Modal,
  StatusBar
} from "react-native";
import { colors } from "../../../Helper Classes/Colors";
import NavigationServices from '../../../Helper Classes/NavigationServices'
import { TextField } from "react-native-material-textfield";
import Images from "../../../Helper Classes/Images";
import CardView from 'react-native-cardview'
import TextInputDropDown from "../../../Helper Classes/TextInputDropDown";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import SelectTraveller from "../../Flight/SelectTraveller";
import { CalendarList } from "react-native-calendars";
import GuestCountModel from "../GuestCountModel/GuestCountModel";
import { styles } from "./Style";
import { Button } from "../../../Helper Classes/Button";
import { connect } from "react-redux";
import { saveHotelSearchParams } from "../../../Redux/Actions/Actions";
import moment from "moment";
import Fonts from "../../../Helper Classes/Fonts";
import SubmitButton from '../../../Helper Classes/SubmitButton'
//var ViewPager = require('react-native-viewpager');

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;


class HotelSearch extends Component {
  constructor(props) {
    super(props);

    this.state = {
      position: 1,
      data: [],
      cityDict: {
        id: 16373,
        country: "India",
        name: "New Delhi",
        country_code: "IN"
      },
      openedCalendar: false,
      todayDate: new Date(),
      interval: null,
      isForHotel: true,


      selectTravellersModel: false,
      numberOfGuest: 1,
      numberOfRooms: 1,
      numberOfAdults: 1,
      numberOfChilds: 0,
      checkIndate: new Date(),
      isWorkSelected: false,

      checkOutDate: moment().add(1, 'days').format(),
      nightDifference: 1,
      CalendarModal: false,
      // roomdata contains data of room no and guest count
      roomData: [
        {
          adult: 1,
          children: 0,
          childrenage: null
        }
      ],
      showGuestCountModel: false,
      selectedIndex: 0,
    }


    this.modalCloseHandler = this.modalCloseHandler.bind(this);
    this.dateSelectedHandler = this.dateSelectedHandler.bind(this);
    this.dataSource = [
      {
        url: "http://static.asiawebdirect.com/m/bangkok/portals/vietnam/homepage/da-nang/top10/top10-da-nang-hotels/pagePropertiesImage/danang-best-hotels.jpg.jpg",
        title: "Destination",
        caption: "Singapore"
      },
      {
        url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQP0y1dmHKLgUAdmvz77d3qDnS8IbHhmB_vqyL0nl-CgWCKtGMOA",
        title: "Destination",
        caption: "Malaysia"
      },
      {
        url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS04UXOiL_W23z2fe_yb0IPWEYwRC9GXy_-vQX3QvXlcAMDuQx-Yg",
        title: "Destination",
        caption: "Hong Kong"
      }
    ]
  }


  //------------------ Component Life cycle methods -----------

  componentDidMount() {

  }




  dateSelectedHandler = date => {

    if (this.state.selectedIndex === 0) {
      let checkOutDate = moment(date.dateString).add(1, 'days');
      this.setState({
        checkIndate: date.dateString,
        checkOutDate: checkOutDate,
        nightDifference: 1
      })

    } else {

      let diff = moment(date.dateString).diff(this.state.checkIndate, 'days');

      this.setState({
        checkOutDate: date.dateString,
        nightDifference: diff
      });
    }

  }

  citySelectedHandler = cityDict => {
    this.setState({
      cityDict: cityDict
    })
  }



  modalCloseHandler(arr, totalRoom, totalGuest) {
    // alert("Modal handler")

    console.log("in modal close handler" + JSON.stringify(arr) + " ", totalRoom + " ", totalGuest);
    if (arr != null && totalRoom != null && totalGuest != null) {
      console.log("setting state")
      this.setState({
        roomData: arr,
        numberOfGuest: totalGuest,
        numberOfRooms: totalRoom
      })
    }
    this.setModalVisible(false);
  }

  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position:
            this.state.position === this.dataSource.length - 1
              ? 0
              : this.state.position + 1
        });
      }, 5000)
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.interval);
  }



  getDate(date) {
    //return dateFormat(date, "ddd,dd mmm'yy");
    return moment(date).format("DD MMM YY");
  }

  // returnTotalNumbersOfGuest() {
  //   let numAdults = this.state.numberOfAdults;
  //   let numChild = this.state.numberOfChilds;
  //   let str = numAdults + " Adult";
  //   if (numChild > 0) {
  //     str = str + "," + numChild + " Child";
  //   }
  //   return str;
  // }

  setModalVisible(visible) {
    this.setState({ showGuestCountModel: visible });
  }



  changeWork(t) {
    if (this.state.isWorkSelected != t)
      this.setState({
        isWorkSelected: !this.state.isWorkSelected
      })
  }

  _renderCalenderPopUp() {

    let selectedDate = (this.state.selectedIndex == 0) ? this.state.checkIndate : this.state.checkOutDate
    let markingDates = {}

    markingDates[selectedDate] = { selected: true }

    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.CalendarModal}
          onRequestClose={() => { }}
        >
          <View style={styles.containerPopup}>
            <View style={styles.stylePopUpView}>
              <View style={styles.styleTitleView}>
                <Text
                  style={{ color: "black", fontSize: 18, fontWeight: "600" }}
                >
                  Calender
                </Text>
              </View>
              <View style={{ height: 300, margin: 20 }}>
                <CalendarList
                  calendarWidth={"100%"}
                  pastScrollRange={0}
                  minDate={this.state.selectedIndex === 0 ? new Date() : this.state.checkIndate}
                  onDayPress={this.dateSelectedHandler}

                  //markedDates = {this.state.selectedIndex === 0 ? this.state.checkIndate : this.state.checkOutDate}
                  markedDates={markingDates}
                  // markingType='period'
                  theme={{
                    backgroundColor: "white",
                    calendarBackground: "white",
                    todayTextColor: colors.colorBlue,
                    selectedDayBackgroundColor: colors.colorBlue
                  }}
                />
              </View>

              <View style={styles.styleBottomBtnsView}>
                {/* <TouchableOpacity
                  style={[styles.styleCancelOrDoneBtnView, { marginRight: 10 }]}
                  underlayColor="transparent"
                  onPress={() => {
                    this.setState({
                      CalendarModal: false,

                    })
                  }}
                >
                  <Text style={{ color: "rgba(25,89,189,1)", fontSize: 14 }}>
                    Done
                  </Text>
                </TouchableOpacity> */}

                <TouchableOpacity
                  style={styles.styleCancelOrDoneBtnView}
                  underlayColor="transparent"
                  onPress={() => {
                    this.setState({
                      CalendarModal: false
                    })
                  }}
                >
                  <Text style={{ color: "gray", fontSize: 14 }}>Close</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
  closeCalendarModal = () => {
    this.setState({ CalendarModal: false });
  }

  SearchHotels() {

    let dict = {
      name: this.state.cityDict.name,
      country: this.state.cityDict.country,
      id: this.state.cityDict.id,
      checkIn: this.state.checkIndate,
      checkOut: this.state.checkOutDate,
      roomData: this.state.roomData,
      numberOfGuest: this.state.numberOfGuest,
      numberOfRooms: this.state.numberOfRooms
    }
    //alert(JSON.stringify(dict))
    console.log("SEARCH:" + JSON.stringify(dict))
    this.props.saveParams(dict)

    // Actions.HotelList({jsonDict:dict});
    NavigationServices.navigate('HotelList');
  }
  _renderGuestCountModel() {
    return (
      <Modal
        animationType="slide"
        transparent={true}

        visible={this.state.showGuestCountModel}>
        <GuestCountModel
          roomData={this.state.roomData}
          roomCount={this.state.numberOfRooms}
          guestCount={this.state.numberOfGuest}
          onModalClose={(arr, totalRoom, totalGuest) => this.modalCloseHandler(arr, totalRoom, totalGuest)} />
      </Modal>
    )
  }

  _renderBackgroundImage() {
    return (
      <Image source={Images.imghomebg}
        style={{
          height: screenHeight / 2.5,
          width: screenWidth,
          resizeMode: "stretch"
        }}></Image >
    )
  }

  _renderHeader() {
    return (<View style={styles.header}>
      <TouchableOpacity style={styles.headerBackButton} onPress={() => NavigationServices.goBack()}>
        <Image source={Images.imgback} style={{ width: 15, height: 15, resizeMode: "contain" }}></Image>
        <Text style={{
          color: colors.colorWhite,
          fontFamily: Fonts.semiBold
        }}> Back</Text>
      </TouchableOpacity>
      <Text style={styles.headerTitle}>Search Hotel</Text>
      <TouchableOpacity style={styles.headerButton}>
        <Image source={Images.imgnotifications} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
      </TouchableOpacity>
    </View>)
  }

  render() {
    let { cityDict, CalendarModal, isForHotel } = this.state
    return (
      <View style={styles.container}>
        {this._renderCalenderPopUp()}
        {this._renderGuestCountModel()}
        {this._renderBackgroundImage()}
        <View style={styles.mainContainer}>
          {this._renderHeader()}
          <KeyboardAwareScrollView contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false} style={{ width: '100%', padding: 20 }}>
            <CardView style={{
              paddingBottom: 50,
              padding: 20,
              width: '100%',
              backgroundColor: 'white'
            }}
              cardElevation={3}
              cardMaxElevation={3}
              cornerRadius={10}>
              <TouchableOpacity onPress={() => { NavigationServices.navigate('SearchAirport', { forFlightSearch: false, onCitySelect: this.citySelectedHandler }) }}>
                <View style={styles.cityNameRow}>
                  <View style={styles.cityWrapper}>
                    <Text style={styles.cityHeading}>City/Area/Hotel Name</Text>
                  </View>
                  <View style={styles.cityWrapper}>
                    <Text style={styles.cityValue}>{cityDict.name}</Text>
                  </View>
                  <View style={styles.cityWrapper}>
                    <Text style={styles.cityHeading}>{cityDict.country}</Text>
                  </View>
                </View></TouchableOpacity>
              <View style={styles.border}></View>
              <View style={styles.cardRow}>
                <TouchableOpacity style={styles.cardBtn} onPress={() => {
                  this.setState({
                    selectedIndex: 0,
                    CalendarModal: true
                  })
                }}>
                  <Text style={styles.cityHeading}>Check In</Text>
                  <View style={styles.cityWrapper}>
                    <Text style={styles.cityValue}>{this.getDate(this.state.checkIndate)}</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconButton} >
                  <Image source={Images.imgCalender}
                    style={styles.icon}></Image>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                  this.setState({
                    selectedIndex: 1,
                    CalendarModal: true
                  })
                }} style={styles.cardBtn}>
                  <Text style={styles.cityHeading}>Check Out</Text>
                  <View style={styles.cityWrapper}>
                    <Text style={styles.cityValue}>{this.getDate(this.state.checkOutDate)}</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.border}></View>
              <View style={styles.cardRow}>
                <TouchableOpacity style={styles.cardBtn} onPress={() => {
                  this.setModalVisible(true, true);
                }}>
                  <Text style={styles.cityHeading}>Guests</Text>
                  <View style={styles.cityWrapper}>
                    <Text style={styles.cityValue}>{this.state.numberOfGuest}</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconButton} >
                  <Image source={Images.imguser}
                    style={styles.icon}></Image>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                  this.setModalVisible(true, true);
                }} style={[styles.cardBtn, {}]}>
                  <Text style={styles.cityHeading}>Room</Text>
                  <View style={styles.cityWrapper}>
                    <Text style={styles.cityValue}>{this.state.numberOfRooms}</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.border}></View>

              <View style={{}}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ color: colors.colorBlack, fontSize: 13, fontFamily: Fonts.semiBold }}>Travel Purpose</Text>
                  <TouchableOpacity onPress={() => this.changeWork(false)}>
                    <View style={{ borderRadius: 5, backgroundColor: !this.state.isWorkSelected ? colors.colorBlue : colors.lightBlue, alignItems: 'center', justifyContent: 'center', paddingVertical: 7, marginLeft: 20, width: 70 }}>
                      <Text style={{ color: !this.state.isWorkSelected ? colors.colorWhite : colors.colorBlack, fontSize: 11, fontFamily: Fonts.semiBold }}>LEISURE</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.changeWork(true)}>
                    <View style={{ borderRadius: 5, backgroundColor: this.state.isWorkSelected ? colors.colorBlue : colors.lightBlue, alignItems: 'center', justifyContent: 'center', paddingVertical: 7, marginLeft: 10, width: 70 }}>
                      <Text style={{ color: this.state.isWorkSelected ? colors.colorWhite : colors.colorBlack, fontSize: 11, fontFamily: Fonts.semiBold }}>Work</Text>
                    </View>
                  </TouchableOpacity>
                </View>

              </View>

            </CardView>
            <SubmitButton marginTop={-25} onPress={() => { this.SearchHotels() }} />
          </KeyboardAwareScrollView>
          {/* <View style={{ margin: 10 }}>
          <Button
            height={47}
            onPress={
              () => {
                let dict = {
                  name: this.state.cityDict.name,
                  country: this.state.cityDict.country,
                  id: this.state.cityDict.id,
                  checkIn: this.state.checkIndate,
                  checkOut: this.state.checkOutDate,
                  roomData: this.state.roomData,
                  numberOfGuest: this.state.numberOfGuest,
                  numberOfRooms: this.state.numberOfRooms
                }

                this.props.saveParams(dict)

                // Actions.HotelList({jsonDict:dict});
                NavigationServices.navigate('HotelList');
              }}
            text="Search Hotels"
          />
            </View>*/}
        </View >
      </View >
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
    userData: state.userDataReducer,
    isLogin: state.isLoginReducer,
    userToken: state.userTokenReducer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveParams: (params) => dispatch(saveHotelSearchParams(params))
  }
}

export default connect(null, mapDispatchToProps)(HotelSearch)
