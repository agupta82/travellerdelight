import { Platform, StyleSheet, Dimensions } from "react-native";
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import { colors } from "../../../Helper Classes/Colors";
import Fonts from "../../../Helper Classes/Fonts";

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FDFDFD"
  },
  mainContainer: {
    position: "absolute",
    top: Platform.OS == "ios" ? (getStatusBarHeight() + 5) : 0,
    width: screenWidth,
    height: screenHeight,
    flex: 1,
    left: 0,
    alignItems: "center",
    // backgroundColor: "red"
  },
  headerBackButton: {
    width: 70,
    // backgroundColor: "red",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  containerStyle: {
    borderRadius: 20
  },
  innercontainer: {
    margin: 10
  },
  styleTextFieldsSuperView: {
    width: "100%",
    padding: 20,
    elevation: 4,
    backgroundColor: "white",
    marginTop: 15,
    borderRadius: 10,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 5
  },
  textInputTitle: {
    fontFamily: "Montserrat-SemiBold",
    color: "grey",
    fontSize: 11
  },
  imageSlider: {
    borderRadius: 8
  },
  styleDestinationView: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: "black"
  },
  styleTextStyleOfValues: {
    color: colors.colorBlack,
    fontSize: 14,
    fontFamily: "Montserrat-SemiBold",
    marginTop: 10
  },
  stylePopUpView: {
    width: "80%",
    backgroundColor: "white",
    borderRadius: 15
  },
  styleTitleView: {
    margin: 15,
    height: 20,
    justifyContent: "center"
  },
  styleBottomBtnsView: {
    flexDirection: "row",
    justifyContent: "flex-end",
    margin: 15,
    height: 35,
    paddingRight: 10
  },
  styleCancelOrDoneBtnView: {
    height: "100%",
    width: 60,
    justifyContent: "center",
    alignItems: "center"
  },
  containerPopup: {
    backgroundColor: "rgba(0,0,0,0.5)",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  header: {
    height: 45,
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  headerButton: {
    height: "100%",
    width: 50,
    // backgroundColor: "blue",
    justifyContent: "center",
    alignItems: "center"
  },
  headerTitle: {
    color: colors.colorWhite,
    fontSize: 16,
    // fontWeight: "600"
    fontFamily: Fonts.bold
  },
  cardText: {
    fontFamily: Fonts.medium,
    fontSize: 14,
    color: colors.colorBlack,
    lineHeight: 25
  },
  cardRow: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  cardBtn: {
    height: "100%",
    // backgroundColor: "red"
  },
  icon: {
    width: 35, height: 35,
    resizeMode: "contain"
  },
  iconButton: {
    width: 50,
    height: "100%",
    paddingTop: 5,
    alignItems: "center",
  }, border: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgrey",
    marginVertical: 15
  },
  cityNameRow: {

  },
  cityHeading: {
    color: colors.colorBlack,
    fontSize: 11,
    fontFamily: Fonts.regular
  },
  cityValue: {
    fontFamily: Fonts.bold,
    fontSize: 16,
    color: colors.colorBlue
  },
  cityWrapper: {
    paddingTop: 4
  }
});