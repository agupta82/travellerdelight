import { StyleSheet } from "react-native";
import { colors } from "../../../Helper Classes/Colors";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(244,248,254,1)",
  },
  itemContainer: {
    padding: 15,
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    shadowRadius: 3,
    elevation: 6,
  }, imageContainer: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center'
  }, confirmButtonStyle: {
    height: 50,
    width: "90%",
    backgroundColor: colors.colorBlue,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
    bottom: 15
  }, bookingIdContainer: {
    padding: 15,
    margin: 10,
    backgroundColor: colors.colorBlue,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    shadowRadius: 3,
    elevation: 6,
  }

});