import React, { Component } from "react";
import { View, Image, FlatList, ScrollView, TextInput, Text, StyleSheet, TouchableOpacity, Modal, StatusBar, DeviceEventEmitter, SafeAreaView } from 'react-native';
import NavigationBar from '../../../Helper Classes/NavigationBar'
import Images from '../../../Helper Classes/Images';
import RatinBar from "../../../Helper Classes/RatingBar";
import Dash from "react-native-dash"
import Spinner from "react-native-loading-spinner-overlay"
import NavigationServices from './../../../Helper Classes/NavigationServices'
import APIManager from "../../../Helper Classes/APIManager/APIManagerUpdated";
import StringConstants from "../../../Helper Classes/StringConstants";
import { styles } from "./Style";
import CardView from 'react-native-cardview'
import { connect } from "react-redux"
import { getHotelBookingDetailsAction } from "../../../Redux/Actions";
import moment from "moment";
import { colors } from "../../../Helper Classes/Colors";
import CommonHeader from "../../../Helper Classes/CommonHeader";
import { commonstyle } from "../../../Helper Classes/Commonstyle";




class HotelBookingDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      spinner: false,
      BookingId: 75,
      pdfLink: "",
      BookingDetails: null
    }

    this.updateHotelBookingDetailsFromApi = this.updateHotelBookingDetailsFromApi.bind(this)
  }
  getBookingData() {
    this.props.getHotelBookingDetailsAction(this.state.BookingId)

    // this.setState({
    //     spinner: true
    // })

    // APIManager.getHotelBookingData(this.state.BookingId, (success, response) => {
    //     this.setState({
    //         spinner: false
    //     })
    //     console.log("Hotel Booking Details " + JSON.stringify(response));
    //     if (success) {
    //         if (response.code.code == 200) {
    //             this.setState({
    //                 pdfLink: response.data.pdfPath,
    //                 BookingDetails: response.data.booking
    //             })
    //         } else if (response.code.code == 500) {
    //             setTimeout(() => {
    //                 alert(JSON.stringify(StringConstants.session_expired))
    //             }, 100)
    //         }

    //     } else {
    //         setTimeout(() => {
    //             alert("Error " + JSON.stringify(JSON.stringify(response)))
    //         }, 100)
    //     }
    // })
  }

  componentDidMount() {
    //   DeviceEventEmitter.addListener(StringConstants.HOTEL_BOOKING_DETAILS_EVENT, this.updateHotelBookingDetailsFromApi)
    //this.getBookingData()
    const { navigation } = this.props;
    const BookingDetails = navigation.getParam('BookingDetails', null);

    this.setState({
      BookingDetails: BookingDetails
    })
  }

  componentWillUnmount() {
    //   DeviceEventEmitter.removeListener(StringConstants.HOTEL_BOOKING_DETAILS_EVENT, this.updateHotelBookingDetailsFromApi)

  }

  updateHotelBookingDetailsFromApi(response) {
    if (response && response.code && response.code.code && response.code.code == 200) {
      this.setState({
        pdfLink: response.data.pdfPath,
        BookingDetails: response.data.booking
      })
    } else if (response && response.code && response.code.code && response.code.code == 500) {
      setTimeout(() => {
        alert(JSON.stringify(StringConstants.session_expired))
      }, 100)
    }
  }

  _renderTarrifDetail() {
    let otherCharge = this.state.BookingDetails.total_price;

    return (

      <View style={[styles.itemContainer, { padding: 0 }]}>
        <View style={{ padding: 15 }}>
          <Text style={{ fontSize: 15, fontWeight: '500', color: 'rgb(76,76,76' }}>
            Tarrif Details
            </Text>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
            <Text style={{ color: colors.colorBlue }}>
              Base Fare
                </Text>
            <Text style={{ color: colors.colorBlue }}>
              {this.state.roomDetails.baseFare}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
            <Text style={{ color: colors.colorBlue }}>
              Tax
                </Text>
            <Text style={{ color: colors.colorBlue }}>
              {this.state.roomDetails.tax}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
            <Text style={{ color: colors.colorBlue }}>
              Convenience Fees & Taxes
                </Text>
            <Text style={{ color: colors.colorBlue }}>
              {otherCharge}
            </Text>
          </View>
        </View>


        <Dash
          style={{
            width: "98.7%",
            flexDirection: "row",
            alignSelf: 'center'
          }}
          dashColor="gray"
          dashLength={7.5}
        />

        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 15 }}>
          <Text >
            You Pay
                </Text>
          <Text >
            {this.state.roomDetails.agencyFare}
          </Text>
        </View>

      </View>
    )
  }

  // cancellationPolicyItem() {
  //   return (
  //     <TouchableOpacity
  //       underlayColor="transparent"
  //       onPress={() => {
  //         var Policy = this.state.BookingDetails.messages[0].message
  //         alert(Policy)
  //       }}>
  //       <View style={[styles.itemContainer, { flexDirection: 'row', justifyContent: 'space-between' }]}>
  //         <View>
  //           <Text style={{ color: colors.colorBlue, fontSize: 15, fontWeight: '500' }}>
  //             Cancellation Policy
  //                  </Text>
  //           <Text style={{ marginTop: 5 }}>
  //             Rules and Policy
  //                  </Text>
  //         </View>
  //         <Image
  //           style={{ height: 15, width: 10, alignSelf: 'flex-start' }}
  //           source={Images.imgrightarrow}
  //         />
  //       </View>
  //     </TouchableOpacity>
  //   )
  // }

  _renderConfirmButton() {
    return (
      <TouchableOpacity
        style={styles.confirmButtonStyle}
        underlayColor={"transparent"}
        onPress={() => {
          // this.validateEntryFields()
        }}>
        <Text
          style={{ color: 'white' }}>
          Continue
                     </Text>
      </TouchableOpacity>
    )
  }

  _renderCheckInItem() {
    //  const checkIn = dateFormat(this.state.BookingDetails.check_in, "ddd, dd mmm yyyy")
    //  const checkOut = dateFormat(this.state.BookingDetails.check_out, "ddd, dd mmm yyyy")
    const checkIn = moment(this.state.BookingDetails.check_in).format("ddd, DD MMM YYYY")
    const checkOut = moment(this.state.BookingDetails.check_out).format("ddd, DD MMM YYYY")
    return (
      <View style={[styles.itemContainer, { backgroundColor: colors.colorBlue }, { flexDirection: 'row' }]}>
        <View style={{ width: "50%", padding: 5 }}>
          <Text style={{ color: 'white', fontSize: 13 }}>
            CHECK-IN
                </Text>
          <Text style={{ color: 'white', fontWeight: '600', marginTop: 10 }}>
            {checkIn}
          </Text>
        </View>
        <View style={{ width: 1, height: 42, alignSelf: 'center', backgroundColor: 'white' }}>
          {/* {this.state.hotelSearchParam.checkOut} */}
        </View>
        <View style={{ width: "50%", padding: 5, alignItems: 'flex-end' }}>
          <Text style={{ color: 'white', fontSize: 13 }}>
            CHECK-OUT
                </Text>
          <Text style={{ color: 'white', fontWeight: "500", marginTop: 10 }}>
            {checkOut}
          </Text>
        </View>
      </View>
    )
  }
  _renderRoomItem() {
    return (
      <View style={{
        paddingHorizontal: 12,
      }}>
        <CardView style={{
          //overflow: 'hidden',
          width: '100%',
          marginTop: 20,
          backgroundColor: colors.colorWhite,
        }}
          cardElevation={2}
          cardMaxElevation={3}
          cornerRadius={10}>
          <View style={{ width: '100%', overflow: 'hidden', borderRadius: 10, }}>
            <View style={{ width: '100%', backgroundColor: '#e6e6e6', height: 45, flexDirection: 'row', padding: 10 }}>
              <View style={{ width: '50%', alignItems: 'center', flexDirection: 'row', height: '100%' }}>
                <View style={{ width: 25, height: 25, borderRadius: 15, backgroundColor: colors.colorBlue, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.room_service_blue} style={{ width: 15, height: 15, tintColor: colors.colorWhite, resizeMode: 'center' }} />
                </View>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, paddingLeft: 7 }}>
                  Rooms
              </Text>

              </View>
            </View>
            <View style={{ width: '100%', paddingVertical: 15, paddingHorizontal: 10 }}>
              {this.state.BookingDetails.guests.map((item, index) => {
                return (
                  <Text style={{ fontFamily: Fonts.regular, fontSize: 12, color: colors.colorBlack, paddingBottom: 5 }}>
                    {this.state.BookingDetails.room} Room   {Object.keys(this.state.BookingDetails.guests).length} Guest
                  </Text>
                )
              })}

            </View>

          </View></CardView>
      </View>
    )
  }

  _renderRoomItem2() {
    return (
      <View style={styles.itemContainer}>
        <Text style={{ fontSize: 15, fontWeight: '600', color: 'rgb(76,76,76)' }}>
          {/* {this.state.BookingDetails.name} */}
          {/* {room name required from api end} */}
          Room name
            </Text>

        {/* {Object.keys(this.state.roomDetails.amenities).length>0 ? <Text style={{color:'rgb(76,76,76)',fontSize:12,marginTop:10}}>{this.state.roomDetails.amenities[0]}</Text> : <Text>{""}</Text>} */}

        <Text style={{ marginTop: 5, color: 'rgb(76,76,76)', fontWeight: '500' }}>
          {this.state.BookingDetails.room} Room   {Object.keys(this.state.BookingDetails.guests).length} Guest
            </Text>
      </View>
    )
  }
  _renderHotelItem() {
    return (
      <View style={styles.itemContainer}>
        <View style={{ flexDirection: 'row' }}>
          <View style={styles.imageContainer}>
            <Image
              style={{
                height: 80,
                width: 80,
                borderRadius: 30
              }}
              source={Images.hotelRoomImage}
              resizeMode="contain"
            />
          </View>
          <View style={{ flex: 1, marginLeft: 15 }}>

            <Text style={{ fontSize: 15, fontWeight: '600', color: colors.colorBlue }}>
              {this.state.BookingDetails.hotel_name}
            </Text>
            <View style={{ marginTop: 5 }}>
              <Text style={{ color: 'rgb(76,76,76)', fontSize: 12 }}>
                {/* {this.state.hotelDetails.address} */}
              </Text>
              <View style={{ width: 90, marginTop: 10 }}>
                <RatinBar
                //   count={this.state.hotelDetails.star}
                />
              </View>

            </View>
          </View>
        </View>
      </View>
    )
  }
  _renderBookingItem() {
    return (
      <View style={styles.bookingIdContainer}>
        <Text style={{ color: "white", fontWeight: "600" }}>
          BOOKING ID
                </Text>
        <Text style={{ color: "white", marginTop: 5 }}>
          {this.state.BookingDetails.booking_id}
        </Text>
      </View>
    )
  }

  _renderContactDetails() {

    return (
      <View style={{
        paddingHorizontal: 12,
      }}>
        <CardView style={{
          //overflow: 'hidden',
          width: '100%',
          marginTop: 20,
          backgroundColor: colors.colorWhite,
        }}
          cardElevation={2}
          cardMaxElevation={3}
          cornerRadius={10}>
          <View style={{ width: '100%', overflow: 'hidden', borderRadius: 10, }}>
            <View style={{ width: '100%', backgroundColor: '#e6e6e6', height: 45, flexDirection: 'row', padding: 10 }}>
              <View style={{ width: '50%', alignItems: 'center', flexDirection: 'row', height: '100%' }}>
                <View style={{ width: 25, height: 25, borderRadius: 15, backgroundColor: colors.colorBlue, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.img_primary_contact} style={{ width: 25, height: 25, resizeMode: 'cover' }} />
                </View>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, paddingLeft: 7 }}>
                  Primary Contact
              </Text>

              </View>
            </View>
            <View style={{ width: '100%', paddingVertical: 15, paddingHorizontal: 10 }}>
              <View style={styles.cardCOntent}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ fontFamily: Fonts.medium, fontSize: 11, color: colors.colorBlack, }}>Email  :-  </Text>
                  <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: colors.colorBlack, }}>{this.state.BookingDetails.email}</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                  <Text style={{ fontFamily: Fonts.medium, fontSize: 11, color: colors.colorBlack }}>Phone  :-  </Text>
                  <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: colors.colorBlack }}>{this.state.BookingDetails.mobile}</Text>
                </View>
              </View>

            </View>

          </View></CardView>

      </View>)
  }

  renderGuestDetail() {
    return (
      <View style={{
        paddingHorizontal: 12,
      }}>
        <CardView style={{
          //overflow: 'hidden',
          width: '100%',
          marginTop: 20,
          backgroundColor: colors.colorWhite,
        }}
          cardElevation={2}
          cardMaxElevation={3}
          cornerRadius={10}>
          <View style={{ width: '100%', overflow: 'hidden', borderRadius: 10, }}>
            <View style={{ width: '100%', backgroundColor: '#e6e6e6', height: 45, flexDirection: 'row', padding: 10 }}>
              <View style={{ width: '50%', alignItems: 'center', flexDirection: 'row', height: '100%' }}>
                <View style={{ width: 25, height: 25, borderRadius: 15, backgroundColor: colors.colorBlue, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.imguserprofile} style={{ width: 15, height: 15, resizeMode: 'center' }} />
                </View>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, paddingLeft: 7 }}>
                  Guests Detail
              </Text>

              </View>
            </View>
            <View style={{ width: '100%', paddingVertical: 15, paddingHorizontal: 10 }}>
              {this.state.BookingDetails.guests.map((item, index) => {
                return (
                  <Text style={{ fontFamily: Fonts.regular, fontSize: 12, color: colors.colorBlack, paddingBottom: 5 }}>
                    {(item.title + " " + item.first_name) + "" + (item.middle_name ? (" " + item.middle_name) : "") + " " + item.last_name}
                  </Text>
                )
              })}

            </View>

          </View></CardView>
      </View>
    )
  }

  cancellationPolicyItem() {

    return (
      <View style={{
        paddingHorizontal: 12,
      }}>
        <CardView style={{
          //overflow: 'hidden',
          width: '100%',
          marginTop: 20,
          backgroundColor: colors.colorWhite,
        }}
          cardElevation={2}
          cardMaxElevation={3}
          cornerRadius={10}>
          <TouchableOpacity
            onPress={() => {
              var Policy = this.state.BookingDetails.messages[0].message
              alert(Policy)
            }} style={{ width: '100%', overflow: 'hidden', borderRadius: 10, }}>
            <View style={{ width: '100%', height: 45, flexDirection: 'row', padding: 10 }}>
              <View style={{ width: '50%', alignItems: 'center', flexDirection: 'row', height: '100%' }}>
                <View style={{ width: 25, height: 25, borderRadius: 15, backgroundColor: colors.colorBlue, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.imgcancellation} style={{ width: 25, height: 25, resizeMode: 'cover' }} />
                </View>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, paddingLeft: 7 }}>
                  Cancellation Policy
              </Text>

              </View>
              <View style={{ width: '50%', alignItems: 'flex-end', justifyContent: 'center' }}>

                <Text style={{ fontFamily: Fonts.bold, fontSize: 15, color: colors.colorBlack, }}>
                  >
              </Text>

              </View>
            </View>
          </TouchableOpacity>
        </CardView>
      </View>
    )
  }


  render() {
    if (!this.state.BookingDetails) return null
    return (
      <View style={styles.container}>
        <SafeAreaView backgroundColor={colors.colorBlue} />
        <CommonHeader title={"Booking Details"} />
        <Spinner
          visible={this.props.isLoading}
          textContent={"Loading..."}
          textStyle={{ color: "white" }}
        />
        <ScrollView >
          <View style={{ padding: 10, paddingBottom: 40 }}>
            {this._renderHotelItem()}
            {this._renderBookingItem()}
            {this._renderCheckInItem()}
            {this._renderRoomItem()}
            {this.renderGuestDetail()}
            {this._renderContactDetails()}
            {this.cancellationPolicyItem()}

            {/* {this._renderTarrifDetail()} */}
          </View>
        </ScrollView>
        {/* {this._renderConfirmButton()} */}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getHotelBookingDetailsAction: (data) => dispatch(getHotelBookingDetailsAction(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HotelBookingDetails)