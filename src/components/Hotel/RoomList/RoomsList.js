import React, { Component } from "react";
import { View, Image, FlatList, ScrollView, TextInput, Text, StyleSheet, SafeAreaView, TouchableOpacity, Modal, StatusBar } from 'react-native';
import NavigationBar from '../../../Helper Classes/NavigationBar'
import Images from '../../../Helper Classes/Images';
import NavigationServices from './../../../Helper Classes/NavigationServices'
import { styles } from "./Style";
import { connect } from "react-redux";
import { saveSelectedRoomData } from "../../../Redux/Actions/Actions";
import { commonstyle } from "../../../Helper Classes/Commonstyle";
import { colors } from "../../../Helper Classes/Colors";

import CommonHeader from '../../../Helper Classes/CommonHeader'

let SOURCES = null
class RoomsList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hotelDetails: [],
      roomsData: null,
      loading: false,
      popUp: false,
      CancellationPolicy: ""
    }
    this._renderHotelRoomListItem = this._renderHotelRoomListItem.bind(this);
  }

  //--------------- Component Life Cycle Methods -----------

  componentWillMount() {

    // let roomData = this.props.hotelDetails.rooms.roomlist
    // let hotelDetails = this.props.hotelDetails.hotel
    let roomData = this.props.hotelData.rooms.roomlist;
    let hotelDetails = this.props.hotelData.hotel;
    this.setState({
      roomsData: roomData,
      hotelDetails: hotelDetails
    })
    console.log("room list");
    console.log(roomData);
    // console.log( "Room List hotel search param " + JSON.stringify(this.props.hotelSearchParam ))
  }

  componentDidMount() {
    let { navigation } = this.props;
    let traceId = navigation.getParam('traceId', null)
    let source = navigation.getParam('source', null)
    SOURCES = {
      source: source,
      sourceDetail: {
        trace: traceId
      }
    }
  }

  roomPoPup() {
    let CancellationPolicy = this.state.CancellationPolicy
    return (
      <Modal
        transparent={true}
        animationType={'slide'}
        visible={this.state.popUp}>
        <View style={styles.containerPopup}>
          <View style={styles.stylePopUpView}>
            <Text style={{ fontSize: 16, fontWeight: '600' }}>Cancellation Policy</Text>

            <Text style={{ marginTop: 20 }}>
              {String(CancellationPolicy)}
            </Text>
            <TouchableOpacity
              underlayColor={'transparent'}
              onPress={() => this.setState({
                popUp: false
              })}>
              <Text style={{ color: colors.colorBlue, fontWeight: '700', fontSize: 16, alignSelf: 'flex-end', marginTop: 10 }}>
                Got It
                    </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }

  // _renderHotelRoomListItem({item}){
  //     return(
  //         <View style={styles.listStyle}>
  //         <View style={styles.listcontent}>
  //             <View style={styles.imageContainer}>

  //                 <Image
  //                         style={{
  //                             height: 100,
  //                             borderRadius: 30
  //                         }}
  //                         source={Images.hotelRoomImage}
  //                         resizeMode="contain"
  //                     />
  //                     <Image
  //                         style={{
  //                             height: 15,
  //                             borderRadius: 30
  //                         }}
  //                         source={Images.cameraIcon}
  //                         resizeMode="contain"
  //                     />
  //                     <Text style={{fontSize:12}} >
  //                     1/2
  //                     </Text>
  //             </View>
  //             <View style={{flex:1,marginLeft:10}}>

  //                 <Text style={{fontSize:16,fontWeight:'500',color:'black'}}>
  //                     {item.name}
  //                 </Text>
  //                 <Text style={{color:'grey',fontWeight:'600'}}>
  //                     Max Guest: 1 Adult,1 Children
  //                 </Text>
  //             <View style={{marginLeft:10,marginTop:7}}>
  //                 <Text>
  //                 <Image
  //                 style={{
  //                     height: 13,
  //                     width:13,
  //                     borderRadius: 30,
  //                        }}
  //                 source={Images.imgCheckMark}
  //                 resizeMode="contain"
  //                 />
  //                     {item.amenities[0]}
  //                 </Text>
  //                 <Text style={{color:"rgb(0, 125, 202)", fontSize:16}}>
  //                     View All
  //                 </Text>
  //                 <Text style={{color:"rgb(0, 125, 202)", fontSize:20}}>
  //                     {item.price}
  //                </Text>
  //                 <Text style={{color:"rgb(243, 63, 69)", fontSize:12}}>
  //                     1 Room Left
  //                 </Text>
  //             </View>    
  //             </View>
  //         </View>

  //         <View style={styles.buttonContainer}>
  //             <TouchableOpacity
  //                 style={styles.roomOverview}
  //                 underlayColor="transparent"
  //                 onPress={() => {
  //                     Actions.HotelReview();
  //                 }}
  //             >
  //                 <Text
  //                     style={{ fontSize: 13, fontWeight: "200", color: "black" }}
  //                 >
  //                     Room Overview
  //          </Text>
  //             </TouchableOpacity>
  //             <TouchableOpacity
  //                 style={styles.roomOverview}
  //                 underlayColor="transparent"
  //                 onPress={() => {
  //                     //Actions.SelectRoom();
  //                 }}
  //             >
  //                 <Text
  //                     style={{ fontSize: 13, fontWeight: "200", color: "black" }}
  //                 >
  //                     Non Refundable
  //                 </Text>
  //             </TouchableOpacity>
  //             <TouchableOpacity
  //                 style={styles.bookButton}
  //                 underlayColor="transparent"
  //                 onPress={() => {
  //                     //Actions.SelectRoom();
  //                 }}
  //             >
  //                 <Text
  //                     style={{ fontSize: 15, color: "white", fontWeight: "500" }}
  //                 >
  //                     Book Now
  //                 </Text>
  //             </TouchableOpacity>
  //         </View>
  //     </View>
  //     )
  // }

  _renderHotelRoomListItem(item, index) {
    return (
      <View style={[commonstyle.itemBackground, { marginLeft: 2, marginRight: 2 }]}>

        <View style={{ flexDirection: "row", justifyContent: "space-between", width: '100%' }}>
          <View style={{ width: "70%" }}>
            <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 15 }}>
              {item.RoomTypeName}
            </Text></View>
          <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 14, color: colors.colorBlue }}>
            {item.Price.CurrencyCode} {item.Price.AgencyFare}
          </Text>
        </View>
        <View style={{ marginTop: 15, maxHeight: 130 }}>
          <FlatList

            data={item.Amenity}
            numColumns={2}
            renderItem={item =>
              (<View style={{ flexDirection: "row", alignItems: "center", marginLeft: 7, marginRight: 7, flex: 1, marginTop: 5 }}>
                <Image
                  style={{ height: 10, width: 10, }}
                  source={Images.imgcircularcheck} />
                <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 10, margin: 5 }}>{item.item}</Text>
              </View>)
            }
          />
        </View>

        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.button}
            underlayColor="transparent"
            onPress={() => {
              //  Actions.GuestDetails({hotelDetails:this.state.hotelDetails,selectedRoomData:item,hotelSearchParam:this.props.hotelSearchParam})
              //Actions.SelectRoom();
              this.props.saveRoomdata(item);
              NavigationServices.navigate('GuestDetails', { roomIndex: index, sources: SOURCES });
            }}>
            <Text
              style={{ fontSize: 15, color: "white", fontFamily: "Montserrat-Medium" }}
            >
              Book Now
                    </Text>
          </TouchableOpacity>


          <TouchableOpacity
            style={[styles.button, { backgroundColor: "white", marginLeft: 10 }]}
            underlayColor="transparent"
            onPress={() => {
              this.setState({
                CancellationPolicy: item.CancellationPolicy,
                popUp: true
              })
            }}
          >

            <Text
              style={{ fontSize: 15, fontFamily: "Montserrat-Medium", color: "black" }}
            >
              Cancellation Policy
                             </Text>


          </TouchableOpacity>

        </View>
      </View>
    )
  }


  renderAmenities = (item) => {
    return (
      (<View style={{ flexDirection: "row", alignItems: "center", marginLeft: 5, marginRight: 5 }}>
        <Image
          style={{ height: 10, width: 10, }}
          source={Images.imgcircularcheck} />
        <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 10, margin: 5 }}>{item.item}</Text>
      </View>)
    )
  }






  render() {

    return (
      <View style={styles.container}>
        <SafeAreaView style={{ backgroundColor: colors.colorBlue }}></SafeAreaView>
        <StatusBar barStyle="light-content" />
        <CommonHeader title={"Select Room"}></CommonHeader>
        <View style={styles.innercontainer}>
          <ScrollView>
            {this.state.roomsData.map((item, index) => (
              this._renderHotelRoomListItem(item, index)
            ))}
            {this.roomPoPup()}
          </ScrollView>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    hotelData: state.HotelListreducer.selectedHotelItem
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveRoomdata: (data) => dispatch(saveSelectedRoomData(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomsList);