import React, { Component } from "react";
import {
  View,
  ScrollView,
  TouchableOpacity,
  Text,
  Platform,
  Dimensions,
  FlatList,
  Image,
  InteractionManager,
  SafeAreaView,
  Modal,
  Button,
  StatusBar,
  DeviceEventEmitter
} from "react-native";
import CommomHeader from '../../../Helper Classes/CommonHeader'
import Images from "../../../Helper Classes/Images";
import NavigationBar from "../../../Helper Classes/NavigationBar";
import ImageSliderz from "react-native-image-slideshow";
import RatinBar from "../../../Helper Classes/RatingBar";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import CardView from 'react-native-cardview'
import hotel from '../../../json/hotelOverview'
import { filterAmenities } from '../../../Utility'
import Spinner from "react-native-loading-spinner-overlay";
import NavigationServices from './../../../Helper Classes/NavigationServices'
import MapView from "react-native-maps";
import { Marker, Callout } from "react-native-maps";
import { styles } from "./Style"
import { ifIphoneX, getStatusBarHeight } from "react-native-iphone-x-helper";
import { getHotelDetailsApi } from "../../../Helper Classes/APIManager/ApiProvider";
import { connect } from "react-redux";
import { selectedHotel } from "../../../Redux/Actions/Actions";
import { getHotelDetailsAction } from "../../../Redux/Actions";
import StringConstants from "../../../Helper Classes/StringConstants";
import { colors } from "../../../Helper Classes/Colors";
import CommonHeader from '../../../Helper Classes/CommonHeader'
import HotelItemElement from "./HotelItemElement";
import Fonts from "../../../Helper Classes/Fonts";

//import hotelOverview from "../../../json/hotelOverview.json"

const screen = Dimensions.get("window");

const ASPECT_RATIO = screen.width / screen.height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class HotelOverview extends Component {
  constructor(props) {
    super(props);

    this.mapViewRef = null;

    this.state = {
      position: 1,
      interval: null,
      spinner: false,
      hotelDetails: null,
      imagesArray: [],
      showFilter: false,
      loading: false,
      readMoreFacilities: false,
      readMoreAboutHotel: false,
      latitude: 0,
      longitude: 0,
      mapRegion: {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      jsonDict: null,
      isMarkerSelected: false
    };

    this._renderImageSliderView = this._renderImageSliderView.bind(this);
    this._renderHeaderView = this._renderHeaderView.bind(this);
    this._renderFacilitiesView = this._renderFacilitiesView.bind(this);
    this._renderAboutHotelView = this._renderAboutHotelView.bind(this);
    this._renderContactDetailsView = this._renderContactDetailsView.bind(this);
    this._renderMarkerView = this._renderMarkerView.bind(this);
    this.updateHotelDetailsFromApi = this.updateHotelDetailsFromApi.bind(this);
  }

  //---------------Component LifeCycle Methods ---------------

  componentDidMount() {
    const { navigation } = this.props;
    const jsonDict = navigation.getParam('jsonDict', null);
    // let jsonDict = { "code": "1297786", "id": "51", "source": "tbo", "traceId": "9d4d6269-38d6-4049-a1f9-f1a35c3e2e00", "price": 1581.37 }
    this.setState({
      jsonDict: jsonDict
    }, () => {
      DeviceEventEmitter.addListener(StringConstants.HOTEL_DETAILS_EVENT, this.updateHotelDetailsFromApi)
      // this.updateHotelDetailsFromApi(hotel)
      this.getHotelDetails();
    })

  }

  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position:
            this.state.position === this.state.imagesArray.length - 1
              ? 0
              : this.state.position + 1
        });
      }, 7000)
    });
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.HOTEL_DETAILS_EVENT, this.updateHotelDetailsFromApi)
    clearInterval(this.state.interval);
  }

  updateHotelDetailsFromApi(response) {
    if (response) {
      let imgArray = response.hotel.images;

      let newImgArray = [];
      imgArray.forEach((item, index) => {

        newImgArray.push({
          url: item,
        });

      });

      this.setState({
        spinner: false,
        hotelDetails: response,
        imagesArray: newImgArray,
        loading: !this.state.loading,
        latitude: response.hotel.latitude && response.hotel.latitude != "" ? parseFloat(response.hotel.latitude) : 0,
        longitude: response.hotel.longitude && response.hotel.longitude != "" ? parseFloat(response.hotel.longitude) : 0,
      });

      this.props.saveHotelDetail(response)
    }
  }

  //-------------------- API Calling Methods --------------

  getHotelDetails() {

    this.props.getHotelDetailsAction(this.state.jsonDict)


  }

  //------------ Image SliderView ----------------

  _renderImageSliderView() {
    return (
      <View style={styles.styleImageSliderContainerView}>

        <ImageSliderz
          height={175}

          width={screen.width}
          containerStyle={{ borderRadius: 20, }}
          dataSource={this.state.imagesArray}
          indicatorColor="white"
          indicatorSelectedColor={colors.colorBlue}
          indicatorSize={7}
          position={this.state.position}
          arrowSize={0}
          onPositionChanged={position => this.setState({ position: position, loading: !this.state.loading })}
        //titleViewBackgroundColor='rgba(0,0,0,0.5)'
        //imageBorderRadius={10}
        //indicatorAlignment='center'
        />


      </View>
    );
  }

  //-------------- Render Header View ------------

  _renderHeaderView() {
    let dict = this.state.hotelDetails;
    return (
      <View style={styles.styleHeaderSuper}>
        <View style={{ flexDirection: "row", flex: 1, width: "100%" }}>
          <View style={styles.styleTitelContainerView}>
            <Text style={styles.styleTitleText}>{dict.hotel.name}</Text>

            <View style={{ height: 20, width: 80, marginTop: 10 }}>
              <RatinBar starSize={14} count={dict.hotel.star} />
            </View>

            <Text style={styles.styleDistanceText} adjustsFontSizeToFit>
              {dict.hotel.address}
            </Text>
          </View>

          <View
            style={[styles.styleTitelContainerView, { alignItems: "flex-end", width: '30%' }]}
          >
            <Text style={{ color: "blue", fontSize: 16, fontFamily: "Montserrat-SemiBold" }}>
              {dict.hotel.priceDetail && dict.hotel.priceDetail.CurrencyCode == "INR" ? "₹ " : null}{" "}
              {this.state.jsonDict.price}
            </Text>
          </View>
        </View>

        <View style={{ height: "40%", width: "100%", marginTop: 20, marginBottom: 15 }}>
          <TouchableOpacity
            style={styles.styleSelectRoomView}
            underlayColor="transparent"
            onPress={() => {
              console.log(JSON.stringify(this.state.hotelDetails))
              // Actions.RoomsList({ hotelDetails: this.state.hotelDetails ,hotelSearchParam:this.props.hotelParam});
              // Actions.RoomsList({ hotelSearchParam:this.props.hotelParam});
              NavigationServices.navigate('RoomsList', { traceId: this.state.jsonDict.traceId, source: this.state.jsonDict.source });
            }}
          >
            <Text style={{ color: "white", fontFamily: "Montserrat-SemiBold", fontSize: 16 }}>
              Select Room
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }



  //---------------- Render Facilities View --------------

  _renderFacilitiesView() {
    if (this.state.hotelDetails.hotel.facilities.length === 0) {
      return null;
    }
    return (
      <CardView style={{
        //overflow: 'hidden',
        backgroundColor: colors.colorWhite,
        marginVertical: 10,
        marginHorizontal: 10
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ width: '100%', overflow: 'hidden', padding: 1 }}>
          <View style={[styles.styleHeaderSuper, { height: "auto", flex: 1, flexWrap: "wrap" }]}>
            <View style={[styles.styleSectionTitleView, { paddingLeft: 10, paddingRight: 10 }]}>
              <Text style={{ fontSize: 15, fontFamily: Fonts.semiBold, color: colors.colorBlack }}>
                Amenities
        </Text>
              <TouchableOpacity
                underlayColor="transparent"
                onPress={() => {
                  this.setState({
                    readMoreFacilities: !this.state.readMoreFacilities,
                  });
                }}>

                <Text style={{ color: colors.colorBlue, fontSize: 13, fontFamily: "Montserrat-SemiBold" }}>{this.state.readMoreFacilities ? "Less" : "More"}</Text>
              </TouchableOpacity>
            </View>
            <FlatList
              // horizontal
              scrollEnabled={false}
              // horizontal = {true}

              style={{ height: this.state.readMoreFacilities ? "auto" : 127 }}
              data={this.state.hotelDetails.hotel.facilities}

              extraData={this.state.loading}
              renderItem={this._renderAnimitesItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </View></View>
      </CardView>
    );
  }

  //----------Render List Item for Animities ---------

  _renderAnimitesItem({ item, index }) {
    return (
      <View style={{ flexDirection: "row", margin: 5, marginLeft: 10 }}>
        <Image
          style={{ height: 15, width: 15 }}
          source={Images.imgcircularcheck}
        />
        <Text style={{ fontFamily: "Montserrat-SemiBold", marginLeft: 5, color: "#626262", fontSize: 12 }}>
          {item}
        </Text>
      </View>

    );
  }

  //----------- Render About Hotel View -----------

  _renderAboutHotelView() {
    const regex = /(&nbsp;|<([^>]+)>)/ig;
    return (
      <CardView style={{
        //overflow: 'hidden',
        backgroundColor: colors.colorWhite,
        marginVertical: 10,
        marginHorizontal: 10
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ width: '100%', overflow: 'hidden', padding: 1 }}>
          <View style={[styles.styleHeaderSuper, { height: this.state.readMoreAboutHotel ? "auto" : 200, paddingBottom: 10 }]}>
            {/* {this._renderTitleView("About Hotel", 1)} */}

            <View style={[styles.styleSectionTitleView, { paddingLeft: 10, paddingRight: 10 }]}>
              <Text style={{ fontSize: 15, fontFamily: "Montserrat-SemiBold", color: colors.colorBlack }}>
                About Hotel
        </Text>
              <TouchableOpacity
                underlayColor="transparent"
                onPress={() => {
                  this.setState({
                    readMoreAboutHotel: !this.state.readMoreAboutHotel,
                  });
                }} >
                <Text style={{ color: colors.colorBlue, fontSize: 13, fontFamily: "Montserrat-SemiBold" }}>{this.state.readMoreAboutHotel ? "Less" : "More"}</Text>
              </TouchableOpacity>
            </View>

            <Text
              style={{
                fontSize: 12,
                color: "#626262",
                lineHeight: 20,
                margin: 10,
                textAlign: "justify",
                fontFamily: Fonts.semiBold,
              }}
              numberOfLines={this.state.readMoreAboutHotel ? null : 7}
              ellipsizeMode={"tail"}
            >
              {this.state.hotelDetails.hotel.description.replace(regex, "")}
            </Text>
          </View>
        </View></CardView>
    );
  }

  //-------------------- Render Contact Details View ----------

  checkImageNullabilityForMarker(dict) {
    if (dict.images[0]) {
      return (
        <TouchableOpacity
          onPress={() => this.setState({ isMarkerSelected: !this.state.isMarkerSelected })}>
          <Image
            style={styles.styleMarkerUnselected}
            source={{ uri: dict.photo || dict.images[0] }}
            defaultSource={Images.imgHolidays}
            resizeMode="cover"
          />
        </TouchableOpacity>

      );
    }

    return (
      <Image
        style={styles.styleMarkerUnselected}
        source={Images.imgHolidays}
        defaultSource={Images.imgHolidays}
        resizeMode="cover"
      />
    );
  }

  _renderMarkerView(dict) {
    if (!this.state.isMarkerSelected) {
      if (dict.images[0]) {
        return (
          <Image
            style={styles.styleMarkerUnselected}
            source={{ uri: dict.photo || dict.images[0] }}
            defaultSource={Images.imgHolidays}
            resizeMode="cover"
          />
        );
      }

      return (
        <Image
          style={styles.styleMarkerUnselected}
          source={Images.imgHolidays}
          defaultSource={Images.imgHolidays}
          resizeMode="cover"
        />
      );

    } else {
      return (
        <View style={styles.styleMarkerViewSelected}>
          {this.checkImageNullabilityForMarker(dict)}
          <View style={[styles.styleMarkerSelectedTitleView,]}>
            <Text
              ellipsizeMode={"tail"}
              style={{ fontSize: 13, color: "white", fontFamily: "Montserrat-SemiBold" }}
              adjustsFontSizeToFit={true}
            >
              {dict.name}
            </Text>
            <View style={{ marginTop: 5, height: 20, width: 50 }}>
              <RatinBar
                starSize={15}
                count={dict.star} />
            </View>
          </View>
        </View>
      );
    }
  }

  _renderContactDetailsView() {
    let dict = this.state.hotelDetails.hotel;
    return (
      <CardView style={{
        //overflow: 'hidden',
        backgroundColor: colors.colorWhite,
        marginVertical: 10,
        marginHorizontal: 10
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ width: '100%', overflow: 'hidden', padding: 1, borderTopLeftRadius: 10, borderTopRightRadius: 10, }}>
          <View style={[styles.styleHeaderSuper, { borderBottomWidth: 0, paddingLeft: 20, paddingRight: 20, marginBottom: 20 }]}>
            {/* {this._renderTitleView("Contact Details", 2)} */}
            <Text style={{ fontSize: 15, fontFamily: "Montserrat-SemiBold", color: colors.colorBlack }}>
              Contact Details
        </Text>
            <Text style={{ fontSize: 12, color: "#626262", fontFamily: "Montserrat-Medium", marginTop: 4 }}>
              Phone Number - {dict.phone}
            </Text>

            <View style={{ height: 200, width: "100%", marginTop: 15 }}>
              <MapView
                ref={ref => {
                  this.mapViewRef = ref;
                }}
                style={[styles.map]}
                provider={MapView.PROVIDER_GOOGLE}
                onPress={() => {
                  this.setState({
                    isMarkerSelected: !this.state.isMarkerSelected
                  })
                  //Actions.refresh()
                }}
                // fitToSuppliedMarkers={}
                initialRegion={{
                  latitude: 37.78825,
                  longitude: -122.4324,
                  latitudeDelta: LATITUDE_DELTA,
                  longitudeDelta: LONGITUDE_DELTA
                }}
                zoomEnabled={false}
                pitchEnabled={false}
                scrollEnabled={false}
              >
                <Marker

                  coordinate={{
                    latitude: this.state.latitude,
                    longitude: this.state.longitude
                  }}
                // title={this.state.hotelDetails.hotel.name}
                >
                  <TouchableOpacity
                    style={
                      this.state.isMarkerSelected
                        ? { height: 60, width: 180 }
                        : styles.styleMarkerViewUnselected
                    }
                    underlayColor='transparent'
                    onPress={() => {
                      this.setState({
                        isMarkerSelected: !this.state.isMarkerSelected
                      })
                      alert('hi')
                      //Actions.refresh()
                    }}
                  >
                    {this._renderMarkerView(dict)}
                  </TouchableOpacity>
                </Marker>
              </MapView>
            </View>
          </View>
        </View></CardView>
    );
  }

  //------------- renderMethod --------------
  render() {
    if (this.state.hotelDetails == null) {
      return (
        <View style={styles.container}>
          <SafeAreaView style={{ backgroundColor: colors.colorBlue }} />

          <CommomHeader title={"Review Hotel"} />
          <Spinner
            style={{ flex: 1 }}
            visible={this.state.spinner}
            textContent={"Loading..."}
            textStyle={{ color: colors.colorWhite }}
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ backgroundColor: colors.colorBlue }} />

        <CommomHeader title={"Review Hotel"}></CommomHeader>


        <ScrollView automaticallyAdjustContentInsets={true} style={{ flex: 1, backgroundColor: "#FDFDFD" }} >
          <View style={{ flex: 1 }}>
            <Spinner
              style={{ flex: 1 }}
              visible={this.state.spinner}
              textContent={"Loading..."}
              textStyle={{ color: colors.colorWhite }}
            />


            <CardView style={{
              //overflow: 'hidden',
              backgroundColor: colors.colorWhite,
              marginVertical: 10,
              marginHorizontal: 10
            }}
              cardElevation={2}
              cardMaxElevation={3}
              cornerRadius={10}>
              <View style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10, width: '100%', overflow: 'hidden', padding: 0 }}>
                {this._renderImageSliderView()}

                {this._renderHeaderView()}
              </View></CardView>

            {this._renderFacilitiesView()}

            {this._renderAboutHotelView()}

            {this._renderContactDetailsView()}
          </View>
        </ScrollView>
        <SafeAreaView />

      </View >
    );
  }
  // render() {
  //   // if (this.state.roomIndex == null) return <View></View>
  //   return (
  //     <View style={styles.container}>
  //       <Spinner
  //         style={{ flex: 1 }}
  //         visible={this.state.spinner}
  //         textContent={"Loading..."}
  //         textStyle={{ color: colors.colorWhite }}
  //       />
  //       <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} />
  //       <View style={{
  //         marginTop: Platform.OS == "ios" ? (getStatusBarHeight(false) + 5) : 0,
  //         flex: 1,
  //         backgroundColor: colors.colorWhite
  //       }}>
  //         <CommonHeader title={"Review Hotel"} />
  //         <View style={{ padding: 15, flex: 1 }}>
  //           <KeyboardAwareScrollView>
  //             <HotelItemElement fullDetails={true} hotel={hotel} imagesArray={this.state.imagesArray} />

  //             <CardView style={{
  //               //overflow: 'hidden',
  //               backgroundColor: colors.colorWhite,
  //               marginVertical: 10,
  //               marginHorizontal: 2
  //             }}
  //               cardElevation={2}
  //               cardMaxElevation={3}
  //               cornerRadius={10}>
  //               <View style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10, padding: 10, overflow: "hidden" }}>
  //                 <Text style={{ fontSize: 15, Vertical: 4, fontFamily: Fonts.semiBold, color: colors.colorBlack }}>Amenities</Text>
  //                 {this.renderAmenities()}

  //               </View>
  //             </CardView>

  //             <CardView style={{
  //               //overflow: 'hidden',
  //               backgroundColor: colors.colorWhite,
  //               marginVertical: 10,
  //               marginHorizontal: 2
  //             }}
  //               cardElevation={2}
  //               cardMaxElevation={3}
  //               cornerRadius={10}>
  //               <View style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10, padding: 10, overflow: "hidden" }}>
  //                 <Text style={{ fontSize: 15, Vertical: 4, fontFamily: Fonts.semiBold, color: colors.colorBlack }}>About the Place</Text>
  //                 {this.renderAboutThisPlace()}

  //               </View>
  //             </CardView>


  //           </KeyboardAwareScrollView>
  //         </View>

  //       </View>
  //     </View>
  //   )
  // }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveHotelDetail: (data) => dispatch(selectedHotel(data)),
    getHotelDetailsAction: (data) => dispatch(getHotelDetailsAction(data))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(HotelOverview);

