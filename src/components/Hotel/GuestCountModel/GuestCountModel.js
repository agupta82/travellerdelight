import React, { Component } from "react";
import {
    View,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    ScrollView,
    Platform,
    FlatList,
    StatusBar
} from "react-native";

import { getStatusBarHeight } from "react-native-iphone-x-helper";
import Images from "../../../Helper Classes/Images";
import { commonstyle } from "../../../Helper Classes/Commonstyle";

const statusbarHeight = getStatusBarHeight(true);
const screenDimens = Dimensions.get('window');

export default class GuestCountModel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
            loading: false,
            roomCount: this.props.roomCount,
            GuestCount: this.props.guestCount,
        }
        this.roomData = this.props.roomData;
        this._renderRoomView = this._renderRoomView.bind(this)

    }
    

    removeitem = (index) => {
       
            console.log("deleting item " + index)

            this.roomData.splice(index,1);
            // var arr = [...this.state.roomData];
            // arr.splice(index, 1);
            // this.setState({
            //     roomData: arr
            // }, () => {  })
            this.totalRoomAndGuest()
       
    }


    incrementCount = (item, TYPE) => {


        console.log("increase item count")
        console.log(JSON.stringify(item))
        console.log(TYPE)
        if (TYPE === "ADULT") {
            if (item.adult < 4) {
            

                item.adult = item.adult + 1
                // this.setState({
                //     loading:!this.state.loading
                // })
                this.totalRoomAndGuest()
            } else {
                alert("Max 4 allowed")
            }
        } else if (TYPE === "CHILD") {
            if (item.children < 5) {
               
                item.children = item.children + 1
                if(!item.childrenage){
                  item.childrenage = []
                  item.childrenage.push("10");
                  }else{
                  item.childrenage.push("10");
                  }
                //item.childrenage.push("10");
                // childrenAgeItem.push("10");
                // item.childrenage =  childrenAgeItem;
                // this.setState({
                //     loading:!this.state.loading
                // })
                this.totalRoomAndGuest();
            } else {
                alert("Max 5 allowed")
            }
        }
    }


    totalRoomAndGuest() {
        console.log("executing total room")
        let arr = this.roomData;
        console.log(JSON.stringify(arr));
        let totalGuest = 0;
        arr.forEach(element => {
            totalGuest = totalGuest + element.children + element.adult;
        });
        this.setState({
            roomCount: arr.length,
            GuestCount: totalGuest,
            loading:!this.state.loading
        })
        console.log("guest count" + this.state.GuestCount);
        console.log("room count" + this.state.roomCount);
    }

    decrementCount = (item, TYPE) => {
       
        console.log("decrease item count")
        console.log(JSON.stringify(item))
        console.log(TYPE)
       
        if (TYPE === "ADULT") {
            if (item.adult > 1) {
              
                item.adult = item.adult - 1;
                this.totalRoomAndGuest();
            }
        } else if (TYPE === "CHILD") {
            if (item.children > 0) {
              if(item.childrenage){
                item.childrenage.splice(-1,1)
                }
                // this.setState({
                //     GuestCount:this.state.GuestCount-1
                // })
                item.children = item.children - 1
              
                this.totalRoomAndGuest();
            }
        }
    }

    componentDidMount(){
    
    }

    _renderRoomView({ item, index }) {
        
        return (
            <View style={styles.itemContainer}>
                <View style={{
                    padding: 15, borderTopLeftRadius: 10,
                    borderTopRightRadius: 10, backgroundColor: "rgb(251,251,251)",
                    flexDirection: "row", justifyContent: "space-between"
                }}>
                    <Text style={commonstyle.greyText}>
                        Room {index + 1}
                    </Text>
                    { this.roomData.length != 1 && <TouchableOpacity
                        style={{ width: 30 }}
                        underlayColor="transparent"
                        onPress={() => {
                            console.log("remove")
                           
                            this.removeitem(index);
                            
                        }}>
                        <Image
                            style={{ height: 20, width: 20 }}
                            source={Images.imgdustbin} />
                    </TouchableOpacity>}
                </View>
                {console.log("index is " + index)}
                {/* Adult View */}
                <View style={{
                    flexDirection: 'row', justifyContent: 'space-between',
                    margin: 15, borderBottomWidth: 1, borderBottomColor: 'rgb(0,104,208)', paddingBottom: 15
                }}>
                    <View>
                        <Text style={commonstyle.greyText}>
                            Adults
                       </Text>
                        <Text style={[commonstyle.greyText,{ fontSize: 11, marginTop: 5, }]}>
                            Above 12 years
                 </Text>
                    </View>
                    <View style={{
                        flexDirection: 'row', width: 100,
                        justifyContent: "space-evenly", alignItems: 'center'
                    }}>
                        <TouchableOpacity
                            underlayColor="transparent"
                            style={{ alignItems: "center" }}
                            onPress={
                                () => {
                                    this.decrementCount(item, "ADULT")
                                }}>
                            <Text style={{ color: 'rgb(0,104,208)', fontSize: 16 }}>
                                -
                 </Text>
                        </TouchableOpacity>
                        <View style={{
                            backgroundColor: 'rgb(0,104,208)',
                            justifyContent: 'center', alignItems: 'center', height: 25, width: 25
                        }}>

                            <Text style={{ color: 'white',fontFamily:"Montserrat-Medium" }}>
                                {item.adult}
                            </Text>
                        </View>
                        <TouchableOpacity

                            underlayColor="transparent"
                            style={{ alignItems: "center" }}
                            onPress={() => {
                                this.incrementCount(item, "ADULT")
                            }}>
                            <Text style={{ color: 'rgb(0,104,208)', fontSize: 16 }}>
                                +
                 </Text>
                        </TouchableOpacity>
                    </View>
                </View>

                {/* Child View */}

                <View style={{
                    flexDirection: 'row', justifyContent: 'space-between',
                    margin: 15, marginTop: 10, marginBottom: 20
                }}>
                    <View>
                        <Text style = {commonstyle.greyText}>
                            Children
                 </Text>
                        <Text style={[commonstyle.greyText,{ fontSize: 11, marginTop: 5}]}>
                            Below 12 years
                 </Text>
                    </View>
                    <View style={{
                        flexDirection: 'row', width: 100,
                        justifyContent: "space-evenly", alignItems: 'center'
                    }}>
                        <TouchableOpacity
                            underlayColor="transparent"
                            style={{ alignItems: "center" }}
                            onPress={
                                () => {
                                    this.decrementCount(item, "CHILD")
                                 }}>
                            <Text style={{ color: 'rgb(0,104,208)', fontSize: 16 }}>
                                -
                 </Text>
                        </TouchableOpacity>
                        <View style={{
                            backgroundColor: 'rgb(0,104,208)',
                            justifyContent: 'center', alignItems: 'center', height: 25, width: 25
                        }}>

                            <Text style={{ color: 'white',fontFamily:"Montserrat-Medium" }}>
                                {item.children}
                            </Text>
                        </View>
                        <TouchableOpacity
                            underlayColor="transparent"
                            style={{ alignItems: "center" }}
                            onPress={
                                () => {
                                    this.incrementCount(item, "CHILD")
                                 }}>
                            <Text style={{ color: 'rgb(0,104,208)', fontSize: 16 }}>
                                +
                 </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                
            </View>
        )
    }


    render() {

        const roomDict = {
            adult: 1,
            children: 0,
            childrenage:[]
        }

       
        return (
            <View style={{flex:1}}>
                <StatusBar barStyle="light-content" />
                <View style={{
                    height: Platform.OS === 'ios' ? statusbarHeight + 44 : 50, width: "100%", backgroundColor: "white",
                    alignItems: Platform.OS === 'ios' ?"flex-end":"center", flexDirection: "row",paddingBottom:Platform.OS === "ios"? 10:0
                }}>
                    <View style={{ width: "90%", alignItems: "center" }}>
                        <Text style={{ fontFamily: "Montserrat-SemiBold" ,fontSize: 15 }}>
                            {this.state.roomCount}  Rooms, {this.state.GuestCount} Guests
                        </Text>
                    </View>
                    <TouchableOpacity
                        underlayColor="transparent"
                        onPress={() => {
                            this.props.onModalClose(this.roomData, this.state.roomCount, this.state.GuestCount);
                        }}>
                        <Image
                            style={{ height: 20, width: 20,  marginRight: 15 }}
                            source={Images.imgClose}
                        />
                    </TouchableOpacity>
                </View>
                <ScrollView style={styles.container} >
                    {/* {this._renderRoomView()} */}
                    <View style={{ marginBottom: 50 }}>
                        <View  >
                            <FlatList
                                data={this.roomData}
                                extraData={this.state.loading}
                                renderItem={(item, index) => this._renderRoomView(item, index)}
                            />
                        </View>

                        <TouchableOpacity
                            style={styles.addButtonStyle}
                            underlayColor={"transparent"}
                            onPress={() => {
                                this.roomData.push(roomDict);
                                this.totalRoomAndGuest()
                                this.setState({
                                    loading:!this.state.loading
                                })
                                // this.setState({
                                //     roomData: [...this.state.roomData, roomDict]
                                // }, () => { this.totalRoomAndGuest() })
                            }}>
                            <Text
                                style={{ color: 'white', fontWeight: "600" }}>
                                Add Another Room
                     </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
        paddingTop:10,
        backgroundColor: "rgba(244,248,254,1)"
    },
    itemContainer: {
            marginTop:5,
            marginBottom:5,
            marginRight:10,
            marginLeft:10,
            backgroundColor: 'white',
            shadowColor: "gray",
            shadowOpacity: 0.5,
            shadowOffset: { width: 1, height: 1 },
            shadowRadius: 5,
            borderRadius: 10,
            elevation:4
    },
    addButtonStyle: {
        height: 40,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: 'rgb(0,104,208)',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
        margin:10,
        elevation:4,
        borderRadius: 7
    },
})
