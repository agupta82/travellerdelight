import React, { Component } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  DeviceEventEmitter,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions
} from 'react-native'
import Spinner from "react-native-loading-spinner-overlay";
import { WebView } from 'react-native-webview';
import { colors } from '../../Helper Classes/Colors'
import { paypalUrlAction } from '../../Redux/Actions/paypalUrlAction'
import { bookHotelAction } from '../../Redux/Actions/hotelActions'
import { bookFlightAction } from '../../Redux/Actions/flightActions'
import { paypalTokenAction } from '../../Redux/Actions/paypalTokenAction'
import { connect } from "react-redux";
import StringConstants from '../../Helper Classes/StringConstants';
import { getPaymentExecuteApi } from "../../Helper Classes/APIManager/PaypalApiProvider"
import CommonHeader from '../../Helper Classes/CommonHeader';
import Fonts from '../../Helper Classes/Fonts';


class PaypalWebView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      accessToken: null,
      approvalUrl: null,
      paymentId: null,
      price: null,
      currency: null,
      bookingObj: null,
      isFlight: null,
    }
  }

  componentDidMount() {
    let { navigation } = this.props;
    let bookingObj = navigation.getParam('bookingObj', null);
    let price = navigation.getParam('price', null);
    let currency = navigation.getParam('currency', null);
    let isFlight = navigation.getParam('isFlight', null)

    this.setState({
      bookingObj: bookingObj, price: price, currency: currency, isFlight: isFlight
    })

    this.paypalURLHandler = this.paypalURLHandler.bind(this);
    this.paypalTokenHandler = this.paypalTokenHandler.bind(this);

    DeviceEventEmitter.addListener(StringConstants.PAYPAL_URL, this.paypalURLHandler)
    DeviceEventEmitter.addListener(StringConstants.PAYPAL_TOKEN, this.paypalTokenHandler)

    this.props.paypalTokenAction(null)
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.PAYPAL_URL, this.paypalURLHandler)
    DeviceEventEmitter.removeListener(StringConstants.PAYPAL_TOKEN, this.paypalTokenHandler)
  }

  paypalTokenHandler(accessToken) {
    if (!this.state.price) return
    let dataDetail = {
      'intent': 'sale',
      'payer': {
        'payment_method': 'paypal',
      },
      'transactions': [{
        'amount': {
          'total': this.state.price,
          'currency': this.state.currency ? this.state.currency : "INR",
          'details': {
            'subtotal': this.state.price,
            'tax': '0',
            'shipping': '0',
            'handling_fee': '0',
            'shipping_discount': '0',
            'insurance': '0',
          },
        },

      }],
      'redirect_urls': {
        'return_url': 'https://example.com',
        'cancel_url': 'https://example.com',
      },
    };
    let payload = { param: dataDetail, accessToken: accessToken }
    console.log("payload:" + JSON.stringify(payload))
    this.props.paypalUrlAction(payload)
  }
  paypalURLHandler(res) {
    this.setState({
      paymentId: res.paymentId,
      approvalUrl: res.approvalUrl,
      accessToken: res.accessToken,
    })
  }


  getAllUrlParams = (url) => {

    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

      // stuff after # is not part of query string, so get rid of it
      queryString = queryString.split('#')[0];

      // split our query string into its component parts
      var arr = queryString.split('&');

      for (var i = 0; i < arr.length; i++) {
        // separate the keys and the values
        var a = arr[i].split('=');

        // set parameter name and value (use 'true' if empty)
        var paramName = a[0];
        var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

        // (optional) keep case consistent
        // paramName = paramName.toLowerCase();
        // if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

        // if the paramName ends with square brackets, e.g. colors[] or colors[2]
        if (paramName.match(/\[(\d+)?\]$/)) {

          // create key if it doesn't exist
          var key = paramName.replace(/\[(\d+)?\]/, '');
          if (!obj[key]) obj[key] = [];

          // if it's an indexed array e.g. colors[2]
          if (paramName.match(/\[\d+\]$/)) {
            // get the index value and add the entry at the appropriate position
            var index = /\[(\d+)\]/.exec(paramName)[1];
            obj[key][index] = paramValue;
          } else {
            // otherwise add the value to the end of the array
            obj[key].push(paramValue);
          }
        } else {
          // we're dealing with a string
          if (!obj[paramName]) {
            // if it doesn't exist, create property
            obj[paramName] = paramValue;
          } else if (obj[paramName] && typeof obj[paramName] === 'string') {
            // if property does exist and it's a string, convert it to an array
            obj[paramName] = [obj[paramName]];
            obj[paramName].push(paramValue);
          } else {
            // otherwise add the property
            obj[paramName].push(paramValue);
          }
        }
      }
    }

    return obj;
  }

  _onNavigationStateChange = (webViewState) => {

    console.log("NAVIGATION:" + JSON.stringify(webViewState))
    if (webViewState.url.includes('https://example.com/')) {

      this.setState({
        approvalUrl: null
      })

      const payerID = this.getAllUrlParams(webViewState.url).PayerID
      const paymentId = this.getAllUrlParams(webViewState.url).paymentId

      // const { PayerID, paymentId } = webViewState.url
      console.log("PAYER:" + payerID + " PAYMENT:" + paymentId);
      getPaymentExecuteApi(payerID, paymentId, this.state.accessToken)
        .then((response) => {
          console.log('EXECUTE_RES:' + JSON.stringify(response));
          if (response && response.state && response.state == "approved" && response.cart) {
            let newBookObj = this.state.bookingObj
            newBookObj.orderID = response.cart
            newBookObj.captureID = response.transactions[0].related_resources[0].sale.id
            console.log("New Object", JSON.stringify(newBookObj))
            if (this.state.isFlight) {
              this.props.bookFlightAction(newBookObj)
            }
            else {
              this.props.bookHotelAction(newBookObj, this.props.navigation)
            }
          }
          else {
            // setTimeout(() => { alert(JSON.stringify(response)); }, 1000);
            setTimeout(() => { alert("Payment Failed"); }, 1000);
            NavigationServices.goBack();

          }
          //setTimeout(() => { alert(JSON.stringify(response)); }, 1000);
        })
        .catch((err) => {
          console.log('failed');
          console.log(err);
          NavigationServices.goBack();
          setTimeout(() => { alert(err); }, 1000);
        });


    }
  }
  render() {
    const { approvalUrl } = this.state
    return (
      // <StatusBar barStyle = "light-content" />

      <View style={{ flex: 1 }}>
        <SafeAreaView backgroundColor={colors.colorBlue}>
          <CommonHeader title={"Do not Close This Tab"} />
          <Spinner
            style={{ flex: 1 }}
            visible={this.props.isLoading}
            textContent={"Loading..."}
            textStyle={{ color: colors.colorWhite }}
          />

          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            <View style={styles.body}>

              {/* <View style={{ height: 400, width: '100%',marginTop: 20  }}> */}
              {
                approvalUrl ? <WebView
                  style={{ height: Dimensions.get('window').height - 20, width: '100%', marginTop: 20, flex: 1 }}
                  source={{ uri: approvalUrl }}
                  onNavigationStateChange={this._onNavigationStateChange}
                  javaScriptEnabled={true}
                  domStorageEnabled={true}
                  startInLoadingState={false}
                />
                  : <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Text style={{ color: colors.colorBlack, fontSize: 14, fontFamily: Fonts.semiBold }}>
                      Processing . . .
                      </Text>
                  </View>
              }
              {/* </View> */}
            </View>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: colors.colorBlue,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: colors.colorWhite,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: colors.colorBlack,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: colors.gray,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: colors.gray,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
    isLogin: state.isLoginReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    paypalTokenAction: () => dispatch(paypalTokenAction()),
    paypalUrlAction: (data) => dispatch(paypalUrlAction(data)),
    bookHotelAction: (data, nav) => dispatch(bookHotelAction(data, nav)),
    bookFlightAction: (data) => dispatch(bookFlightAction(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaypalWebView)