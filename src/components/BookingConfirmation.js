import React, { Component } from 'react'
import {
  View,
  Image,
  Text,
  StyleSheet,
  StatusBar,
  TouchableHighlight,
  Modal,
  ScrollView,
  SafeAreaView
} from 'react-native'
import NavigationBar from '../Helper Classes/NavigationBar'
import Images from '../Helper Classes/Images'
import NavigationServices from './../Helper Classes/NavigationServices'
import { colors } from "../Helper Classes/Colors";

export default class BookingConfirmation extends Component {
  constructor(props) {
    super(props)
    this.state = {
      status: 0,
      bookingId: this.props.bookingId,
      failedError: "iusdgfksdfkdsbfkjsufbsdhfiusdguiiufgdfhjdsfsfshkjhaskjdhaskjhdkjsahdjkas asdj askjdhaskjd kjashdkj askjd askjdaskj dascdsbcjksbjsajd asjd j"
    }
  }

  bookingDetailBtn() {
    return (
      <TouchableHighlight
        style={styles.Buttons}
        underlayColor={'transparent'}
      // onPress={() => {
      //   Actions.BookingDetails({
      //     bookingId: this.props.bookingId,
      //   })
      // }}
      >
        <Text style={{ color: 'white', fontWeight: '600', fontSize: 15 }}>
          Booking Details
      </Text>
      </TouchableHighlight>
    )
  }

  anotherBookingBtn() {
    return (
      <TouchableHighlight
        style={styles.Buttons}
        underlayColor={'transparent'}
        onPress={() => {
          NavigationServices.navigate('HomeTabStack');
        }}
      >
        <Text style={{ color: 'white', fontWeight: '600', fontSize: 15 }}>
          Book Another Ticket
      </Text>
      </TouchableHighlight>
    )
  }

  bookingSuccessContent() {
    return (
      <View>
        <View style={styles.suceessImageContainer}>
          <Image
            style={{ width: "100%", height: "100%", borderRadius: 150 }}
            source={Images.imgbookingsuccess}
          />
        </View>

        <View style={{ margin: 20, alignItems: 'center' }}>
          <Text style={{ color: 'gray', margin: 20, fontSize: 16, fontWeight: '600' }}>
            THANK YOU FOR BOOKING
    </Text>
          <View style={styles.bookingIdContainer}>
            <Text style={{ fontSize: 16 }}>BOOKING ID </Text>
            <Text style={{ marginTop: 10, color: colors.colorBlue, fontSize: 15 }}>{this.props.bookingId}</Text>
          </View>
          {this.bookingDetailBtn()}
          {this.anotherBookingBtn()}
        </View>
      </View>
    )
  }

  bookAgainbtn() {
    return (
      <TouchableHighlight
        style={[styles.Buttons, { marginTop: 0 }]}
        underlayColor={'transparent'}
        onPress={() => {
          NavigationServices.navigate('HomeTabStack');
        }}
      >
        <Text style={{ color: 'white', fontWeight: '600', fontSize: 15 }}>
          Book Again
      </Text>
      </TouchableHighlight>
    )
  }

  bookingFailedContent() {
    return (
      <View>
        <View style={styles.failedImageContainer}>
          <Image
            style={{ width: "100%", height: "100%", }}
            source={Images.imgbookingfailed}
          />
        </View>

        <View style={{ margin: 20, alignItems: 'center' }}>
          <Text style={{ color: 'gray', margin: 20, fontSize: 16, fontWeight: '600' }}>
            BOOKING FAILED
    </Text>
          <View style={styles.errorContentContainer}>
            <Text style={{ fontSize: 14, color: 'black' }} numberOfLines={0}>
              {this.props.failedError}
            </Text>
          </View>
          {this.bookAgainbtn()}
        </View>
      </View>
    )
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="light-content" />
        <NavigationBar title='Booking Confirmation' />
        <ScrollView>
          {this.props.status == 1 ? this.bookingSuccessContent() : this.bookingFailedContent()}
        </ScrollView>
      </SafeAreaView>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(244,248,254,1)'
  },
  suceessImageContainer: {
    marginTop: 20,
    width: 200,
    height: 200,
    alignSelf: 'center',
    backgroundColor: 'white',
    borderRadius: 150,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "black",
    shadowOpacity: 0.3,
    elevation: 4,
    shadowOffset: { width: 0, height: 3 },

  },
  Buttons: {
    height: 50,
    width: "100%",
    backgroundColor: colors.colorBlue,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
    elevation: 2
  },
  bookingIdContainer: {
    margin: 20,
    width: "100%",
    backgroundColor: 'white',
    alignItems: 'center',
    height: 80,
    justifyContent: 'center',
    borderRadius: 10,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    shadowRadius: 4,
    elevation: 1,
    padding: 15
  },
  errorContentContainer: {
    margin: 20,
    width: "100%",
    backgroundColor: 'white',
    // alignItems: 'center',
    // height: 80,
    justifyContent: 'center',
    borderRadius: 10,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    shadowRadius: 4,
    elevation: 4,
    padding: 15
  },
  failedImageContainer: {
    marginTop: 20,
    width: 250,
    height: 250,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 4
  }
})
