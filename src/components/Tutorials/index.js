import React, { Component } from 'react'
import { View, Text, ImageBackground, Image } from 'react-native'
import Swiper from 'react-native-swiper';
import Images from '../../Helper Classes/Images'
import { colors } from '../../Helper Classes/Colors'
import NavigationServices from '../../Helper Classes/NavigationServices';
import AsyncStorage from '@react-native-community/async-storage';



class Tutorials extends Component {
  constructor() {
    super()
  }


  _onItemChanged(index) {
    if (index == 2) {
      // this.props.isTutorialAction(true)
      AsyncStorage.setItem("isTutorial", JSON.stringify(true))


      setTimeout(() => {
        NavigationServices.navigate('DashboardStack', { NavigateFrom: "Tutorials" })
      }, 1000);
    }
  }


  Step1() {
    return (
      <View style={{ flex: 1, justifyContent: 'flex-start' }}>
        <ImageBackground style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
          imageStyle={{ top: 0, position: 'absolute' }}
          source={Images.img_spl_1} >
          <Image resizeMode='contain' source={Images.imglogo} style={{ width: '50%' }} />
        </ImageBackground>
      </View>
    )
  }
  Step2(data) {
    let heading, desc, img;
    switch (data) {
      case 'F':
        heading = 'Book A Flight'
        desc = "Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the"
        img = Images.img_spl_2
        break;
      case 'H':
        heading = 'Book A Hotel'
        desc = "Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the "
        img = Images.img_spl_3
        break;
    }
    return (
      <View style={{ flex: 1, justifyContent: 'flex-start' }}>
        <ImageBackground style={{ flex: 1 }}
          imageStyle={{ top: 0, position: 'absolute' }}
          source={img} >
          <View style={{ marginTop: 55, padding: 30 }} >
            <Text style={{ fontSize: 25, fontWeight: 'bold', color: 'white', margin: 5 }}> {heading}</Text>
            <Text style={{ fontSize: 12, fontWeight: '600', color: 'white', textAlign: 'justify', lineHeight: 20, margin: 5, marginTop: 10 }}> {desc}</Text>
          </View>
        </ImageBackground>
      </View>
    )

  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 9 }}>
          <Swiper
            // ref={pager => this.pager = pager}
            onIndexChanged={(index) => { this._onItemChanged(index) }}
            loop={false}
          >

            {this.Step1()}
            {this.Step2('F')}
            {this.Step2('H')}

          </Swiper>
        </View>
        <View style={{ flex: 1 }} />
      </View>
    )
  }
}


export default Tutorials
