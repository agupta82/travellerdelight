import React, { Component } from 'react'
import { View, Text, Image, StyleSheet, StatusBar, TouchableHighlight, Alert, SafeAreaView, ScrollView } from 'react-native'
import NavigationBar from '../Helper Classes/NavigationBar';
import Images from '../Helper Classes/Images'
import CustomTextInput from '../Helper Classes/Custom TextFiel/CustomTextInput';
import APIManager from '../Helper Classes/APIManager/APIManagerUpdated';
import { connect } from "react-redux";
import { forgotPasswordAction } from '../Redux/Actions'
import CommonHeader from '../Helper Classes/CommonHeader.js'
import { colors } from '../Helper Classes/Colors';

class ForgotPassword extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: ''
    }
  }

  recoverPasswordBtn() {
    return (
      <TouchableHighlight
        style={styles.Buttons}
        underlayColor={'transparent'}
        onPress={() => {
          this.callApi()
        }}
      >
        <Text style={{ color: 'white', fontWeight: '600', fontSize: 15 }}>
          Recover Your Password
          </Text>
      </TouchableHighlight>
    )
  }

  isEmailValid(text) {
    console.log(text)
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    if (reg.test(text) === false) {
      console.log('Email is Not Correct')
      return false
    } else {
      console.log('Email is Correct')
      return true
    }
  }


  callApi() {
    if (this.state.email.trim().length == 0) {
      alert("Please Enter Your Email")
      return;
    }
    if (!this.isEmailValid(this.state.email)) {
      alert("Please Enter Valid Email")
      return;
    }

    let param = {
      email: this.state.email
    }

    let msg = "Reset password link has been sent to your server"

    this.props.forgotPasswordAction(param)
    //  APIManager.forgotPasswordApi(param,(success,response)=>{
    //    if(success){
    //      console.log('success',response.data)
    //      if(response.data === "RESET_LINK_SENT"){
    //        setTimeout(()=>{
    //          Alert.alert('Alert',msg,[
    //           { text: 'OK', onPress: () => Actions.Login() }
    //          ])
    //        },100)
    //      }
    //    }else{
    //      setTimeout(()=>{
    //        alert('error occured')
    //      },100)
    //      console.log('failed'+response)
    //    }
    //  })  
  }


  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView backgroundColor={colors.colorBlue}></SafeAreaView>
        <StatusBar barStyle='light-content' />
        <CommonHeader title={"Forgot Password"} />
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <Image
            source={Images.imgforgotpassword}
            style={styles.forgotImage}
          />
          <Text style={{ margin: 20, alignSelf: 'center' }}>
            FORGOT YOUR PASSWORD
                </Text>
          <View style={styles.ContentContainer}>
            <View style={{ alignItems: 'center', marginBottom: 20 }}>
              <Text>
                Enter your E-mail below to receive your
                 </Text>
              <Text>
                password reset instruction
                 </Text>
            </View>


            <CustomTextInput
              placeholder="Type Your Email"
              icon={Images.imgmail}
              contentType='emailAddress'
              style={{ width: "80%", height: 50 }}
              tintColor="rgb(0,114,198)"
              autoCapitalize={false}
              autoCorrect={false}
              onChangeText={text =>
                this.setState({
                  email: text
                })
              }
            />
            {this.recoverPasswordBtn()}

          </View>
        </ScrollView>
        <SafeAreaView />

      </View >
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    forgotPasswordAction: (data) => dispatch(forgotPasswordAction(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(244,248,254,1)'
  },
  forgotImage: {
    marginTop: 50,
    width: 250,
    height: 250,
    alignSelf: 'center',
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 0, height: 3 },

  },
  Buttons: {
    height: 50,
    width: "80%",
    backgroundColor: 'rgb(0,104,208)',
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7
  },
  ContentContainer: {
    margin: 20,

    backgroundColor: 'white',
    alignItems: 'center',
    // height: 80,
    justifyContent: 'center',
    borderRadius: 10,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    shadowRadius: 4,
    elevation: 6,
    padding: 20
  },
})