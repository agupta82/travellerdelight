import React, { Component } from 'react'
import {
  Text,
  View,
  Image,
  TextInput,
  TouchableHighlight,
  StyleSheet,
  StatusBar,
  Dimensions,
  Alert,
  SafeAreaView,
  DeviceEventEmitter,
} from 'react-native'
import Images from '../Helper Classes/Images'
import NavigationServices from './../Helper Classes/NavigationServices'
import Spinner from 'react-native-loading-spinner-overlay'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from "react-redux";
import { doRegisterAction } from '../Redux/Actions'
import StringConstants from '../Helper Classes/StringConstants';
const screenwidth = Dimensions.get('window').width
const screenheight = Dimensions.get('window').height

class Register extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      password: '',
      confirmPassword: '',
      checked: false,
      spinner: false
    }

    this.updateRegisterDataFromApi = this.updateRegisterDataFromApi.bind(this);
  }

  componentDidMount() {
    DeviceEventEmitter.addListener(StringConstants.REGISTER_EVENT, this.updateRegisterDataFromApi)
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.REGISTER_EVENT, this.updateRegisterDataFromApi)
  }

  updateRegisterDataFromApi(data) {
    var msg =
      'Verification link has been sent to your mail please verify email to continue login'
    if (data && data.message && data.message === 'VERIFICATION_MAIL_SENT') {
      setTimeout(() => {
        Alert.alert('Alert', msg, [
          { text: 'OK', onPress: () => NavigationServices.goBack() }
        ])
      }, 100)
    }
  }

  textInputFirstName(imgsource, placeholder) {
    return (
      <View style={styles.textinputcontainer}>
        <View style={styles.textinputimageview}>
          <Image
            source={imgsource}
            style={{ tintColor: 'rgb(0,114,198)', height: 13, width: 13 }}
          />
        </View>
        <View style={{ width: '90%', height: 40, justifyContent: 'center' }}>
          <TextInput
            style={{ marginLeft: 15 }}
            onChangeText={text => this.setState({ firstName: text })}
            placeholder={placeholder}
            autoCapitalize={false}
            autoCorrect={false}
            placeholderTextColor={'grey'}
          />
        </View>
      </View>
    )
  }
  textInputPassword(imgsource, placeholder) {
    return (
      <View style={styles.textinputcontainer}>
        <View style={styles.textinputimageview}>
          <Image
            source={imgsource}
            style={{ tintColor: 'rgb(0,114,198)', height: 13, width: 13 }}
          />
        </View>
        <View style={{ width: '90%', height: 40, justifyContent: 'center' }}>
          <TextInput
            style={{ marginLeft: 15 }}
            onChangeText={text => this.setState({ password: text })}
            placeholder={placeholder}
            autoCapitalize={false}
            autoCorrect={false}
            secureTextEntry
            placeholderTextColor={'grey'}
          />
        </View>
      </View>
    )
  }

  textInputConfirmPassword(imgsource, placeholder) {
    return (
      <View style={styles.textinputcontainer}>
        <View style={styles.textinputimageview}>
          <Image
            source={imgsource}
            style={{ tintColor: 'rgb(0,114,198)', height: 13, width: 13 }}
          />
        </View>
        <View style={{ width: '90%', height: 40, justifyContent: 'center' }}>
          <TextInput
            style={{ marginLeft: 15 }}
            onChangeText={text => this.setState({ confirmPassword: text })}
            placeholder={placeholder}
            autoCapitalize={false}
            autoCorrect={false}
            secureTextEntry
            placeholderTextColor={'grey'}
          />
        </View>
      </View>
    )
  }

  textInputLastName(imgsource, placeholder) {
    return (
      <View style={styles.textinputcontainer}>
        <View style={styles.textinputimageview}>
          <Image
            source={imgsource}
            style={{ tintColor: 'rgb(0,114,198)', height: 13, width: 13 }}
          />
        </View>
        <View style={{ width: '90%', height: 40, justifyContent: 'center' }}>
          <TextInput
            style={{ marginLeft: 15 }}
            onChangeText={text => this.setState({ lastName: text })}
            placeholder={placeholder}
            autoCapitalize={false}
            autoCorrect={false}
            placeholderTextColor={'grey'}
          />
        </View>
      </View>
    )
  }

  textInputEmail() {
    return (
      <View style={styles.textinputcontainer}>
        <View style={styles.textinputimageview}>
          <Image
            source={Images.imgemail}
            style={{ tintColor: 'rgb(0,114,198)', height: 18, width: 18 }}
          />
        </View>
        <View style={{ width: '90%', height: 40, justifyContent: 'center' }}>
          <TextInput
            style={{ marginLeft: 15 }}
            onChangeText={text => this.setState({ email: text })}
            placeholder={'Email'}
            autoCapitalize={false}
            autoCorrect={false}
            keyboardType={'email-address'}
            placeholderTextColor={'grey'}
          />
        </View>
      </View>
    )
  }

  textInputPhone() {
    return (
      <View style={styles.textinputcontainer}>
        <View style={styles.textinputimageview}>
          <Image
            source={Images.imgphone}
            style={{ tintColor: 'rgb(0,114,198)', height: 20, width: 20 }}
          />
        </View>
        <View style={{ width: '90%', height: 40, justifyContent: 'center' }}>
          <TextInput
            style={{ marginLeft: 15 }}
            onChangeText={text => this.setState({ phone: text })}
            placeholder={'Phone'}
            autoCapitalize={false}
            keyboardType={'phone-pad'}
            autoCorrect={false}
            maxLength={10}
            placeholderTextColor={'grey'}
          />
        </View>
      </View>
    )
  }

  agreeTermsText() {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          marginTop: 10,
          marginLeft: 8,
          alignItems: 'center',
          height: 20
        }}
      >
        <View style={{ width: '10%' }}>
          <Image
            source={Images.imguncheck}
            style={{ width: 13, height: 13, tintColor: 'rgb(0,114,198)' }}
          />
        </View>
        <View style={{ width: '90%' }}>
          <Text style={{ fontWeight: '300', fontSize: 12 }}>
            I agree with terms & condition
          </Text>
        </View>
      </View>
    )
  }

  registerButton() {
    return (
      <TouchableHighlight
        style={styles.registerButton}
        underlayColor='transparent'
        onPress={() => {
          this.validateFields()
        }}
      >
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-end',
            marginRight: 8
          }}
        >
          <Text
            style={{ color: 'white', alignSelf: 'center', marginRight: 20 }}
          >
            Register
          </Text>
          <Image
            style={{ width: 15, height: 15 }}
            source={Images.imgnextArrow}
          />
        </View>
      </TouchableHighlight>
    )
  }
  isEmailValid(text) {
    console.log(text)
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    if (reg.test(text) === false) {
      console.log('Email is Not Correct')
      return false
    } else {
      console.log('Email is Correct')
      return true
    }
  }

  validateFields() {
    if (this.state.firstName.trim().length == 0) {
      alert('Please Enter your first name')
      return
    }
    if (this.state.lastName.trim().length == 0) {
      alert('Please Enter your last name')
      return
    }
    if (this.state.email.trim().length == 0) {
      alert('Please Enter your Email')
      return
    }
    if (!this.isEmailValid(this.state.email)) {
      alert('Please Enter valid Email')
      return
    }
    if (this.state.phone.trim().length == 0) {
      alert('Please Enter your Phone Number')
      return
    }
    if (this.state.phone.trim().length < 10) {
      alert('Please Enter valid Phone Number')
      return
    }
    if (this.state.password.trim().length == 0) {
      alert('Please Enter your Password')
      return
    }
    if (this.state.confirmPassword.trim().length == 0) {
      alert('Please Enter your Password to confirm')
      return
    }
    if (!this.state.confirmPassword === this.state.confirmPassword) {
      console.log(
        'password: ' +
        this.state.password +
        ' confirm password: ' +
        this.state.confirmPassword
      )
      alert('Password not matched')
      return
    }
    if (!this.state.checked) {
      console.log('uncheck')
      alert('Please tick the terms and conditions')
      return
    }
    console.log('register')
    this.callRegisterApi()
  }

  callRegisterApi() {
    let param = {
      first_name: this.state.firstName,
      last_name: this.state.lastName,
      email: this.state.email,
      phone: this.state.phone,
      password: this.state.password,
      password_confirmation: this.state.confirmPassword
    }
    var msg =
      'Verification link has been sent to your mail please verify email to continue login'

    this.props.doRegisterAction(param)
    // this.setState({ spinner: true })
    // APIManager.registerUserApi(param, (success, response) => {
    //   this.setState({ spinner: false })
    //   if (success) {
    //     console.log('success' + response.message)
    //     if (response.message === 'VERIFICATION_MAIL_SENT') {
    //       setTimeout(() => {
    //         Alert.alert('Alert', msg, [
    //           { text: 'OK', onPress: () => Actions.pop() }
    //         ])
    //       }, 100)
    //     }
    //   } else {
    //     setTimeout(() => {
    //       alert('error occured')
    //     }, 100)
    //     console.log('failed' + response)
    //   }
    // })
  }

  checkItem() {
    var imgsrc = this.state.checked ? Images.imgcheck : Images.imguncheck
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          marginTop: 10,
          marginLeft: 8,
          alignItems: 'center',
          height: 20
        }}
      >
        <View style={{ width: '10%' }}>
          <TouchableHighlight
            onPress={() => this.setState({ checked: !this.state.checked })}
            underlayColor={'transparent'}
          >
            <Image
              source={imgsrc}
              style={{ width: 13, height: 13, tintColor: 'rgb(0,114,198)' }}
            />
          </TouchableHighlight>
        </View>
        <View style={{ width: '90%' }}>
          <Text style={{ fontWeight: '300', fontSize: 12 }}>
            I agree with terms & condition
          </Text>
        </View>
      </View>
    )
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Spinner
          visible={this.props.isLoading}
          textContent={'Loading...'}
          textStyle={{ color: 'white' }}
        />
        <StatusBar barStyle='light-content' />
        <KeyboardAwareScrollView>
          <Image
            source={Images.imgregister}
            style={{ width: screenwidth, height: screenheight / 3 }}
          />
          <TouchableHighlight
            style={{ position: "absolute", height: 30, width: 30, marginTop: 15, marginLeft: 15 }}
            underlayColor={"transparent"}
            onPress={() =>  NavigationServices.goBack()}
          >
            <Image
              tintColor={"#fff"}
              source={Images.imgback}
              style={{ height: 25, width: 25, tintColor: "white" }}
              resizeMode="contain"
            />
          </TouchableHighlight>
          <View style={styles.innercontainer}>
            {/* {this.textinput(Images.imgusericon,"First Name")} */}
            {this.textInputFirstName(Images.imgusericon, 'First Name')}
            {this.textInputLastName(Images.imgusericon, 'Last Name')}
            {this.textInputEmail()}
            {this.textInputPhone()}
            {this.textInputPassword(Images.imgkey, 'Password')}
            {this.textInputConfirmPassword(Images.imgkey, 'Confirm Password')}
            {this.checkItem()}
            {this.registerButton()}
            <View
              style={{
                flexDirection: 'row',
                marginTop: 10,
                width: '100%',
                height: 50,
                justifyContent: 'center'
              }}
            >
              <Text>Already have an account ?</Text>
              <Text
                style={{ color: 'rgb(48,145,210)' }}
                onPress={() => NavigationServices.replace('Login')}
              >
                {' '}
                Sign In
              </Text>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    doRegisterAction: (data) => dispatch(doRegisterAction(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(244,248,254,1)'
  },
  innercontainer: {
    marginLeft: 50,
    marginRight: 50,
    marginTop: 20,
    flex: 1
  },
  textinputcontainer: {
    flexDirection: 'row',
    height: 40,
    margin: 5,
    // backgroundColor:'red',
    borderBottomColor: 'rgb(0,114,198)',
    borderBottomWidth: 1
  },
  textinputimageview: {
    width: '10%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center'
  },
  registerButton: {
    backgroundColor: 'rgb(0,114,198)',
    height: 40,
    borderRadius: 20,
    width: 130,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 30
  }
})
