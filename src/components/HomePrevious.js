
import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  ScrollView,
  FlatList,
  Image,
  TouchableHighlight,
  Dimensions,
  DeviceEventEmitter
} from "react-native";
import NavigationServices from '../Helper Classes/NavigationServices'
import NavigationBar from '../Helper Classes/NavigationBar'
import APIManager from '../Helper Classes/APIManager/APIManagerUpdated'
import Spinner from 'react-native-loading-spinner-overlay';
import StringConstants from '../Helper Classes/StringConstants';
import Images from "../Helper Classes/Images";
import Constants from "../Helper Classes/Constants";
//import HotelBookingDetails from "./Hotel/HotelBookingDetail/HotelBookingDetails";
import { connect } from "react-redux";
import { getAirportListAction, setLoadingAction, userDataAction, isLoginAction, userTokenAction } from '../Redux/Actions'

const screenWidth = Dimensions.get('window').width;

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false,

      arrBookinType: [
        {
          name: 'Flight',
          Image: Images.imgfligth,
          color: 'rgba(174,46,246,1)'
        },
        {
          name: 'Hotel',
          Image: Images.imgBed,
          color: 'rgba(238,132,48,1)'
        },
        {
          name: 'Holiday',
          Image: Images.imgHolidays,
          color: 'rgba(229,35,99,1)'
        },
        {
          name: 'Bus',
          Image: Images.imgbus,
          color: 'rgba(45,124,227,1)'
        }

      ],
      loading: false,
      arrGiofareData: [
        {
          name: 'Kerala',
          image: Images.imglistpic1,
          price: 85000
        },
        {
          name: 'Orrisa',
          image: Images.imglistpic2,
          price: 89000
        },
        {
          name: 'Goa',
          image: Images.imglistpic3,
          price: 90000
        },
      ],
      spinner: false,

    }
    this.lastBackButtonPress = null;
    this.loginEventHandler = this.loginEventHandler.bind(this);
    this.logoutEventHandler = this.logoutEventHandler.bind(this);
    this._renderListItem = this._renderListItem.bind(this);
  }


  //-----------------Components Life cycle Methods ---------------

  componentDidMount() {
    //console.log("home component did mount");
    // APIManager.getUserData()


    // APIManager.getDataFromAsync(StringConstants.IS_LOGIN, isLogin => {
    //   if (isLogin === "TRUE") {
    //     APIManager.getDataFromAsync(StringConstants.USER_DATA, dictStr => {
    //       if (dictStr) {
    //         let json = JSON.parse(dictStr);
    //         Constants.isLogin = true;
    //         Constants.userData = json.user;
    //         Constants.userToken = json.token;

    //         this.props.setLoadingAction(true)
    //         this.props.getAirportListAction()
    //       } else {
    //         this.props.setLoadingAction(true)
    //         this.props.getAirportListAction()
    //       }
    //     });
    //   } else {
    //     Constants.isLogin = false;
    //     this.props.setLoadingAction(true)
    //     this.props.getAirportListAction()
    //   }
    // });

    this.props.getAirportListAction()

    DeviceEventEmitter.addListener(StringConstants.IS_LOGINEVENT, this.loginEventHandler)
    DeviceEventEmitter.addListener(StringConstants.IS_LOGOUTEVENT, this.logoutEventHandler)
  }


  loginEventHandler(isLogin) {
    console.log("in hander login event handler ")
    if (isLogin) {
      this.setState({
        isLogin: true
      })

    } else {
      this.setState({
        isLogin: false
      })
    }
  }

  logoutEventHandler(islogout) {
    console.log(" in hander logout event handler ")
    if (islogout) {
      console.log("logging out")
      this.setState({
        isLogin: false
      })
      //Actions.refresh()
    } else {
      setTimeout(() => {
        alert("Unable to logout user")
      }, 100)
    }
  }


  _renderGioPlacesListItem({ item, index }) {
    return (
      <View style={{ height: 150, width: screenWidth / 1.5, marginRight: 10, justifyContent: 'flex-end' }}>

        <Image
          style={{ height: '100%', width: '100%', position: 'absolute', borderRadius: 10 }}
          source={item.image}
          resizeMode='cover'
        />

        <View style={{ height: 50, width: '100%', borderRadius: 10, backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: 'center' }}>

          <Text style={{ fontSize: 13, fontFamily: "Montserrat-Bold", color: 'white', marginLeft: 10 }}>
            {item.name}
          </Text>

          <Text style={{ fontSize: 11, color: 'white', fontFamily: "Montserrat-Regular", marginLeft: 10 }}>
            Starting at Rs.{item.price}
          </Text>

        </View>


      </View>
    )
  }

  _renderListItem({ item, index }) {
    return (
      <TouchableHighlight
        underlayColor='transparent'
        onPress={() => {
          switch (index) {
            case 0:
              NavigationServices.navigate('FlightSearch');
              // Actions.FlightSearch()
              break;
            case 1:
              NavigationServices.navigate('HotelSearch');
              // Actions.HotelSearch()
              break;
            case 2:

              break;
            case 3:
              break
          }
        }}
      >

        <View style={styles.styleListSuperView}>

          <View style={[styles.styleListItem, { backgroundColor: item.color }]}>

            <Image
              style={{ height: 20, width: 20 }}
              source={item.Image}
              resizeMode='contain'
            />

          </View>

          <Text style={{ fontSize: 14, color: 'gray', fontFamily: "Montserrat-Medium", marginTop: 10 }}>
            {item.name}
          </Text>

        </View>

      </TouchableHighlight>
    )
  }

  createUserHeaderView() {
    return (
      <View style={styles.styleUserView}>
        <View style={styles.styleUserNameView}>
          <Text
            style={{
              fontSize: 18,
              fontFamily: "Montserrat-Bold",
              color: "black",
              margin: 5
            }}
          >
            {(this.props.isLogin && this.props.userData) ? "Hi, " + this.props.userData.first_name + " " + this.props.userData.last_name : "Hi Guest"}
          </Text>
          {
            //console.log("eew",this.props.userData)
          }

          <Text style={{ fontSize: 12, color: "gray", fontFamily: "Montserrat-Medium", margin: 5 }}>
            Summer time, Let's book a flight for a vacation!!!
          </Text>
        </View>

        <View style={styles.styleUserImageView}>
          <Image
            style={{
              height: 60,
              width: 60,
              borderRadius: 30
            }}
            source={Images.imguserprofile}
            resizeMode="contain"
          />
        </View>
      </View>
    );
  }

  createUserwinningAndwalletView() {
    return (
      <View style={styles.styleUserPointsandWalletView}>
        <View style={styles.stylewinnerView}>
          <Image
            style={{ height: 20, width: 20, marginLeft: 20 }}
            source={Images.imgwinner}
            resizeMode="contain"
          />

          <Text
            style={{
              fontSize: 13,
              fontWeight: "600",
              color: "black",
              marginLeft: 20
            }}
          >
            1130
          </Text>

          <Text style={{ fontSize: 11, color: "gray" }}>pts</Text>
        </View>

        <View style={styles.stylewinnerView}>
          <Image
            style={{ height: 20, width: 20, marginLeft: 20 }}
            source={Images.imgwallet}
            resizeMode="contain"
          />

          <Text style={{ fontSize: 12, color: "gray", marginLeft: 20 }}>$</Text>

          <Text style={{ fontSize: 13, fontWeight: "600", color: "black" }}>
            1130
          </Text>
        </View>
      </View>
    );
  }


  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.IS_LOGINEVENT, this.loginEventHandler)
    DeviceEventEmitter.removeListener(StringConstants.IS_LOGOUTEVENT, this.logoutEventHandler)
  }

  render() {
    return (

      <View style={styles.container}>

        <Spinner
          visible={this.props.isLoading}
          textContent={"Loading..."}
          textStyle={{ color: 'white' }}
        />

        <StatusBar barStyle="light-content" />
        <NavigationBar title='Home' />
        <ScrollView>
          <View style={{ marginLeft: 20, marginRight: 20, height: "100%" }}>
            {this.createUserHeaderView()}

            {/* {this.createUserwinningAndwalletView()} */}

            <View style={{ width: '100%', marginTop: 40 }}>

              <FlatList
                horizontal
                pagingEnabled={true}
                data={this.state.arrBookinType}
                extraData={this.state.loading}
                renderItem={this._renderListItem}
                keyExtractor={(item, index) => index.toString()}

              />

            </View>

            <View style={{ width: '100%', marginTop: 30 }}>

              <View style={{ height: 30, width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>

                <Text style={{ fontSize: 14, fontFamily: "Montserrat-Bold", color: 'rgba(133,132,133,1)' }}>
                  Giofare Specials
                  </Text>

                <TouchableHighlight>

                  <Text style={{ fontSize: 12, color: 'red', fontFamily: "Montserrat-Medium", marginRight: 10 }}>
                    View More
                    </Text>

                </TouchableHighlight>

              </View>

              <FlatList
                style={{ marginTop: 10 }}
                horizontal
                data={this.state.arrGiofareData}
                extraData={this.state.loading}
                renderItem={this._renderGioPlacesListItem}
                keyExtractor={(item, index) => index.toString()}
              />


            </View>
          </View>
        </ScrollView>

      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
    userData: state.userDataReducer,
    isLogin: state.isLoginReducer,
    userToken: state.userTokenReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAirportListAction: () => dispatch(getAirportListAction()),
    setLoadingAction: (data) => dispatch(setLoadingAction(data)),
    userDataAction: (data) => dispatch(userDataAction(data)),
    isLoginAction: (data) => dispatch(isLoginAction(data)),
    userTokenAction: (data) => dispatch(userTokenAction(data)),

  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: "rgba(244,248,254,1)"
    backgroundColor: "#FDFDFD"
  },
  styleUserView: {
    height: 100,
    width: "100%",
    flexDirection: "row"
  },
  styleUserNameView: {
    width: "70%",
    height: "100%",
    justifyContent: "center"
  },
  styleUserImageView: {
    width: "30%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  styleUserPointsandWalletView: {
    height: 60,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  stylewinnerView: {
    height: 50,
    width: "48%",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "white",
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 5, height: 2 },
    elevation: 10
  },
  styleListSuperView: {
    height: ((screenWidth - 40) / 4) + 10,
    width: (screenWidth - 40) / 4,
    justifyContent: 'center',
    alignItems: 'center'
  },
  styleListItem: {
    height: 50,
    width: 50,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 5, height: 2 },
    elevation: 10
  }
});
