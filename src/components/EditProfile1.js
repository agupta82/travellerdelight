import React, { Component } from "react";
import {
  Animated,
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList,
  Image,
  TouchableHighlight,
  ScrollView,
  DeviceEventEmitter,
  Alert,
  SafeAreaView
} from "react-native";
import NavigationBar from "../Helper Classes/NavigationBar";
import Images from "../Helper Classes/Images";
import ActionSheet from "react-native-actionsheet";
import NavigationServices from './../Helper Classes/NavigationServices'
import { ifIphonex, getStatusBarHeight } from "react-native-iphone-x-helper";
import CustomTextInput from "../Helper Classes/Custom TextFiel/CustomTextInput";
import Spinner from "react-native-loading-spinner-overlay";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import DateTimePicker from "react-native-modal-datetime-picker";
import ImagePicker from 'react-native-image-picker';
import APIManager from '../Helper Classes/APIManager/APIManagerUpdated'
// import Constants from "../Helper Classes/Constants";
import { connect } from "react-redux";
import { updateUserProfileAction } from "../Redux/Actions";

const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class EditProfile1 extends Component {
  constructor(props) {
    super(props);
    this.ActionSheet = null;
    this.state = {
      isDateTimePickerVisible: false,
      titlesArr: ["Mr.", "Mrs.", "Ms.", "Cancel"],
      userData: null,
      selectedUserImgPath: null,
      spinner: false
    };

    this._renderPersonalDetailView = this._renderPersonalDetailView.bind(this);
    this._renderContactDetailsView = this._renderContactDetailsView.bind(this);
    this._renderAddressDetailsView = this._renderAddressDetailsView.bind(this);
    this._renderProfilePictureView = this._renderProfilePictureView.bind(this);
  }

  //MARK:-------------- Component Life cycle method -----------

  componentDidMount() {
    const user = this.props.userData
    newUserData = Object.assign({}, user)
    this.setState({
      userData: newUserData,
      selectedUserImgPath: newUserData.image
    });
    DeviceEventEmitter.addListener("citySelected", dict => {
      let data = this.state.userData;
      let address = data.address;
      address["city"] = dict.name;
      address.country["gta_code"] = dict.country_code;
      address.country["id"] = dict.country_id;
      data["address"] = address;
      this.setState({
        userData: data
      });
      //Actions.refresh();
    });

  }


  //MARK:----------------- Update User Profile ------------------

  APIUpdateUserProfile() {

    let userDict = this.state.userData

    let address= {
      "phone": userDict.address.phone,
      "address1": userDict.address.address1,
      "address2": userDict.address.address2,
      "city": userDict.address.city,
      "country_id": userDict.address.country.id,
      "zip": userDict.address.zip
    }

    // let param = new FormData();
    // param.append("image", this.state.selectedUserImgPath);
    // param.append("first_name", userDict.first_name,);
    // param.append("middle_name", userDict.middle_name,);
    // param.append("last_name", userDict.last_name);
    // param.append("address", address);

    let param = {
      "first_name": userDict.first_name,
      "middle_name": userDict.middle_name,
      "last_name": userDict.last_name,
      "address":address
    }

    this.props.updateUserProfileAction(param, this.props)


    // this.setState({spinner:true})
    // APIManager.updateUserProfile(param,(success,response)=>{
    //   this.setState({spinner:false})
    //   if(success)
    //   {
    //     console.log('Update API Response  '+ JSON.stringify(response))
    //     setTimeout(() => {
    //       Alert.alert('Alert','Profile updated successfully.',[{text:'Ok',onPress:()=>{
    //         this.props.updateSuccess()
    //       }}])
    //     }, 100);
    //   }
    //   else 
    //   {
    //     console.log('login failed' + response)
    //       setTimeout(() => {
    //         Alert.alert('Alert',response,[{text:'Ok',onPress:()=>{
    //           Actions.pop()
    //         }}])
    //       }, 100);
    //   }
    // })
  }

  validateFiels() {
    let userDict = this.state.userData


    if (String(userDict.first_name).trim().length == 0) {
      alert("Please Enter First Name.");
      return;
    }


    if (String(userDict.last_name).trim().length == 0) {
      alert("Please Enter Last Name.");
      return;
    }

    if (userDict.address.phone.trim().length == 0) {
      alert("Please enter phone number.");
      return;
    }

    if (userDict.address.phone.trim().length < 10) {
      alert("Please enter valid phone number");
      return;
    }

    if (userDict.address.address1.trim().length == 0) {
      alert("Please Enter Address line 1.");
      return;
    }

    if (userDict.address.address2.trim().length == 0) {
      alert("Please Enter Address line 2.");
      return;
    }

    if (userDict.address.city.trim().length == 0) {
      alert("Please Enter City.");
      return;
    }

    if (userDict.address.zip.trim().length == 0) {
      alert("Please Enter Postal Code.");
      return;
    }

    this.APIUpdateUserProfile()
  }

  showImagePicker() {
    // return
    // this.setState({ loading: true });
    const options = {
      rotation: 360,
      allowsEditing: true,
      noData: true,
      mediaType: "photo",
      maxWidth: 300,
      maxHeight: 300,
      storageOptions: {
        skipBackup: true
      }
    };
    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);
      // this.setState({ loading: false });
      if (response.didCancel) {
        //   console.log(JSON.stringify(source));
        console.warn("User cancelled image picker");
      } else if (response.error) {
        //  console.log(JSON.stringify(source));
        console.warn("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        //  console.log('User tapped custom button: ', response.customButton);
      } else {

        const source = { uri: response.uri };

        this.setState({
          selectedUserImgPath: source.uri,
        });
      }
    });
  }

  //MARK:----------------- Open ImagePicker Controller -----------------
  showImagePicker2() {
    // ImagePicker.showImagePicker(null, (response) => {
    //   console.log('Response = ', response);

    //   if (response.didCancel) {
    //     console.log('User cancelled image picker');
    //   } else if (response.error) {
    //     console.log('ImagePicker Error: ', response.error);
    //   } else if (response.customButton) {
    //     console.log('User tapped custom button: ', response.customButton);
    //   } else {
    //     const source = { uri: response.uri };

    //     // You can also display the image using data:
    //     // const source = { uri: 'data:image/jpeg;base64,' + response.data };

    //     this.setState({
    //       selectedUserImgPath: source.uri,
    //     });
    //   }
    // });
  }


  //MARK:-------------- Date Picker Methods ----------

  //   _showDateTimePicker = (index) => );

  _hideDateTimePicker = index =>
    this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    console.log("A date has been picked: ", date.dateString);

    this._hideDateTimePicker();
  };

  //MARK:--------------- Action Sheet Show Method --------------
  showActionSheet(index) {
    this.ActionSheet.show();
  }

  //MARK:-------------- Render Profile Picture View   --------------
  _renderProfilePictureView() {
    const data = this.state.userData;
    return (
      <View
        style={{
          height: 150,
          width: "100%",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white"
        }}
      >
        <View style={{ height: 120, width: 120, borderRadius: 60 }}>
          <Image
            style={{ height: '100%', width: '100%', borderRadius: 60, }}
            source={this.state.selectedUserImgPath ? { uri: this.state.selectedUserImgPath } : Images.imgprofiledefault}
            resizeMode="cover"
          />
          <TouchableHighlight
            style={{
              height: 30,
              width: 30,
              position: "absolute",
              alignSelf: "flex-end",
              bottom: 0
            }}
            underlayColor='transparent'
            onPress={() => {
              this.showImagePicker()
            }}
          >
            <Image
              style={{ height: 25, width: 25 }}
              source={Images.imgCamera}
              resizeMode="contain"
            />
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  //MARK:-  ------------ On TextChange Method ---------------

  onTextChange(value, key, isAddress) {
    let data = this.state.userData;
    if (isAddress) {
      let address = data.address;
      address[key] = value;
      data.address = address;
    } else {
      data[key] = value;
    }
    this.setState({
      userData: data
    });
  }

  //MARK:---------------- Render title method -----------------
  _renderTitleView(title) {
    return (
      <View style={styles.styleTitleViewContainer}>
        <Text style={{ fontSize: 16, fontWeight: "bold", color: "black" }}>
          {title}
        </Text>
      </View>
    );
  }

  //MARK:-------------- Render Personal Details View------------------
  _renderPersonalDetailView() {
    const userDict = this.state.userData;
    return (
      <View>
        {/* <CustomTextInput
          placeholder="Title"
          value=""
          tintColor="rgb(0,114,198)"
          isEditable={false}
          // icon={Images.imguserprofile}
          onChangeText={text => {}}
          onResponderStart={() => this.showActionSheet()}
        /> */}

        <CustomTextInput
          placeholder="First Name"
          value={userDict.first_name}
          tintColor="rgb(0,114,198)"
          isEditable={true}
          // icon={Images.imguserprofile}
          onChangeText={text => this.onTextChange(text, "first_name", false)}
        />

        <CustomTextInput
          placeholder="Middle Name"
          value={userDict.middle_name}
          tintColor="rgb(0,114,198)"
          // icon={Images.imguserprofile}
          isEditable={true}
          onChangeText={text => this.onTextChange(text, "middle_name", false)}
        />

        <CustomTextInput
          placeholder="Last Name"
          value={userDict.last_name}
          tintColor="rgb(0,114,198)"
          isEditable={true}
          // icon={Images.imguserprofile}
          onChangeText={text => this.onTextChange(text, "last_name", false)}
        />

        {/* <View style={styles.styleDobAndNationalityContainer}>
          <CustomTextInput
            style={{ height: 55, width: "40%" }}
            placeholder="DOB"
            value=""
            tintColor="rgb(0,114,198)"
            icon={Images.imgCalender}
            onChangeText={text => {}}
            isEditable={false}
            onResponderStart={() => {
              this.setState({
                isDateTimePickerVisible: true
              });
            }}
          />

          <CustomTextInput
            style={{ height: 55, width: "40%" }}
            placeholder="Nationality"
            value=""
            tintColor="rgb(0,114,198)"
            isEditable={true}
            icon={Images.imgNationatily}
            onChangeText={text => {}}
          />
        </View> */}

        {/* <View style={styles.styleGenderContainerView}>
          <Text style={{ fontSize: 14, color: "black", fontWeight: "600" }}>
            Gender
          </Text>

          <View style={styles.styleGenderView}>
            <TouchableHighlight
              style={styles.styleGenderMarkBtn}
              underlayColor="transparent"
              onPress={() => {}}
            >
              <Image
                style={{ height: 15, width: 15 }}
                source={Images.imguncheck}
                resizeMode="contain"
              />
            </TouchableHighlight>

            <Text style={{ fontSize: 12, color: "black", marginLeft: 10 }}>
              Male
            </Text>
          </View>

          <View style={styles.styleGenderView}>
            <TouchableHighlight
              style={styles.styleGenderMarkBtn}
              underlayColor="transparent"
              onPress={() => this.onChangeText(index, "Female", "gender")}
            >
              <Image
                style={{ height: 15, width: 15 }}
                source={Images.imguncheck}
                resizeMode="contain"
              />
            </TouchableHighlight>

            <Text style={{ fontSize: 12, color: "black", marginLeft: 10 }}>
              Female
            </Text>
          </View>
        </View> */}
      </View>
    );
  }

  //MARK:-------------- Render Address Details View ---------------

  citySelectedHandler = cityDict => {
    console.log("cityDictD", cityDict.name)
    this.setState(prevState => ({
      userData: {
        ...prevState.userData,           // copy all other key-value pairs of userData object
        address: {                     // specific object of address object
          ...prevState.userData.address,   // copy all address key-value pairs
          city: cityDict.name          // update value of specific key
        }
      }
    }))
  }

  _renderAddressDetailsView() {
    const address = this.state.userData.address;
    return (
      <View>
        <View style={styles.styleAdultOrChildViewContainer}>
          {this._renderTitleView("Address Details")}
          <CustomTextInput
            placeholder="Address Line 1"
            value={address.address1 ? address.address1 : ""}
            tintColor="rgb(0,114,198)"
            isEditable={true}
            icon={Images.imgAddress}
            onChangeText={text => this.onTextChange(text, "address1", true)}
          />

          <CustomTextInput
            placeholder="Address Line 2"
            isEditable={true}
            value={address.address2 ? address.address2 : ""}
            tintColor="rgb(0,114,198)"
            icon={Images.imgAddress}
            onChangeText={text => this.onTextChange(text, "address2", true)}
          />

          <CustomTextInput
            placeholder="City"
            value={address.city ? address.city : ""}
            tintColor="rgb(0,114,198)"
            isEditable={false}
            icon={Images.imgCity}
            onChangeText={text => {
              //   let dict = this.state.addressDict;
              //   dict.city = text;
              //   this.setState({
              //     addressDict: dict
              //   });
            }}
            onResponderStart={() => {
              NavigationServices.navigate('SearchAirport', { forFlightSearch: false, onCitySelect: this.citySelectedHandler });
            }}
          />

          <CustomTextInput
            placeholder="Postal Code"
            value={address.zip ? address.zip : ""}
            tintColor="rgb(0,114,198)"
            isEditable={true}
            keyboardType="number-pad"
            contentType="postalCode"
            maxLength={6}
            icon={Images.imgZipCode}
            onChangeText={text => this.onTextChange(text, "zip", true)}
          />

          <CustomTextInput
            placeholder="Country Code"
            value={address.country.gta_code ? address.country.gta_code : ""}
            tintColor="rgb(0,114,198)"
            isEditable={false}
            icon={Images.imgCountryCode}
            onChangeText={text => { }}
          />
        </View>
      </View>
    );
  }

  //MARK: ----------------- Render Contact Details View --------------
  _renderContactDetailsView() {
    const userData = this.state.userData;
    return (
      <View>
        <View style={styles.styleAdultOrChildViewContainer}>
          {this._renderTitleView("Contact Details")}
          <CustomTextInput
            placeholder="Email"
            value={userData.email}
            tintColor="rgb(0,114,198)"
            isEditable={false}
            icon={Images.imgmail}
            contentType="postalCode"
            onChangeText={text => this.onTextChange(text, "email", false)}
          />

          <CustomTextInput
            placeholder="Phone Number"
            isEditable={true}
            value={userData.address.phone ? userData.address.phone : ""}
            tintColor="rgb(0,114,198)"
            keyboardType="number-pad"
            contentType="telephoneNumber"
            maxLength={10}
            icon={Images.imgphone}
            onChangeText={text => this.onTextChange(text, "phone", true)}
          />
        </View>
      </View>
    );
  }

  //MARK:---------------- Render Save Button -------------
  _renderSaveBtn() {
    return (
      <View style={{ padding: 20 }}>
        <TouchableHighlight
          style={[
            styles.styleAdultOrChildViewContainer,
            {
              backgroundColor: "rgb(0,114,198)",
              alignItems: "center",
              marginBottom: 15
            }
          ]}
          underlayColor="transparent"
          onPress={() => {
            this.validateFiels()
          }}
        >
          <Text style={{ fontSize: 18, color: "white", fontWeight: "bold" }}>
            SAVE
          </Text>
        </TouchableHighlight>
      </View>
    );
  }

  //MARK:---------------- Render Method -------------------------
  render() {
    if (!this.state.userData) {
      return (
        <View style={styles.container}>
          <StatusBar barStyle="light-content" />
          <NavigationBar title={"Edit Profile"} showBack={true} />
        </View>
      );
    }
    return (
      <SafeAreaView style={styles.container}>
        <Spinner
          visible={this.props.isLoading}
          textContent={"Loading..."}
          textStyle={{ color: 'white' }}
        />
        <StatusBar barStyle="light-content" />
        <NavigationBar title={"Edit Profile"} showBack={true} />
        {this._renderProfilePictureView()}
        <KeyboardAwareScrollView>
          <View style={{ margin: 20 }}>
            <View style={styles.styleAdultOrChildViewContainer}>
              {this._renderTitleView("Personal Details")}

              {this._renderPersonalDetailView()}
            </View>

            {this._renderAddressDetailsView()}

            {this._renderContactDetailsView()}
          </View>
        </KeyboardAwareScrollView>
        {this._renderSaveBtn()}
        <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
        />
        <ActionSheet
          ref={o => (this.ActionSheet = o)}
          title={"Select Title"}
          options={this.state.titlesArr}
          cancelButtonIndex={3}
          //   destructiveButtonIndex={1}
          onPress={index => { }}
        />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
    userData: state.userDataReducer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateUserProfileAction: (data, props) => dispatch(updateUserProfileAction(data, props))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(244,248,254,1)"
  },
  styleAdultOrChildViewContainer: {
    width: "100%",
    padding: 15,
    shadowColor: "gray",
    shadowOpacity: 0.5,
    shadowOffset: { width: 1, height: 1 },
    shadowRadius: 5,
    backgroundColor: "white",
    marginTop: 10,
    borderRadius: 10,
    marginBottom: 10
  },
  styleTitleViewContainer: {
    height: 30,
    width: "100%",
    justifyContent: "center",
    marginLeft: 5
  },
  styleDobAndNationalityContainer: {
    flexDirection: "row",
    height: 55,
    width: "100%",
    justifyContent: "space-between"
  },
  styleGenderContainerView: {
    flexDirection: "row",
    height: 40,
    alignItems: "center",
    padding: 5
  },
  styleGenderView: {
    marginLeft: 15,
    flexDirection: "row",
    alignItems: "center"
  },
  styleGenderMarkBtn: {
    height: 30,
    width: 30,
    justifyContent: "center",
    alignItems: "center"
  }
});
