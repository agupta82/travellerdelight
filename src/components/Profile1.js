import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  DeviceEventEmitter,
  TouchableHighlight,
  Share,
  ScrollView
} from "react-native";
// akash.kulshreshtha@enukesoftware.com
// 123456
import NavigationServices from './../Helper Classes/NavigationServices'
import NavigationBar from "../Helper Classes/NavigationBar";
import Images from "../Helper Classes/Images";
import Constants from "../Helper Classes/Constants";
import Spinner from "react-native-loading-spinner-overlay";
import StringConstants from "../Helper Classes/StringConstants";
const screenSize = Dimensions.get("window");
import { connect } from "react-redux";
import { doLogoutAction, userDataAction, isLoginAction, userTokenAction } from '../Redux/Actions'

class profile1 extends Component {
  constructor(props) {
    if(!props.isLogin){
      NavigationServices.replace("Authentication")
    }
    super(props);
    this.state = {
      name: "username",
      email: "useremail",
      phone: "userphone",
      userData: null,
      isLogin: false,
      spinner: false
    };
  }

  componentDidMount() {

    // if (this.props.userData && this.props.isLogin) {
    //   this.setState({
    //     isLogin: true,
    //     name: this.props.userData.full_name,
    //     email: this.props.userData.email,
    //     phone: this.props.userData.address.phone,
    //     userData: this.props.userData
    //   });
    // }

    DeviceEventEmitter.addListener(StringConstants.IS_LOGOUTEVENT, success => {
      if (success) {
        this.setState({
          isLogin: false,
          userData: null
        })
        // Actions.refresh();
      } else {
        setTimeout(() => {
          alert("Unable to Logout");
        }, 100)
      }
    })
  }

  onProfileUpdate() {
    NavigationServices.goBack();
    // this.setState({
    //   isLogin: true,
    //   name: Constants.userData.full_name,
    //   email: Constants.userData.email,
    //   phone: Constants.userData.address.phone,
    //   userData: Constants.userData
    // });
    //Actions.refresh()
  }

  onLoginSuccessHandler() {
    // Actions.pop()
    console.log("in login success handler")
    this.setState({
      // isLogin: true,
      // name: Constants.userData.full_name,
      // email: Constants.userData.email,
      // phone: Constants.userData.address.phone,
      // userData: Constants.userData
    });
  }

  //MARK:----------------- Logout API Calling --------------

  // APILogout(){
  //   this.setState({spinner:true})
  //   APIManager.logoutUser((success,response)=>{
  //     this.setState({spinner:false})
  //     if(success)
  //     {
  //       this.setState({
  //         isLogin:false,
  //         userData:null
  //       })
  //       DeviceEventEmitter.emit(StringConstants.IS_LOGINEVENT,false)
  //       Actions.refresh()
  //     }
  //     else 
  //     {
  //       console.log('Logout Error  '+JSON.stringify(response))
  //       setTimeout(() => {
  //         alert(response)
  //       }, 100);
  //     }
  //   })
  // }


  cardItemRow(itemname, icon) {
    return (
      <TouchableHighlight
        underlayColor="transparent"
        onPress={() => {
          if (itemname == 'Share') {
            Share.share({
              message: this.state.text + 'http://kokotala.com',
              url: 'http://kokotala.com',
              title: 'Wow, did you see that?'
            }, {
              // Android only:
              dialogTitle: 'Share Invitation using',
              // iOS only:
              excludedActivityTypes: [
                'com.apple.UIKit.activity.PostToTwitter'
              ]
            })
          }
          else {
            alert(itemname);
          }

        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            borderBottomColor: "rgb(0,114,208)",
            borderBottomWidth: 0.2,
            padding: 20,
            height: 60
          }}
        >
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Image style={{ height: 20, width: 20 }} source={icon} />
            <Text style={{ marginLeft: 20 }}>{itemname}</Text>
          </View>
          <Image
            style={{ width: 7, height: 15 }}
            source={Images.imgrightarrow}
          />
        </View>
      </TouchableHighlight>
    );
  }

  cardComponent() {
    return (
      <View style={styles.itemContainer}>
        {this.cardItemRow("Help", Images.imgquestionmark)}
        {this.cardItemRow("Share", Images.imgshare)}
        {this.cardItemRow("Rate our app", Images.imgstar)}
        {this.cardItemRow("Settings", Images.imgsettings)}
      </View>
    );
  }

  profileComponent() {
    return (
      <View
        style={{
          width: screenSize.width,
          height: 250,
          backgroundColor: "white",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Image
          style={{ height: 90, width: 90 }}
          source={Images.imguserprofile}
        />
        {this.props.isLogin && this.props.userData ? (
          <View>
            <Text style={{ fontSize: 16, fontWeight: "700", marginTop: 15, textAlign: 'center' }}>
              {this.props.userData.first_name}
            </Text>
            <Text style={{ marginTop: 10, fontSize: 13, textAlign: 'center' }}>
              {this.props.userData.email}
            </Text>
            <Text style={{ marginTop: 5, fontSize: 13, textAlign: 'center' }}>
              {this.props.userData.address.phone}
            </Text>
          </View>
        ) : (
            <Text style={{ marginTop: 15, textAlign: 'center' }}>You are not logged in</Text>
          )}

        {this.props.isLogin && this.props.userData ? <TouchableHighlight
          style={{
            height: 30,
            width: 30,
            position: "absolute",
            alignSelf: "flex-end",
            top: 5,
            right: 5
          }}
          underlayColor='transparent'
          onPress={() => {
            NavigationServices.navigate("EditProfile")
            //TODO 
            //  Actions.EditProfile({userData:this.state.userData,updateSuccess:()=>this.onProfileUpdate()})
          }}
        >
          <Image
            style={{ height: 25, width: 25 }}
            source={Images.imgpencilfilled}
            resizeMode="contain"
          />
        </TouchableHighlight> : <View />}

      </View>
    );
  }

  addressComponent() {
    const address = this.props.userData.address
    return (
      <View style={styles.itemContainer}>
        <Text style={{ marginTop: 15, marginLeft: 15, fontSize: 16, color: 'black', fontWeight: 'bold' }}>
          Address Details
      </Text>
        <View style={{ padding: 15, borderBottomColor: 'grey', borderBottomWidth: 1 }}>
          <Text style={{ color: 'grey', fontWeight: '600', fontSize: 15 }}>
            Address Line 1
        </Text>
          <Text style={{ marginTop: 10, color: 'black', fontSize: 13 }}>
            {address.address1}
          </Text>
        </View>
        <View style={{ padding: 15, borderBottomColor: 'grey', borderBottomWidth: 1 }}>
          <Text style={{ color: 'grey', fontWeight: '600', fontSize: 15 }}>
            Address Line 2
        </Text>
          <Text style={{ marginTop: 10, color: 'black', fontSize: 13 }}>
            {address.address2}
          </Text>
        </View>
        <View style={{ padding: 15 }}>
          <Text style={{ fontWeight: '600' }}>
            City Name - {address.city}
          </Text>
          <Text style={{ fontWeight: '600', marginTop: 10 }}>
            Postal Code- {address.zip}
          </Text>
        </View>
      </View>
    )
  }

  _renderBackgroundImage() {
    return (
      <Image source={Images.imghomebg}
        style={{
          height: screenHeight / 2.5,
          width: screenWidth,
          resizeMode: "stretch"
        }}></Image >
    )
  }

  render() {
    if (!this.props.isLogin) {
      //NavigationServices.navigate("Authentication")
    }
    let loginText = "Login";
    let logoutText = "Logout";

    console.log("profileUserData:", this.props.userData)
    return (
      <View style={styles.container}>
        <Spinner
          visible={this.props.isLoading}
          textContent={'Loading...'}
          textStyle={{ color: 'white' }}
        />
        <StatusBar barStyle={"light-content"} />
        <NavigationBar title="Profile" />
        <ScrollView>
          {this.profileComponent()}
          {this.cardComponent()}
          {this.props.isLogin && this.props.userData && this.props.userData.address ? this.addressComponent() : <View />}
          <TouchableHighlight
            style={styles.login_button}
            underlayColor='transparent'
            onPress={() => {
              if (this.props.isLogin && this.props.userData) {
                // AuthManagement.APILogout()
                this.props.doLogoutAction()
              }
              else {
                NavigationServices.navigate('Authentication')
              }
            }}>

            <Text style={{ color: 'white', fontSize: 16, fontWeight: '700' }}>
              {this.props.isLogin && this.props.userData ? logoutText : loginText}
            </Text>
          </TouchableHighlight>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  // console.log("PROFILE_STATE:" + JSON.stringify(state))
  return {
    isLoading: state.isLoadingReducer,
    userData: state.userDataReducer,
    isLogin: state.isLoginReducer,
    userToken: state.userTokenReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userDataAction: (data) => dispatch(userDataAction(data)),
    isLoginAction: (data) => dispatch(isLoginAction(data)),
    userTokenAction: (data) => dispatch(userTokenAction(data)),
    doLogoutAction: () => dispatch(doLogoutAction())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(profile)

const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(244,248,254,1)",
    flex: 1
  },
  buttoncontainer: {
    backgroundColor: "rgba(244,248,254,1)",
    flexDirection: "row",
    margin: 20,
    padding: 20,
    height: 100,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: 2 },
    elevation: 10
    // backgroundColor:'white',
  },
  login_button: {
    height: 50,
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: "rgb(0,114,198)",
    borderRadius: 10,
    marginTop: 15,
    elevation: 4,
    marginBottom: 10,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    shadowRadius: 4,
  },
  signup_button: {
    width: "50%",
    height: 40,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10
  },
  itemContainer: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    shadowRadius: 4,
    elevation: 6
  }
});
