
import React, { Component } from "react";
import { Text, Platform, View, StyleSheet, ScrollView, Image, Dimensions, ImageBackground, TouchableOpacity } from "react-native";
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import { Card } from 'react-native-elements';

import NavigationServices from './../Helper Classes/NavigationServices'
import NavigationBar from '../Helper Classes/NavigationBar'
import APIManager from '../Helper Classes/APIManager/APIManagerUpdated'
import Spinner from 'react-native-loading-spinner-overlay';
import StringConstants from '../Helper Classes/StringConstants';
import Images from "../Helper Classes/Images";
import Constants from "../Helper Classes/Constants";
//import HotelBookingDetails from "./Hotel/HotelBookingDetail/HotelBookingDetails";
import { connect } from "react-redux";
import { getAirportListAction, setLoadingAction, userDataAction, isLoginAction, userTokenAction } from '../Redux/Actions'
import { colors } from "../Helper Classes/Colors";
import CommonHeader from '../Helper Classes/CommonHeader'


const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
      banners: [{
        image: Images.imgplane1,
        title: "Up to 25% instant discount on international flights",
        validity: "Valid Till 25 December"
      }, {
        image: Images.imgplane2,
        title: "Up to 15% instant discount on international flights",
        validity: "Valid Till 31 January"
      }, {
        image: Images.imgplane1,
        title: "Up to 25% instant discount on international flights",
        validity: "Valid Till 25 December"
      }],
      destinations: [{
        image: Images.imgfrance,
        name: "France"
      }, {
        image: Images.imgsingapore,
        name: "Singapore"
      }, {
        image: Images.imgtaj,
        name: "Taj"
      }, {
        image: Images.imgliberty,
        name: "Statue of Liberty"
      },]
    }
  }
  //-----------------Components Life cycle Methods ---------------

  componentDidMount() {
    // console.log("home component did mount");
    this.props.getAirportListAction();
  }

  render() {
    // console.log("home component did mount");

    let { banners, destinations } = this.state;
    let { isLoading } = this.props;
    return (
      <View style={styles.container}>
        <Spinner visible={isLoading}
          textContent={"Loading..."}
          textStyle={{ color: 'white' }}
        />
        {/* <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} /> */}
        <Image source={Images.imghomebg}
          style={{
            height: screenHeight / 2.5,
            width: screenWidth,
            resizeMode: "stretch"
          }}></Image >
        <View style={styles.mainContainer}>
          {/* <View style={styles.header}>
            <TouchableOpacity style={styles.headerButton}>
            </TouchableOpacity>
            <Text style={styles.headerTitle}>Traveller Delight</Text>
            <TouchableOpacity style={styles.headerButton}>
              <Image source={Images.imgnotifications} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
            </TouchableOpacity>
          </View> */}
          <CommonHeader title={"Traveller Delight"} noBack={true} isTransparent={true}
            fireEvent={() => {
              
            }}
            rightImg={Images.imgnotifications} />
          <View style={{
            width: screenWidth,
            // height: 470
          }}>
            <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom:100}}>
              <View style={styles.mainButtonContainer}>
                <TouchableOpacity style={styles.mainButton} onPress={() => {
                  NavigationServices.navigate('FlightSearch');
                }}>
                  <Image
                    source={Images.imgsearchflight}
                    style={{ height: 35, width: 35, resizeMode: "contain" }}></Image>
                  <Text style={styles.mainButtontext}>Flight Search</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.mainButton} onPress={() => {
                  NavigationServices.navigate('HotelSearch');
                }}>
                  <Image
                    source={Images.imghotelsearch}
                    style={{ height: 35, width: 35, resizeMode: "contain" }}></Image>
                  <Text style={styles.mainButtontext}>Hotel Search</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.bannerContainer}>
                <ScrollView horizontal={true}
                  showsHorizontalScrollIndicator={false}>
                  {
                    banners.map((banner, index) => (
                      <View key={index} style={{
                        marginLeft: 6,
                        marginRight: 6,
                      }}>
                        <ImageBackground style={styles.bannerImage} source={banner.image}>
                          <View style={styles.innerFrame}>
                            <Text style={styles.bannerTitle}>{banner.title}</Text>
                            <Text style={styles.bannerValidity}>{banner.validity}</Text>
                          </View>
                        </ImageBackground>
                        <View style={styles.bannerButton} >
                          <TouchableOpacity style={{
                            flex: 1, width: 130,
                            justifyContent: "center", alignItems: "center"
                          }} onPress={() => {}}>
                            <Text style={styles.bannerButtonText}>VIEW DETAILS</Text>
                          </TouchableOpacity>
                        </View>
                      </View>

                    ))
                  }
                </ScrollView>
              </View>
              <View style={{
                width: "100%", height: 40,
                marginTop: 10,
                // backgroundColor: "red",
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                paddingLeft: 14,
                paddingRight: 14,
              }}>
                <Text style={{
                  color: colors.colorBlack,
                  fontSize: 14,
                  fontWeight: "500"
                }}>Explore Highlighted Destinations</Text>
                <TouchableOpacity onPress={() => alert("Viewing all")}>
                  <Text style={{
                    color: colors.colorBlack,
                    fontSize: 14,
                    fontWeight: "300"
                  }}>VIEW ALL</Text>
                </TouchableOpacity>
              </View>
              <View style={{
                width: screenWidth,
                marginTop: 10,
                paddingLeft: 6,
                paddingRight: 6
              }}>
                <ScrollView horizontal={true}
                  showsHorizontalScrollIndicator={false}>
                  {
                    destinations.map((destination, index) => (
                      <View key={index} style={{
                        marginLeft: 6,
                        marginRight: 6,
                      }}>
                        <ImageBackground style={{
                          width: (screenWidth / 2) - 45,
                          height: 200,
                          borderRadius: 10,
                          overflow: "hidden"
                        }} source={destination.image}>
                          <View style={{
                            flex: 1,
                            backgroundColor: Platform.OS == "ios" ? 'rgba(0, 0, 0, .7)' : 'rgba(0, 0, 0, .5)',
                            justifyContent: "flex-end"
                          }}>
                            <Text style={{
                              fontWeight: "500",
                              fontSize: 14,
                              color: colors.colorWhite,
                              margin: 10
                            }}>{destination.name}</Text>
                          </View>
                        </ImageBackground>
                      </View>
                    ))
                  }
                </ScrollView>
              </View>
              <View style={{ height: 50 }}></View>
            </ScrollView>
          </View>
        </View>
      </View >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
    userData: state.userDataReducer,
    isLogin: state.isLoginReducer,
    userToken: state.userTokenReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAirportListAction: () => dispatch(getAirportListAction()),
    setLoadingAction: (data) => dispatch(setLoadingAction(data)),
    userDataAction: (data) => dispatch(userDataAction(data)),
    isLoginAction: (data) => dispatch(isLoginAction(data)),
    userTokenAction: (data) => dispatch(userTokenAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home)

const styles = StyleSheet.create({
  bannerButtonText: {
    fontSize: 14,
    fontWeight: "500",
    color: colors.colorWhite
  },
  bannerButton: {
    backgroundColor: colors.colorBlue,
    height: 40, width: 130,
    justifyContent: "center", alignItems: "center",
    position: "absolute",
    bottom: 10,
    left: 14,
    zIndex: 2,
    borderRadius: 8,
    overflow: "hidden"
  },
  bannerValidity: {
    fontWeight: "400",
    fontSize: 14,
    color: colors.colorWhite,
    lineHeight: 25,
    marginTop: 5
  },
  bannerTitle: {
    fontWeight: "700",
    fontSize: 16,
    color: colors.colorWhite,
    lineHeight: 25,
  },
  bannerImage: {
    height: 130,
    width: screenWidth - 70,
    borderRadius: 10,
    backgroundColor: "red",
    overflow: 'hidden',
    resizeMode: "cover",
    zIndex: 0
  },
  bannerContainer: {
    height: 160,
    marginTop: 40,
    paddingLeft: 6,
    paddingRight: 7
  },
  innerFrame: {
    position: "absolute",
    top: 0,
    left: 0,
    zIndex: 1,
    height: "100%",
    width: "100%",
    paddingLeft: 14,
    paddingTop: 14,
    backgroundColor: Platform.OS == "ios" ? 'rgba(0, 0, 0, .7)' : 'rgba(0, 0, 0, .5)',
  },
  mainButtonContainer: {
    width: "100%",
    paddingLeft: 12, paddingRight: 12,
    flexDirection: "row",
    marginTop: 25,
    justifyContent: "space-between"
  },
  mainButtontext: {
    paddingLeft: 15,
    color: colors.colorBlack,
    fontSize: 14,
    fontWeight: "500"
  },
  mainButton: {
    height: 60,
    width: (screenWidth / 2) - 20,
    backgroundColor: colors.colorWhite,
    borderRadius: 8,
    borderWidth: .5,
    borderColor: "lightgrey",
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: 12
  },
  header: {
    height: 45,
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  mainContainer: {
    position: "absolute",
    top: Platform.OS == "ios" ? (getStatusBarHeight() + 5) : 0,
    width: screenWidth,
    height: screenHeight,
    // flex: 1,
    // left: 0,
    // backgroundColor: "red"
  },
  headerButton: {
    height: "100%",
    width: 50,
    // backgroundColor: "blue",
    justifyContent: "center",
    alignItems: "center"
  },
  headerTitle: {
    color: colors.colorWhite,
    fontSize: 16,
    fontWeight: "600"
  },
  container: {
    flex: 1,
    //backgroundColor: "rgba(244,248,254,1)"
    backgroundColor: "#FDFDFD"
  },
  styleUserView: {
    height: 100,
    width: "100%",
    flexDirection: "row"
  },
  styleUserNameView: {
    width: "70%",
    height: "100%",
    justifyContent: "center"
  },
  styleUserImageView: {
    width: "30%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  styleUserPointsandWalletView: {
    height: 60,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  stylewinnerView: {
    height: 50,
    width: "48%",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "white",
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 5, height: 2 },
    elevation: 10
  },
  styleListSuperView: {
    height: ((screenWidth - 40) / 4) + 10,
    width: (screenWidth - 40) / 4,
    justifyContent: 'center',
    alignItems: 'center'
  },
  styleListItem: {
    height: 50,
    width: 50,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 5, height: 2 },
    elevation: 10
  }
});
