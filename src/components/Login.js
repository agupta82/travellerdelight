import React, { Component, Fragment } from 'react'
import {
  View,
  Text,
  Image,
  Dimensions,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableHighlight,
  SafeAreaView,
  DeviceEventEmitter
} from 'react-native'
import Images from '../Helper Classes/Images'

import NavigationServices from './../Helper Classes/NavigationServices'

import Spinner from 'react-native-loading-spinner-overlay';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";


import { connect } from "react-redux";
import { doLoginAction } from '../Redux/Actions'

const screenWidth = Dimensions.get('window').width
class login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: 'akash.kulshreshtha@enukesoftware.com',
      password: '123456',
      spinner: false,
      onLoginSuccess: null
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    const onLoginSuccess = navigation.getParam('onLoginSuccess', '');
    this.setState({
      onLoginSuccess: onLoginSuccess
    })
  }

  loginUser() {

    let param = {
      email: this.state.email,
      password: this.state.password
    }
    // this.setState({spinner:true})

    // loginUser(param)
    // .then((response)=>{
    //   this.setState({spinner:false})
    //   if(response.code.code == 200){
    //     if(response != null && response.data!=null && response.data.token != null &&  response.data.user != null){
    //          console.log("Login Success")   
    //          Constants.isLogin = true;
    //          Constants.userData = response.data.user;
    //          Constants.userToken = response.data.token;
    //          saveJsonString(StringConstants.USER_DATA,
    //            JSON.stringify(response.data)
    //          )
    //          saveJsonString(StringConstants.IS_LOGIN,"TRUE")
    //         DeviceEventEmitter.emit(StringConstants.IS_LOGINEVENT,true)
    //         Actions.pop( this.props.onLoginSuccess() )
    //         }else{ 
    //           console.log("Data not found" + JSON.stringify(response))
    //           setTimeout(()=>{alert("User data not found")},1000)
    //         }
    //   }else{
    //          console.log("login failed" + JSON.stringify(response));
    //          setTimeout(()=>{alert("Login failed " +response.code.description)},1000);
    //   }
    // })
    // .catch((err)=>{
    //   this.setState({spinner:false})
    //   console.log("failed")
    //   console.log(err)
    //   setTimeout(()=>{alert(err)},1000)
    // })

    this.props.doLoginAction(param, this.props)

    // APIManager.loginUserApi(param, (success, response) => {
    //   this.setState({spinner:false})
    //   if (success) {
    //     console.log('login success');
    //     DeviceEventEmitter.emit(StringConstants.IS_LOGINEVENT,true)
    //     Actions.pop( this.props.onLoginSuccess() )    
    //   } else {
    //     console.log('login failed' + response)
    //     setTimeout(() => {
    //       alert(JSON.stringify(response));
    //     }, 100);
    //   }
    // })

  }

  isEmailValid(text) {
    console.log(text)
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    if (reg.test(text) === false) {
      console.log('Email is Not Correct')
      return false
    } else {
      console.log('Email is Correct')
      return true
    }
  }

  validateFields() {
    if (this.state.email.trim().length == 0) {
      alert('Please Enter your email')
      return
    }

    if (this.state.password.trim().length == 0) {
      alert('Please Enter Your Password')
      return
    }

    if (!this.isEmailValid(this.state.email)) {
      alert('Please enter an valid email Id.')
      return
    }
    this.loginUser()
  }

  textInputPassword() {
    return (
      <View style={styles.textinputcontainer}>
        <View style={styles.textinputimageview}>
          <Image
            source={Images.imgkey}
            style={{ tintColor: 'rgb(0,114,198)', height: 13, width: 13 }}
          />
        </View>
        <View style={{ width: '90%', height: 40, justifyContent: 'center' }}>
          <TextInput
            style={{ marginLeft: 15 }}
            value={this.state.password}
            onChangeText={text => this.setState({ password: text })}
            placeholder={"Password"}
            autoCapitalize={"none"}
            autoCorrect={false}
            secureTextEntry
            placeholderTextColor={'grey'}
          />
        </View>
      </View>
    )
  }

  textInputEmail() {
    return (
      <View style={styles.textinputcontainer}>
        <View style={styles.textinputimageview}>
          <Image
            source={Images.imgemail}
            style={{ tintColor: 'rgb(0,114,198)', height: 18, width: 18 }}
          />
        </View>
        <View style={{ width: '90%', height: 40, justifyContent: 'center' }}>
          <TextInput
            style={{ marginLeft: 15 }}
            value={this.state.email}
            onChangeText={text => this.setState({ email: text })}
            placeholder={'Email'}
            autoCapitalize={"none"}
            keyboardType={'email-address'}
            autoCorrect={false}
            placeholderTextColor={'grey'}
          />
        </View>
      </View>
    )
  }

  render() {
    return (

      <Fragment>
        <SafeAreaView style={{ flex: 1, backgroundColor: 'rgb(244,248,254,1)' }}>
          <Spinner
            visible={this.props.isLoading}
            textContent={'Loading...'}
            textStyle={{ color: 'white' }}
          />
          <StatusBar barStyle='light-content' />
          <KeyboardAwareScrollView style={{ flex: 1 }}>
            <Image
              source={Images.logintopImage}
              style={{ height: 200, width: screenWidth + 5, marginTop: -30 }}
              resizeMode='contain'
            />
            <TouchableHighlight
              style={{ position: "absolute", height: 30, width: 30, marginTop: 15, marginLeft: 15 }}
              underlayColor={"transparent"}
              onPress={() => NavigationServices.goBack()}
            >
              <Image
                tintColor={"#fff"}
                source={Images.imgback}
                style={{ height: 25, width: 25, tintColor: "white" }}
                resizeMode="contain"
              />
            </TouchableHighlight>
            <Text
              style={{
                color: 'black',
                fontSize: 22,
                fontWeight: '200',
                alignSelf: 'center',

              }}
            >
              LOGIN
        </Text>
            <View style={styles.innercontent}>
              {this.textInputEmail()}
              {this.textInputPassword()}
              {/* <TextField
            label='Email'
            autoCapitalize={false}
            autoCorrect={false}
            value={this.state.email}
            onChangeText={email => this.setState({ email: email })}
          />
          <TextField
            autoCapitalize={false}
            autoCorrect={false}
            label='password'
            value={this.state.password}
            secureTextEntry
            onChangeText={password => this.setState({ password: password })}
          /> */}
              <TouchableHighlight
                underlayColor='transparent'
                onPress={() => {
                   NavigationServices.replace('ForgotPassword')
                }}
              >
                <Text style={{ marginTop: 10, color: 'grey' }}>Forgot Password</Text>
              </TouchableHighlight>
              <TouchableHighlight
                style={styles.signupbutton}
                underlayColor='transparent'
                onPress={() => {
                  this.validateFields()
                }}
              >
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                    marginRight: 8
                  }}
                >
                  <Text
                    style={{ color: 'white', alignSelf: 'center', marginRight: 25 }}
                  >
                    Sign In
              </Text>
                  <Image
                    style={{ width: 15, height: 15 }}
                    source={Images.imgnextArrow}
                  />
                </View>
              </TouchableHighlight>
              <Text style={{ margin: 20, alignSelf: 'center' }}>
                -------OR-------
          </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
              >
                <Image
                  source={Images.imgfacebook}
                  style={{ width: 30, height: 30 }}
                />
                <Image
                  source={Images.imggmail}
                  style={{ width: 35, height: 35, marginLeft: 10 }}
                />
              </View>

              <View style={{ flexDirection: 'row', alignSelf: 'center', margin: 10 }}>
                <Text>Don't have an account ?</Text>
                <Text style={{ color: 'rgb(48,145,210)' }} onPress={() => 
                  NavigationServices.replace('Register')}> Create Account</Text>
              </View>
            </View>

          </KeyboardAwareScrollView>

        </SafeAreaView>
      </Fragment>

    )
  }
}

const mapStateToProps = (state) => {
  console.log("LOGIN_STATE:" + JSON.stringify(state))
  return {
    isLoading: state.isLoadingReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    doLoginAction: (data, props) => dispatch(doLoginAction(data, props))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(login)

const styles = StyleSheet.create({
  innercontent: {
    marginLeft: 70,
    marginRight: 70,
    marginTop: 30
  },
  signupbutton: {
    backgroundColor: 'rgb(0,114,198)',
    height: 40,
    borderRadius: 20,
    width: 130,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 40
  },
  textinputcontainer: {
    flexDirection: 'row',
    height: 40,
    marginTop: 25,
    // backgroundColor:'red',
    borderBottomColor: 'rgb(0,114,198)',
    borderBottomWidth: 1
  },
  textinputimageview: {
    width: '10%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  searchIcon: {
    padding: 10
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242'
  }
})
