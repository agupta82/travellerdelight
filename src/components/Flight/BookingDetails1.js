import React, { Component } from 'react'
import {
  View,
  StatusBar,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
  Text,
  FlatList,
  TouchableHighlight,
  Modal,
  SafeAreaView,
  DeviceEventEmitter,
} from 'react-native'
import NavigationBar from '../../Helper Classes/NavigationBar'
import Images from '../../Helper Classes/Images'
import Spinner from 'react-native-loading-spinner-overlay'
import CustomTextInput from '../../Helper Classes/Custom TextFiel/CustomTextInput'
import { ifIphoneX, getStatusBarHeight } from "react-native-iphone-x-helper";
import APIManager from '../../Helper Classes/APIManager/APIManagerUpdated';
import NavigationServices from './../../Helper Classes/NavigationServices'
import moment from 'moment';
import { connect } from "react-redux";
import { getFlightBookingDetailsAction } from '../../Redux/Actions';
import StringConstants from '../../Helper Classes/StringConstants';


const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

const gender = {
  "male": "Male",
  "female": "Female"
}
// cont innerContainerHeight
class BookingDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      spinner: false,
      bookingData: [
        {
          "id": 506,
          "phone": "9638529632",
          "email": "naresh.singh@enukesoftware.com",
          "cancelled": 0,
          "user_id": 0,
          "pnr": "SFWQ4B",
          "booking_id": "1436178",
          "traceId": null,
          "parent_id": 506,
          "refundable": 0,
          "applied_code": null,
          "booking_by": 67,
          "mailed": 0,
          "created_at": "2019-02-11 12:01:47",
          "updated_at": null,
          "doj": null,
          "cancelled_at": null,
          "deleted_at": null,
          "base_fare": "2437.00",
          "airline_fuel_surcharges": "0.00",
          "total": "4514.30",
          "user": null,
          "price": {
            "id": 458,
            "booking_id": 506,
            "price": "2437.00",
            "tax": "2077.30",
            "adult_price": "0.00",
            "child_price": "0.00",
            "infant_price": "0.00",
            "baggage_price": "0.00",
            "meal_price": "0.00",
            "promo_code": "",
            "discount_amount": "0.00",
            "discounted_price": "0.00",
            "enuke_commission": "25.89",
            "agent_commission": "-1.04",
            "grant_total": "4514.30",
            "created_at": null,
            "updated_at": null,
            "deleted_at": null
          },
          "flights": [
            {
              "id": 453,
              "booking_id": 506,
              "airline": "9W",
              "flight_id": 383,
              "equipment": "73H",
              "origin": "BOM",
              "origin_terminal": "2",
              "destination": "IDR",
              "destination_terminal": "",
              "direction": 1,
              "depart_at": "2019-02-12 22:10:00",
              "arrive_at": "2019-02-12 23:35:00",
              "travel_time": "0",
              "created_at": "2019-02-11 12:01:47",
              "updated_at": null
            },
            {
              "id": 454,
              "booking_id": 506,
              "airline": "9W",
              "flight_id": 792,
              "equipment": "73H",
              "origin": "IDR",
              "origin_terminal": "",
              "destination": "DEL",
              "destination_terminal": "3",
              "direction": 2,
              "depart_at": "2019-02-13 05:55:00",
              "arrive_at": "2019-02-13 07:30:00",
              "travel_time": "0",
              "created_at": "2019-02-11 12:01:47",
              "updated_at": null
            }
          ],
          "passengers": [
            {
              "id": 697,
              "booking_id": 506,
              "name_prefix": "Mr",
              "first_name": "Naresh",
              "last_name": "Kumar",
              "dob": "2006-12-31",
              "gender": "1",
              "passport_no": "",
              "baggages": 0,
              "baggages_1": 0,
              "hand_luggages": 0,
              "hand_luggages1": 0,
              "created_at": null,
              "updated_at": null,
              "deleted_at": null,
              "type": "1"
            }
          ]
        },
        {
          "id": 507,
          "phone": "9638529632",
          "email": "naresh.singh@enukesoftware.com",
          "cancelled": 0,
          "user_id": 0,
          "pnr": "SFW28V",
          "booking_id": "1436179",
          "traceId": null,
          "parent_id": 506,
          "refundable": 0,
          "applied_code": null,
          "booking_by": 67,
          "mailed": 0,
          "created_at": "2019-02-11 12:01:47",
          "updated_at": null,
          "doj": null,
          "cancelled_at": null,
          "deleted_at": null,
          "base_fare": "2812.00",
          "airline_fuel_surcharges": "0.00",
          "total": "4857.94",
          "user": null,
          "price": {
            "id": 459,
            "booking_id": 507,
            "price": "2812.00",
            "tax": "2045.94",
            "adult_price": "0.00",
            "child_price": "0.00",
            "infant_price": "0.00",
            "baggage_price": "0.00",
            "meal_price": "0.00",
            "promo_code": "",
            "discount_amount": "0.00",
            "discounted_price": "0.00",
            "enuke_commission": "29.87",
            "agent_commission": "-1.20",
            "grant_total": "4857.94",
            "created_at": null,
            "updated_at": null,
            "deleted_at": null
          },
          "flights": [
            {
              "id": 455,
              "booking_id": 507,
              "airline": "9W",
              "flight_id": 755,
              "equipment": "AT7",
              "origin": "DEL",
              "origin_terminal": "3",
              "destination": "JAI",
              "destination_terminal": "2",
              "direction": 1,
              "depart_at": "2019-02-11 20:00:00",
              "arrive_at": "2019-02-11 21:05:00",
              "travel_time": "0",
              "created_at": "2019-02-11 12:01:47",
              "updated_at": null
            },
            {
              "id": 456,
              "booking_id": 507,
              "airline": "9W",
              "flight_id": 698,
              "equipment": "73H",
              "origin": "JAI",
              "origin_terminal": "2",
              "destination": "BOM",
              "destination_terminal": "2",
              "direction": 2,
              "depart_at": "2019-02-12 08:35:00",
              "arrive_at": "2019-02-12 10:35:00",
              "travel_time": "0",
              "created_at": "2019-02-11 12:01:47",
              "updated_at": null
            }
          ],
          "passengers": [
            {
              "id": 698,
              "booking_id": 507,
              "name_prefix": "Mr",
              "first_name": "Naresh",
              "last_name": "Kumar",
              "dob": "2006-12-31",
              "gender": "1",
              "passport_no": "",
              "baggages": 0,
              "baggages_1": 0,
              "hand_luggages": 0,
              "hand_luggages1": 0,
              "created_at": null,
              "updated_at": null,
              "deleted_at": null,
              "type": "1"
            }
          ]
        }
      ]

      //bookingData: null,
    }

    this.BookingDetails = this.BookingDetails.bind(this);
    this.BookingDetailsItem = this.BookingDetailsItem.bind(this);
    this.updateBookingDetailsDataFromApi = this.updateBookingDetailsDataFromApi.bind(this);
  }

  componentDidMount() {
    DeviceEventEmitter.addListener(StringConstants.FLIGHT_BOOKING_DETAILS_EVENT, this.updateBookingDetailsDataFromApi)
    this.callflightBookingDetailsApi()
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.FLIGHT_BOOKING_DETAILS_EVENT, this.updateBookingDetailsDataFromApi)

  }

  updateBookingDetailsDataFromApi(bookings) {
    this.setState({
      bookingData: bookings
    })
  }

  // pnrNoItem() {
  //   return (

  //   )
  // }


  callflightBookingDetailsApi() {
    var bookingId = this.props.bookingId;
    this.props.getFlightBookingDetailsAction("479")
    // APIManager.getBookedFlightData(bookingId, (success, response) => {
    //   console.log(JSON.stringify(response))
    //   if (success) {
    //     if (response.code.code == 200) {
    //       this.setState({
    //         bookingData: response.data.bookings
    //       })
    //     } else {
    //       setTimeout(() => {
    //         alert("some error occured" + JSON.stringify(response))
    //       }, 100)
    //     }
    //   } else {

    //   }
    // })
  }

  downloadPdf() {
    setTimeout(() => {
      alert("Link not provided")
    }, 100)
  }

  passengerDetailItem(item) {
    return (
      <View style={[styles.itemContainer, { padding: 0 }]}>
        <View style={{ borderBottomColor: "black", borderBottomWidth: 0.3, padding: 10 }}>
          <Text style={{ margin: 10, fontWeight: "500", fontSize: 15 }}>
            Passenger Details
         </Text>
          <View style={{ flexDirection: 'row', margin: 10, marginTop: 15 }}>
            <Image
              source={Images.imgusericon}
              style={{ height: 20, width: 20 }}
            />
            <Text style={{ marginLeft: 15 }}>
              {item[0].name_prefix} {item[0].first_name} {item[0].last_name}
            </Text>
          </View>
          <View style={{ flexDirection: "row", margin: 10 }}>
            <View style={{ flexDirection: "row", width: "50%" }}>
              <Image
                style={{ height: 20, width: 20 }}
                source={Images.imgCalender}
              />
              <Text style={{ marginLeft: 15 }}>
                {item[0].dob}
              </Text>
            </View>
            <View style={{ flexDirection: "row", width: "50%" }}>
              <Image
                style={{ height: 20, width: 20 }}
                source={Images.imgNationatily}
              />
              <Text style={{ marginLeft: 15 }}>
                British
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: "row", margin: 10 }}>
            <View style={{ flexDirection: "row", width: "50%" }}>
              <Image
                style={{ height: 20, width: 20 }}
                source={Images.imgbaggage}
              />
              <Text style={{ marginLeft: 15 }}>
                {item[0].baggages}
              </Text>
            </View>
            <View style={{ flexDirection: "row", width: "50%" }}>
              <Image
                style={{ height: 20, width: 20, tintColor: "rgb(0,114,208)" }}
                source={Images.imgdinner}
              />
              <Text style={{ marginLeft: 15 }}>
                {item[0].mea}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: "row", justifyContent: "space-between", margin: 10 }}>
            <View style={{ flexDirection: 'row', }}>
              <Text style={styles.blackTextStyle}>
                Gender
            </Text>
              <Text style={[styles.blueTextStyle, { marginLeft: 15 }]}>
                {(item[0].gender == 1) ? gender.male : gender.female}
              </Text>
            </View>
            {item.length > 1 ? <TouchableHighlight
              style={styles.listofpassengersbtn}
              underlayColor="transparent"
              onPress={() => {

              }}>
              <Text style={{ color: "white" }}>
                List of Passengers
            </Text>
            </TouchableHighlight> : <View></View>}

          </View>
        </View>
      </View>
    )
  }

  // flightDetailsItem() {
  //   return (
  //     <View style={[styles.itemContainer, { marginBottom: 20, marginTop: 20 }]}>
  //       <Text style={{ fontWeight: "600", marginBottom: 20 }}>
  //         Flight Details
  //       </Text >
  //       <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
  //         <Text style={styles.blackTextStyle}>
  //           Flight No
  //       </Text>
  //         <Text style={styles.blueTextStyle}>
  //           TCIN2345
  //       </Text>
  //       </View>
  //       <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
  //         <Text style={styles.blackTextStyle}>
  //           AirLine
  //       </Text>
  //         <Text style={styles.blueTextStyle}>
  //           SPICEJET
  //       </Text>
  //       </View>
  //       <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
  //         <Text style={styles.blackTextColor}>
  //           Flight Type
  //       </Text>
  //         <Text style={styles.blueTextStyle}>
  //           Domestic
  //       </Text>
  //       </View>
  //     </View>
  //   )
  // }

  listItemText(text1, text2) {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 5, marginBottom: 5 }}>
        <Text style={styles.blackTextStyle}>
          {text1}
        </Text>
        <Text style={styles.blueTextStyle}>
          {text2}
        </Text>
      </View>
    )
  }

  // travelDetailItem(){
  //   return(
  //     <View style={styles.itemContainer}>
  //      {this.listItemText("Booking Date","FRI 4 JAN 2019")}
  //      {this.listItemText("FROM/TERMINAL","MUMBAI/T1")}
  //      {this.listItemText("TO/TERMINAL","DELHI/T2")}
  //      {this.listItemText("DEP.TIME","7:35 PM")}
  //      {this.listItemText("ARR TIME","10:05 PM")}
  //      {this.listItemText("PNR NO.","76788735")}
  //      {this.listItemText("BOOKING ID","5319857")}
  //     </View>
  //   )
  // }

  // fareDetailItem() {
  //   return (
  //     <View style={styles.itemContainer}>
  //       <Text style={{ fontWeight: "600" }}>
  //         Fare Details
  //         </Text>
  //       {this.listItemText("Ticket Price", "$ 5580.00")}
  //       {this.listItemText("Total", "$ 5580.00")}
  //     </View>
  //   )
  // }

  BookingDetailsItem(bookingitem, index) {
    const imgsource = index == 0 ? Images.imgrightarrowbig : Images.imgleftarrowbig
    console.log(JSON.stringify("booking item " + JSON.stringify(bookingitem)))
    return (
      <View
        style={{
          marginBottom: 5
        }}>
        <View style={{
          backgroundColor: "rgb(0,114,208)",
          padding: 20,
          shadowColor: "black",
          shadowOpacity: 0.3,
          borderBottomColor: "white",
          borderBottomWidth: 1,
          shadowOffset: { width: 1, height: 2 },
          elevation: 4
        }}>
          <FlatList
            data={bookingitem.flights}
            renderItem={(item) => this.fromToItem(item, imgsource)}
          />
          <View style={{ marginTop: 10 }} >
            {/* <Text style={{
              color: "white", fontSize: 16,
              alignSelf: "center"
            }}>
              FRI 4 JAN 2019
        </Text> */}
          </View>
          <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 10 }}>
            <View style={{ justifyContent: "center" }}>
              <Text style={{ color: "white", fontWeight: "500" }}>
                PNR NO.
                </Text>
              <Text style={{ color: "white", fontWeight: "500", marginTop: 5 }}>
                BOOKING ID
               </Text>
            </View>

            <View style={{ justifyContent: "center", alignItems: "flex-end" }}>
              <Text style={{ color: "white", fontWeight: "500" }}>
                {bookingitem.pnr}
              </Text>
              <Text style={{ color: "white", fontWeight: "500", marginTop: 5 }}>
                {bookingitem.booking_id}
              </Text>
            </View>
          </View>

        </View>

        <View style={{ flexDirection: "row", backgroundColor: "rgb(0,104,208)", justifyContent: "space-between", padding: 20 }}>
          <View style={{ justifyContent: "center" }}>
            <Text style={{ color: "white", fontWeight: "500" }}>
              TICKET PRICE
                </Text>
            <Text style={{ color: "white", fontWeight: "500", marginTop: 5 }}>
              TAX
               </Text>
            <Text style={{ color: "white", fontWeight: "500", marginTop: 5 }}>
              TOTAL
               </Text>
          </View>

          <View style={{ justifyContent: "center", alignItems: "flex-end" }}>
            <Text style={{ color: "white", fontWeight: "500" }}>
              {bookingitem.price.price}
            </Text>
            <Text style={{ color: "white", fontWeight: "500", marginTop: 5 }}>
              {bookingitem.price.tax}
            </Text>
            <Text style={{ color: "white", fontWeight: "500", marginTop: 5 }}>
              {bookingitem.price.grant_total}
            </Text>
          </View>
        </View>

        {/* <View
          style={{
            height: 70,
            width: screenWidth,
            marginTop: 1,
            marginBottom: 15,
            backgroundColor: 'rgb(249,249,249)',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            shadowColor: "black",
            shadowOpacity: 0.3,
            shadowOffset: { width: 1, height: 1 },
            elevation: 4
          }}
        >

          <View style={{ margin: 10, alignItems: 'center' }}>
            <Text style={{ color: "#707070", fontWeight: '500', fontSize: 16 }}>STATUS</Text>
            <Text style={{ color: "#707070", marginTop: 7, fontWeight: '700' }}>Booking status {bookingitem.created_at}</Text>
          </View>
        </View> */}
      </View>
    )
  }

  fromToItem({ item }, img) {
    return (
      <View style={{

      }}>
        <View style={{

          alignItems: "center",
          justifyContent: "flex-end",
          marginTop: 5
        }}>
          <Text style={{ color: "white", fontWeight: "600" }}>
            {item.airline} | {item.flight_id}
          </Text>
        </View>
        <View style={{
          flexDirection: "row",

          marginTop: 5
        }}>
          <View style={{ flex: 2, justifyContent: "center" }}>
            <Text style={{ color: "white", fontSize: 16, fontWeight: "600" }}>
              {item.origin}/{item.origin_terminal}
            </Text>
            <Text style={{ color: "white", marginTop: 5 }}>
              {/* 12:40:13 */}

              {/* {dateFormat(item.depart_at, "HH:MM:ss")} */}
              {moment(item.depart_at).format('h:mm:ss a')}
            </Text>


          </View>

          <View style={{
            flex: 1,
            height: 60,

            justifyContent: "center"
          }}>
            <Image
              resizeMode={"contain"}
              style={{ height: "100%", width: "100%" }}
              source={Images.imgrightarrowround}
            />
          </View>
          <View
            style={{
              flex: 2,
              justifyContent: "center"
            }}>

            <Text style={{ color: "white", fontSize: 16, fontWeight: "600", alignSelf: "flex-end" }}>
              {item.destination}/{item.destination_terminal}
            </Text>
            <Text style={{ color: "white", marginTop: 5, alignSelf: "flex-end" }}>

              {/* {dateFormat(item.arrive_at, "HH:MM:ss")} */}
              {item.arrive_at}
            </Text>

          </View>
        </View>
      </View>
    )
  }

  BookingDetails() {
    return (
      <View>

        {this.BookingDetailsItem(this.state.bookingData[0], 0)}
        {this.state.bookingData.length > 1 ? this.BookingDetailsItem(this.state.bookingData[1], 1) : <View></View>}
        {this.passengerDetailItem(this.state.bookingData[0].passengers)}
        {this._renderContactDetails(this.state.bookingData[0])}

        <TouchableHighlight
          style={styles.btnstyle}
          underlayColor="transparent"
          onPress={() => {
            NavigationServices.navigate('HomeTabStack')
          }}>
          <Text style={styles.btntext}>
            Continue
                </Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.btnstyle}
          underlayColor="transparent"
          onPress={() => {
            this.downloadPdf()
          }}>
          <Text style={styles.btntext}>
            Download Pdf
                </Text>
        </TouchableHighlight>

      </View>
    )
  }
  _renderContactDetails() {
    return (
      <View style={styles.itemContainer}>

        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={{ fontSize: 15, fontWeight: '600', color: 'rgb(76,76,76)' }}>
            Contact Details
            </Text>
        </View>

        <View style={{ flexDirection: 'row', marginTop: 15 }}>
          <View style={{ justifyContent: 'center' }}>
            <Image
              style={{ height: 15, width: 20, tintColor: 'rgb(0,104,208)' }}
              source={Images.imgmail} />
          </View>
          <Text style={{ marginLeft: 15, fontSize: 14 }}>
            {this.state.bookingData[0].email}

          </Text>
        </View>

        <View style={{ flexDirection: 'row', marginTop: 15 }}>
          <Image
            style={{ height: 20, width: 20, tintColor: 'rgb(0,104,208)' }}
            source={Images.imgphone} />
          <Text style={{ marginLeft: 15, fontSize: 14 }}>
            {this.state.bookingData[0].phone}
          </Text>
        </View>

      </View>
    )
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <NavigationBar title={'Booking Details'} showBack />
        <Spinner
          visible={this.props.isLoading}
          textContent={"Loading..."}
          textStyle={{ color: 'white' }}
        />
        <StatusBar barStyle="light-content" />
        <ScrollView style={styles.scrollcontainer} >
          {this.state.bookingData != null ? this.BookingDetails() : <View></View>}
        </ScrollView>

      </SafeAreaView>



    )
  }
}


const mapStateToProps = state => {
  return {
    isLoading: state.isLoadingReducer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getFlightBookingDetailsAction: (data) => dispatch(getFlightBookingDetailsAction(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BookingDetails)

styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(244,248,254,1)'
  },
  scrollcontainer: {
    flex: 1,

  },
  itemContainer: {
    padding: 20,
    backgroundColor: 'white',
    shadowColor: "black",
    shadowOpacity: 0.3,
    margin: 10,
    shadowOffset: { width: 1, height: 1 },
    borderRadius: 10,
    elevation: 4
  },
  blackTextStyle: {
    color: 'black'
  },
  listofpassengersbtn: {
    backgroundColor: "rgb(0,114,208)",
    padding: 8,
    borderRadius: 5,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 1 },
    elevation: 4
  },
  btnstyle: {
    backgroundColor: "rgb(0,114,208)",
    padding: 15,
    margin: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 1 },
    elevation: 4
  },
  btntext: {
    color: "white",
    fontSize: 15,
    fontWeight: "600"
  }

})
