import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  Image,
  ScrollView,
  StatusBar,
  Alert,
  Modal,
  Animated,
  DeviceEventEmitter,
  TouchableOpacity
} from "react-native";

import NavigationBar from "../../../Helper Classes/NavigationBar";
import Images from "../../../Helper Classes/Images";
import NavigationServices from '../../../Helper Classes/NavigationServices'
//import APIManager from "../../../Helper Classes/APIManager/APIManagerUpdated";
import Spinner from "react-native-loading-spinner-overlay";
import { Button } from "../../../Helper Classes/Button";
import { commonstyle } from "../../../Helper Classes/Commonstyle";
import Header from "../../../Helper Classes/CommonHeader";
import { SafeAreaView } from "react-navigation";
import Fonts from "../../../Helper Classes/Fonts";
import { colors } from "../../../Helper Classes/Colors";

//import moment from "moment"
import { PassengersModal } from "../../../Helper Classes/PassengerModal";
import { confireQuoteApi } from "../../../Helper Classes/APIManager/ApiProvider";
import { bookFlightApi } from "../../../Helper Classes/APIManager/ApiProvider";
import { connect } from "react-redux";
import { bookFlightAction, confirmQuoteAction } from '../../../Redux/Actions';
import StringConstants from "../../../Helper Classes/StringConstants";
import Flightadata from "../../../json/FlightData";
import Passengers from "../../../json/Passengers";

class ReviewFlightBooking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      traceId: null,
      spinner: false,
      fareQuotes: [],
      expandBaseFare: false,
      expandTaxes: false,
      maxHeight: 52,
      minHeight: 0,
      maxTaxHeight: 70,
      taxAnimation: new Animated.Value(),
      animation: new Animated.Value(),
      email: null,
      phoneNumber: null,
      flightType: null,
      origin: null,
      destination: null,
      isRoundTrip: null,
      isInterNational: null,
      fareDetails: null,
      passengers: null,
      flightData: null,
      currency: "INR"
    }
    // this.total_Fare = 0;
    this.total_Fare = { amount: 0, currency: "INR" }
    this._renderFareDetailsView = this._renderFareDetailsView.bind(this);
    this.updateFareQuoteFromApi = this.updateFareQuoteFromApi.bind(this);

  }

  bookTicket = () => {
    let flightData = this.state.flightData;
    let flights = [];
    if (flightData.length == 2) {
      flightData.forEach(obj => {
        flights.push(obj.index);
      });
    } else {
      let obj = flightData[0];
      flights.push(obj.index);
    }
    this.APIFlightBooking(flights);
  }

  editPassengerHandler = () => {
    this.setState({
      isModalVisible: false,
    }, () => NavigationServices.goBack())
  }

  APIConfirmQuote(flights) {
    let param = {
      type: "TBO",
      flight: flights,
      traceId: this.state.traceId
    };
    console.log("param for Quotes" + JSON.stringify(param));
    this.props.confirmQuoteAction(param);
  }

  APIFlightBooking(indexes) {
    const price = this.total_Fare;
    let finalDict = {};
    if (this.state.flightType == 0) {
      let fareQuote = this.state.fareQuotes[0].OB.farequote;
      finalDict["OB"] = {
        Status: fareQuote.IsLCC,
        Baggage: [],
        MealDynamic: [],
        farequote: fareQuote
      };
      finalDict["IB"] = {
        Status: false,
        Baggage: "",
        MealDynamic: "",
        farequote: ""
      };
    } else {
      let OBfareQuote = this.state.fareQuotes[0].OB.farequote;
      let IBfareQuote = this.state.fareQuotes[1].IB.farequote;

      finalDict["OB"] = {
        Status: OBfareQuote.IsLCC,
        Baggage: [],
        MealDynamic: [],
        farequote: OBfareQuote
      };
      finalDict["IB"] = {
        Status: IBfareQuote.IsLCC,
        Baggage: [],
        MealDynamic: [],
        farequote: IBfareQuote
      };
    }

    let adlt = [];
    let child = [];
    let infant = [];
    this.state.passengers.forEach((obj) => {
      let passengerDict = {
        title: "",
        fname: "",
        mname: "",
        lname: "",
        dob: "",
        dep_baggage: "0",
        ret_baggage: "0",
        dep_meal: "0",
        ret_meal: "0",
        passport_no: "",
        passport_expiry: "",
        passportrqd: false

      }
      passengerDict.fname = obj.fName;
      passengerDict.mname = obj.mName;
      passengerDict.lname = obj.lName;
      passengerDict.dob = obj.dob;
      passengerDict.title = String(obj.title).substring(0, obj.title.length - 1)
      if (obj.passenger_type === "Adult") {
        adlt.push(passengerDict);
      } else if (obj.passenger_type === "Children") {
        child.push(passengerDict);
      } else {
        infant.push(passengerDict);
      }
    })
    let param = {
      email: this.state.email,
      mobile: this.state.phoneNumber,
      ADT: adlt,
      CHD: child,
      INF: infant,
      attributes: {
        traceId: this.state.traceId,
        index: indexes
      },
      type: "TBO",
      fQuoteSSR: finalDict,
      final_booking_amount: price.amount,
      orderID: "",
      captureID: "",
      isFlight: true,
      price: price,
      currency: this.state.currency
    };
    console.clear();
    console.log("BOOK_FLIGHT_REQ:")
    console.log(JSON.stringify(param));
    NavigationServices.navigate("PaymentScreen", { data: param, isFlight: true });
    // this.props.bookFlightAction(param);
  }

  componentDidMount() {
    DeviceEventEmitter.addListener(StringConstants.CONFIRM_QUOTE_EVENT, this.updateFareQuoteFromApi)
    let { navigation } = this.props;
    // let flightData = Flightadata;
    // let passengers = Passengers;
    let flightData = navigation.getParam('flightdata', null);
    let currency = navigation.getParam('currency', "INR");
    let passengers = navigation.getParam('passengers', null);
    let traceId = navigation.getParam('traceid', null);
    let flightType = navigation.getParam('flightType', null);
    let origin = navigation.getParam('origin', null);
    let destination = navigation.getParam('destination', null);
    let isRoundTrip = navigation.getParam('isRoundTrip', null);
    let email = navigation.getParam('email', null);
    let phoneNumber = navigation.getParam('phoneNumber', null);
    let isInterNational = navigation.getParam('isInterNational', null);
    //console.log("traceId:", JSON.stringify(traceId))
    //console.log("DDDeTa",JSON.stringify(flightData))

    let flights = [];

    if (flightData.length == 2) {
      flightData.forEach(obj => {
        let dict = {};
        dict["index"] = obj.index;
        dict["isLCC"] = obj.isLCC;
        flights.push(dict);
      });
    } else {
      let obj = flightData[0];
      let dict = {};
      dict["index"] = obj.index;
      dict["isLCC"] = obj.isLCC;
      flights.push(dict);
    }

    this.setState({
      currency,
      fareDetails: flightData.fare,
      traceId: traceId,
      passengers: passengers,
      flightData: flightData,
      flightType: flightType,
      origin: origin,
      destination: destination,
      isRoundTrip: isRoundTrip,
      isInterNational: isInterNational,
      email: email,
      phoneNumber: phoneNumber
    }, () => {
      this.APIConfirmQuote(flights);
    });
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.CONFIRM_QUOTE_EVENT, this.updateFareQuoteFromApi)
  }

  updateFareQuoteFromApi(response) {
    this.setState({
      fareQuotes: response
    })
  }

  toggleTax() {
    //Step 1
    let initialValue = this.state.expandTaxes ? this.state.maxTaxHeight + this.state.minHeight : this.state.minHeight;
    let finalValue = this.state.expandTaxes ? this.state.minHeight : this.state.maxTaxHeight + this.state.minHeight;
    this.setState({
      expandTaxes: !this.state.expandTaxes  //Step 2
    });
    this.state.taxAnimation.setValue(initialValue);  //Step 3
    Animated.spring(     //Step 4
      this.state.taxAnimation,
      {
        toValue: finalValue
      }
    ).start();  //Step 5
  }

  _renderfromToItem() {
    return (
      <View style={{
        width: "100%", height: 50, flexDirection: "row",
        justifyContent: "center", alignItems: "center",
        backgroundColor: colors.colorBlue,
        borderTopWidth: 1,
        borderTopColor: colors.lightBlue
      }}>
        <View style={{ flex: 2, alignItems: "center" }}>
          <Text style={{
            color: colors.colorWhite,
            fontSize: 16, fontFamily: Fonts.medium
          }}>
            {this.state.origin}
          </Text>
        </View>
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", }}>
          <Image style={{ height: 15, width: 15 }}
            source={Images.imgrightarrowbig}
            tintColor={colors.colorWhite} />
          {this.state.isRoundTrip && <Image
            style={{ height: 15, width: 15 }}
            source={Images.imgleftarrowbig}
            tintColor={colors.colorWhite}
          />}
        </View>
        <View style={{ flex: 2, flexDirection: "row", alignItems: "center" }}>
          <Text style={{
            color: colors.colorWhite,
            fontSize: 16, fontFamily: Fonts.medium,
            flexGrow: 1, textAlign: "center"
          }} >
            {this.state.destination}
          </Text>
          <TouchableOpacity
            onPress={() => {
              NavigationServices.navigate('FlightDetails', {
                flightdata: this.state.flightData,
                isNavigateFromReviewPage: true,
                traceid: this.state.traceId
              });
            }} >
            <Image style={{ height: 20, width: 20, marginRight: 10 }}
              source={Images.imginfoi}
              tintColor={colors.colorWhite}
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  _renderTitleView(title, image) {
    return (
      <View style={styles.cardheader}>
        <Image source={image ? image : Images.imgpayment} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
        <Text style={styles.cardheaderText}>{title}</Text>
      </View>
    );
  }

  _renderContactDetails() {
    let { email, phoneNumber } = this.state;
    return (
      <View style={styles.itemBackground}>
        {this._renderTitleView("Contact Details", Images.imgcontact)}
        <View style={{ padding: 12 }}>
          <Text style={styles.contactRow}>Email :- <Text style={{ fontFamily: Fonts.regular }}>{email ? email : "test123@gmail.com"}</Text>
          </Text>
          <Text style={styles.contactRow}>Phone :- <Text style={{ fontFamily: Fonts.regular }}>{phoneNumber ? phoneNumber : "7676767676"}</Text>
          </Text>
        </View>
        {/* <View style={{ flexDirection: 'row', marginTop: 15 }}>
          <View style={{ justifyContent: 'center' }}>
            <Image
              style={{ height: 15, width: 20, tintColor: 'rgb(0,104,208)' }}
              source={Images.imgmail} />
          </View>
          <Text style={[commonstyle.greyText, { marginLeft: 15, fontSize: 14 }]}>
            {this.state.email}
          </Text>
        </View> */}
        {/* <View style={{ flexDirection: 'row', marginTop: 15 }}>
          <Image
            style={{ height: 20, width: 20, tintColor: 'rgb(0,104,208)' }}
            source={Images.imgphone} />
          <Text style={[commonstyle.greyText, { marginLeft: 15, fontSize: 14 }]}>
            {this.state.phoneNumber}
          </Text>
        </View> */}
      </View>
    )
  }

  getPassengerText = () => {
    let { passengers } = this.state;
    let totalChild = 0;
    let totalAdult = 0;
    let totalInfant = 0;
    let adultText = "";
    let childText = "";
    let infantText = "";
    for (let i = 0; i < passengers.length; i++) {
      switch (passengers[i].passenger_type) {
        case "Adult":
          totalAdult++;
          break;
        case "Children":
          totalChild++;
          break;
        case "Infant":
          totalInfant++;
          break;
      }
    }
    if (totalAdult > 0) {
      adultText += totalAdult + " Adult"
    }
    if (totalAdult > 1) {
      adultText = totalAdult + " Adults"
    }
    if (totalChild > 0) {
      childText += totalChild + " Child"
    }
    if (totalChild > 1) {
      childText = totalChild + " Children"
    }
    if (totalInfant > 0) {
      infantText += totalInfant + " Infant"
    }
    if (totalInfant > 1) {
      infantText = totalInfant + " Infants"
    }
    let titleText = "";
    if (adultText != "" && childText == "" && infantText == "") {
      titleText = adultText;
    }
    if (adultText == "" && childText != "" && infantText == "") {
      titleText = childText;
    }
    if (adultText == "" && childText == "" && infantText != "") {
      titleText = infantText;
    }
    if (adultText != "" && childText != "" && infantText == "") {
      titleText = adultText + " & " + childText;
    }
    if (adultText == "" && childText != "" && infantText != "") {
      titleText = childText + " & " + infantText;
    }
    if (adultText != "" && childText != "" && infantText != "") {
      titleText = adultText + " & " + childText + " More";
    }
    return titleText;
  }

  passengerDetailItem(item) {
    return (
      <View style={{
        width: "100%", padding: 12
      }}>
        <View style={styles.grayStrip}>
          <View style={{
            width: "10%", justifyContent: "center",
            alignItems: "center"
          }}>
            <Image style={{ height: 25, width: 25, resizeMode: "contain" }} source={Images.imgpassender} />
          </View>
          <View style={{
            width: "90%",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
          }}>
            <View style={{
              width: "75%",
            }}>
              <Text style={[styles.boldText, { fontFamily: Fonts.medium }]}>{item[0].title} {item[0].fName} {item[0].mName} {item[0].lName}
              </Text>
              <Text style={styles.normalText}>{this.getPassengerText()}</Text>
            </View>
            <TouchableOpacity style={{ padding: 5, width: "25%", alignItems: "center" }}
              onPress={() => {
                // this.setState({ isModalVisible: true });
                NavigationServices.goBack();
              }}>
              <Text style={{ color: colors.colorBlack, fontFamily: Fonts.medium, fontSize: 12 }}>
                VIEW ALL
            </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }

  _renderFareAmountView(title, fare) {
    return (
      <View style={styles.styleFareAmountView}>
        <Text style={{
          fontSize: 14, color: "rgb(0,114,198)",
          fontFamily: "Montserrat-Bold"
        }} > {title} </Text>
        <Text style={{ fontSize: 14, color: "black", fontFamily: "Montserrat-Medium" }}>{fare}</Text>
      </View>
    );
  }

  _renderFareDetailsView() {
    let fare = this.state.flightData[0].fare;
    if (fare == null) {
      return <View />;
    }
    let fareDetail = this.state.flightData[0].fareDetails;
    let fareDetail2 = null;
    let currency = fare.currency;
    let baseFare;
    let tax_charges;
    let discount;
    let totalFare = this.totalFare;
    let otherCharges;
    let tax;
    let serviceFee;
    let fuelCharge;
    if (this.state.isInterNational) {
      let fare = this.state.flightData[0].fare;
      baseFare = currency + " " + Math.ceil(fare.baseFare);
      otherCharges = currency + " " + Math.ceil(fare.otherCharges);
      discount = (fare.discount == "NaN" || fare.discount == null)
        ? "0"
        : fare.discount;
      discount = currency + " " + Math.round(discount);
      tax = currency + " " + fare.tax;
      serviceFee = currency + fare.serviceFee;
      fuelCharge = currency + fare.fuelCharge;
      tax_charges = fare.tax + fare.serviceFee + fare.fuelCharge;
      tax_charges = currency + " " + tax_charges;
      totalFare = currency + " " + Math.ceil(fare.agencyFare);
      // this.total_Fare = totalFare;
      this.total_Fare.currency = currency
      this.total_Fare.amount = fare.agencyFare
      console.log("TOTAL_FARE:1" + JSON.stringify(this.total_Fare))
    } else {
      if (this.state.isRoundTrip) {
        fareDetail2 = this.state.flightData[1].fareDetails;
        let fare1 = this.state.flightData[0].fare;
        // let fareDetail = this.state.flightData[0].fareDetails;
        let fare2 = this.state.flightData[1].fare;
        baseFare = Math.ceil(fare1.baseFare) + Math.ceil(fare2.baseFare);
        baseFare = currency + " " + baseFare;
        otherCharges = Math.ceil(fare1.otherCharges) + Math.ceil(fare2.otherCharges);
        otherCharges = currency + " " + otherCharges;
        let discount1 = (fare1.discount == "NaN" || fare1.discount == null)
          ? "0"
          : fare1.discount;
        discount1 = Math.round(discount1);
        let discount2 = (fare2.discount == "NaN" || fare2.discount == null)
          ? "0"
          : fare2.discount;
        discount2 = Math.round(discount2);
        discount = discount1 + discount2;
        discount = currency + " " + discount;
        tax = fare1.tax + fare2.tax;
        tax = currency + tax;
        serviceFee = fare1.serviceFee + fare2.serviceFee;
        serviceFee = currency + serviceFee;
        fuelCharge = fare1.fuelCharge = fare2.fuelCharge;
        fuelCharge = currency + fuelCharge;
        tax_charges = fare1.tax + fare2.tax + fare1.serviceFee + fare2.serviceFee + fare1.fuelCharge + fare2.fuelCharge;
        tax_charges = currency + " " + tax_charges;

        totalFare = Math.ceil(fare1.agencyFare) + Math.ceil(fare2.agencyFare)
        totalFare = currency + " " + totalFare;
        // this.total_Fare = totalFare;
        this.total_Fare.currency = currency
        this.total_Fare.amount = Math.ceil(fare1.agencyFare) + Math.ceil(fare2.agencyFare)
        console.log("TOTAL_FARE:2" + JSON.stringify(this.total_Fare))

      } else {
        let fare = this.state.flightData[0].fare;
        baseFare = currency + " " + Math.ceil(fare.baseFare);
        otherCharges = currency + " " + Math.ceil(fare.otherCharges);
        discount = (fare.discount == "NaN" || fare.discount == null)
          ? "0"
          : fare.discount;
        discount = currency + " " + Math.round(discount);
        tax = currency + " " + fare.tax;

        serviceFee = currency + " " + fare.serviceFee;
        fuelCharge = currency + " " + fare.fuelCharge;

        tax_charges = fare.tax + fare.serviceFee + fare.fuelCharge;
        tax_charges = currency + " " + tax_charges;
        totalFare = currency + " " + Math.ceil(fare.agencyFare);
        // this.total_Fare = totalFare;
        this.total_Fare.currency = currency
        this.total_Fare.amount = fare.agencyFare
        console.log("TOTAL_FARE:3" + JSON.stringify(this.total_Fare))
      }
    }

    return (
      <View style={styles.itemBackground}>
        {this._renderTitleView("Fare Details", Images.imgfare)}
        <View style={{ padding: 12 }}>
          <View style={styles.fareRow}>
            <Text style={styles.boldText}>Base Fare</Text>
            <Text style={styles.boldText}>{baseFare}</Text>
          </View>
          {this.renderExpandedBaseFare(currency, fareDetail, fareDetail2)}
          <View style={styles.border}></View>
          <View style={styles.fareRow}>
            <Text style={styles.boldText}>Taxes & Charges</Text>
            <Text style={styles.boldText}>{tax_charges}</Text>
          </View>
          {this.renderExpandedTaxes(tax, serviceFee, fuelCharge)}
          <View style={styles.border}></View>
          <View style={styles.fareRow}>
            <Text style={styles.boldText}>Other Charges</Text>
            <Text style={styles.boldText}>{otherCharges}</Text>
          </View>
          <View style={[styles.fareRow, { marginBottom: 0 }]}>
            <Text style={styles.boldText}>Discount</Text>
            <Text style={styles.boldText}>{discount}</Text>
          </View>
        </View>
        <View style={[styles.border]}></View>
        <View style={[styles.fareRow, { padding: 12, paddingTop: 0 }]}>
          <Text style={styles.boldText}>Total</Text>
          <Text style={styles.boldText}>{totalFare}</Text>
        </View>
      </View>
    );
  }

  renderExpandedTaxes(tax, serviceFee, fuelCharge) {
    return (
      <Animated.View>
        <View style={styles.fareRow}>
          <Text style={styles.normalText}>Tax</Text>
          <Text style={styles.normalText}>{tax}</Text>
        </View>
        <View style={styles.fareRow}>
          <Text style={styles.normalText}>Passenger Service Fee</Text>
          <Text style={styles.normalText}>{serviceFee}</Text>
        </View>
        <View style={styles.fareRow}>
          <Text style={styles.normalText}>Airline Fuel Surcharge</Text>
          <Text style={styles.normalText}>{fuelCharge}</Text>
        </View>
      </Animated.View>
    )
  }

  renderExpandedBaseFare(currency, fareDetail, fareDetail2) {
    if (fareDetail2 == null) {
      return (
        <Animated.View >
          <FlatList
            data={fareDetail}
            renderItem={({ item }) => {
              let perHeadCost = Math.ceil(item.baseFare / item.passengerCount);
              perHeadCost = currency + " " + perHeadCost;
              return (
                <View style={styles.fareRow}>
                  <Text style={styles.normalText}>
                    {item.passengerCount} {item.passengerType == 1 ? "Adult" : item.passengerType == 2 ? "Child " : "Infant"}
                  </Text>
                  <Text style={styles.normalText}>{perHeadCost}</Text>
                </View>)
            }}
          />
        </Animated.View>
      )
    }
    else {
      return (
        <Animated.View>
          <FlatList data={fareDetail}
            renderItem={({ item, index }) => {
              let item2 = fareDetail2[index];
              let cost = Math.ceil(item2.baseFare / item2.passengerCount)
              let perHeadCost = Math.ceil(item.baseFare / item.passengerCount);
              perHeadCost = cost + perHeadCost
              return (
                <View style={styles.fareRow}>
                  <Text style={styles.normalText}>
                    {item.passengerCount} {item.passengerType == 1 ? "Adult" : item.passengerType == 2 ? "Child " : "Infant"}
                  </Text>
                  <Text style={styles.normalText}>{perHeadCost}</Text>
                </View>)
            }}
          />
        </Animated.View>
      )
    }
  }

  modalcloseHandler = () => {
    this.setState({ isModalVisible: false })
  }

  render() {
    if (!this.state.flightData) return <View></View>
    //  console.log("flightData",JSON.stringify(this.state.flightData))
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.colorBlue }}>
        <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} />

        <Header title="Review Booking" />

        <Spinner visible={this.props.isLoading}
          textContent={"Loading..."}
          textStyle={{ color: colors.colorWhite }}
        />
        {this._renderfromToItem()}
        <ScrollView contentContainerStyle={{ backgroundColor: colors.colorWhite }}>

          {this.passengerDetailItem(this.state.passengers)}
          {this._renderFareDetailsView()}
          {this._renderContactDetails()}
          <View style={{ height: 50 }}></View>
        </ScrollView>
        <Button
          height={45}
          borderRadius={7}
          onPress={() => { this.bookTicket() }}
          text={"Book Ticket"}
        />
        <Modal transparent={true}
          visible={this.state.isModalVisible}>
          <PassengersModal
            passengers={this.state.passengers}
            editPassengerHandler={this.editPassengerHandler}
            modalCloseHandler={this.modalcloseHandler}
          />
        </Modal>
      </SafeAreaView>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    bookFlightAction: (data) => dispatch(bookFlightAction(data)),
    confirmQuoteAction: (data) => dispatch(confirmQuoteAction(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReviewFlightBooking)

const styles = StyleSheet.create({
  border: {
    width: "100%", height: 1,
    backgroundColor: colors.lightgrey,
    marginTop: 5, marginBottom: 12
  },
  fareRow: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 5
  },
  normalText: {
    marginLeft: 10, color: colors.colorBlack,
    fontFamily: Fonts.regular,
    lineHeight: 15, fontSize: 10,
  },
  boldText: {
    marginLeft: 10, color: colors.colorBlack,
    fontFamily: Fonts.semiBold, fontSize: 12,
    lineHeight: 15, letterSpacing: .7
  },
  grayStrip: {
    flexDirection: "row", width: "100%",
    backgroundColor: colors.gray,
    padding: 12, elevation: 5,
    shadowColor: colors.colorBlack,
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: 2 },
    shadowRadius: 5, borderRadius: 10
  },
  contactRow: {
    color: colors.colorBlack,
    fontFamily: Fonts.medium,
    fontSize: 12, lineHeight: 20
  },
  cardheader: {
    height: 40, width: "100%",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "rgb(245,245,245)",
    padding: 10
  },
  cardheaderText: {
    color: colors.colorBlack,
    fontFamily: Fonts.semiBold,
    fontSize: 14, marginLeft: 12
  },
  itemBackground: {
    backgroundColor: colors.colorWhite,
    margin: 12,
    marginBottom: 15,
    borderRadius: 10,
    padding: 0,
    elevation: 5,
    shadowColor: colors.colorBlack,
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: 2 },
    shadowRadius: 5,
    // height: 200
    // overflow: "hidden"
  },
  listofpassengersbtn: {
    backgroundColor: "rgb(0,114,208)",
    padding: 8,
    borderRadius: 5,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 1 },
    elevation: 4
  },
  styleFareAmountView: {
    height: 30,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",

  },
  arrowStyledown: {
    height: 12,
    width: 12

  },
  arrowStyleUp: {
    height: 12,
    width: 12,
    transform: [{ rotate: '180deg' }]
  }
})