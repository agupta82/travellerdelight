import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  Image,
  TouchableHighlight,
  ScrollView,
  StatusBar,
  Alert,
  Modal,
  SafeAreaView,
  Animated,
  DeviceEventEmitter
} from "react-native";

import NavigationBar from "../../../Helper Classes/NavigationBar";
import Images from "../../../Helper Classes/Images";
import NavigationServices from '../../../Helper Classes/NavigationServices'
//import APIManager from "../../../Helper Classes/APIManager/APIManagerUpdated";
import Spinner from "react-native-loading-spinner-overlay";
import { Button } from "../../../Helper Classes/Button";
import { colors } from "../../../Helper Classes/Colors";
import { commonstyle } from "../../../Helper Classes/Commonstyle";
//import moment from "moment"
import { PassengersModal } from "../../../Helper Classes/PassengerModal";
import { confireQuoteApi } from "../../../Helper Classes/APIManager/ApiProvider";
import { bookFlightApi } from "../../../Helper Classes/APIManager/ApiProvider";
import { connect } from "react-redux";
import { bookFlightAction, confirmQuoteAction } from '../../../Redux/Actions'
import StringConstants from "../../../Helper Classes/StringConstants";


class ReviewFlightBooking extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      traceId: null,
      spinner: false,
      fareQuotes: [],
      expandBaseFare: false,
      expandTaxes: false,
      maxHeight: 52,
      minHeight: 0,
      maxTaxHeight: 70,
      taxAnimation: new Animated.Value(),
      animation: new Animated.Value(),
      email: null,
      phoneNumber: null,
      flightType: null,
      origin: null,
      destination: null,
      isRoundTrip: null,
      isInterNational: null,
      fareDetails: null,
      passengers: null,
      flightData: null,
    }
    // this.total_Fare = 0;
    this.total_Fare = { amount: 0, currency: "INR" }
    this._renderFareDetailsView = this._renderFareDetailsView.bind(this);
    this.updateFareQuoteFromApi = this.updateFareQuoteFromApi.bind(this);

  }

  bookTicket = () => {

    let flightData = this.state.flightData;
    let flights = [];
    if (flightData.length == 2) {
      flightData.forEach(obj => {
        flights.push(obj.index);
      });
    } else {
      let obj = flightData[0];
      flights.push(obj.index);
    }

    this.APIFlightBooking(flights);

  }


  editPassengerHandler = () => {

    this.setState({
      isModalVisible: false,
    }, () => NavigationServices.goBack())
  }


  // APIConfirmFare(flights) {
  //   let param = {
  //     type: "TBO",
  //     flight: flights,
  //     traceId: this.state.traceId
  //   };

  //   APIManager.confireFareAPI(param, (success, response) => {
  //     if (!success) {
  //       console.log("error" + response);
  //       setTimeout(() => {
  //         alert(response);
  //       }, 100);
  //     } else {
  //       console.log("Fare API " + response);
  //     }
  //   });
  // }

  APIConfirmQuote(flights) {
    let param = {
      type: "TBO",
      flight: flights,
      traceId: this.state.traceId
    };

    console.log("param for Quotes" + JSON.stringify(param));


    this.props.confirmQuoteAction(param)
    // this.setState({ spinner: true });

    // confireQuoteApi(param).then(response => {
    //   console.log("success")
    //   console.log(response);
    //   this.setState({
    //     spinner: false
    //   })

    //   this.setState({
    //     fareQuotes: response
    //   })

    //   // setTimeout(() => {
    //   //   Alert.alert("Error", response, [
    //   //     { text: "OK", onPress: () => Actions.FlightSearch() }
    //   //   ]);
    //   // }, 100);


    // }).catch(error => {
    //   console.log("Failure")
    //   console.log(error)
    //   this.setState({
    //     spinner: false
    //   })

    //   setTimeout(() => {
    //     Alert.alert("Error", error, [
    //       { text: "OK", onPress: () => Actions.FlightSearch() }
    //     ]);
    //   }, 100);
    // })

  }


  APIFlightBooking(indexes) {


    // let adtDict = this.props.passengers[0];

    const price = this.total_Fare;
    let finalDict = {};

    if (this.state.flightType == 0) {

      let fareQuote = this.state.fareQuotes[0].OB.farequote;

      finalDict["OB"] = {
        Status: fareQuote.IsLCC,
        Baggage: [],
        MealDynamic: [],
        farequote: fareQuote
      };
      finalDict["IB"] = {
        Status: false,
        Baggage: "",
        MealDynamic: "",
        farequote: ""
      };

    } else {
      let OBfareQuote = this.state.fareQuotes[0].OB.farequote;
      let IBfareQuote = this.state.fareQuotes[1].IB.farequote;

      finalDict["OB"] = {
        Status: OBfareQuote.IsLCC,
        Baggage: [],
        MealDynamic: [],
        farequote: OBfareQuote
      };
      finalDict["IB"] = {
        Status: IBfareQuote.IsLCC,
        Baggage: [],
        MealDynamic: [],
        farequote: IBfareQuote
      };
    }

    let adlt = [];
    let child = [];
    let infant = [];
    this.state.passengers.forEach((obj) => {
      let passengerDict = {
        title: "",
        fname: "",
        mname: "",
        lname: "",
        dob: "",
        dep_baggage: "0",
        ret_baggage: "0",
        dep_meal: "0",
        ret_meal: "0",
        passport_no: "",
        passport_expiry: "",
        passportrqd: false

      }
      passengerDict.fname = obj.fName;
      passengerDict.mname = obj.mName;
      passengerDict.lname = obj.lName;
      passengerDict.dob = obj.dob;
      passengerDict.title = String(obj.title).substring(0, obj.title.length - 1)

      if (obj.passenger_type === "Adult") {
        adlt.push(passengerDict);
      } else if (obj.passenger_type === "Children") {
        child.push(passengerDict);
      } else {
        infant.push(passengerDict);
      }

    })

    let param = {
      email: this.state.email,
      mobile: this.state.phoneNumber,
      adt: adlt,
      chd: child,
      INF: infant,
      attributes: {
        traceId: this.state.traceId,
        index: indexes
      },
      type: "TBO",
      fQuoteSSR: finalDict,
      final_booking_amount: price.amount,
      price: price
    };

    console.log("BOOK_FLIGHT_REQ:")
    console.log(JSON.stringify(param))

    // this.setState({ spinner: true });

    // bookFlightApi(param).then(response => {

    //   this.setState({ spinner: false });
    //   console.log(response)
    //   setTimeout(() => {
    //     Actions.BookingConfirmation({
    //       status: 1,
    //       bookingId: response.booking_id
    //     });
    //   }, 100);

    // }).catch(err => {

    //   this.setState({
    //     spinner: false
    //   })

    //   setTimeout(() => alert(err), 1000);

    // })

    this.props.bookFlightAction(param)

    // APIManager.flightBooking(param1, (success, response) => {
    //   this.setState({ spinner: false });
    //   if (success) {
    //     console.log("booked successfully " + JSON.stringify(response));
    //     setTimeout(() => {
    //       Actions.BookingConfirmation({
    //         status: 1,
    //         bookingId: response.booking_id
    //       });
    //     }, 100);
    //   } else {
    //     console.log("error" + response);
    //     setTimeout(() => {
    //       Actions.BookingConfirmation({ status: 0, failedError: response });
    //     }, 100);
    //   }
    // });
  }


  componentDidMount() {

    DeviceEventEmitter.addListener(StringConstants.CONFIRM_QUOTE_EVENT, this.updateFareQuoteFromApi)

    let { navigation } = this.props;
    let flightData = navigation.getParam('flightdata', null);
    let traceId = navigation.getParam('traceid', null);
    let passengers = navigation.getParam('passengers', null);
    let flightType = navigation.getParam('flightType', null);
    let origin = navigation.getParam('origin', null);
    let destination = navigation.getParam('destination', null);
    let isRoundTrip = navigation.getParam('isRoundTrip', null);
    let email = navigation.getParam('email', null);
    let phoneNumber = navigation.getParam('phoneNumber', null);
    let isInterNational = navigation.getParam('isInterNational', null);
    //console.log("traceId:", JSON.stringify(traceId))
    //console.log("DDDeTa",JSON.stringify(flightData))

    let flights = [];

    if (flightData.length == 2) {
      flightData.forEach(obj => {
        let dict = {};
        dict["index"] = obj.index;
        dict["isLCC"] = obj.isLCC;
        flights.push(dict);
      });
    } else {
      let obj = flightData[0];
      let dict = {};
      dict["index"] = obj.index;
      dict["isLCC"] = obj.isLCC;
      flights.push(dict);
    }

    this.setState({
      fareDetails: flightData.fare,
      traceId: traceId,
      passengers: passengers,
      flightData: flightData,
      flightType: flightType,
      origin: origin,
      destination: destination,
      isRoundTrip: isRoundTrip,
      isInterNational: isInterNational,
      email: email,
      phoneNumber: phoneNumber
    }, () => {
      this.APIConfirmQuote(flights);
    });

    // this.APIConfirmFare(flights);


    // console.log(this.state..)
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.CONFIRM_QUOTE_EVENT, this.updateFareQuoteFromApi)
  }

  updateFareQuoteFromApi(response) {
    this.setState({
      fareQuotes: response
    })
  }

  toggleTax() {
    //Step 1
    let initialValue = this.state.expandTaxes ? this.state.maxTaxHeight + this.state.minHeight : this.state.minHeight;
    let finalValue = this.state.expandTaxes ? this.state.minHeight : this.state.maxTaxHeight + this.state.minHeight;

    this.setState({
      expandTaxes: !this.state.expandTaxes  //Step 2
    });

    this.state.taxAnimation.setValue(initialValue);  //Step 3
    Animated.spring(     //Step 4
      this.state.taxAnimation,
      {
        toValue: finalValue
      }
    ).start();  //Step 5
  }
  _renderfromToItem() {

    return (
      <View style={{ width: "100%", height: 70, justifyContent: "center", alignItems: "center", flexDirection: "row", backgroundColor: colors.colorBlue }}>
        <View style={{ flex: 2, alignItems: "center" }}>
          <Text style={{ color: colors.colorWhite, fontSize: 18, fontFamily: "Montserrat-Medium" }}>
            {this.state.origin}
          </Text>
        </View>
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", }}>

          <Image
            style={{ height: 20, width: 20 }}
            source={Images.imgrightarrowbig}
            tintColor={colors.colorWhite}
          />
          {this.state.isRoundTrip && <Image
            style={{ height: 20, width: 20 }}
            source={Images.imgleftarrowbig}
            tintColor={colors.colorWhite}
          />}
        </View>
        <View style={{ flex: 2, flexDirection: "row", alignItems: "center" }}>
          <Text style={{ color: colors.colorWhite, fontSize: 18, fontFamily: "Montserrat-Medium", flexGrow: 1, textAlign: "center" }} >
            {this.state.destination}
          </Text>
          <TouchableHighlight
            underlayColor={'transparent'}
            onPress={() => {

              NavigationServices.navigate('FlightDetails', {
                flightdata: this.state.flightData,
                isNavigateFromReviewPage: true,
                traceid: this.state.traceId
              });

            }
            }
          >
            <Image
              style={{ height: 20, width: 20, marginRight: 10 }}
              source={Images.imginfoi}
              tintColor={colors.colorWhite}
            />
          </TouchableHighlight>

        </View>

      </View>
    )
  }

  _renderContactDetails() {
    return (
      <View style={commonstyle.itemBackground}>

        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={commonstyle.itemTextHeading}>
            Contact Details
              </Text>
          <TouchableHighlight
            underlayColor={"transparent"}
            onPress={() => {
              NavigationServices.goBack()
            }}
            style={{ height: 20, width: 30, justifyContent: "center" }}>
            <Image
              style={{ height: 20, width: 22 }}
              source={Images.imgpencil}
              tintColor={colors.colorBlue}
            />
          </TouchableHighlight>
        </View>

        <View style={{ flexDirection: 'row', marginTop: 15 }}>
          <View style={{ justifyContent: 'center' }}>
            <Image
              style={{ height: 15, width: 20, tintColor: 'rgb(0,104,208)' }}
              source={Images.imgmail} />
          </View>
          <Text style={[commonstyle.greyText, { marginLeft: 15, fontSize: 14 }]}>
            {this.state.email}

          </Text>
        </View>

        <View style={{ flexDirection: 'row', marginTop: 15 }}>
          <Image
            style={{ height: 20, width: 20, tintColor: 'rgb(0,104,208)' }}
            source={Images.imgphone} />
          <Text style={[commonstyle.greyText, { marginLeft: 15, fontSize: 14 }]}>
            {this.state.phoneNumber}
          </Text>
        </View>

      </View>
    )
  }

  passengerDetailItem(item) {

    return (
      <View style={[commonstyle.itemBackground, { flexDirection: "row", alignItems: "center" }]}>
        <Image
          style={{ height: 15, width: 15 }}
          source={Images.imgusericon} />
        <Text style={{ marginLeft: 10, fontFamily: "Montserrat-Medium", flex: 1 }}>
          {item[0].title} {item[0].fName} {item[0].mName} {item[0].lName}
        </Text>
        <TouchableHighlight
          style={{ padding: 5 }}
          underlayColor={"transparent"}
          onPress={() => this.setState({ isModalVisible: true })}>
          <Text style={{ color: colors.colorBlue, fontFamily: "Montserrat-Medium", fontSize: 12 }}>
            View All
        </Text>
        </TouchableHighlight>
      </View>
    )

  }

  // _renderFareDetails(){
  //     return(
  //         <View style = {styles._renderFareDetailsStyle}>
  //         <Text style = {{marginTop:10,marginBottom:10,
  //             fontSize:18,fontFamily:"Montserrat-Regular"}}>
  //             Fare Details
  //         </Text>
  //         <View style ={{flexDirection:"row",justifyContent:"space-between"}}>
  //             <Text style = {styles.greyColorText}>
  //             Ticket Price 
  //             </Text>
  //             <Text style = {styles.blueColorText}>
  //              $ 347
  //             </Text>    
  //          </View>   
  //          <View style ={{flexDirection:"row",marginTop:10,justifyContent:"space-between"}}>
  //             <Text style = {styles.greyColorText}>
  //             Ticket Price 
  //             </Text>
  //             <Text style = {styles.blueColorText}>
  //             $ 347
  //             </Text>    
  //          </View>   
  //         </View>    
  //     )
  // }

  _renderFareAmountView(title, fare) {
    return (
      <View style={styles.styleFareAmountView}>
        <Text
          style={{ fontSize: 14, color: "rgb(0,114,198)", fontFamily: "Montserrat-Bold" }}
        >
          {title}
        </Text>
        <Text style={{ fontSize: 14, color: "black", fontFamily: "Montserrat-Medium" }}>{fare}</Text>
      </View>
    );
  }

  _renderFareDetailsView() {
    let fare = this.state.flightData[0].fare;
    if (fare == null) {
      return <View />;
    }



    let fareDetail = this.state.flightData[0].fareDetails;
    let fareDetail2 = null;


    let currency = fare.currency;
    let baseFare;
    let tax_charges;
    let discount;
    let totalFare = this.totalFare;
    let otherCharges;
    let tax;
    let serviceFee;
    let fuelCharge;
    if (this.state.isInterNational) {



      let fare = this.state.flightData[0].fare;
      baseFare = currency + " " + Math.ceil(fare.baseFare);
      otherCharges = currency + " " + Math.ceil(fare.otherCharges);
      discount = (fare.discount == "NaN" || fare.discount == null)
        ? "0"
        : fare.discount;
      discount = currency + " " + Math.round(discount);
      tax = currency + " " + fare.tax;

      serviceFee = currency + fare.serviceFee;
      fuelCharge = currency + fare.fuelCharge;

      tax_charges = fare.tax + fare.serviceFee + fare.fuelCharge;
      tax_charges = currency + " " + tax_charges;
      totalFare = currency + " " + Math.ceil(fare.agencyFare);

      // this.total_Fare = totalFare;
      this.total_Fare.currency = currency
      this.total_Fare.amount = fare.agencyFare

      console.log("TOTAL_FARE:1" + JSON.stringify(this.total_Fare))
    } else {

      if (this.state.isRoundTrip) {

        fareDetail2 = this.state.flightData[1].fareDetails;

        let fare1 = this.state.flightData[0].fare;
        // let fareDetail = this.state.flightData[0].fareDetails;
        let fare2 = this.state.flightData[1].fare;

        baseFare = Math.ceil(fare1.baseFare) + Math.ceil(fare2.baseFare);
        baseFare = currency + " " + baseFare;

        otherCharges = Math.ceil(fare1.otherCharges) + Math.ceil(fare2.otherCharges);
        otherCharges = currency + " " + otherCharges;

        let discount1 = (fare1.discount == "NaN" || fare1.discount == null)
          ? "0"
          : fare1.discount;
        discount1 = Math.round(discount1);

        let discount2 = (fare2.discount == "NaN" || fare2.discount == null)
          ? "0"
          : fare2.discount;
        discount2 = Math.round(discount2);

        discount = discount1 + discount2;
        discount = currency + " " + discount;

        tax = fare1.tax + fare2.tax;
        tax = currency + tax;

        serviceFee = fare1.serviceFee + fare2.serviceFee;
        serviceFee = currency + serviceFee;

        fuelCharge = fare1.fuelCharge = fare2.fuelCharge;
        fuelCharge = currency + fuelCharge;



        tax_charges = fare1.tax + fare2.tax + fare1.serviceFee + fare2.serviceFee + fare1.fuelCharge + fare2.fuelCharge;
        tax_charges = currency + " " + tax_charges;

        totalFare = Math.ceil(fare1.agencyFare) + Math.ceil(fare2.agencyFare)
        totalFare = currency + " " + totalFare;
        // this.total_Fare = totalFare;

        this.total_Fare.currency = currency
        this.total_Fare.amount = Math.ceil(fare1.agencyFare) + Math.ceil(fare2.agencyFare)


        console.log("TOTAL_FARE:2" + JSON.stringify(this.total_Fare))

      } else {

        let fare = this.state.flightData[0].fare;
        baseFare = currency + " " + Math.ceil(fare.baseFare);
        otherCharges = currency + " " + Math.ceil(fare.otherCharges);
        discount = (fare.discount == "NaN" || fare.discount == null)
          ? "0"
          : fare.discount;
        discount = currency + " " + Math.round(discount);
        tax = currency + " " + fare.tax;

        serviceFee = currency + " " + fare.serviceFee;
        fuelCharge = currency + " " + fare.fuelCharge;

        tax_charges = fare.tax + fare.serviceFee + fare.fuelCharge;
        tax_charges = currency + " " + tax_charges;
        totalFare = currency + " " + Math.ceil(fare.agencyFare);
        // this.total_Fare = totalFare;

        this.total_Fare.currency = currency
        this.total_Fare.amount = fare.agencyFare


        console.log("TOTAL_FARE:3" + JSON.stringify(this.total_Fare))
      }
    }

    //   let fare = this.state.fareDetails


    return (

      <View style={commonstyle.itemBackground}>
        <Text style={commonstyle.itemTextHeading}>
          Fare Details
            </Text>
        <View style={styles.styleFareAmountView}>
          <Text
            style={{ fontSize: 14, color: "rgb(0,114,198)", fontFamily: "Montserrat-Bold", flex: 1 }}
          >
            Base Fare
             </Text>
          <TouchableHighlight

            style={{ justifyContent: "center", alignItems: "center", alignSelf: "center", height: 30, width: 40 }}
            underlayColor={"transparent"}
            onPress={() => {
              this.toggle()
            }}>
            <Image
              style={this.state.expandBaseFare ? styles.arrowStyleUp : styles.arrowStyledown}
              source={Images.imgarrow}
              resizeMode={"contain"}
            />

          </TouchableHighlight>

          <Text style={{ fontSize: 14, color: "black", fontFamily: "Montserrat-Medium", textAlign: "right", flex: 1 }}>{baseFare}</Text>
        </View>

        {this.state.expandBaseFare && this.renderExpandedBaseFare(currency, fareDetail, fareDetail2)}

        <View style={[styles.styleFareAmountView]}>
          <Text
            style={{ fontSize: 14, color: "rgb(0,114,198)", fontFamily: "Montserrat-Bold", flex: 1 }}
          >
            Taxes & charges
                </Text>

          <TouchableHighlight

            style={{ justifyContent: "center", alignItems: "center", alignSelf: "center", height: 30, width: 40 }}
            underlayColor={"transparent"}
            onPress={() => {
              this.toggleTax()
            }}>
            <Image
              style={this.state.expandTaxes ? styles.arrowStyleUp : styles.arrowStyledown}
              source={Images.imgarrow}
              resizeMode={"contain"}
            />

          </TouchableHighlight>

          <Text style={{ fontSize: 14, color: "black", textAlign: "right", fontFamily: "Montserrat-Medium", flex: 1 }}>{tax_charges}</Text>
        </View>






        {this.state.expandTaxes && this.renderExpandedTaxes(tax, serviceFee, fuelCharge)}

        {this._renderFareAmountView("Other Charges", otherCharges)}

        {this._renderFareAmountView("Discount", discount)}

        <View
          style={{ height: 2, width: "30%", alignSelf: "center", backgroundColor: "rgb(211,211,211)", margin: 20 }}
        />

        {this._renderFareAmountView("Total", totalFare)}
      </View>

    );
  }


  toggle() {
    //Step 1
    let initialValue = this.state.expandBaseFare ? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
      finalValue = this.state.expandBaseFare ? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

    this.setState({
      expandBaseFare: !this.state.expandBaseFare  //Step 2
    });

    this.state.animation.setValue(initialValue);  //Step 3
    Animated.spring(     //Step 4
      this.state.animation,
      {
        toValue: finalValue
      }
    ).start();  //Step 5
  }

  renderExpandedTaxes(tax, serviceFee, fuelCharge) {
    return (
      <Animated.View
        style={[styles.styleFareAmountView,
        {
          height: this.state.taxAnimation,
          flexDirection: "column", paddingLeft: 10, paddingRight: 10
        }]}>

        <View style={[styles.styleFareAmountView, { height: 18 }]}>
          <Text
            style={{ fontSize: 13, color: "rgb(0,114,198)", fontFamily: "Montserrat-regular" }}
          >
            Tax
           </Text>
          <Text style={{ fontSize: 13, color: "black", fontFamily: "Montserrat-Medium" }}>{tax}</Text>
        </View>

        <View style={[styles.styleFareAmountView, { height: 18 }]}>
          <Text
            style={{ fontSize: 13, color: "rgb(0,114,198)", fontFamily: "Montserrat-regular" }}
          > Passenger Service Fee
            </Text>
          <Text style={{ fontSize: 13, color: "black", fontFamily: "Montserrat-Medium" }}>{serviceFee}</Text>
        </View>

        <View style={[styles.styleFareAmountView, { height: 18 }]}>
          <Text
            style={{ fontSize: 13, color: "rgb(0,114,198)", fontFamily: "Montserrat-regular", marginLeft: 2 }}
          >
            Airline Fuel Surcharge
           </Text>
          <Text style={{ fontSize: 13, color: "black", fontFamily: "Montserrat-Medium" }}>{fuelCharge}</Text>
        </View>
      </Animated.View>
    )
  }


  renderExpandedBaseFare(currency, fareDetail, fareDetail2) {
    if (fareDetail2 == null) {

      return (
        <Animated.View style={[styles.styleFareAmountView, { height: this.state.animation }]}>

          <FlatList
            data={fareDetail}
            renderItem={({ item }) => {
              let perHeadCost = Math.ceil(item.baseFare / item.passengerCount);
              perHeadCost = currency + " " + perHeadCost;
              return (
                <View style={[styles.styleFareAmountView, { paddingLeft: 10, paddingRight: 10, height: 18 }]}>
                  <Text
                    style={{ fontSize: 13, color: "rgb(0,114,198)", fontFamily: "Montserrat-regular" }}
                  >
                    {item.passengerCount} {item.passengerType == 1 ? "Adult" : item.passengerType == 2 ? "Child " : "Infant"}
                  </Text>
                  <Text style={{ fontSize: 13, color: "black", fontFamily: "Montserrat-Medium" }}>{perHeadCost}</Text>
                </View>)
            }}
          />
        </Animated.View>
      )
    }
    else {

      return (
        <Animated.View style={[styles.styleFareAmountView, { height: this.state.animation }]}>

          <FlatList
            data={fareDetail}
            renderItem={({ item, index }) => {
              let item2 = fareDetail2[index];
              let cost = Math.ceil(item2.baseFare / item2.passengerCount)
              let perHeadCost = Math.ceil(item.baseFare / item.passengerCount);
              perHeadCost = cost + perHeadCost
              return (
                <View style={[styles.styleFareAmountView, { paddingLeft: 10, paddingRight: 10, height: 18 }]}>
                  <Text
                    style={{ fontSize: 13, color: "rgb(0,114,198)", fontFamily: "Montserrat-regular" }}
                  >
                    {item.passengerCount} {item.passengerType == 1 ? "Adult" : item.passengerType == 2 ? "Child " : "Infant"}
                  </Text>
                  <Text style={{ fontSize: 13, color: "black", fontFamily: "Montserrat-Medium" }}>{perHeadCost}</Text>
                </View>)
            }}
          />
        </Animated.View>
      )


    }


  }



  modalcloseHandler = () => {
    this.setState({ isModalVisible: false })
  }



  // data = [{ "passenger_type": "Adult", "title": "Mr.", "fName": "Bxbchch", "mName": "", "lName": "Bxhxhhch", "dob": "2014-05-03T00:00:00", "nationality": "", "gender": "Male", "dep_beg": "", "dep_meal": "" }, { "passenger_type": "Adult", "title": "Mrs.", "fName": "Cncn n n", "mName": "", "lName": "Bxbxb bx", "dob": "2009-05-03T00:00:00", "nationality": "", "gender": "Male", "dep_beg": "", "dep_meal": "" }, { "passenger_type": "Adult", "title": "Mr.", "fName": "Bxbchch", "mName": "", "lName": "Bxhxhhch", "dob": "2014-05-03T00:00:00", "nationality": "", "gender": "Male", "dep_beg": "", "dep_meal": "" }, { "passenger_type": "Adult", "title": "Mrs.", "fName": "Cncn n n", "mName": "", "lName": "Bxbxb bx", "dob": "2009-05-03T00:00:00", "nationality": "", "gender": "Male", "dep_beg": "", "dep_meal": "" }]
  render() {
    if (!this.state.flightData) return <View></View>
    //  console.log("flightData",JSON.stringify(this.state.flightData))
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
        <NavigationBar showBack title="Review Booking" />
        <StatusBar barStyle="light-content" />

        <Spinner
          visible={this.props.isLoading}
          textContent={"Loading..."}
          textStyle={{ color: "white" }}
        />
        <ScrollView style={{ flex: 1 }}>

          {this._renderfromToItem()}
          {this.passengerDetailItem(this.state.passengers)}
          {this._renderFareDetailsView()}
          {this._renderContactDetails()}

        </ScrollView>
        <View
          style={{ padding: 10 }}>
          <Button
            height={45}
            borderRadius={7}
            text={"Book Ticket"}
            onPress={() => {
              this.bookTicket()
            }}
          />
        </View>
        <Modal

          transparent={true}
          visible={this.state.isModalVisible}>
          <PassengersModal
            passengers={this.state.passengers}
            editPassengerHandler={this.editPassengerHandler}
            modalCloseHandler={this.modalcloseHandler}
          />
        </Modal>
      </SafeAreaView>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    bookFlightAction: (data) => dispatch(bookFlightAction(data)),
    confirmQuoteAction: (data) => dispatch(confirmQuoteAction(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReviewFlightBooking)

const styles = StyleSheet.create({

  listofpassengersbtn: {
    backgroundColor: "rgb(0,114,208)",
    padding: 8,
    borderRadius: 5,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 1 },
    elevation: 4
  },
  styleFareAmountView: {
    height: 30,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",

  },
  arrowStyledown: {
    height: 12,
    width: 12

  },
  arrowStyleUp: {
    height: 12,
    width: 12,
    transform: [{ rotate: '180deg' }]
  }
})