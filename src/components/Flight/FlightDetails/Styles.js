import { StyleSheet } from "react-native"
import { colors } from "../../../Helper Classes/Colors";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(244,248,254,1)"
  },
  itemContainer: {
    margin: 10,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    shadowRadius: 3,
    elevation: 4,
    padding: 15
  },
  Buttons: {
    height: 50,
    width: "90%",
    backgroundColor: "rgb(0,104,208)",
    margin: 10,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 7,
    alignSelf: "center",
  },
  modalclosebtn: {
    alignSelf: "center", marginTop: 10,
    backgroundColor: "rgb(0,104,208)",
    padding: 20, paddingTop: 8,
    paddingBottom: 8, borderRadius: 5
  },
  modalstyle: {
    flex: 1,
    backgroundColor: colors.colorBlue
  }
});
