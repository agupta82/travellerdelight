import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableHighlight,
  StyleSheet,
  ScrollView,
  Switch,
  DeviceEventEmitter,
  SectionList,
  SafeAreaView,
  WebView,
  StatusBar,
  Modal
} from "react-native";
import NavigationBar from "../../../Helper Classes/NavigationBar";
import Images from "../../../Helper Classes/Images";
import NavigationServices from '../../../Helper Classes/NavigationServices'
import Dash from "react-native-dash";
import { convertMinsIntoHandM } from "../../../Utility";

import Spinner from "react-native-loading-spinner-overlay";
import { imageBaseUrl } from "../../../Helper Classes/APIManager/APIConstants";
import { commonstyle } from "../../../Helper Classes/Commonstyle";
import { getFlightRuleApi } from "../../../Helper Classes/APIManager/ApiProvider"
import { Button } from "../../../Helper Classes/Button";
import { styles } from "./Styles";
import { connect } from "react-redux";
import { getFlightRuleAction } from '../../../Redux/Actions'
import StringConstants from '../../../Helper Classes/StringConstants';
import moment from "moment";


// var dateFormat = require("dateformat");

class FlightDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isNavigateFromReviewPage: null,
      flightType: null,
      origin: null,
      destination: null,
      isRoundTrip: null,
      isInterNational: null,
      switch: false,
      nonRefundable: true,
      spinner: false,
      visible: false,
      disabled: false,
      // item: this.props.flightdata,
      flightData: [],
      finalData: [],
      seactionHeaderDict: {
        origin: "",
        destination: "",
        isRefundable: "",
        dept_date: "",
        stops: 0,
        duration: "",

        //if isLcc true then meal is paid
        isLCC: false
      },
      listDict: {
        key: "",
        data: []
      },
      traceId: "",
      passengerDetails: null,
      loading: false,
      html: "",
      mealType: ""
    };
    this._renderListItem = this._renderListItem.bind(this);
    this.updateFlightRuleFromApi = this.updateFlightRuleFromApi.bind(this);
  }

  //MARK:------------------------- Component Lifecycle Methods ----------------
  componentDidMount() {
    let { navigation } = this.props;
    let flightData = navigation.getParam('flightdata', null);
    let traceId = navigation.getParam('traceid', null);
    let passengers = navigation.getParam('passengers', null);
    let flightType = navigation.getParam('flightType', null);
    let origin = navigation.getParam('origin', null);
    let destination = navigation.getParam('destination', null);
    let isRoundTrip = navigation.getParam('isRoundTrip', null);
    let isInterNational = navigation.getParam('isInterNational', null);
    let isNavigateFromReviewPage = navigation.getParam('isNavigateFromReviewPage', null);
    let finalData = [];

    if (flightData.length == 2) {

      finalData = this.domesticRoundTripData(flightData);

    } else {
      let dict = flightData[0];
      // let dict = dict1[0]
      let flightSagments = dict.flightSegment;

      if (flightSagments.length > 1) {
        let dataArr = this.calculateInterNationalRoundTrip(
          dict,
          flightSagments
        );
        finalData = dataArr;
      } else {
        let listDict = this.calculateDomesticSingle(dict, flightSagments);
        finalData.push(listDict);
      }
    }

    this.setState({
      flightData: flightData,
      finalData: finalData,
      traceId: traceId,
      passengerDetails: passengers,
      flightType: flightType,
      origin: origin,
      destination: destination,
      isRoundTrip: isRoundTrip,
      isInterNational: isInterNational,
      isNavigateFromReviewPage: isNavigateFromReviewPage
    }, () => {
      DeviceEventEmitter.addListener(StringConstants.FLIGHT_RULE_EVENT, this.updateFlightRuleFromApi)
    });



  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.FLIGHT_RULE_EVENT, this.updateFlightRuleFromApi)
  }

  updateFlightRuleFromApi(data) {
    if (data) {
      let hml = "";
      data.forEach((element, index) => {
        var num = index + 1;
        hml = hml + "<H1 style='text-align:center;font-size:15px;'>Flight Rule for flight " + num + "</H1>" + element.FareRuleDetail
      });
      this.setState({
        html: hml,
        visible: true
      })
    }
  }

  disabledMultipleTap() {
    this.setState({
      disabled: true,
    });
    setTimeout(() => {
      this.setState({
        disabled: false,
      });
    }, 3000)
  }

  getFlightRule() {
    let param = {
      type: this.state.flightData[0].source,
      flight: [{
        index: this.state.flightData[0].index
        , isLCC: this.state.flightData[0].isLCC
      }],
      traceId: this.state.traceId
    }
    this.setState({
      spinner: true
    })
    console.log("get flight rule");
    console.log(param);

    this.props.getFlightRuleAction(param)
  }

  //MARK:-------------------- Calculate Domestic Single Way Data ------------

  calculateDomesticSingle(dict, flightSagments) {
    let headerDict = this.state.seactionHeaderDict;
    let listDict = this.state.listDict;

    headerDict.origin = flightSagments[0][0].origin.airport.airportCode;
    headerDict.destination = dict.destination;
    headerDict.dept_date = flightSagments[0][0].stopPointDepartureTime;
    headerDict.isRefundable = dict.isRefundable;
    headerDict.stops = flightSagments.length - 1;
    headerDict.isLCC = dict.isLCC;

    let duration = 0;
    flightSagments[0].forEach(item => {
      duration = duration + item.duration;
    });

    headerDict.duration = duration;

    listDict.key = headerDict;
    listDict.data = flightSagments[0];
    return listDict;
  }

  //MARK:-------------- Calculate InterNational round Trip Data --------------

  calculateInterNationalRoundTrip(dict, flightSagments) {
    let finalData = [];
    flightSagments.forEach(objArr => {
      let headerDict = {
        origin: "",
        destination: "",
        isRefundable: "",
        dept_date: "",
        stops: 0,
        duration: "",
        isLCC: dict.isLCC
      };
      let listDict = {
        key: "",
        data: []
      };

      let flightOriginDict = objArr[0];
      let flightDestdict = objArr[objArr.length - 1];

      headerDict.origin = flightOriginDict.origin.airport.airportCode;
      headerDict.destination = flightDestdict.destination.airport.airportCode;

      headerDict.dept_date = flightOriginDict.stopPointDepartureTime;

      headerDict.isRefundable = dict.isRefundable;
      headerDict.stops = objArr.length - 1;


      let duration = 0;
      objArr.forEach(item => {
        duration = duration + item.duration;
      });

      headerDict.duration = duration;

      listDict.key = headerDict;
      listDict.data = objArr;

      finalData.push(listDict);
    });
    return finalData;
  }

  //MARK:----------------- Domestic Round Trip Data --------------
  domesticRoundTripData(flightData) {
    let finalData = [];
    flightData.forEach((obj, index) => {
      let flightSegment = obj.flightSegment[0];
      let headerDict = {
        origin: "",
        destination: "",
        isRefundable: "",
        dept_date: "",
        stops: 0,
        duration: "",
        isLCC: obj.isLCC
      };
      let listDict = {
        key: "",
        data: []
      };

      headerDict.origin = flightSegment[0].origin.airport.airportCode;
      headerDict.destination = obj.destination;
      headerDict.dept_date = flightSegment[0].stopPointDepartureTime;
      headerDict.isRefundable = obj.isRefundable;
      headerDict.stops = flightSegment.length - 1;

      let duration = 0;

      flightSegment.forEach(item => {
        duration = duration + item.duration;
      });

      headerDict.duration = duration;

      listDict.key = headerDict;
      listDict.data = flightSegment;

      finalData.push(listDict);
    });

    return finalData;
  }

  calculateArrivalAndDepartureTime(item) {
    let data = item.flightSegment;
    if (data.lenght == 1) {
      let deptTime = moment(data[0].stopPointDepartureTime).format("hh:mm");
      let arrTime = moment(data[0].stopPointArrivalTime).format("hh:mm");
      return deptTime + "-" + arrTime;
    } else {
      let deptTime = moment(data[0].stopPointDepartureTime).format("hh:mm");
      let arrTime = moment(data[data.length - 1].stopPointArrivalTime).format("hh:mm");
      return deptTime + "-" + arrTime;
    }
  }

  calculateDurationTime(data) {
    let totalDuration = 0;
    for (i = 0; i < data.length; i++) {
      let item = data[i];
      totalDuration = totalDuration + item.duration;
    }
    return this.convertMinsIntoHandM(totalDuration);
  }

  convertMinsIntoHandM(n) {
    var num = n;
    var hours = num / 60;
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + "h" + rminutes + "m";
  }

  flightFromToItem({ section }) {
    let dict = section.key;
    console.log("DICT:" + JSON.stringify(dict))
    return (
      <View
        style={[commonstyle.itemBackground, { backgroundColor: "rgb(0,104,208)" }]}
      >
        <View style={{ flexDirection: "row", width: "100%" }}>
          <View style={{ width: "10%", alignItems: "flex-start" }}>
            <Image style={{ height: 25, width: 25 }} source={Images.imgPlane} />
          </View>
          <View style={{ width: "45%", paddingLeft: 10 }}>
            <Text style={{ fontSize: 14, color: "white", fontFamily: "Montserrat-SemiBold" }}>
              {dict.origin} - {dict.destination}
            </Text>
            <TouchableHighlight
              style={{
                height: 25,
                width: "100%",
                backgroundColor: (section.isRefundable) ? 'green' : 'red',
                marginTop: 10,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 5,

              }}
            >
              <Text style={{ fontSize: 13, color: "white", fontFamily: "Montserrat-SemiBold" }}>
                {(section.isRefundable) ? 'Refundable' : 'Non-Refundable'}
              </Text>
            </TouchableHighlight>
          </View>

          <View style={{ width: "45%", alignItems: "flex-end" }}>
            <Text style={{ fontSize: 12, color: "white", textAlign: "right", fontFamily: "Montserrat-Medium" }}>
              {moment(dict.dept_date).format("DD MMM YYYY")}
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: "white",
                textAlign: "right",
                marginTop: 10,
                fontFamily: "Montserrat-Medium"
              }}
            >
              {dict.stops} Stop | {convertMinsIntoHandM(dict.duration)}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  cancellationPolicyItem() {
    return (

      <TouchableHighlight
        underlayColor="transparent"
        onPress={() => {
          this.getFlightRule();

        }}
      >
        <View
          style={[
            commonstyle.itemBackground,
            { flexDirection: "row", justifyContent: "space-between" }
          ]}
        >
          <View>
            <Text
              style={{
                color: "rgb(0,104,208)",
                fontSize: 15,
                fontFamily: "Montserrat-SemiBold"
              }}
            >
              Cancellation and Baggage Policy
            </Text>
            <Text style={commonstyle.greyText}>Rules and Policy</Text>
          </View>
          <Image
            style={{ height: 15, width: 10, }}
            source={Images.imgrightarrow}
          />
        </View>
      </TouchableHighlight>
    );
  }

  travelAssistanceInsuranceItem() {
    return (
      <View
        style={[
          styles.itemContainer,
          { justifyContent: "center", alignItems: "flex-start" }
        ]}
      >
        <Text style={{ color: "black", fontWeight: "500" }}>
          Travel Assistance & Insurance
        </Text>
        <View style={{ marginTop: 10, flexDirection: "row" }}>
          <Text style={{ width: "80%" }}>
            Yes add travel assistance and Insurance to protect my trip
          </Text>

          <Switch
            trackColor="rgb(0,104,208)"
            style={{
              transform: [{ scaleX: 0.9 }, { scaleY: 0.75 }],
              marginLeft: 7
            }}
            onValueChange={() => {
              this.setState({
                switch: !this.state.switch
              });
            }}
            value={this.state.switch}
            onTintColor="rgb(0,104,208)"
          //  tintColor='rgb(0,104,208)'
          // ios_backgroundColor='rgb(0,104,208)'
          />
        </View>
        <Text
          style={{
            color: "black",
            fontWeight: "500",
            alignSelf: "flex-start",
            marginTop: 10,
            marginBottom: 10
          }}
        >
          Rs 269 for 1 passengers
        </Text>
        <Text numberOfLines={0}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tempus
          quam leo, et convallis libero imperdiet nec. Donec tincidunt et eros
          eget vulputate. Quisque lacinia dignissim volutpat. Mauris cursus vel
          dui eget consectetur. Fusce convallis, diam semper aliquet porttitor,
          est tellus tincidunt mi, eu vestibulum diam neque vel dolor. Donec et
          vehicula nisl. Phasellus vel tristique nunc. Cras ultrices finibus
          faucibus. Aliquam at libero ipsum. Praesent malesuada non lorem at
          volutpat.
        </Text>
        <TouchableHighlight
          underlayColor="transparent"
          onPress={() => {
            alert("terms and condition");
          }}
        >
          <Text style={{ color: "rgb(0,104,208)", marginTop: 10 }}>
            Terms & Condition
          </Text>
        </TouchableHighlight>
      </View>
    );
  }

  continueBtn() {
    return (
      <View style={{ margin: 10 }}>
        <Button
          height={45}
          borderRadius={5}
          text={"Continue"}
          disabled={this.state.disabled}
          onPress={
            () => {
              this.disabledMultipleTap();
              // console.log("traceId:",JSON.stringify(this.state.traceId))
              NavigationServices.navigate('PassengerDetails', {
                flightdata: this.state.flightData,
                traceid: this.state.traceId,
                passengers: this.state.passengerDetails,
                flightType: this.state.flightType,
                origin: this.state.origin,
                destination: this.state.destination,
                isRoundTrip: this.state.isRoundTrip,
                isInterNational: this.state.isInterNational,
              });
            }}
        />
      </View>
    );
  }

  _renderVerticalDashLine() {
    return (
      <Dash
        style={{
          height: 1,
          width: "98.5%",
          alignSelf: "center"
          // position: "absolute"
        }}
        dashColor="gray"
        dashLength={8}
      />
    );
  }

  _renderListItem({ item, index, section }) {
    console.log(section)
    console.log(item)
    let destTerminal = item.destination.airport.terminal;
    let originTerminal = item.origin.airport.terminal;
    return (
      <View
        style={[commonstyle.itemBackground, { padding: 5, marginTop: 0 }]}
      >
        <View style={{ flexDirection: "row", padding: 15 }}>
          <Image
            style={{ height: 40, width: 40, borderRadius: 20 }}
            source={{ uri: imageBaseUrl + item.airline.image }}
          />
          <View
            style={{
              justifyContent: "space-between",
              alignItems: "center",
              paddingLeft: 10
            }}
          >
            <Text style={[commonstyle.blackSemiBold, { fontSize: 14 }]}>
              {item.airline.airlineName}
            </Text>
            <Text style={[commonstyle.greySemiBold, { fontSize: 13 }]}>
              {item.airline.airlineCode} {item.airline.flightNumber}{" "}
              {item.airline.fareClass}
            </Text>
          </View>
        </View>

        <Dash
          style={{
            height: 1,
            width: "98%",
            alignSelf: "center"
            // position: "absolute"
          }}
          dashColor="gray"
          dashLength={8}
        />

        <View style={{ padding: 15, flexDirection: "row" }}>
          <View style={{ width: "50%" }}>
            <Text style={[commonstyle.blackText, { fontSize: 14 }]}>
              {item.origin.airport.cityName}
              {" ("}
              {item.origin.airport.airportCode}
              {")"}
            </Text>
            <Text style={[commonstyle.blackText, { marginTop: 5, fontSize: 14, }]}>
              {moment(item.stopPointDepartureTime).format("DD MMM, HH:mm")}
            </Text>
            {originTerminal !== "" && <Text style={[commonstyle.greyText, { marginTop: 5, fontSize: 12, }]}>
              Terminal - {originTerminal}
            </Text>}
          </View>
          <View style={{ width: "50%", justifyContent: "flex-end" }}>
            <Text style={[commonstyle.blackText, { fontSize: 14, textAlign: "right" }]}>
              {item.destination.airport.cityName}
              {" ("}
              {item.destination.airport.airportCode}
              {")"}
            </Text>
            <Text
              style={[commonstyle.blackText, {
                marginTop: 5,
                fontSize: 14,

                textAlign: "right"
              }]}
            >
              {moment(item.stopPointArrivalTime).format("DD MMM, HH:mm")}

            </Text>
            {destTerminal !== "" && <Text
              style={[commonstyle.greyText, {
                marginTop: 5,
                fontSize: 12,
                textAlign: "right"
              }]}
            >
              Terminal - {destTerminal}
            </Text>}
          </View>
        </View>

        <Dash
          style={{
            height: 1,
            width: "98%",
            alignSelf: "center"
            // position: "absolute"
          }}
          dashColor="gray"
          dashLength={8}
        />

        <View style={{ padding: 15, flexDirection: "row" }}>
          <View style={{ width: "70%", justifyContent: "space-between" }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Image
                style={{ height: 20, width: 20 }}
                source={Images.imgbaggage}
                resizeMode="contain"
              />
              <Text style={[commonstyle.blackText, { marginLeft: 5, fontSize: 12 }]}>
                {item.baggage} Check-In,  {item.cabinBaggage} Cabin
              </Text>
            </View>

            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop: 10
              }}
            >
              <Image
                style={{ height: 20, width: 20 }}
                source={Images.imgpaidmeal}
                resizeMode="contain"
              />
              <Text style={[commonstyle.blackText, { marginLeft: 5, fontSize: 12, color: "black" }]}>
                Meal {section.key.isLCC ? "Paid" : "Free"}
              </Text>

            </View>
          </View>

        </View>
      </View>
    );

  }

  render() {
    if (this.state.flightData == null) { return <View></View> }
    return (
      <SafeAreaView style={styles.container}>
        <NavigationBar showBack title="Flight Details" />
        <StatusBar barStyle="light-content" />
        <ScrollView>
          <Spinner
            style={{ flex: 1 }}
            textContent={"Loading..."}
            visible={this.props.isLoading}
            textStyle={{ color: "white" }}
          />

          <View >
            <SectionList
              sections={this.state.finalData}
              renderItem={this._renderListItem}
              extraData={this.state.loading}
              renderSectionHeader={this.flightFromToItem}
              keyExtractor={(item, index) => index.toString()}
            />

            {/* {this._fareDetailItem()} */}
            {this.cancellationPolicyItem()}
            {/* {this.travelAssistanceInsuranceItem()} */}
            <Modal
              transparent={false}
              visible={this.state.visible}>

              <View style={styles.modalstyle}>
                <WebView
                  originWhitelist={['*']}
                  source={{ html: this.state.html }}
                />
                <TouchableHighlight
                  style={styles.modalclosebtn}
                  onPress={() => {
                    this.setState({
                      visible: false
                    })
                  }}>
                  <Text style={{ color: "white", fontSize: 15, fontFamily: "Montserrat-Medium" }}>
                    Close
              </Text>
                </TouchableHighlight>
              </View>
            </Modal>
          </View>
        </ScrollView>
        {this.state.isNavigateFromReviewPage ? null : this.continueBtn()}
      </SafeAreaView>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getFlightRuleAction: (data) => dispatch(getFlightRuleAction(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FlightDetails)