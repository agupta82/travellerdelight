import React, { Component } from "react";
import {
  View, Dimensions,
  Text,
  Image,
  TouchableHighlight,
  StyleSheet,
  Platform,
  Switch,
  DeviceEventEmitter,
  SectionList,
  WebView,
  StatusBar,
  Modal,
  TouchableOpacity,
  TextInput
} from "react-native";
import NavigationBar from "../../../Helper Classes/NavigationBar";
import Images from "../../../Helper Classes/Images";
import NavigationServices from './../../../Helper Classes/NavigationServices'
import Dash from "react-native-dash";
import { convertMinsIntoHandM } from "../../../Utility";

import Spinner from "react-native-loading-spinner-overlay";
import { imageBaseUrl } from "../../../Helper Classes/APIManager/APIConstants";
import { commonstyle } from "../../../Helper Classes/Commonstyle";
import { getFlightRuleApi } from "../../../Helper Classes/APIManager/ApiProvider"
import { Button } from "../../../Helper Classes/Button";
import { styles } from "./Styles";
import { connect } from "react-redux";
import { getFlightRuleAction } from '../../../Redux/Actions'
import StringConstants from '../../../Helper Classes/StringConstants';
import moment from "moment";
import { SafeAreaView, NavigationEvents } from "react-navigation";
import { colors } from "../../../Helper Classes/Colors";
import Header from "../../../Helper Classes/CommonHeader";
import Fonts from "../../../Helper Classes/Fonts";
import BottomStrip from "../../../Helper Classes/BottomStrip";
import Flightadata from "../../../json/FlightData";
const screenWidth = Dimensions.get("window").width;

class FlightDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalFare: 0,
      isNavigateFromReviewPage: null,
      flightType: null,
      origin: null,
      destination: null,
      isRoundTrip: null,
      isInterNational: null,
      switch: false,
      nonRefundable: true,
      spinner: false,
      visible: false,
      disabled: false,
      // item: this.props.flightdata,
      flightData: [],
      finalData: [],
      seactionHeaderDict: {
        origin: "",
        destination: "",
        isRefundable: "",
        dept_date: "",
        stops: 0,
        duration: "",

        //if isLcc true then meal is paid
        isLCC: false
      },
      listDict: {
        key: "",
        data: []
      },
      traceId: "",
      passengerDetails: null,
      loading: false,
      html: "",
      mealType: "",
      currency: ""
    };
    this._renderListItem = this._renderListItem.bind(this);
    this.updateFlightRuleFromApi = this.updateFlightRuleFromApi.bind(this);
  }

  //MARK:------------------------- Component Lifecycle Methods ----------------
  componentDidMount() {
    let { navigation } = this.props;
    console.log("flight details hello");
    // let flightData = Flightadata;
    // let passengers = {
    //   adult: 2,
    //   child: 2,
    //   infant: 1
    // };
    let flightData = navigation.getParam('flightdata', null);
    let passengers = navigation.getParam('passengers', null);
    let totalFare = navigation.getParam("totalFare", 0);
    // console.clear();
    console.log("totalFare:--" + totalFare);
    console.log(JSON.stringify(flightData));
    let traceId = navigation.getParam('traceid', null);
    console.log(JSON.stringify(passengers));
    let flightType = navigation.getParam('flightType', null);
    let origin = navigation.getParam('origin', null);
    let destination = navigation.getParam('destination', null);
    let isRoundTrip = navigation.getParam('isRoundTrip', null);
    let isInterNational = navigation.getParam('isInterNational', null);
    let currency = navigation.getParam('currency', null);
    let isNavigateFromReviewPage = navigation.getParam('isNavigateFromReviewPage', null);
    let finalData = [];
    if (flightData.length == 2) {
      finalData = this.domesticRoundTripData(flightData);
    } else {
      let dict = flightData[0];
      // let dict = dict1[0]
      let flightSagments = dict.flightSegment;

      if (flightSagments.length > 1) {
        let dataArr = this.calculateInterNationalRoundTrip(
          dict,
          flightSagments
        );
        finalData = dataArr;
      } else {
        let listDict = this.calculateDomesticSingle(dict, flightSagments);
        finalData.push(listDict);
      }
    }

    this.setState({
      totalFare,
      flightData: flightData,
      finalData: finalData,
      traceId: traceId,
      passengerDetails: passengers,
      flightType: flightType,
      origin: origin,
      destination: destination,
      isRoundTrip: isRoundTrip,
      isInterNational: isInterNational,
      isNavigateFromReviewPage: isNavigateFromReviewPage,
      currency: currency ? currency : "INR"
    }, () => {
      DeviceEventEmitter.addListener(StringConstants.FLIGHT_RULE_EVENT, this.updateFlightRuleFromApi)
    });
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.FLIGHT_RULE_EVENT, this.updateFlightRuleFromApi)
  }

  updateFlightRuleFromApi(data) {
    if (data) {
      let hml = "";
      data.forEach((element, index) => {
        var num = index + 1;
        hml = hml + "<H1 style='text-align:center;font-size:15px;'>Flight Rule for flight " + num + "</H1>" + element.FareRuleDetail
      });
      this.setState({
        html: hml,
        visible: true
      })
    }
  }

  disabledMultipleTap() {
    this.setState({
      disabled: true,
    });
    setTimeout(() => {
      this.setState({
        disabled: false,
      });
    }, 3000)
  }

  getFlightRule() {
    // this.setState({
    //   visible: true
    // })
    // return
    let param = {
      type: this.state.flightData[0].source,
      flight: [{
        index: this.state.flightData[0].index
        , isLCC: this.state.flightData[0].isLCC
      }],
      traceId: this.state.traceId
    }
    this.setState({
      spinner: true
    })
    console.log("get flight rule");
    console.log(param);

    this.props.getFlightRuleAction(param)
  }

  //MARK:-------------------- Calculate Domestic Single Way Data ------------

  calculateDomesticSingle(dict, flightSagments) {
    let headerDict = this.state.seactionHeaderDict;
    let listDict = this.state.listDict;

    headerDict.origin = flightSagments[0][0].origin.airport.airportCode;
    headerDict.destination = dict.destination;
    headerDict.dept_date = flightSagments[0][0].stopPointDepartureTime;
    headerDict.isRefundable = dict.isRefundable;
    headerDict.stops = flightSagments.length - 1;
    headerDict.isLCC = dict.isLCC;

    let duration = 0;
    flightSagments[0].forEach(item => {
      duration = duration + item.duration;
    });

    headerDict.duration = duration;

    listDict.key = headerDict;
    listDict.data = flightSagments[0];
    return listDict;
  }

  //MARK:-------------- Calculate InterNational round Trip Data --------------

  calculateInterNationalRoundTrip(dict, flightSagments) {
    let finalData = [];
    flightSagments.forEach(objArr => {
      let headerDict = {
        origin: "",
        destination: "",
        isRefundable: "",
        dept_date: "",
        stops: 0,
        duration: "",
        isLCC: dict.isLCC
      };
      let listDict = {
        key: "",
        data: []
      };

      let flightOriginDict = objArr[0];
      let flightDestdict = objArr[objArr.length - 1];

      headerDict.origin = flightOriginDict.origin.airport.airportCode;
      headerDict.destination = flightDestdict.destination.airport.airportCode;

      headerDict.dept_date = flightOriginDict.stopPointDepartureTime;

      headerDict.isRefundable = dict.isRefundable;
      headerDict.stops = objArr.length - 1;


      let duration = 0;
      objArr.forEach(item => {
        duration = duration + item.duration;
      });

      headerDict.duration = duration;

      listDict.key = headerDict;
      listDict.data = objArr;

      finalData.push(listDict);
    });
    return finalData;
  }

  //MARK:----------------- Domestic Round Trip Data --------------
  domesticRoundTripData(flightData) {
    let finalData = [];
    flightData.forEach((obj, index) => {
      let flightSegment = obj.flightSegment[0];
      let headerDict = {
        origin: "",
        destination: "",
        isRefundable: "",
        dept_date: "",
        stops: 0,
        duration: "",
        isLCC: obj.isLCC
      };
      let listDict = {
        key: "",
        data: []
      };

      headerDict.origin = flightSegment[0].origin.airport.airportCode;
      headerDict.destination = obj.destination;
      headerDict.dept_date = flightSegment[0].stopPointDepartureTime;
      headerDict.isRefundable = obj.isRefundable;
      headerDict.stops = flightSegment.length - 1;

      let duration = 0;

      flightSegment.forEach(item => {
        duration = duration + item.duration;
      });

      headerDict.duration = duration;

      listDict.key = headerDict;
      listDict.data = flightSegment;

      finalData.push(listDict);
    });

    return finalData;
  }

  calculateArrivalAndDepartureTime(item) {
    let data = item.flightSegment;
    if (data.lenght == 1) {
      let deptTime = moment(data[0].stopPointDepartureTime).format("hh:mm");
      let arrTime = moment(data[0].stopPointArrivalTime).format("hh:mm");
      return deptTime + "-" + arrTime;
    } else {
      let deptTime = moment(data[0].stopPointDepartureTime).format("hh:mm");
      let arrTime = moment(data[data.length - 1].stopPointArrivalTime).format("hh:mm");
      return deptTime + "-" + arrTime;
    }
  }

  calculateDurationTime(data) {
    let totalDuration = 0;
    for (i = 0; i < data.length; i++) {
      let item = data[i];
      totalDuration = totalDuration + item.duration;
    }
    return this.convertMinsIntoHandM(totalDuration);
  }

  convertMinsIntoHandM(n) {
    var num = n;
    var hours = num / 60;
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + "h" + rminutes + "m";
  }

  flightFromToItem({ section }) {
    let dict = section.key;
    console.log("DICT:" + JSON.stringify(dict))
    return (
      <View
        style={[commonstyle.itemBackground, { backgroundColor: "rgb(0,104,208)" }]}
      >
        <View style={{ flexDirection: "row", width: "100%" }}>
          <View style={{ width: "10%", alignItems: "flex-start" }}>
            <Image style={{ height: 25, width: 25 }} source={Images.imgPlane} />
          </View>
          <View style={{ width: "45%", paddingLeft: 10 }}>
            <Text style={{ fontSize: 14, color: "white", fontFamily: "Montserrat-SemiBold" }}>
              {dict.origin} - {dict.destination}
            </Text>
            <TouchableHighlight
              style={{
                height: 25,
                width: "100%",
                backgroundColor: (section.isRefundable) ? 'green' : 'red',
                marginTop: 10,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 5,

              }}
            >
              <Text style={{ fontSize: 13, color: "white", fontFamily: "Montserrat-SemiBold" }}>
                {(section.isRefundable) ? 'Refundable' : 'Non-Refundable'}
              </Text>
            </TouchableHighlight>
          </View>

          <View style={{ width: "45%", alignItems: "flex-end" }}>
            <Text style={{ fontSize: 12, color: "white", textAlign: "right", fontFamily: "Montserrat-Medium" }}>
              {moment(dict.dept_date).format("DD MMM YYYY")}
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: "white",
                textAlign: "right",
                marginTop: 10,
                fontFamily: "Montserrat-Medium"
              }}
            >
              {dict.stops} Stop | {convertMinsIntoHandM(dict.duration)}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  cancellationPolicyItem() {
    return (

      <TouchableOpacity onPress={() => this.getFlightRule()}
        style={[commonstyle.itemBackground, {
          flexDirection: "row",
          justifyContent: "space-between",
          paddingLeft: 0
        }]} >
        <View style={{
          alignItems: "center",
          justifyContent: "center",
          padding: 10, paddingBottom: 0, paddingTop: 0
        }}>
          <Image source={Images.imgcancellation} style={{ width: 30, height: 30, resizeMode: "contain" }}></Image>
        </View>
        <View style={{ flex: 1, alignItems: "flex-start", paddingLeft: 0, }}>
          <Text style={{
            color: colors.colorBlue,
            fontSize: 14,
            fontFamily: Fonts.semiBold
          }}>Cancellation and Baggage Policy
          </Text>
          <Text style={commonstyle.greyText}>Rules and Policy</Text>
        </View>
      </TouchableOpacity>
    );
  }

  travelAssistanceInsuranceItem() {
    return (
      <View
        style={[
          styles.itemContainer,
          { justifyContent: "center", alignItems: "flex-start" }
        ]}
      >
        <Text style={{ color: "black", fontWeight: "500" }}>
          Travel Assistance & Insurance
        </Text>
        <View style={{ marginTop: 10, flexDirection: "row" }}>
          <Text style={{ width: "80%" }}>
            Yes add travel assistance and Insurance to protect my trip
          </Text>

          <Switch
            trackColor="rgb(0,104,208)"
            style={{
              transform: [{ scaleX: 0.9 }, { scaleY: 0.75 }],
              marginLeft: 7
            }}
            onValueChange={() => {
              this.setState({
                switch: !this.state.switch
              });
            }}
            value={this.state.switch}
            onTintColor="rgb(0,104,208)"
          //  tintColor='rgb(0,104,208)'
          // ios_backgroundColor='rgb(0,104,208)'
          />
        </View>
        <Text
          style={{
            color: "black",
            fontWeight: "500",
            alignSelf: "flex-start",
            marginTop: 10,
            marginBottom: 10
          }}
        >
          Rs 269 for 1 passengers
        </Text>
        <Text numberOfLines={0}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tempus
          quam leo, et convallis libero imperdiet nec. Donec tincidunt et eros
          eget vulputate. Quisque lacinia dignissim volutpat. Mauris cursus vel
          dui eget consectetur. Fusce convallis, diam semper aliquet porttitor,
          est tellus tincidunt mi, eu vestibulum diam neque vel dolor. Donec et
          vehicula nisl. Phasellus vel tristique nunc. Cras ultrices finibus
          faucibus. Aliquam at libero ipsum. Praesent malesuada non lorem at
          volutpat.
        </Text>
        <TouchableHighlight
          underlayColor="transparent"
          onPress={() => {
            alert("terms and condition");
          }}
        >
          <Text style={{ color: "rgb(0,104,208)", marginTop: 10 }}>
            Terms & Condition
          </Text>
        </TouchableHighlight>
      </View>
    );
  }

  _renderVerticalDashLine() {
    return (
      <Dash
        style={{
          height: 1,
          width: "98.5%",
          alignSelf: "center"
          // position: "absolute"
        }}
        dashColor="gray"
        dashLength={8}
      />
    );
  }

  _renderListItem({ item, index, section }) {
    // console.clear();
    console.log(section)
    console.log(item)
    let destTerminal = item.destination.airport.terminal;
    let originTerminal = item.origin.airport.terminal;
    return (
      <View style={[commonstyle.itemBackground, { padding: 0 }]} >
        <View style={commonstyle.cardHeader}>
          <View style={commonstyle.cardHeaderCol}>
            <Image source={{ uri: imageBaseUrl + item.airline.image }}
              style={styles1.cardheaderFlightImg}></Image>
            <Text style={[styles1.price, { fontFamily: Fonts.semiBold, fontSize: 13 }]}>{item.airline.airlineName + " | "}{item.airline.airlineCode + "-" + item.airline.flightNumber}</Text>
          </View>
          <View style={[commonstyle.cardHeaderCol, {
            justifyContent: "flex-end"
          }]}>
            <Text style={styles1.price}>{(section.key.isRefundable ? "Refundable" : "Non-Refundable") + " | "}{section.key.stops ? section.key.stops + " stop" : "Nonstop"}</Text>
          </View>
        </View>
        <View style={{
          width: "100%",
          padding: 12, paddingBottom: 0,
          flexDirection: "row",
          alignItems: "center"
        }}>
          <View style={{
            width: "25%",
            // backgroundColor: "red",
          }}>
            <Text style={styles1.estimate}>{moment(item.origin.depTime).format("HH:mm")}</Text>
            <Text style={[styles1.stnName, { color: colors.colorBlue }]}>{item.origin.airport.cityName}</Text>
          </View>
          <View style={{
            width: "50%",
            height: 15,
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}>
            <View style={{
              width: "45%",
              alignItems: "center"
              // backgroundColor: "blue"
            }}>
              <View style={styles1.dotView}>
                <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
                <Dash style={{
                  width: "75%",
                  height: 1,
                }} dashColor={colors.lightgrey} dashLength={5}
                />
              </View>
            </View>
            <View style={{
              width: "10%",
              height: 20,
              marginTop: 3
            }}>
              <Image source={Images.imgplaneblue} style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
            </View>
            <View style={{
              width: "45%",
              alignItems: "center"
            }}>
              <View style={styles1.dotView}>
                <Dash style={{
                  width: "75%",
                  height: 1,
                }} dashColor={colors.lightgrey} dashLength={5}
                />
                <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
              </View>
            </View>
          </View>
          <View style={{
            width: "25%",
            alignItems: "flex-end",
            // backgroundColor: "red",
          }}>
            <Text style={styles1.estimate}>{moment(item.destination.arrTime).format("HH:mm")}</Text>
            <Text style={[styles1.stnName, { color: colors.colorBlue }]}>{item.destination.airport.cityName}</Text>
          </View>
        </View>
        <View style={{
          width: "100%", padding: 12,
          flexDirection: "row", paddingTop: 0,
          justifyContent: "space-between",
          alignItems: "center", paddingBottom: 0
        }}>
          <View style={{ width: "35%" }}>
            <Text style={styles1.stnName}>{moment(item.origin.depTime).format("DD MMM, YYYY")}</Text>
          </View>
          <View style={{
            width: "30%",
            alignItems: "center",
          }}>
            <Text style={[styles1.stnName, { fontFamily: Fonts.medium, marginTop: -12 }]}>{convertMinsIntoHandM(section.key.duration)}</Text>
          </View>
          <View style={{ width: "35%", alignItems: "flex-end" }}>
            <Text style={[styles1.stnName, { fontFamily: Fonts.medium, }]}>{moment(item.destination.arrTime).format("DD MMM, YYYY")}</Text>
          </View>
        </View>
        <View style={{
          width: "100%", padding: 12,
          flexDirection: "row", paddingTop: 0,
          justifyContent: "space-between",
          alignItems: "center",
        }}>
          <View style={{ width: "50%" }}>
            <Text style={[styles1.stnName]}>{item.origin.airport.airportName}</Text>
            {originTerminal !== "" && <Text style={styles1.stnName}>
              Terminal - {originTerminal}
            </Text>}
          </View>
          <View style={{ width: "50%", alignItems: "flex-end" }}>
            <Text style={[styles1.stnName]}>{item.destination.airport.airportName}</Text>
            {destTerminal !== "" && <Text style={styles1.stnName}>
              Terminal - {destTerminal}
            </Text>}
          </View>
        </View>
        <Dash style={{ height: 1, width: "100%", }} dashColor={colors.lightgrey}
          dashLength={5} />
        <View style={{ padding: 15, flexDirection: "row" }}>
          <View style={{ width: "70%", justifyContent: "space-between" }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Image style={{ height: 20, width: 20 }}
                source={Images.imgbaggage}
                resizeMode="contain" />
              <Text style={[commonstyle.blackText, { marginLeft: 5, fontSize: 12 }]}>
                {item.baggage} Check-In,  {item.cabinBaggage} Cabin
              </Text>
            </View>
            <View style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 10
            }} >
              <Image style={{ height: 20, width: 20 }}
                source={Images.imgpaidmeal}
                resizeMode="contain"
              />
              <Text style={[commonstyle.blackText, { marginLeft: 5, fontSize: 12, color: "black" }]}>
                Food {section.key.isLCC ? "Paid" : "Free"}
              </Text>
            </View>
          </View>
        </View>
      </View >
    );
  }

  continue = () => {
    // alert("Hello")
    NavigationServices.navigate('PassengerDetails', {
      flightdata: this.state.flightData,
      traceid: this.state.traceId,
      passengers: this.state.passengerDetails,
      flightType: this.state.flightType,
      origin: this.state.origin,
      destination: this.state.destination,
      isRoundTrip: this.state.isRoundTrip,
      isInterNational: this.state.isInterNational,
      currency: this.state.currency
    });
  }

  renderCouponView = () => {
    return (
      <View style={[commonstyle.itemBackground, { padding: 0 }]} >
        <View style={[commonstyle.cardHeader, { justifyContent: "flex-start" }]}>
          <Image source={Images.imgcoupon} style={{ height: 30, width: 30, resizeMode: "contain", margin: 7 }}></Image>
          <Text style={{
            color: colors.colorBlack,
            fontSize: 14,
            fontFamily: Fonts.semiBold
          }}>Coupon Code</Text>
        </View>
        <View style={{
          backgroundColor: colors.colorWhite,
          padding: 12, borderBottomLeftRadius: 10,
          borderBottomRightRadius: 10,
          flexDirection: "row",
        }}>
          <TextInput style={{
            width: "80%",
            height: 40,
            justifyContent: "center",
            alignItems: "flex-start",
            borderWidth: 1,
            borderRightWidth: 0,
            borderColor: colors.lightgrey,
            borderRadius: 7,
            paddingLeft: 10
          }} placeholder="Enter Coupon Code"
            placeholderTextColor={colors.colorBlack}></TextInput>
          <TouchableOpacity style={{
            width: "20%",
            backgroundColor: colors.colorBlue,
            height: 40, alignItems: "center",
            justifyContent: "center",
            borderTopRightRadius: 7,
            borderBottomRightRadius: 7
          }}>
            <Text style={{
              color: colors.colorWhite,
              fontSize: 14, fontFamily: Fonts.semiBold
            }}>APPLY</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {
    if (this.state.flightData == null) { return <View></View> }
    let { isLoading } = this.props;
    let { visible, html, isNavigateFromReviewPage, totalFare, currency } = this.state;
    return (
      <SafeAreaView style={styles1.container} forceInset={{ bottom: "always" }}>
        <NavigationEvents
          onWillFocus={() => {
            let { navigation } = this.props;
            let isNavigateFromReviewPage = navigation.getParam('isNavigateFromReviewPage', null);
            this.setState({
              isNavigateFromReviewPage
            }, () => {
              console.log("isNavigateFromReviewPage:=" + isNavigateFromReviewPage);
            })
            //  this.refreshPage();
            //Call whatever logic or dispatch redux actions and update the screen!
          }} />
        <Spinner
          style={{ flex: 1 }}
          textContent={"Loading..."}
          visible={isLoading}
          textStyle={{ color: colors.colorWhite }}
        />
        {/* <NavigationBar showBack title="Flight Details" /> */}
        <Header title={"Flight Details"}></Header>
        <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} />
        <View style={styles1.mainContainer}>
          <SectionList sections={this.state.finalData}
            showsVerticalScrollIndicator={false}
            renderItem={this._renderListItem}
            extraData={this.state.loading}
            // renderSectionHeader={this.flightFromToItem}
            keyExtractor={(item, index) => index.toString()}
          />

          {/* {this._fareDetailItem()} */}
          {this.cancellationPolicyItem()}
          {/* {this.renderCouponView()} */}
          <View style={{ height: Platform.OS == "ios" ? 20 : 40 }}></View>
          {/* {this.travelAssistanceInsuranceItem()} */}
          <Modal transparent={false} visible={visible}>
            <SafeAreaView style={styles.modalstyle}>
              <View style={styles1.header}>
                <TouchableOpacity style={styles1.headerBtn} onPress={() => {
                  this.setState({
                    visible: false
                  })
                }}>
                  <Image source={Images.imgClose} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
                </TouchableOpacity>
                <View style={styles1.headerTitle}>
                  <Text style={styles1.titleText}>Cancellation And Baggage Policy</Text>
                </View>
              </View>
              <View style={{
                flex: 1, backgroundColor: colors.colorWhite,
                padding: 10
              }}>
                <WebView
                  originWhitelist={['*']}
                  source={{ html: html }}
                />
              </View>
            </SafeAreaView>
          </Modal>
        </View>
        {isNavigateFromReviewPage ? null : <BottomStrip price={currency + " " + totalFare} fireEvent={this.continue}></BottomStrip>}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getFlightRuleAction: (data) => dispatch(getFlightRuleAction(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FlightDetails)

const styles1 = StyleSheet.create({
  headerBtn: {
    width: 50,
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  header: {
    height: 50,
    width: "100%",
    backgroundColor: "rgb(246,245,246)",
    flexDirection: "row",
    alignItems: "center",
    // backgroundColor: "yellow"
  },
  headerTitle: {
    width: screenWidth - 100,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "red"
  },
  titleText: {
    fontSize: 14,
    fontFamily: Fonts.medium,
    color: colors.colorBlack
  },
  dotView: {
    height: 10,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  estimate: {
    fontFamily: Fonts.semiBold,
    color: colors.colorBlack,
    fontSize: 14
  },
  stnName: {
    fontFamily: Fonts.regular,
    color: colors.colorBlack,
    fontSize: 14,
    lineHeight: 20
  },
  cardheaderFlightImg: {
    width: 20, height: 20,
    resizeMode: "contain", marginRight: 5
  },
  container: {
    flex: 1,
    backgroundColor: colors.colorBlue
  },
  mainContainer: {
    flex: 1,
    backgroundColor: colors.colorWhite
  },
  price: {
    color: colors.colorBlack,
    fontFamily: Fonts.medium,
    fontSize: 12
  },
})