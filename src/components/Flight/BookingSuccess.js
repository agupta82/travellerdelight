import { View, Text, Image, StyleSheet, Dimensions, ScrollView, ImageBackground } from "react-native";
import React from "react";
import NavigationServices from '../../Helper Classes/NavigationServices';
import Images from "../../Helper Classes/Images";
import { colors } from "../../Helper Classes/Colors";
import { TouchableOpacity } from "react-native-gesture-handler";
import Fonts from "../../Helper Classes/Fonts";
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class BookingSuccess extends React.Component {
  state = {
    data: null,
    isHotel: null
  };

  componentDidMount = () => {
    let { navigation } = this.props;
    let data = navigation.getParam('data', null);
    let isHotel = navigation.getParam('isHotel', null);

    this.setState({ data, isHotel }, () => {
      console.log("data is :--" + JSON.stringify(this.state.data));
    });
  }

  render() {
    let { data, isHotel } = this.state;
    if (!data) return null
    return (
      <View style={{
        flex: 1,
        backgroundColor: colors.backgroundColor,
        // padding: 14,
      }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <ImageBackground source={Images.imgbooksuccess} style={{
            width: screenWidth, alignItems: "center",
            height: screenHeight / 2.5, justifyContent: "center"
          }} resizeMode="stretch">
            {/* <Image source={require("../../Assets/images/booked-checked.png")}
              style={{ width: "70%", height: "50%", resizeMode: "contain" }}></Image> */}
          </ImageBackground>
          <View style={{
            flex: 1,
            padding: 20
          }}>
            {isHotel ?
              <View style={styles.cardView}>
                <View style={styles.row}>
                  <Text style={styles.key}>Booking ID</Text>
                  <Text style={styles.value}>{data.bookingId}</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.key}>Check In Date</Text>
                  <Text style={styles.value}>{data.checkInDate}</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.key}>Check Out Date</Text>
                  <Text style={styles.value}>{data.checkOutDate}</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.key}>Transaction ID</Text>
                  <Text style={styles.value}>{data.transactionId}</Text>
                </View>
              </View>
              :
              <View style={styles.cardView}>
                <View style={styles.row}>
                  <Text style={styles.key}>Booking ID</Text>
                  <Text style={styles.value}>{12344}</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.key}>Journey Date</Text>
                  <Text style={styles.value}>15 December 2019</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.key}>Service </Text>
                  <Text style={styles.value}>Makemytrip</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.key}>PNR</Text>
                  <Text style={styles.value}>21337867677899</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.key}>Transaction ID</Text>
                  <Text style={styles.value}>7677899</Text>
                </View>
              </View>
            }
            {!isHotel ?
              <View style={{
                flexDirection: "row",
                width: "100%", paddingTop: 30,
                justifyContent: "space-between",
              }}>
                <TouchableOpacity style={styles.button}>
                  <Text style={styles.btnText}>VIEW TICKET</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.button, { backgroundColor: colors.colorRed }]}
                  onPress={() => NavigationServices.navigate("HomeTabStack")}>
                  <Text style={styles.btnText}>BACK HOME</Text>
                </TouchableOpacity>
              </View> : <View style={{
                flexDirection: "row",
                width: "100%", paddingTop: 30,
                justifyContent: "center",
              }}>
                <TouchableOpacity style={[styles.button, { backgroundColor: colors.colorRed }]}
                  onPress={() => NavigationServices.navigate("HomeTabStack")}>
                  <Text style={styles.btnText}>BACK HOME</Text>
                </TouchableOpacity>
              </View>}
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  btnText: {
    fontFamily: Fonts.bold,
    color: colors.colorWhite,
    fontSize: 16, letterSpacing: .7
  },
  button: {
    width: (screenWidth / 2) - 30,
    height: 45,
    backgroundColor: colors.colorBlue,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10
  },
  cardView: {
    width: "100%",
    borderColor: colors.lightBlue,
    borderWidth: 1,
    backgroundColor: colors.colorWhite,
  },
  value: {
    fontSize: 14,
    color: colors.colorBlack,
    fontFamily: Fonts.semiBold,
    letterSpacing: .7
  },
  key: {
    fontSize: 14,
    color: colors.colorBlack,
    fontFamily: Fonts.regular,
    letterSpacing: .7
  },
  row: {
    height: 45, width: "100%",
    borderBottomColor: colors.lightBlue,
    borderBottomWidth: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center", paddingLeft: 14,
    paddingRight: 14
  },
})