import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Modal,
  DeviceEventEmitter,
  TouchableHighlight,
  FlatList,
  Image
} from "react-native";
import Images from "../../Helper Classes/Images";


export default class BaggageOrMealModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      selectedIndex: 0
    };

    this._renderListItem = this._renderListItem.bind(this)
  }


  //MARK:-------------- Render List Item --------------
  _renderListItem({ item, index }) {
    return (
      <View style={{ flexDirection: 'row', height: 50 }}>
        <View style={{ width: '30%', height: '100%', justifyContent: 'center' }}>
          <Text style={{ fontSize: 14, color: 'black', fontWeight: '500' }}>{(this.props.isForBaggage) ? item.Weight + ' Kg' : item.AirlineDescription ? item.AirlineDescription : 'No Meal'} </Text>
        </View>
        <View style={{ height: '100%', width: '70%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
          <Text style={{ fontSize: 14, color: 'gray', fontWeight: '500' }}>{item.Currency} {item.Price}</Text>
          <TouchableHighlight
            style={{ height: 30, width: 30, justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}
            underlayColor='transparent'
            onPress={() => {
              this.setState({
                selectedIndex: index,
                loading: !this.state.loading
              })
            }}
          >
            <Image
              style={{ height: 25, width: 25, tintColor: 'rgb(0,114,198)' }}
              source={this.state.selectedIndex == index ? Images.imgcheck : Images.imguncheck}
              resizeMode='contain'
            />
          </TouchableHighlight>
        </View>
      </View>
    )
  }


  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.props.baggageOrMealModal}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}
        >
          <View style={styles.container}>
            <View style={styles.stylePopUpView}>
              <View style={styles.styleTitleView}>
                <Text
                  style={{ color: "black", fontSize: 18, fontWeight: "600" }}
                >
                  SELECT {String(this.props.type).toUpperCase()}
                </Text>
              </View>

              <View style={{ margin: 10 }}>
                <FlatList
                  style={{ padding: 10 }}
                  data={(this.props.isForBaggage) ? this.props.baggageData : this.props.mealData}
                  extraData={this.state.loading}
                  renderItem={this._renderListItem}
                  keyExtractor={(item, index) => String(index)}
                />
              </View>


              <View style={styles.styleBottomBtnsView}>
                <TouchableHighlight
                  style={[styles.styleCancelOrDoneBtnView, { marginRight: 10 }]}
                  underlayColor="transparent"
                  onPress={() => {
                    let data = {}
                    if (this.props.isForBaggage) {
                      data = this.props.baggageData[this.state.selectedIndex]
                    }
                    else {
                      data = this.props.mealData[this.props.selectedIndex]
                    }
                    this.props.onDonePress(data)
                  }}
                >
                  <Text style={{ color: "rgba(25,89,189,1)", fontSize: 14 }}>Done</Text>
                </TouchableHighlight>

                <TouchableHighlight
                  style={styles.styleCancelOrDoneBtnView}
                  underlayColor='transparent'
                  onPress={() => {
                    this.props.onCancelPress()
                  }}
                >
                  <Text style={{ color: "", fontSize: 14 }}>
                    Cancel
                  </Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(0,0,0,0.5)",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  stylePopUpView: {
    width: "80%",
    backgroundColor: "white",
    borderRadius: 15
  },
  styleTitleView: {
    margin: 15,
    height: 40,
    justifyContent: "center"
  },
  styleBottomBtnsView: {
    flexDirection: "row",
    justifyContent: "flex-end",
    margin: 15,
    height: 35,
    paddingRight: 10
  },
  styleCancelOrDoneBtnView: {
    height: "100%",
    width: 60,
    justifyContent: "center",
    alignItems: "center"
  }
});
