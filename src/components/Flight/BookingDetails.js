import React, { Component } from 'react'
import {
  View,
  StatusBar,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
  Text,
  FlatList,
  TouchableHighlight,
  Modal,
  SafeAreaView,
  DeviceEventEmitter,
} from 'react-native'
import NavigationBar from '../../Helper Classes/NavigationBar'
import Images from '../../Helper Classes/Images'
import Spinner from 'react-native-loading-spinner-overlay'
import NavigationServices from './../../Helper Classes/NavigationServices'
import moment from 'moment';
import { connect } from "react-redux";
import { getFlightBookingDetailsAction } from '../../Redux/Actions';
import { colors } from '../../Helper Classes/Colors'
import CommonHeader from '../../Helper Classes/CommonHeader'
import CardView from 'react-native-cardview'
import { MinutesToHours } from '../../Utility.js'
import Dash from "react-native-dash";
import Fonts from '../../Helper Classes/Fonts'


const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

const gender = {
  "male": "Male",
  "female": "Female"
}

class BookingDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      bookingDataItem: null
    }

  }

  componentDidMount() {
    const { navigation } = this.props;
    const bookingDataItem = navigation.getParam('bookingDataItem', null);

    this.setState({
      bookingDataItem: bookingDataItem
    })

  }

  componentWillUnmount() {

  }



  renderAirlineData(item) {
    return (
      <CardView style={{
        //overflow: 'hidden',
        width: '100%',
        marginTop: 20,
        backgroundColor: colors.colorWhite,
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ width: '100%', overflow: 'hidden', borderRadius: 10, }}>
          <View style={{ width: '100%', backgroundColor: '#e6e6e6', height: 45, flexDirection: 'row', padding: 10 }}>
            <View style={{ width: '50%', alignItems: 'center', flexDirection: 'row', height: '100%' }}>
              <View style={{ width: 25, height: 25, borderRadius: 15, backgroundColor: colors.colorBlue, alignItems: 'center', justifyContent: 'center' }}>
                <Image source={Images.imgplaneblue} style={{ width: 15, height: 15, tintColor: colors.colorWhite, resizeMode: 'center' }} />
              </View>
              <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, paddingLeft: 7 }}>
                {"PNR : "}{this.state.bookingDataItem.pnr}
              </Text>

            </View>
            <View style={{ width: '50%' }}></View>
          </View>
          <View style={{ width: '100%', paddingVertical: 15, paddingHorizontal: 10 }}>
            <View style={{ width: '100%', flexDirection: 'row' }}>
              <View style={{ width: '28%' }}>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, padding: 1 }}>
                  {moment(item.depart_at).format("HH:mm")}
                </Text>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 10, color: colors.colorBlue, padding: 1 }}>
                  {item.origin}
                </Text>
                <Text style={{ fontFamily: Fonts.regular, fontSize: 10, color: colors.colorBlack21, padding: 1 }}>
                  {moment(item.depart_at).format("DD MMM, YYYY")}
                </Text>
              </View>
              <View style={{ width: '44%', alignItems: 'center', justifyContent: 'center', }}>
                <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', }}>
                  <View style={{ width: 7, height: 7, borderRadius: 5, backgroundColor: colors.colorBlue, position: 'absolute', left: 0 }} />
                  <View style={{ width: 7, height: 7, borderRadius: 5, backgroundColor: colors.colorBlue, position: 'absolute', right: 0 }} />
                  <Dash dashColor={colors.colorBlue} dashLength={1} dashGap={2} dashThickness={0.8} style={{ position: 'absolute', left: 0, width: '100%', }} />
                  <Image source={Images.imgplaneblue} style={{ width: 15, height: 15, resizeMode: 'center' }} />
                </View>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 10, color: colors.colorBlack21, padding: 2 }}>
                  {MinutesToHours(item.travel_time)}
                </Text>
              </View>

              <View style={{ width: '28%', alignItems: 'flex-end' }}>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, padding: 1 }}>
                  {moment(item.arrive_at).format("HH:mm")}
                </Text>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 10, color: colors.colorBlue, padding: 1 }}>
                  {item.destination}
                </Text>
                <Text style={{ fontFamily: Fonts.regular, fontSize: 10, color: colors.colorBlack21, padding: 1 }}>
                  {moment(item.arrive_at).format("DD MMM, YYYY")}
                </Text>
              </View>

            </View>

          </View>

        </View></CardView>
    )
  }

  renderPassengerDetail() {
    return (
      <CardView style={{
        //overflow: 'hidden',
        width: '100%',
        marginTop: 20,
        backgroundColor: colors.colorWhite,
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ width: '100%', overflow: 'hidden', borderRadius: 10, }}>
          <View style={{ width: '100%', backgroundColor: '#e6e6e6', height: 45, flexDirection: 'row', padding: 10 }}>
            <View style={{ width: '50%', alignItems: 'center', flexDirection: 'row', height: '100%' }}>
              <View style={{ width: 25, height: 25, borderRadius: 15, backgroundColor: colors.colorBlue, alignItems: 'center', justifyContent: 'center' }}>
                <Image source={Images.imguserprofile} style={{ width: 15, height: 15, resizeMode: 'center' }} />
              </View>
              <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, paddingLeft: 7 }}>
                Passengers Detail
              </Text>

            </View>
          </View>
          <View style={{ width: '100%', paddingVertical: 15, paddingHorizontal: 10 }}>
            {this.state.bookingDataItem.passengers.map((item, index) => {
              return (
                <Text style={{ fontFamily: Fonts.regular, fontSize: 12, color: colors.colorBlack, paddingBottom: 5 }}>
                  {(item.name_prefix + " " + item.first_name) + "" + (item.middle_name ? (" " + item.middle_name) : "") + " " + item.last_name}
                </Text>
              )
            })}

          </View>

        </View></CardView>
    )
  }

  renderContactDetail() {
    return (
      <CardView style={{
        //overflow: 'hidden',
        width: '100%',
        marginTop: 20,
        backgroundColor: colors.colorWhite,
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ width: '100%', overflow: 'hidden', borderRadius: 10, }}>
          <View style={{ width: '100%', backgroundColor: '#e6e6e6', height: 45, flexDirection: 'row', padding: 10 }}>
            <View style={{ width: '50%', alignItems: 'center', flexDirection: 'row', height: '100%' }}>
              <View style={{ width: 25, height: 25, borderRadius: 15, backgroundColor: colors.colorBlue, alignItems: 'center', justifyContent: 'center' }}>
                <Image source={Images.img_primary_contact} style={{ width: 25, height: 25, resizeMode: 'cover' }} />
              </View>
              <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, paddingLeft: 7 }}>
                Primary Contact
              </Text>

            </View>
          </View>
          <View style={{ width: '100%', paddingVertical: 15, paddingHorizontal: 10 }}>
            <View style={styles.cardCOntent}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 11, color: colors.colorBlack, }}>Email  :-  </Text>
                <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: colors.colorBlack, }}>{this.state.bookingDataItem.email}</Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 11, color: colors.colorBlack }}>Phone  :-  </Text>
                <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: colors.colorBlack }}>{this.state.bookingDataItem.phone}</Text>
              </View>
            </View>

          </View>

        </View></CardView>
    )
  }

  renderTotalPrice() {
    return (
      <CardView style={{
        //overflow: 'hidden',
        width: '100%',
        marginTop: 20,
        backgroundColor: colors.colorWhite,
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ width: '100%', overflow: 'hidden', borderRadius: 10, }}>
          <View style={{ width: '100%', height: 45, flexDirection: 'row', padding: 10 }}>
            <View style={{ width: '50%', alignItems: 'center', flexDirection: 'row', height: '100%' }}>
              <View style={{ width: 25, height: 25, borderRadius: 15, backgroundColor: colors.colorBlue, alignItems: 'center', justifyContent: 'center' }}>
                <Image source={Images.img_primary_contact} style={{ width: 25, height: 25, resizeMode: 'cover' }} />
              </View>
              <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, paddingLeft: 7 }}>
                Total Paid
              </Text>

            </View>
            <View style={{ width: '50%', alignItems: 'flex-end', justifyContent: 'center' }}>

              <Text style={{ fontFamily: Fonts.bold, fontSize: 15, color: colors.colorBlack, }}>
                {this.state.bookingDataItem.price.currency ? (
                  this.state.bookingDataItem.price.currency == "INR" ? "₹ " : this.state.bookingDataItem.price.currency
                ) : ""
                }{this.state.bookingDataItem.price.grant_total}
              </Text>

            </View>
          </View>
        </View>
      </CardView>)
  }



  render() {
    if (!this.state.bookingDataItem) return null
    return (
      <View style={styles.container}>
        <SafeAreaView backgroundColor={colors.colorBlue} />
        <CommonHeader title={"Booking Details"} />
        <View style={{ paddingHorizontal: 18, flex: 1 }} >
          <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1, padding: 2 }}>
            {this.state.bookingDataItem.flights.map((item, index) => {
              return this.renderAirlineData(item)
            })}

            {this.renderPassengerDetail()}

            {this.renderContactDetail()}

            {this.renderTotalPrice()}


          </ScrollView>

        </View>
      </View>
    )
  }

}


const mapStateToProps = state => {
  return {
    isLoading: state.isLoadingReducer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getFlightBookingDetailsAction: (data) => dispatch(getFlightBookingDetailsAction(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BookingDetails)

styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: 'rgba(244,248,254,1)'
  },


})
