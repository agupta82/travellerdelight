import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList, Platform,
  Image,
  TouchableHighlight,
  Dimensions,
  TouchableOpacity,
  Modal,
  DeviceEventEmitter,
  ImageBase
} from "react-native";

import Dash from "react-native-dash";
import APIManager from "../../../Helper Classes/APIManager/APIManagerUpdated";
import Spinner from "react-native-loading-spinner-overlay";
import FlightFilters from "../FlightFilters";
import { connect } from "react-redux";
import moment from "moment";
import { baseUrl, imageBaseUrl } from "../../../Helper Classes/APIManager/APIConstants";
import { saveFlightItem } from "../../../Redux/Actions/Actions";
import { ifIphoneX, getStatusBarHeight } from "react-native-iphone-x-helper";

import { getFlightList } from "../../../Helper Classes/APIManager/ApiProvider";
import { searchFlightAction } from '../../../Redux/Actions';
import StringConstants from '../../../Helper Classes/StringConstants';
import NavigationServices from "../../../Helper Classes/NavigationServices";
import { ScrollView } from "react-native-gesture-handler";
import styles from "./Styles";
import { colors } from "../../../Helper Classes/Colors";
import Header from "../../../Helper Classes/FlighListHeader";
import BottomStrip from "../../../Helper Classes/BottomStrip";
import { SafeAreaView } from "react-navigation";

class FlightList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      datas: [1, 2, 3, 4, 5, 6, 7],
      selectedPrice: 0,
      prices: [
        {
          date: "Wed, 04 Dec",
          price: 3237
        },
        {
          date: "Wed, 05 Dec",
          price: 3147
        },
        {
          date: "Wed, 06 Dec",
          price: 7237
        },
        {
          date: "Wed, 07 Dec",
          price: 5237
        },
        {
          date: "Wed, 08 Dec",
          price: 2237
        },
        {
          date: "Wed, 09 Dec",
          price: 3337
        },
        {
          date: "Wed, 10 Dec",
          price: 4237
        },
        {
          date: "Wed, 11 Dec",
          price: 1237
        }
      ]
    };
  }

  filterFlight = () => {
    alert("Filtering");
  }

  checkout = () => {
    alert("Filtering");
  }

  render() {
    let { prices, selectedPrice, datas } = this.state;
    // let dict = this.props.flightSearchParams;
    return (
      <SafeAreaView style={{ backgroundColor: colors.colorBlue, flex: 1 }} forceInset={{ top: "always" }}>
        <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} />
        <Spinner
          style={{ flex: 1 }}
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={{ color: colors.colorWhite }}
        />
        <View style={styles.container}>
          <Header fireEvent={this.filterFlight}></Header>
          <View style={styles.tabs}>
            <TouchableOpacity style={styles.tab}>
              <Text style={styles.tabText}>Departure</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.tab, {
              borderLeftWidth: 1,
              borderRightWidth: 1,
              borderColor: "lightgray"
            }]}>
              <Text style={styles.tabText}>Duration</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.tab, {
              flexDirection: "row"
            }]}>
              <Text style={styles.tabText}>Price</Text>
              <Image source={Images.imgdownarrow} style={{
                width: 20, height: 15, resizeMode: "contain"
              }}></Image>
            </TouchableOpacity>
          </View>
          <View style={styles.mainView}>
            <ScrollView showsVerticalScrollIndicator={false}>
              {
                datas.map((data, index) => (
                  <View style={[styles.card, styles1.card]}>
                    <View style={{
                      width: "80%",
                      height: "100%",
                      borderRightWidth: 1,
                      borderRightColor: colors.lightgrey,
                    }}>
                      <View style={{
                        width: "100%",
                      }}>
                        <View style={[styles.cardHeader, { backgroundColor: colors.colorWhite }]}>
                          <View style={[styles.cardHeaderCol1, { width: "100%" }]}>
                            <View style={styles.cardheaderFlightView}>
                              <Image source={require("../../../Assets/images/jet.png")}
                                style={styles.cardheaderFlightImg}></Image>
                              <Text style={[styles.price, { color: colors.colorBlack }]}>Jet Airways 304</Text>
                            </View>
                            <View style={styles.cardHeaderAmenties}>
                              <Image source={Images.imgfood} style={styles.flightAmenityImg}></Image>
                              <Image source={Images.imgflightinfo} style={styles.flightAmenityImg}></Image>
                              <Image source={Images.imgseat} style={styles.flightAmenityImg}></Image>
                            </View>
                          </View>
                        </View>
                        <View style={[styles.cardContent, { height: 70, paddingRight: 7 }]}>
                          <View style={[styles.cardContentCol1, { width: "100%" }]}>
                            <View style={{ width: "20%" }}>
                              <Text style={styles.estimate}>20:15</Text>
                              <Text style={styles.stnName}>DEL</Text>
                            </View>
                            <View style={{ width: "60%", alignItems: "center" }}>
                              <Text style={styles.estimate}>2h 15m</Text>
                              <View style={styles.dotView}>
                                <Image source={Images.imgbluedot} style={{ width: "10%" }}></Image>
                                <Dash style={{
                                  width: "75%",
                                  height: 1,
                                }} dashColor={colors.gray} dashLength={3}
                                />
                                <Image source={Images.imgbluedot} style={{ width: "10%" }}></Image>
                              </View>
                              <Text style={styles.estimate}>Nonstop</Text>
                            </View>
                            <View style={{ width: "20%", alignItems: "flex-end" }}>
                              <Text style={styles.estimate}>20:15</Text>
                              <Text style={styles.stnName}>DEL</Text>
                            </View>
                          </View>
                        </View>
                      </View>
                      <View style={{
                        height: 1, width: "100%",
                        backgroundColor: colors.lightgrey
                      }}></View>
                      <View style={{
                        width: "100%",
                      }}>
                        <View style={[styles.cardHeader, { backgroundColor: colors.colorWhite }]}>
                          <View style={[styles.cardHeaderCol1, { width: "100%" }]}>
                            <View style={styles.cardheaderFlightView}>
                              <Image source={require("../../../Assets/images/jet.png")}
                                style={styles.cardheaderFlightImg}></Image>
                              <Text style={[styles.price, { color: colors.colorBlack }]}>Jet Airways 304</Text>
                            </View>
                            <View style={styles.cardHeaderAmenties}>
                              <Image source={Images.imgfood} style={styles.flightAmenityImg}></Image>
                              <Image source={Images.imgflightinfo} style={styles.flightAmenityImg}></Image>
                              <Image source={Images.imgseat} style={styles.flightAmenityImg}></Image>
                            </View>
                          </View>
                        </View>
                        <View style={[styles.cardContent, { height: 70, paddingRight: 7 }]}>
                          <View style={[styles.cardContentCol1, { width: "100%" }]}>
                            <View style={{ width: "20%" }}>
                              <Text style={styles.estimate}>20:15</Text>
                              <Text style={styles.stnName}>DEL</Text>
                            </View>
                            <View style={{ width: "60%", alignItems: "center" }}>
                              <Text style={styles.estimate}>2h 15m</Text>
                              <View style={styles.dotView}>
                                <Image source={Images.imgbluedot} style={{ width: "10%" }}></Image>
                                <Dash style={{
                                  width: "75%",
                                  height: 1,
                                }} dashColor={colors.gray} dashLength={3}
                                />
                                <Image source={Images.imgbluedot} style={{ width: "10%" }}></Image>
                              </View>
                              <Text style={styles.estimate}>Nonstop</Text>
                            </View>
                            <View style={{ width: "20%", alignItems: "flex-end" }}>
                              <Text style={styles.estimate}>20:15</Text>
                              <Text style={styles.stnName}>DEL</Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                    <View style={styles.cardContentCol2}>
                      <Text style={styles.priceText}>
                        <Image source={Images.imgrupeeblue} style={styles.priceImg}></Image>
                        3237
                      </Text>
                    </View>
                  </View>
                ))
              }
              <View style={{ height: 200 }}></View>
            </ScrollView>

          </View>
          <BottomStrip fireEvent={this.checkout}></BottomStrip>
        </View>
      </SafeAreaView>
    );
  }
}

const styles1 = StyleSheet.create({
  card: {
    flexDirection: "row",
    height: 210,
  },
})

const mapStateToProps = state => {
  return {
    flightSearchParams: state.FlightsReducer.flightSearchParams,
    isLoading: state.isLoadingReducer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveFlightItem: (data) => dispatch(saveFlightItem(data)),
    searchFlightAction: (data) => dispatch(searchFlightAction(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FlightList)