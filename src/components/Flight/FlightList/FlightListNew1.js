import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList, Platform,
  Image,
  TouchableHighlight,
  Dimensions,
  TouchableOpacity,
  Modal,
  DeviceEventEmitter,
  ImageBase
} from "react-native";

import Dash from "react-native-dash";
import APIManager from "../../../Helper Classes/APIManager/APIManagerUpdated";
import Spinner from "react-native-loading-spinner-overlay";
import FlightFilters from "../FlightFilters";
import { connect } from "react-redux";
import moment from "moment";
import { baseUrl, imageBaseUrl } from "../../../Helper Classes/APIManager/APIConstants";
import { saveFlightItem } from "../../../Redux/Actions/Actions";
import { ifIphoneX, getStatusBarHeight } from "react-native-iphone-x-helper";

import { getFlightList } from "../../../Helper Classes/APIManager/ApiProvider";
import { searchFlightAction } from '../../../Redux/Actions';
import StringConstants from '../../../Helper Classes/StringConstants';
import NavigationServices from "../../../Helper Classes/NavigationServices";
import { ScrollView } from "react-native-gesture-handler";
import styles from "./Styles";
import { colors } from "../../../Helper Classes/Colors";
import Images from "../../../Helper Classes/Images";
import Header from "../../../Helper Classes/FlighListHeader";
import FlightDetailModal from "../../../Modals/FlightDetail/FlightDetailModal";
import { SafeAreaView } from "react-navigation";
import FlightFilterModal from "../FlightFilters";

class FlightList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      datas: [1, 2, 3, 4, 5, 6, 7],
      selectedPrice: 0,
      prices: [
        {
          date: "Wed, 04 Dec",
          price: 3237
        },
        {
          date: "Wed, 05 Dec",
          price: 3147
        },
        {
          date: "Wed, 06 Dec",
          price: 7237
        }
      ],
      openedDetailModel: false,
      openedFilterModel: false,
      tripType: 1,
      //previous values
      data: [],
      flightListData: [],
      filteresFlightData: [],
      completeResponseData: [],
      loading: false,
      selectedIndex: 0,
      expendingIndex: -1,
      spinner: false,
      traceId: "",
      modelVisible: false,
      isFilterActive: false,

      //for selecting two way depart or arrival
      selectedOriginIndex: 0,
      selectedOriginFlightIndex: 0,
      selectedDestinationFlightIndex: 0,
      selectedFilterIndex: 0,

      //for soting of flights
      selectedSort: 3,
      sortArrIncr: false,
      sortDepIncr: false,
      sortPriceIncr: true,
      sortDurIncr: false,
      sortAirlineIncr: false,

      twoWayTotalPrice: null,
      destinationFlightItem: null,
      originFlightItem: null,
      uniqueAirlines: [],
      filterForReturn: false,
      filterForDep: false,
      filterDataForTwoWayDep: null,
      filterDataForTwoWayRet: null,
      isRoundTrip: false,
      // sortDep

      isInterNational: false,

      selectedFiltersDepart: {
        timeFilter: [],
        stopageFilter: [],
        refundable: false,
        freeMeal: false,
        airlines: [],
        timeFilter2: []
      },

      selectedFiltersArr: {
        timeFilter: [],
        stopageFilter: [],
        refundable: false,
        freeMeal: false,
        airlines: [],
        timeFilter2: []
      }

    };

    this.depAirlines = [];
    this.arrvialAirlines = [];
    this.internationalAirlines = [];
    flatListref = null;

    // this._renderListdate = this._renderListdate.bind(this);
    // this._renderFlightListItem = this._renderFlightListItem.bind(this);
    this.getFlightsList = this.getFlightsList.bind(this);
    this.onFilterSelection = this.onFilterSelection.bind(this);
    // this._renderFlightSingleWayListItem = this._renderFlightSingleWayListItem.bind(
    //   this
    // );
    // this._renderSagmentView = this._renderSagmentView.bind(this);
    // this._renderCommonListItem = this._renderCommonListItem.bind(this);
    // this._renderInterNationalFlightListItem = this._renderInterNationalFlightListItem.bind(
    //   this
    // );
    // this.getBackgroundColorForItem = this.getBackgroundColorForItem.bind(this);
    // this._renderBookNowBtn = this._renderBookNowBtn.bind(this);
    this.updateFlightListFromApi = this.updateFlightListFromApi.bind(this)
  }

  componentWillMount = () => {
    // let params=this.state.navigation
    console.log(this.props.navigation.state.params);
    if (this.props.navigation.state.params) {
      this.setState({
        tripType: this.props.navigation.state.params.tripType
      })
    }
  }

  componentDidMount() {
    this.getFlightsList();
    //this.getFlightsCalendar();
    DeviceEventEmitter.addListener(StringConstants.FLIGH_LIST_EVENT, this.updateFlightListFromApi)
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.FLIGH_LIST_EVENT, this.updateFlightListFromApi)
  }

  updateFlightListFromApi(response) {
    console.log("getFlightListSaga");
    this.setState({ traceId: response.others.traceId });
    if (response.results.length == 2) {
      this.setState({
        isRoundTrip: true,
        completeResponseData: response.results,
        flightListData: response.results[0],
        loading: !this.state.loading,
        originFlightItem: response.results[0][0],
        destinationFlightItem: response.results[1][0],
        twoWayTotalPrice: response.results[0][0].fare.offeredFare + response.results[1][0].fare.offeredFare
      }, () => { this.setUniqueAirlines() });
    } else {
      if (response.results[0][0].flightSegment.length === 2) {
        this.setState({
          isInterNational: true
        })
      }
      this.setState({
        completeResponseData: response.results,
        flightListData: response.results[0],
        loading: !this.state.loading,
      }, () => { this.setUniqueAirlines() });
    }
  }

  minutesOfDay(m) {
    return m.minutes() + m.hours() * 60;
  }

  calulateTwoWayPrice() {
    let item1 = this.state.originFlightItem;
    let item2 = this.state.destinationFlightItem;
    let fare = item1.fare.offeredFare + item2.fare.offeredFare;
    fare = Math.ceil(fare);
    this.setState({ twoWayTotalPrice: fare });
  }

  onFilterSelection(dict) {
    console.log("on filter seletion");
    if (this.state.isRoundTrip) {

      if (this.state.selectedOriginIndex == 0) {

        this.setState({
          openedFilterModel: false,
          selectedFiltersDepart: dict
        });

        let timeFilters = dict.timeFilter;
        let stopageFilter = dict.stopageFilter;
        let isRefundable = dict.refundable;
        let isFreeMeal = dict.freeMeal;
        let airLines = dict.airlines;
        let filterDate = this.state.completeResponseData[0][0].flightSegment[0][0].stopPointDepartureTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        console.log(dict);
        let filterData = this.state.completeResponseData[0];

        if (timeFilters.length > 0 && timeFilters.length < 4) {
          timeFilters.forEach((item, index) => {
            let timeArr = String(item).split(" - ");
            //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
            let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
            let finaltime2 = filterDate;
            if (timeArr[1] == "24:00") {
              finaltime2 = finaltime2 + "T" + "23:59" + ":59";
            } else {
              finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
            }
            console.log("time is  " + finaltime1);

            let deptTime1 = finaltime1;
            let depTime2 = finaltime2;
            // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
            let arr = this.state.completeResponseData[0].filter(data => {

              let flightDeptTime = data.flightSegment[0][0].stopPointDepartureTime;

              if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
                console.log("true")
                return true;
              } else {
                return false;
              }

            });
            console.log(arr);
            if (index == 0) {
              filterData = arr;
            } else {
              console.log("arr length in second time " + arr.length);
              filterData = filterData.concat(arr);
            }

            console.log("filter data after time filter " + filterData.length);
          });
        }

        if (stopageFilter.length > 0 && stopageFilter.length < 3) {
          stopageFilter.forEach((item, index) => {
            let arr = filterData.filter(data => {
              if (item == 0) {
                return data.flightSegment[0].length == 1;
              } else if (item == 1) {
                return data.flightSegment[0].length == 2;
              } else {
                return data.flightSegment[0].length > 2;
              }
            });

            if (index == 0) {
              filterData = arr;
            } else {
              filterData = filterData.concat(arr);
            }
          });

          console.log("filter data after stopage filter " + filterData.length);
        }

        if (isRefundable) {
          filterData = filterData.filter(data => {
            return data.isRefundable == true;
          });
          console.log("filter data after refundable filter " + filterData.length);
        }

        if (isFreeMeal) {
          filterData = filterData.filter(data => {
            return data.fare.totalMealCharges == 0;
          });

          console.log("filter data after free meal filter " + filterData.length);
        }

        if (airLines.length > 0) {
          let arr2 = [];
          airLines.forEach((airlineIndex, index) => {

            let arr = filterData.filter((flightItem) => {

              if (flightItem.flightSegment[0][0].airline.airlineCode === this.depAirlines[airlineIndex].airlineCode) {
                return true;
              } else {
                return false;
              }

            })

            if (index == 0) {
              arr2 = arr;
            } else {
              console.log("arr length in second time " + arr.length);
              arr2 = arr2.concat(arr);
            }
          })
          filterData = arr2;
        }


        console.log("total data in list " + this.state.flightListData.length);
        console.log(timeFilters.length);
        console.log(filterData);
        console.log(filterData.length);
        if (timeFilters.length == 0 && stopageFilter.length == 0 && !isRefundable && !isFreeMeal && airLines.length == 0) {
          if (!this.state.filterForReturn) {
            this.setState({
              isFilterActive: false,
              filterForDep: false,
              loading: !this.state.loading
            });
          } else {
            this.setState({
              filterForDep: false,
              loading: !this.state.loading
            });
          }

        } else {
          this.setState({
            filterDataForTwoWayDep: filterData,
            filteresFlightData: filterData,
            selectedOriginFlightIndex: filterData.length > 0 ? 0 : -1,
            isFilterActive: true,
            filterForDep: true,
            loading: !this.state.loading
          });
        }

      }
      else {

        this.setState({
          modelVisible: false,
          selectedFiltersArr: dict
        });

        let timeFilters1 = dict.timeFilter;

        let stopageFilter = dict.stopageFilter;
        let isRefundable = dict.refundable;

        let isFreeMeal = dict.freeMeal;
        let airLines = dict.airlines;

        const lastItem = this.state.completeResponseData[1][0].flightSegment[0].length - 1;
        let filterDate = this.state.completeResponseData[1][0].flightSegment[0][0].stopPointDepartureTime;

        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        console.log(dict);
        let filterData = this.state.completeResponseData[1];

        if (timeFilters1.length > 0 && timeFilters1.length < 4) {
          timeFilters1.forEach((item, index) => {


            let timeArr = String(item).split(" - ");
            //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
            let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
            let finaltime2 = filterDate;
            if (timeArr[1] == "24:00") {
              finaltime2 = finaltime2 + "T" + "23:59" + ":59";
            } else {
              finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
            }
            console.log("time is  " + finaltime1);
            let deptTime1 = finaltime1;
            let depTime2 = finaltime2;
            // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
            let arr = this.state.completeResponseData[1].filter(data => {
              let lastItem = data.flightSegment[0].length - 1;
              let flightDeptTime = data.flightSegment[0][lastItem].stopPointDepartureTime;

              if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
                console.log("true")
                return true;
              } else {
                return false;
              }

            });
            console.log(arr);
            if (index == 0) {
              filterData = arr;
            } else {
              console.log("arr length in second time " + arr.length);
              filterData = filterData.concat(arr);
            }

            console.log("filter data after time filter " + filterData.length);
          });
        }

        if (stopageFilter.length > 0 && stopageFilter.length < 3) {
          stopageFilter.forEach((item, index) => {
            let arr = filterData.filter(data => {
              if (item == 0) {
                return data.flightSegment[0].length == 1;
              } else if (item == 1) {
                return data.flightSegment[0].length == 2;
              } else {
                return data.flightSegment[0].length > 2;
              }
            });

            if (index == 0) {
              filterData = arr;
            } else {
              filterData = filterData.concat(arr);
            }
          });

          console.log("filter data after stopage filter " + filterData.length);
        }

        if (isRefundable) {
          filterData = filterData.filter(data => {
            return data.isRefundable == true;
          });
          console.log("filter data after refundable filter " + filterData.length);
        }

        if (isFreeMeal) {
          filterData = filterData.filter(data => {
            return data.fare.totalMealCharges == 0;
          });

          console.log("filter data after free meal filter " + filterData.length);
        }

        if (airLines.length > 0) {
          let arr2 = [];
          airLines.forEach((airlineIndex, index) => {

            let arr = filterData.filter((flightItem) => {

              if (flightItem.flightSegment[0][lastItem].airline.airlineCode === this.arrvialAirlines[airlineIndex].airlineCode) {
                return true;
              } else {
                return false;
              }
            })


            if (index == 0) {
              arr2 = arr

            } else {
              arr2 = arr2.concat(arr)
            }

            // if (index == 0) {
            //   filterData = arr;
            // } else {
            //   console.log("arr length in second time " + arr.length);
            //   filterData = filterData.concat(arr);
            // }
          })

          filterData = arr2;
        }
        console.log("total data in list " + this.state.flightListData.length);
        console.log(timeFilters1.length);
        console.log(filterData);
        console.log(filterData.length);
        if (timeFilters1.length == 0 && stopageFilter.length == 0 && !isRefundable && !isFreeMeal && airLines.length == 0) {

          if (!this.state.filterForDep) {

            this.setState({
              isFilterActive: false,
              filterForReturn: false,
              loading: !this.state.loading
            });

          } else {

            this.setState({
              filterForReturn: false,
              loading: !this.state.loading
            });

          }

        } else {
          this.setState({
            filteresFlightData: filterData,
            filterDataForTwoWayRet: filterData,
            isFilterActive: true,
            filterForReturn: true,
            selectedDestinationFlightIndex: filterData.length > 0 ? 0 : -1,
            loading: !this.state.loading
          });
        }

      }

    }
    else if (this.state.isInterNational) {

      this.setState({
        openedFilterModel: false,
        selectedFiltersDepart: dict
      });

      let timeFilters = dict.timeFilter;
      let timeFilters2 = dict.timeFilter2;
      let stopageFilter = dict.stopageFilter;
      let isRefundable = dict.refundable;
      let isFreeMeal = dict.freeMeal;
      let airLines = dict.airlines;
      console.log(dict);
      let filterData = this.state.completeResponseData[0];

      if (((timeFilters.length > 0 && timeFilters.length < 4)) && (timeFilters2.length > 0 && timeFilters2.length < 4)) {

        let filterDate = this.state.completeResponseData[0][0].flightSegment[0][0].stopPointDepartureTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }

        timeFilters.forEach((item, index) => {
          let timeArr = String(item).split(" - ");

          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);

          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = this.state.completeResponseData[0].filter(data => {

            let flightDeptTime = data.flightSegment[0][0].stopPointDepartureTime;

            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }

          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }

          console.log("filter data after time filter " + filterData.length);
        });

        filterDate = this.state.completeResponseData[0][0].flightSegment[1][0].stopPointDepartureTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }

        timeFilters2.forEach((item, index) => {

          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);
          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = filterData.filter(data => {

            let flightDeptTime = data.flightSegment[1][0].stopPointDepartureTime;

            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }

          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }

          console.log("filter data after time filter " + filterData.length);
        });

      }

      else if (timeFilters.length > 0 && timeFilters.length < 4) {

        let filterDate = this.state.completeResponseData[0][0].flightSegment[0][0].stopPointDepartureTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }


        timeFilters.forEach((item, index) => {



          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);

          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = filterData.filter(data => {

            let flightDeptTime = data.flightSegment[0][0].stopPointDepartureTime;

            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }

          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }

          console.log("filter data after time filter " + filterData.length);
        });
      }


      else if (timeFilters2.length > 0 && timeFilters2.length < 4) {

        let filterDate = this.state.completeResponseData[0][0].flightSegment[1][0].stopPointDepartureTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }


        timeFilters2.forEach((item, index) => {

          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);
          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = this.state.completeResponseData[0].filter(data => {
            let flightDeptTime = data.flightSegment[1][0].stopPointDepartureTime;

            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }

          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }

          console.log("filter data after time filter " + filterData.length);
        });
      }

      if (stopageFilter.length > 0 && stopageFilter.length < 3) {
        stopageFilter.forEach((item, index) => {
          let arr = filterData.filter(data => {
            if (item == 0) {
              if ((data.flightSegment[0].length == 1) && (data.flightSegment[1].length == 1)) {
                return true;
              }

            } else if (item == 1) {

              if ((data.flightSegment[0].length == 2) && (data.flightSegment[1].length == 2)) {
                return true;
              }
            } else {
              if ((data.flightSegment[0].length > 2) && (data.flightSegment[1].length > 2)) {
                return true;
              }
            }
          });

          if (index == 0) {
            filterData = arr;
          } else {
            filterData = filterData.concat(arr);
          }
        });

        console.log("filter data after stopage filter " + filterData.length);
      }

      if (isRefundable) {
        filterData = filterData.filter(data => {
          return data.isRefundable == true;
        });
        console.log("filter data after refundable filter " + filterData.length);
      }

      if (isFreeMeal) {
        filterData = filterData.filter(data => {
          return data.fare.totalMealCharges == 0;
        });

        console.log("filter data after free meal filter " + filterData.length);
      }

      if (airLines.length > 0) {
        let arr2 = [];
        airLines.forEach((airlineIndex, index) => {

          let arr = filterData.filter((flightItem) => {

            if (flightItem.flightSegment[0][0].airline.airlineCode === this.internationalAirlines[airlineIndex].airlineCode) {
              return true;
            } else {
              return false;
            }

          })
          if (index == 0) {
            arr2 = arr

          } else {
            arr2 = arr2.concat(arr)
          }
        })
        filterData = arr2;
      }


      console.log("total data in list " + this.state.flightListData.length);
      console.log(timeFilters.length);
      console.log(filterData);
      console.log(filterData.length);
      if (timeFilters.length == 0 && timeFilters2.length == 0 && stopageFilter.length == 0 && !isRefundable && !isFreeMeal && airLines.length == 0) {

        this.setState({
          isFilterActive: false,
          loading: !this.state.loading
        });
      } else {
        this.setState({

          filteresFlightData: filterData,
          isFilterActive: true,
          selectedDestinationFlightIndex: filterData.length > 0 ? 0 : -1,
          loading: !this.state.loading
        });
      }
    }
    else {
      this.setState({
        openedFilterModel: false,
        selectedFiltersDepart: dict
      });
      let timeFilters = dict.timeFilter;
      let stopageFilter = dict.stopageFilter;
      let isRefundable = dict.refundable;
      let isFreeMeal = dict.freeMeal;
      let airLines = dict.airlines;
      console.log("FLIGHT_LIST_DATA:" + JSON.stringify(this.state.flightListData[0]))
      let filterDate = this.state.flightListData[0].flightSegment[0][0].stopPointDepartureTime;
      if (filterDate) {
        filterDate = filterDate.split("T")[0];
      }
      console.log(dict);
      let filterData = this.state.flightListData;

      if (timeFilters.length > 0 && timeFilters.length < 4) {
        timeFilters.forEach((item, index) => {
          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);

          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = this.state.flightListData.filter(data => {
            let flightDeptTime = data.flightSegment[0][0].stopPointDepartureTime;

            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }
          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }
          console.log("filter data after time filter " + filterData.length);
        });
      }
      if (stopageFilter.length > 0 && stopageFilter.length < 3) {
        let filterArr = [];
        stopageFilter.forEach((item, index) => {
          let arr = filterData.filter(data => {
            if (item == 0) {
              return data.flightSegment[0].length == 1;
            } else if (item == 1) {
              return data.flightSegment[0].length == 2;
            } else {
              return data.flightSegment[0].length > 2;
            }
          });

          if (index == 0) {
            filterArr = arr;
          } else {
            filterArr = filterArr.concat(arr);
          }
        });
        filterData = filterArr;
        console.log("filter data after stopage filter " + filterData.length);
      }
      if (isRefundable) {
        filterData = filterData.filter(data => {
          return data.isRefundable == true;
        });
        console.log("filter data after refundable filter " + filterData.length);
      }
      if (isFreeMeal) {
        filterData = filterData.filter(data => {
          return data.fare.totalMealCharges == 0;
        });
        console.log("filter data after free meal filter " + filterData.length);
      }
      if (airLines.length > 0) {
        let arr2 = []
        airLines.forEach((airlineIndex, index) => {
          let arr = filterData.filter((flightItem) => {
            if (flightItem.flightSegment[0][0].airline.airlineCode === this.depAirlines[airlineIndex].airlineCode) {
              return true;
            } else {
              return false;
            }
          })
          if (index == 0) {
            arr2 = arr
          } else {
            arr2 = arr2.concat(arr)
          }
        })
        filterData = arr2;
      }
      console.log("total data in list " + this.state.flightListData.length);
      console.log(timeFilters.length);
      console.log(filterData);
      console.log(filterData.length);
      if (timeFilters.length == 0 && stopageFilter.length == 0 && !isRefundable && !isFreeMeal && airLines.length == 0) {
        this.setState({
          isFilterActive: false,
          loading: !this.state.loading
        });
      } else {
        this.setState({
          filteresFlightData: filterData,
          isFilterActive: true,
          selectedOriginFlightIndex: 0,
          loading: !this.state.loading
        });
      }
    }
  }

  setUniqueAirlines() {
    let exp;
    const map = new Map();
    const map2 = new Map();
    console.log("set uniquee")
    if (this.props.flightSearchParams.return_date == null) {
      //for single way airlines filter
      exp = 1;
    } else if (this.props.flightSearchParams.return_date != null && this.state.completeResponseData.length == 2) {
      //for two way domestic airlines filter
      exp = 2;
    } else {
      //for Internatinal flights
      exp = 3;
    }
    console.log(exp)
    switch (exp) {
      case 1: {
        console.log("in case first");
        const response = this.state.completeResponseData[0];
        for (const item of response) {
          if (!map.has(item.flightSegment[0][0].airline.airlineCode)) {
            map.set(item.flightSegment[0][0].airline.airlineCode, true)
            this.depAirlines.push({
              airlineCode: item.flightSegment[0][0].airline.airlineCode,
              airlineName: item.flightSegment[0][0].airline.airlineName,
              airlineImage: item.flightSegment[0][0].airline.image
            })
          }
        }
      }
        break;
      case 2:
        {
          const departData = this.state.completeResponseData[0];
          const arrivalData = this.state.completeResponseData[1];
          for (const item of departData) {
            if (!map.has(item.flightSegment[0][0].airline.airlineCode)) {
              map.set(item.flightSegment[0][0].airline.airlineCode, true)
              this.depAirlines.push({
                airlineCode: item.flightSegment[0][0].airline.airlineCode,
                airlineName: item.flightSegment[0][0].airline.airlineName,
                airlineImage: item.flightSegment[0][0].airline.image
              })
            }
          }
          for (const item of arrivalData) {
            if (!map2.has(item.flightSegment[0][0].airline.airlineCode)) {
              map2.set(item.flightSegment[0][0].airline.airlineCode, true)
              this.arrvialAirlines.push({
                airlineCode: item.flightSegment[0][0].airline.airlineCode,
                airlineName: item.flightSegment[0][0].airline.airlineName,
                airlineImage: item.flightSegment[0][0].airline.image
              })
            }
          }
        }
        break;
      case 3:
        {
          const response = this.state.completeResponseData[0];
          for (const item of response) {
            if (!map.has(item.flightSegment[0][0].airline.airlineCode)) {
              map.set(item.flightSegment[0][0].airline.airlineCode, true)
              this.internationalAirlines.push({
                airlineCode: item.flightSegment[0][0].airline.airlineCode,
                airlineName: item.flightSegment[0][0].airline.airlineName,
                airlineImage: item.flightSegment[0][0].airline.image
              })
            }
          }
        }
        break;
    }
  }

  getFlightsList() {
    let param = this.props.flightSearchParams;
    // this.setState({ spinner: true });
    console.log('parameters for flights list   ' + JSON.stringify(param))
    this.props.searchFlightAction(param);
  }

  getFlightsCalendar() {
    let dict = this.props.flightSearchParams;
    let param = {
      origin: dict.origin,
      destination: dict.destination,
      departure_date: dict.departure_date
    };
    this.setState({ spinner: true });
    APIManager.getFlightsCalender(param, (success, response) => {
      this.setState({ spinner: false });
      this.getFlightsList();
      if (success) {
        let data = response.object;
        let sortData = data.sort(function compare(a, b) {
          let dateA = Date.parse(moment(a.departs_at).format("YYYY-MM-DD"))
          let dateB = Date.parse(moment(b.departs_at).format("YYYY-MM-DD"))
          return dateA - dateB;
        });
        this.setState({ data: sortData, loading: !this.state.loading });
      } else {
        console.log("Got error in calendar  " + JSON.stringify(response));
        // setTimeout(() => {
        //   alert(response.message);
        // }, 100);
      }
    });
  }


  compareTodayWithdate(date) {
    // let now = dateFormat(new Date(), "yyyy-mm-dd");
    let now = moment(new Date()).format("YYYY-MM-DD");
    let dateCompare = moment(date).format("YYYY-MM-DD");

    if (now == dateCompare) {
      return "Today";
    } else {
      return moment(date).format("DD MMM");
    }
  }

  arrivalSort(flightData) {
    console.log("calling arr sort and flightData is");
    console.log(flightData)
    if (this.state.sortArrIncr) {
      console.log("increment")
      flightData.sort((a, b) => {
        let alastflightIndex = a.flightSegment[0].length - 1;
        let blastflightIndex = b.flightSegment[0].length - 1;
        let aDate = new Date(a.flightSegment[0][alastflightIndex].stopPointArrivalTime);
        let bDate = new Date(b.flightSegment[0][blastflightIndex].stopPointArrivalTime);
        if (aDate < bDate) {
          return -1;
        } else if (aDate > bDate) {
          return 1;
        } else {
          return 0;
        }
      })
    } else {
      console.log("decrement")
      flightData.sort((a, b) => {
        let alastflightIndex = a.flightSegment[0].length - 1;
        let blastflightIndex = b.flightSegment[0].length - 1;
        let aDate = new Date(a.flightSegment[0][alastflightIndex].stopPointArrivalTime);
        let bDate = new Date(b.flightSegment[0][blastflightIndex].stopPointArrivalTime);
        if (bDate < aDate) {
          return -1;
        } else if (bDate > aDate) {
          return 1;
        } else {
          return 0;
        }
      })
    }
    console.log("sorted flight Data is ")
    console.log(flightData);
    console.log("returning flight Data");
    return flightData;

  }

  priceSort(flightData) {
    console.log("calling flightSort sort and flightData is");
    console.log(flightData)
    if (this.state.sortPriceIncr) {
      flightData.sort((a, b) => {
        // let aFare = new Date(a.fare.offeredFare);
        // let bDate = new Date(b.fare.offeredFare);
        if (a.fare.offeredFare < b.fare.offeredFare) {
          return -1;
        } else if (a.fare.offeredFare > b.fare.offeredFare) {
          return 1;
        } else {
          return 0;
        }
      })
    } else {
      flightData.sort((a, b) => {
        // let aDate = new Date(a.flightSegment[0][0].origin.depTime);
        // let bDate = new Date(b.flightSegment[0][0].origin.depTime);
        if (a.fare.offeredFare > b.fare.offeredFare) {
          return -1;
        } else if (a.fare.offeredFare < b.fare.offeredFare) {
          return 1;
        } else {
          return 0;
        }
      })
    }
    console.log("sorted flight Data is ")
    console.log(flightData);
    console.log("returning flight Data");
    return flightData;
  }

  durationSort(flightData) {
    console.log("calling duration sort and flightData is");
    console.log(flightData)
    if (this.state.sortDurIncr) {
      flightData.sort((a, b) => {
        let atotalDuration = 0;
        for (i = 0; i < a.flightSegment[0].length; i++) {
          let item = a.flightSegment[0][i];
          atotalDuration = atotalDuration + item.duration;
        }
        let btotalDuration = 0;
        for (i = 0; i < b.flightSegment[0].length; i++) {
          let item = b.flightSegment[0][i];
          btotalDuration = btotalDuration + item.duration;
        }
        if (atotalDuration < btotalDuration) {
          return -1;
        } else if (atotalDuration > btotalDuration) {
          return 1;
        } else {
          return 0;
        }
      })
    } else {
      flightData.sort((a, b) => {
        let atotalDuration = 0;
        for (i = 0; i < a.flightSegment[0].length; i++) {
          let item = a.flightSegment[0][i];
          atotalDuration = atotalDuration + item.duration;
        }
        let btotalDuration = 0;
        for (i = 0; i < b.flightSegment[0].length; i++) {
          let item = b.flightSegment[0][i];
          btotalDuration = btotalDuration + item.duration;
        }
        if (atotalDuration < btotalDuration) {
          return 1;
        } else if (atotalDuration > btotalDuration) {
          return -1;
        } else {
          return 0;
        }
      })
    }
    console.log("sorted flight Data is ")
    console.log(flightData);
    console.log("returning flight Data");
    return flightData;
  }

  depSort(flightData) {
    console.log("calling dep sort and flightData is");
    console.log(flightData)
    if (this.state.sortDepIncr) {

      flightData.sort((a, b) => {
        let aDate = new Date(a.flightSegment[0][0].origin.depTime);
        let bDate = new Date(b.flightSegment[0][0].origin.depTime);
        if (aDate < bDate) {
          return -1;
        } else if (aDate > bDate) {
          return 1;
        } else {
          return 0;
        }
      })
    } else {
      flightData.sort((a, b) => {
        let aDate = new Date(a.flightSegment[0][0].origin.depTime);
        let bDate = new Date(b.flightSegment[0][0].origin.depTime);
        if (bDate < aDate) {
          return -1;
        } else if (bDate > aDate) {
          return 1;
        } else {
          return 0;
        }
      })
    }
    console.log("sorted flight Data is ")
    console.log(flightData);
    console.log("returning flight Data");
    return flightData;
  }

  airlineNameSort(flightData) {
    console.log("calling dep sort and flightData is");
    console.log(flightData)
    if (this.state.sortAirlineIncr) {
      flightData.sort((a, b) => {
        if (a.airlineName < b.airlineName) {
          return -1;
        } else if (a.airlineName > a.airlineName) {
          return 1;
        } else {
          return 0;
        }
      })
    } else {
      flightData.sort((a, b) => {
        if (b.airlineName < a.airlineName) {
          return -1;
        } else if (b.airlineName > a.airlineName) {
          return 1;
        } else {
          return 0;
        }
      })
    }
    return flightData;
  }

  sortFlights() {
    let flightData = [];
    if (this.state.isFilterActive) {
      flightData = this.state.filteresFlightData;
      switch (this.state.selectedSort) {
        case 0:
          // for departure sort    
          flightData = this.depSort(flightData);
          break;
        case 1:
          // for Arrival sort
          flightData = this.arrivalSort(flightData);
          break;
        case 2:
          // for duration sort{}
          {
            flightData = this.durationSort(flightData);
            break;
          }
        case 3:
          // for price sort
          flightData = this.priceSort(flightData);
          break;
        case 4:
          {
            flightData = this.airlineNameSort(flightData);
            break;
          }
      }
      this.setState({
        filteresFlightData: flightData,
        loading: !this.state.loading,
      })
    } else {
      flightData = this.state.flightListData;
      switch (this.state.selectedSort) {
        case 0:
          // for dep sort
          flightData = this.depSort(flightData);
          break;
        case 1:
          // for Arrival sort
          flightData = this.arrivalSort(flightData);
          break;
        case 2:
          // for duration sort{}
          {
            flightData = this.durationSort(flightData);
            break;
          }
        case 3:
          // for price sort
          flightData = this.priceSort(flightData);
          break;
        case 4: {
          flightData = this.airlineNameSort(flightData);
          break;
        }
      }
      this.setState({
        flightListData: flightData,
        loading: !this.state.loading,
      })
    }
  }

  calculateArrivalAndDepartureTime(item) {
    let data = item.flightSegment[0];

    // let deptTime = dateFormat(data[0].stopPointDepartureTime,'hh:mm')
    // let arrTime = dateFormat(data[0].stopPointArrivalTime,'hh:mm')
    // return deptTime + '-' + arrTime

    // console.log('calculated item is  '+ JSON.stringify(data[data.lenght-1].stopPointArrivalTime))
    // console.log('calculated items depart time is  '+ JSON.stringify(data[0].stopPointDepartureTime))
    if (data.lenght == 1) {
      // let deptTime = moment(data[0].stopPointDepartureTime).format("HH:mm");
      // let arrTime = moment(data[0].stopPointArrivalTime).format("HH:mm");
      let deptTime = moment(data[0].stopPointDepartureTime).format("HH:mm");
      let arrTime = moment(data[0].stopPointArrivalTime).format("HH:mm");
      console.log(
        "depart Time is " + moment(data[0].stopPointDepartureTime).format("HH:mm")
      );
      console.log(
        "Arrival Time is " + moment(data[0].stopPointArrivalTime).format("HH:mm")
      );

      let depTime = String(data[0].stopPointDepartureTime).split("T");
      let arTime = String(data[0].stopPointArrivalTime).split("T");

      return (
        depTime[1].substring(0, depTime[1].length - 3) +
        "-" +
        arTime[1].substring(0, arTime[1].length - 3)
      );
    } else {
      let deptTime = moment(data[0].stopPointDepartureTime).format("HH:mm");
      let arrTime = moment(
        data[data.length - 1].stopPointArrivalTime).format("HH:mm");

      let depTime = String(data[0].stopPointDepartureTime).split("T");
      let arTime = String(data[data.length - 1].stopPointArrivalTime).split(
        "T"
      );

      return (
        depTime[1].substring(0, depTime[1].length - 3) +
        "-" +
        arTime[1].substring(0, arTime[1].length - 3)
      );
      // return deptTime + '-' + arrTime
    }
  }

  calculateDurationTime(data) {
    let totalDuration = 0;
    for (i = 0; i < data.length; i++) {
      let item = data[i];
      totalDuration = totalDuration + item.duration;
    }
    return this.convertMinsIntoHandM(totalDuration);
  }

  convertMinsIntoHandM(n) {
    var num = n;
    var hours = num / 60;
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + "h " + rminutes + "m";
  }

  returnNonStopOrByOrigin(item) {
    let viacode;
    if (item.length == 1) {
      return "Non-Stop";
    } else {
      item.forEach((item, index) => {
        if (index != 0) {
          if (index == 1) {
            viacode = "Via " + item.origin.airport.airportCode
          } else {
            viacode = viacode + "-" + item.origin.airport.airportCode
          }

        }
      });
      return viacode;
    }
  }

  filterFlight = () => {
    this.setState({ openedFilterModel: true });
  }

  closeFilterModel = () => {
    this.setState({ openedFilterModel: false })
  }

  closeDetailModel = () => {
    this.setState({ openedDetailModel: false })
  }

  renderSingleTrip = () => {
    let { datas } = this.state;
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        {
          datas.map((data, index) => (
            <TouchableOpacity key={index} style={styles.card} onPress={() => this.setState({ openedDetailModel: true })}>
              <View style={styles.cardHeader}>
                <View style={[styles.cardHeaderCol1, { width: "80%" }]}>
                  <View style={styles.cardheaderFlightView}>
                    <Image source={require("../../../Assets/images/jet.png")}
                      style={styles.cardheaderFlightImg}></Image>
                    <Text style={[styles.price, { color: colors.colorBlack }]}>Jet Airways 304</Text>
                  </View>
                  <View style={styles.cardHeaderAmenties}>
                    <Image source={Images.imgfood} style={styles.flightAmenityImg}></Image>
                    <Image source={Images.imgflightinfo} style={styles.flightAmenityImg}></Image>
                    <Image source={Images.imgseat} style={styles.flightAmenityImg}></Image>
                  </View>
                </View>
                <View style={[styles.cardHeaderCol2, { width: "20%" }]}>
                  <Text style={styles.priceText}>
                    <Image source={Images.imgrupeeblue}
                      style={styles.priceImg}></Image>
                    3237
                          </Text>
                </View>
              </View>
              <View style={styles.cardContent}>
                <View style={[styles.cardContentCol1, { width: "100%", paddingRight: 7 }]}>
                  <View style={{ width: "15%" }}>
                    <Text style={styles.estimate}>20:15</Text>
                    <Text style={styles.stnName}>DEL</Text>
                  </View>
                  <View style={{
                    width: "70%",
                    height: 15,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}>
                    <View style={{
                      width: "45%",
                      alignItems: "center"
                      // backgroundColor: "blue"
                    }}>
                      <Text style={styles.stnName}>Nonstop</Text>
                      <View style={styles.dotView}>
                        <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
                        <Dash style={{
                          width: "75%",
                          height: 1,
                        }} dashColor={colors.gray} dashLength={5}
                        />
                      </View>
                    </View>
                    <View style={{
                      width: "10%",
                      height: 20,
                      marginTop: 15
                    }}>
                      <Image source={Images.imgplaneblue} style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
                    </View>
                    <View style={{
                      width: "45%",
                      alignItems: "center"
                    }}>
                      <Text style={styles.stnName}>2h 15m</Text>
                      <View style={styles.dotView}>
                        <Dash style={{
                          width: "75%",
                          height: 1,
                        }} dashColor={colors.gray} dashLength={5}
                        />
                        <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
                      </View>
                    </View>
                  </View>
                  <View style={{ width: "15%", alignItems: "flex-end" }}>
                    <Text style={styles.estimate}>20:15</Text>
                    <Text style={styles.stnName}>DEL</Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          ))
        }
        <View style={{ height: 200 }}></View>
      </ScrollView>
    )
  }

  renderRoundTrip = () => {
    let { datas } = this.state;
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        {
          datas.map((data, index) => (
            <TouchableOpacity key={index} style={[styles.card, styles1.card]} onPress={() => this.setState({ openedDetailModel: true })}>
              <View style={{
                width: "80%",
                height: "100%",
                borderRightWidth: 1,
                borderRightColor: colors.lightgrey,
              }}>
                <View style={{
                  width: "100%",
                }}>
                  <View style={[styles.cardHeader, { backgroundColor: colors.colorWhite }]}>
                    <View style={[styles.cardHeaderCol1, { width: "100%" }]}>
                      <View style={styles.cardheaderFlightView}>
                        <Image source={require("../../../Assets/images/jet.png")}
                          style={styles.cardheaderFlightImg}></Image>
                        <Text style={[styles.price, { color: colors.colorBlack }]}>Jet Airways 304</Text>
                      </View>
                      <View style={styles.cardHeaderAmenties}>
                        <Image source={Images.imgfood} style={styles.flightAmenityImg}></Image>
                        <Image source={Images.imgflightinfo} style={styles.flightAmenityImg}></Image>
                        <Image source={Images.imgseat} style={styles.flightAmenityImg}></Image>
                      </View>
                    </View>
                  </View>
                  <View style={[styles.cardContent, { height: 70, paddingRight: 7 }]}>
                    <View style={[styles.cardContentCol1, { width: "100%" }]}>
                      <View style={{ width: "20%" }}>
                        <Text style={styles.estimate}>20:15</Text>
                        <Text style={styles.stnName}>DEL</Text>
                      </View>
                      <View style={{ width: "60%", alignItems: "center" }}>
                        <Text style={styles.estimate}>2h 15m</Text>
                        <View style={styles.dotView}>
                          <Image source={Images.imgbluedot} style={{ width: "10%" }}></Image>
                          <Dash style={{
                            width: "75%",
                            height: 1,
                          }} dashColor={colors.gray} dashLength={3}
                          />
                          <Image source={Images.imgbluedot} style={{ width: "10%" }}></Image>
                        </View>
                        <Text style={styles.estimate}>Nonstop</Text>
                      </View>
                      <View style={{ width: "20%", alignItems: "flex-end" }}>
                        <Text style={styles.estimate}>20:15</Text>
                        <Text style={styles.stnName}>DEL</Text>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={{
                  height: 1, width: "100%",
                  backgroundColor: colors.lightgrey
                }}></View>
                <View style={{
                  width: "100%",
                }}>
                  <View style={[styles.cardHeader, { backgroundColor: colors.colorWhite }]}>
                    <View style={[styles.cardHeaderCol1, { width: "100%" }]}>
                      <View style={styles.cardheaderFlightView}>
                        <Image source={require("../../../Assets/images/jet.png")}
                          style={styles.cardheaderFlightImg}></Image>
                        <Text style={[styles.price, { color: colors.colorBlack }]}>Jet Airways 304</Text>
                      </View>
                      <View style={styles.cardHeaderAmenties}>
                        <Image source={Images.imgfood} style={styles.flightAmenityImg}></Image>
                        <Image source={Images.imgflightinfo} style={styles.flightAmenityImg}></Image>
                        <Image source={Images.imgseat} style={styles.flightAmenityImg}></Image>
                      </View>
                    </View>
                  </View>
                  <View style={[styles.cardContent, { height: 70, paddingRight: 7 }]}>
                    <View style={[styles.cardContentCol1, { width: "100%" }]}>
                      <View style={{ width: "20%" }}>
                        <Text style={styles.estimate}>20:15</Text>
                        <Text style={styles.stnName}>DEL</Text>
                      </View>
                      <View style={{ width: "60%", alignItems: "center" }}>
                        <Text style={styles.estimate}>2h 15m</Text>
                        <View style={styles.dotView}>
                          <Image source={Images.imgbluedot} style={{ width: "10%" }}></Image>
                          <Dash style={{
                            width: "75%",
                            height: 1,
                          }} dashColor={colors.gray} dashLength={3}
                          />
                          <Image source={Images.imgbluedot} style={{ width: "10%" }}></Image>
                        </View>
                        <Text style={styles.estimate}>Nonstop</Text>
                      </View>
                      <View style={{ width: "20%", alignItems: "flex-end" }}>
                        <Text style={styles.estimate}>20:15</Text>
                        <Text style={styles.stnName}>DEL</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.cardContentCol2}>
                <Text style={styles.priceText}>
                  <Image source={Images.imgrupeeblue} style={styles.priceImg}></Image>
                  3237
                </Text>
              </View>
            </TouchableOpacity>
          ))
        }
        <View style={{ height: 200 }}></View>
      </ScrollView>
    )
  }

  render() {
    let { prices, tripType, selectedPrice, isRoundTrip, selectedFiltersArr, selectedFiltersDepart,
      openedDetailModel, openedFilterModel, selectedOriginIndex, isInterNational } = this.state;
    let dict = this.props.flightSearchParams;

    const { destination, origin, return_date } = this.props.flightSearchParams;

    let originCity
    let airlines
    if (this.state.isRoundTrip && this.state.selectedOriginIndex == 1) {
      originCity = destination;
      airlines = this.arrvialAirlines
    } else if (this.state.isRoundTrip && this.state.selectedOriginIndex == 0) {
      originCity = origin;
      airlines = this.depAirlines
    } else if (this.state.isInterNational) {
      originCity = origin;
      airlines = this.internationalAirlines
    } else {
      originCity = origin;
      airlines = this.depAirlines
    }

    console.log("params are:-" + JSON.stringify(dict));
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.colorBlue }}>
        <Spinner
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={{ color: colors.colorWhite }}
        />
        <Modal
          transparent={false}
          animationType={"slide"}
          visible={this.state.openedFilterModel}>
          <View
            style={{ flex: 1 }}>
            <FlightFilters
              originCity={originCity}
              destCity={destination}
              airlines={airlines}
              isInterNational={this.state.isInterNational}
              onModelHide={() => {
                this.setState({
                  openedFilterModel: false
                });
              }}
              modelVisible={this.state.openedFilterModel}
              slectedFilters={this.state.isRoundTrip && this.state.selectedOriginIndex == 1 ? this.state.selectedFiltersArr : this.state.selectedFiltersDepart}
              onFilterSelection={filterDict => {
                this.onFilterSelection(filterDict);
              }}
            />
          </View>
        </Modal>
        <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} />
        <FlightDetailModal modalVisible={openedDetailModel}
          closeModal={this.closeDetailModel} />

        <FlightFilterModal modalVisible={openedFilterModel}
          closeModal={this.closeFilterModel} />

        <View style={styles.container}>
          <Header fireEvent={this.filterFlight} data={dict} />
          <View style={styles.tabs}>
            <TouchableOpacity style={styles.tab}>
              <Text style={styles.tabText}>Departure</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.tab, {
              borderLeftWidth: 1,
              borderRightWidth: 1,
              borderColor: "lightgray"
            }]}>
              <Text style={styles.tabText}>Duration</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.tab, {
              flexDirection: "row"
            }]}>
              <Text style={styles.tabText}>Price</Text>
              <Image source={Images.imgdownarrow} style={{
                width: 20, height: 15, resizeMode: "contain"
              }}></Image>
            </TouchableOpacity>
          </View>
          <View style={styles.mainView}>
            {tripType == 1 ?
              this.renderSingleTrip() :
              this.renderRoundTrip()}
          </View>
          <View style={styles.pricesView}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              {
                prices.map((price, index) => (
                  <TouchableOpacity key={index} style={[styles.priceView, {
                    borderTopColor: index == selectedPrice ? colors.colorBlue : "transparent"
                  }]} onPress={() => this.setState({ selectedPrice: index })}>
                    <View style={styles.priceBtn}>
                      <Text style={[styles.price, {
                        color: index == selectedPrice ? colors.colorBlue : colors.colorBlack
                      }]}>Wed, 04 Dec</Text>
                      <Text style={[styles.price, {
                        marginTop: 5,
                        color: index == selectedPrice ? colors.colorBlue : colors.colorBlack,
                        flexDirection: "row",
                      }]}>
                        <Image source={index == selectedPrice ? Images.imgrupeeblue : Images.imgrupeebrown}
                          style={{
                            width: 10, height: 10, resizeMode: "contain"
                          }}></Image>
                        3237
                      </Text>
                    </View>
                    <View style={[styles.priceDevider, {
                      display: prices[index + 1] ? "flex" : "none"
                    }]}></View>
                  </TouchableOpacity>
                ))
              }
            </ScrollView>
          </View>
        </View>
      </SafeAreaView >
    );
  }
}

const styles1 = StyleSheet.create({
  card: {
    flexDirection: "row",
    height: 210,
  },
})

const mapStateToProps = state => {
  return {
    flightSearchParams: state.FlightsReducer.flightSearchParams,
    isLoading: state.isLoadingReducer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveFlightItem: (data) => dispatch(saveFlightItem(data)),
    searchFlightAction: (data) => dispatch(searchFlightAction(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FlightList)