import { StyleSheet, Dimensions, Platform } from "react-native";

const screenWidth = Dimensions.get("window").width;

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(244,248,254,1)"
  },
  styleSuperView: {
    flex: 1,

  },
  styleDateListItemSeperView: {
    height: 80,
    width: (screenWidth - 30) / 4,
    justifyContent: "center",
    alignItems: "center"
  },
  styleListDotView: {
    height: 10,
    width: 10,
    borderRadius: 5,
    marginTop: 8
  },

  styleDepatureView: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 10,
    borderRadius: 5,
    backgroundColor: "white",
    shadowColor: "grey",
    shadowOpacity: 0.3,
    marginLeft: 5,
    marginRight: 5,
    elevation: 6,
    paddingTop: 10,
    paddingBottom: 10,
    shadowOffset: { width: 1, height: 1 }
    // shadowRadius: 5
  },
  styleDepartureText: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  styleFlightListView: {


    marginTop: 10,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 10,
    flex: 1,
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    elevation: 2
    // shadowColor: "black",
    // shadowOpacity: 0.5,
    // shadowOffset: { width: 2, height: 2 },
    // shadowRadius: 5
  },
  styleSagmentVIew: {
    justifyContent: "center",
    justifyContent: "center",
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  styleSagementButtonsView: {
    width: "100%",
    height: "70%",
    backgroundColor: "white",
    flexDirection: "row",
    borderRadius: 21,
    elevation: 6,
    shadowColor: "gray",
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5
  },
  styleSagmentButton: {
    height: "100%",
    width: "50%",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 21
  },

  // my style

  listItemText: {
    fontSize: 12,
    color: "black",
    fontFamily: "Montserrat-Bold"

  }, listItemGreyText: {

    fontSize: 12,
    marginTop: 7,
    color: "#696969",
    fontFamily: "Montserrat-Regular"
  },
  bookViewStyle: {
    backgroundColor: "rgba(25, 89, 189, 1)",
    padding: 10,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    justifyContent: "center",
    borderRadius: 8
  },
  selectedflighticon:
  {
    height: 20,
    width: 20,
    borderRadius: 10,
    backgroundColor: "white"
  },
  BookbtnStyle:
  {
    backgroundColor: "white",
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  styleNavBar: {
    height: ((Platform.OS == 'ios') ? 44 : 52),
    // height: ((Platform.OS == 'ios')? getStatusBarHeight(true):0) + ((Platform.OS == 'ios')? 44:52), 
    width: screenWidth,
    backgroundColor: 'transparent'
  },
  styleNavBarSubView: {
    height: ((Platform.OS == 'ios') ? 44 : 52),
    width: "100%",
    // marginTop: ((Platform.OS == 'ios')? getStatusBarHeight(true):0),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: 'center',
    // backgroundColor:'green'
  },
  styleLeftBtn: {
    alignItems: "center",
    justifyContent: "center",
    width: 30,
    height: 30,
    marginLeft: 10
  },
  styleNavTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: "black",
    alignSelf: 'center'
    // marginLeft: -50,

  },
  styleNavBarRightView: {
    height: 35,
    flexDirection: "row",
    alignSelf: "flex-end",
    justifyContent: "flex-end"
  }

});
