import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList, Platform,
  Image,
  TouchableHighlight,
  Dimensions,
  SafeAreaView,
  TouchableOpacity,
  Modal,
  DeviceEventEmitter,
  ImageBase
} from "react-native";

import Dash from "react-native-dash";
import APIManager from "../../../Helper Classes/APIManager/APIManagerUpdated";
import Spinner from "react-native-loading-spinner-overlay";
import FlightFilters from "../FlightFilters";
import { connect } from "react-redux";
import moment from "moment";
import { baseUrl, imageBaseUrl } from "../../../Helper Classes/APIManager/APIConstants";
import { saveFlightItem } from "../../../Redux/Actions/Actions";
import { ifIphoneX, getStatusBarHeight } from "react-native-iphone-x-helper";

import { getFlightList } from "../../../Helper Classes/APIManager/ApiProvider";
import { searchFlightAction } from '../../../Redux/Actions';
import StringConstants from '../../../Helper Classes/StringConstants';
import NavigationServices from "../../../Helper Classes/NavigationServices";
import { ScrollView } from "react-native-gesture-handler";
import styles from "./Styles";
import { colors } from "../../../Helper Classes/Colors";
class FlightList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      datas: [1, 2, 3, 4, 5, 6, 7],
      selectedPrice: 0,
      prices: [
        {
          date: "Wed, 04 Dec",
          price: 3237
        },
        {
          date: "Wed, 05 Dec",
          price: 3147
        },
        {
          date: "Wed, 06 Dec",
          price: 7237
        },
        {
          date: "Wed, 07 Dec",
          price: 5237
        },
        {
          date: "Wed, 08 Dec",
          price: 2237
        },
        {
          date: "Wed, 09 Dec",
          price: 3337
        },
        {
          date: "Wed, 10 Dec",
          price: 4237
        },
        {
          date: "Wed, 11 Dec",
          price: 1237
        }
      ]
    };
  }
  twoWaydummy = [
    [
      {
        "index": "OB1",
        "origin": "DEL",
        "destination": "BOM",
        "isLCC": true,
        "airlineCode": "6E",
        "airlineName": "Indigo",
        "isRefundable": true,
        "source": "TBO",
        "direction": 0,
        "tripType": "OB1",
        "flightSegment": [
          [
            {
              "baggage": "40 KG",
              "cabinBaggage": " 7 KG",
              "tripIndicator": 1,
              "airline": {
                "airlineCode": "6E",
                "airlineName": "Indigo",
                "flightNumber": "129",
                "fareClass": "O",
                "image": "airline_logos/6E.png"
              },
              "seatAvailable": 3,
              "origin": {
                "airport": {
                  "airportCode": "DEL",
                  "airportName": "Indira Gandhi Airport",
                  "terminal": "1",
                  "cityCode": "DEL",
                  "cityName": "Delhi",
                  "countryCode": "IN",
                  "countryName": "India"
                },
                "depTime": "2019-04-10T17:30:00"
              },
              "destination": {
                "airport": {
                  "airportCode": "BOM",
                  "airportName": "Mumbai",
                  "terminal": "",
                  "cityCode": "BOM",
                  "cityName": "Mumbai",
                  "countryCode": "IN",
                  "countryName": "India"
                }
              },
              "duration": 130,
              "stopOver": false,
              "stopPoint": "",
              "stopPointArrivalTime": "2019-04-10T19:40:00",
              "stopPointDepartureTime": "2019-04-10T17:30:00",
              "remark": null,
              "isETicketEligible": true,
              "flightStatus": "Confirmed"
            }
          ]
        ],
        "fareDetails": [
          {
            "currency": "INR",
            "passengerType": 1,
            "passengerCount": 1,
            "baseFare": 2819,
            "tax": 910,
            "fuelCharge": 400,
            "AdditionalTxnFeeOfrd": 0,
            "AdditionalTxnFeePub": 0,
            "PGCharge": 0
          }
        ],
        "fareRules": [
          {
            "airline": "6E",
            "fareCode": "O0IP",
            "fareRuleDetail": "",
            "fareRestriction": ""
          }
        ],
        "fare": {
          "currency": "INR",
          "baseFare": 2819,
          "tax": 910,
          "fuelCharge": 400,
          "markupFare": 3783.23,
          "otherCharges": 54.23000000000002,
          "discount": 0,
          "commissionEarned": 65.96,
          "optionalCharge": 0,
          "serviceFee": 0,
          "totalBaggageCharges": 0,
          "totalMealCharges": 0,
          "totalSeatCharges": 0,
          "enukeMarkup": 0,
          "enukeFare": 3783.23,
          "agencyMarKup": 0,
          "agencyFare": 3783.23,
          "offeredFare": 3783.23,
          "publishedFare": 3965,
          "websiteFare": null
        }
      },
    ],
    [
      {
        "index": "IB1",
        "origin": "BOM",
        "destination": "DEL",
        "isLCC": true,
        "airlineCode": "6E",
        "airlineName": "Indigo",
        "isRefundable": true,
        "source": "TBO",
        "direction": 1,
        "tripType": "IB1",
        "flightSegment": [
          [
            {
              "baggage": "46 KG",
              "cabinBaggage": " 7 KG",
              "tripIndicator": 1,
              "airline": {
                "airlineCode": "6E",
                "airlineName": "Indigo",
                "flightNumber": "176",
                "fareClass": "L",
                "image": "airline_logos/6E.png"
              },
              "seatAvailable": 2,
              "origin": {
                "airport": {
                  "airportCode": "BOM",
                  "airportName": "Mumbai",
                  "terminal": "1",
                  "cityCode": "BOM",
                  "cityName": "Mumbai",
                  "countryCode": "IN",
                  "countryName": "India"
                },
                "depTime": "2019-04-11T13:35:00"
              },
              "destination": {
                "airport": {
                  "airportCode": "DEL",
                  "airportName": "Indira Gandhi Airport",
                  "terminal": "1",
                  "cityCode": "DEL",
                  "cityName": "Delhi",
                  "countryCode": "IN",
                  "countryName": "India"
                }
              },
              "duration": 130,
              "stopOver": false,
              "stopPoint": "",
              "stopPointArrivalTime": "2019-04-11T15:45:00",
              "stopPointDepartureTime": "2019-04-11T13:35:00",
              "remark": null,
              "isETicketEligible": true,
              "flightStatus": "Confirmed"
            }
          ]
        ],
        "fareDetails": [
          {
            "currency": "INR",
            "passengerType": 1,
            "passengerCount": 1,
            "baseFare": 2280,
            "tax": 935,
            "fuelCharge": 400,
            "AdditionalTxnFeeOfrd": 0,
            "AdditionalTxnFeePub": 0,
            "PGCharge": 0
          }
        ],
        "fareRules": [
          {
            "airline": "6E",
            "fareCode": "L0IP",
            "fareRuleDetail": "",
            "fareRestriction": ""
          }
        ],
        "fare": {
          "currency": "INR",
          "baseFare": 2280,
          "tax": 935,
          "fuelCharge": 400,
          "markupFare": 3303.99,
          "otherCharges": 88.98999999999978,
          "discount": 0,
          "commissionEarned": 53.35,
          "optionalCharge": 0,
          "serviceFee": 0,
          "totalBaggageCharges": 0,
          "totalMealCharges": 0,
          "totalSeatCharges": 0,
          "enukeMarkup": 0,
          "enukeFare": 3303.99,
          "agencyMarKup": 0,
          "agencyFare": 3303.99,
          "offeredFare": 3303.99,
          "publishedFare": 3451,
          "websiteFare": null
        }
      }
    ]
  ]

  dummyData = [
    {
      "index": "OB53",
      "origin": "DEL",
      "destination": "BOM",
      "isLCC": true,
      "airlineCode": "SG",
      "airlineName": "SpiceJet",
      "isRefundable": true,
      "source": "TBO",
      "direction": 0,
      "tripType": "OB53",
      "flightSegment": [
        [
          {
            "baggage": "15 KG",
            "cabinBaggage": "7 KG",
            "tripIndicator": 1,
            "airline": {
              "airlineCode": "SG",
              "airlineName": "SpiceJet",
              "flightNumber": "169",
              "fareClass": "V",
              "image": "/var/www/travel_portal/htdocs/public/airline_logos/SG.png"
            },
            "seatAvailable": 15,
            "origin": {
              "airport": {
                "airportCode": "DEL",
                "airportName": "Indira Gandhi Airport",
                "terminal": "1D",
                "cityCode": "DEL",
                "cityName": "Delhi",
                "countryCode": "IN",
                "countryName": "India"
              },
              "depTime": "2019-04-05T19:45:00"
            },
            "destination": {
              "airport": {
                "airportCode": "BOM",
                "airportName": "Mumbai",
                "terminal": "1",
                "cityCode": "BOM",
                "cityName": "Mumbai",
                "countryCode": "IN",
                "countryName": "India"
              }
            },
            "duration": 140,
            "stopOver": false,
            "stopPoint": "",
            "stopPointArrivalTime": "2019-04-05T22:05:00",
            "stopPointDepartureTime": "2019-04-05T19:45:00",
            "remark": null,
            "isETicketEligible": true,
            "flightStatus": "Confirmed"
          }
        ]
      ],
      "fareDetails": [
        {
          "currency": "INR",
          "passengerType": 1,
          "passengerCount": 1,
          "baseFare": 2500,
          "tax": 528,
          "fuelCharge": 0,
          "AdditionalTxnFeeOfrd": 0,
          "AdditionalTxnFeePub": 0,
          "PGCharge": 0
        }
      ],
      "fareRules": [
        {
          "airline": "SG",
          "fareCode": "VSAVER",
          "fareRuleDetail": "",
          "fareRestriction": ""
        }
      ],
      "fare": {
        "currency": "INR",
        "baseFare": 2500,
        "tax": 528,
        "fuelCharge": 0,
        "markupFare": 2883.83,
        "otherCharges": 0,
        "discount": 144.17000000000007,
        "commissionEarned": 63.75,
        "optionalCharge": 0,
        "serviceFee": 0,
        "totalBaggageCharges": 0,
        "totalMealCharges": 0,
        "totalSeatCharges": 0,
        "enukeMarkup": 0,
        "enukeFare": 2883.83,
        "agencyMarKup": 0,
        "agencyFare": 2883.83,
        "offeredFare": 2883.83,
        "publishedFare": 3029.71,
        "websiteFare": null
      }
    },
    {
      "index": "OB54",
      "origin": "DEL",
      "destination": "BOM",
      "isLCC": true,
      "airlineCode": "SG",
      "airlineName": "SpiceJet",
      "isRefundable": true,
      "source": "TBO",
      "direction": 0,
      "tripType": "OB54",
      "flightSegment": [
        [
          {
            "baggage": "15 KG",
            "cabinBaggage": "7 KG",
            "tripIndicator": 1,
            "airline": {
              "airlineCode": "SG",
              "airlineName": "SpiceJet",
              "flightNumber": "159",
              "fareClass": "V",
              "image": "/var/www/travel_portal/htdocs/public/airline_logos/SG.png"
            },
            "seatAvailable": 15,
            "origin": {
              "airport": {
                "airportCode": "DEL",
                "airportName": "Indira Gandhi Airport",
                "terminal": "1D",
                "cityCode": "DEL",
                "cityName": "Delhi",
                "countryCode": "IN",
                "countryName": "India"
              },
              "depTime": "2019-04-05T21:35:00"
            },
            "destination": {
              "airport": {
                "airportCode": "BOM",
                "airportName": "Mumbai",
                "terminal": "1",
                "cityCode": "BOM",
                "cityName": "Mumbai",
                "countryCode": "IN",
                "countryName": "India"
              }
            },
            "duration": 144,
            "stopOver": false,
            "stopPoint": "",
            "stopPointArrivalTime": "2019-04-05T23:59:00",
            "stopPointDepartureTime": "2019-04-05T21:35:00",
            "remark": null,
            "isETicketEligible": true,
            "flightStatus": "Confirmed"
          },
          {
            "baggage": "15 KG",
            "cabinBaggage": "7 KG",
            "tripIndicator": 1,
            "airline": {
              "airlineCode": "SG",
              "airlineName": "SpiceJet",
              "flightNumber": "159",
              "fareClass": "V",
              "image": "/var/www/travel_portal/htdocs/public/airline_logos/SG.png"
            },
            "seatAvailable": 15,
            "origin": {
              "airport": {
                "airportCode": "DEL",
                "airportName": "Indira Gandhi Airport",
                "terminal": "1D",
                "cityCode": "DEL",
                "cityName": "Delhi",
                "countryCode": "IN",
                "countryName": "India"
              },
              "depTime": "2019-04-05T21:35:00"
            },
            "destination": {
              "airport": {
                "airportCode": "BOM",
                "airportName": "Mumbai",
                "terminal": "1",
                "cityCode": "BOM",
                "cityName": "Mumbai",
                "countryCode": "IN",
                "countryName": "India"
              }
            },
            "duration": 144,
            "stopOver": false,
            "stopPoint": "",
            "stopPointArrivalTime": "2019-04-05T23:59:00",
            "stopPointDepartureTime": "2019-04-05T21:35:00",
            "remark": null,
            "isETicketEligible": true,
            "flightStatus": "Confirmed"
          },
          {
            "baggage": "15 KG",
            "cabinBaggage": "7 KG",
            "tripIndicator": 1,
            "airline": {
              "airlineCode": "SG",
              "airlineName": "SpiceJet",
              "flightNumber": "159",
              "fareClass": "V",
              "image": "/var/www/travel_portal/htdocs/public/airline_logos/SG.png"
            },
            "seatAvailable": 15,
            "origin": {
              "airport": {
                "airportCode": "DEL",
                "airportName": "Indira Gandhi Airport",
                "terminal": "1D",
                "cityCode": "DEL",
                "cityName": "Delhi",
                "countryCode": "IN",
                "countryName": "India"
              },
              "depTime": "2019-04-05T21:35:00"
            },
            "destination": {
              "airport": {
                "airportCode": "BOM",
                "airportName": "Mumbai",
                "terminal": "1",
                "cityCode": "BOM",
                "cityName": "Mumbai",
                "countryCode": "IN",
                "countryName": "India"
              }
            },
            "duration": 144,
            "stopOver": false,
            "stopPoint": "",
            "stopPointArrivalTime": "2019-04-05T23:59:00",
            "stopPointDepartureTime": "2019-04-05T21:35:00",
            "remark": null,
            "isETicketEligible": true,
            "flightStatus": "Confirmed"
          }
        ]
      ],
      "fareDetails": [
        {
          "currency": "INR",
          "passengerType": 1,
          "passengerCount": 1,
          "baseFare": 2500,
          "tax": 528,
          "fuelCharge": 0,
          "AdditionalTxnFeeOfrd": 0,
          "AdditionalTxnFeePub": 0,
          "PGCharge": 0
        }
      ],
      "fareRules": [
        {
          "airline": "SG",
          "fareCode": "VSAVER",
          "fareRuleDetail": "",
          "fareRestriction": ""
        }
      ],
      "fare": {
        "currency": "INR",
        "baseFare": 2500,
        "tax": 528,
        "fuelCharge": 0,
        "markupFare": 2883.83,
        "otherCharges": 0,
        "discount": 144.17000000000007,
        "commissionEarned": 63.75,
        "optionalCharge": 0,
        "serviceFee": 0,
        "totalBaggageCharges": 0,
        "totalMealCharges": 0,
        "totalSeatCharges": 0,
        "enukeMarkup": 0,
        "enukeFare": 2883.83,
        "agencyMarKup": 0,
        "agencyFare": 2883.83,
        "offeredFare": 2883.83,
        "publishedFare": 3029.71,
        "websiteFare": null
      }
    }]

  internationalDummyData = [
    [
      {
        "index": "OB84",
        "origin": "KWI",
        "destination": "DXB",
        "isLCC": true,
        "airlineCode": "KU",
        "airlineName": "Kuwait Airways Corporation",
        "isRefundable": true,
        "source": "TBO",
        "direction": 0,
        "tripType": "OB84",
        "flightSegment": [
          [
            {
              "baggage": null,
              "cabinBaggage": null,
              "tripIndicator": 1,
              "airline": {
                "airlineCode": "KU",
                "airlineName": "Kuwait Airways Corporation",
                "flightNumber": "384",
                "fareClass": "V",
                "image": "airline_logos/KU.png"
              },
              "origin": {
                "airport": {
                  "airportCode": "DEL",
                  "airportName": "Indira Gandhi Airport",
                  "terminal": "",
                  "cityCode": "DEL",
                  "cityName": "Delhi",
                  "countryCode": "IN",
                  "countryName": "India"
                },
                "depTime": "2019-01-28T05:50:00"
              },
              "destination": {
                "airport": {
                  "airportCode": "KWI",
                  "airportName": "Kuwait Int'l",
                  "terminal": "",
                  "cityCode": "KWI",
                  "cityName": "Kuwait City",
                  "countryCode": "KW",
                  "countryName": "Kuwait"
                }
              },
              "duration": 280,
              "stopOver": false,
              "stopPoint": "",
              "stopPointArrivalTime": "2019-01-28T08:00:00",
              "stopPointDepartureTime": "2019-01-28T05:50:00",
              "remark": null,
              "isETicketEligible": true,
              "flightStatus": "Confirmed"
            },
            {
              "baggage": null,
              "cabinBaggage": null,
              "tripIndicator": 1,
              "airline": {
                "airlineCode": "KU",
                "airlineName": "Kuwait Airways Corporation",
                "flightNumber": "673",
                "fareClass": "V",
                "image": "airline_logos/KU.png"
              },
              "origin": {
                "airport": {
                  "airportCode": "KWI",
                  "airportName": "Kuwait Int'l",
                  "terminal": "",
                  "cityCode": "KWI",
                  "cityName": "Kuwait City",
                  "countryCode": "KW",
                  "countryName": "Kuwait"
                },
                "depTime": "2019-01-28T15:40:00"
              },
              "destination": {
                "airport": {
                  "airportCode": "DXB",
                  "airportName": "Dubai",
                  "terminal": "",
                  "cityCode": "DXB",
                  "cityName": "Dubai",
                  "countryCode": "AE",
                  "countryName": "United Arab Emirates"
                }
              },
              "duration": 115,
              "stopOver": false,
              "stopPoint": "",
              "stopPointArrivalTime": "2019-01-28T18:35:00",
              "stopPointDepartureTime": "2019-01-28T15:40:00",
              "remark": null,
              "isETicketEligible": true,
              "flightStatus": "Confirmed",
              "accumulatedDuration": 855
            }
          ],
          [
            {
              "baggage": null,
              "cabinBaggage": null,
              "tripIndicator": 2,
              "airline": {
                "airlineCode": "KU",
                "airlineName": "Kuwait Airways Corporation",
                "flightNumber": "676",
                "fareClass": "V",
                "image": "airline_logos/KU.png"
              },
              "origin": {
                "airport": {
                  "airportCode": "DXB",
                  "airportName": "Dubai",
                  "terminal": "",
                  "cityCode": "DXB",
                  "cityName": "Dubai",
                  "countryCode": "AE",
                  "countryName": "United Arab Emirates"
                },
                "depTime": "2019-02-02T20:55:00"
              },
              "destination": {
                "airport": {
                  "airportCode": "KWI",
                  "airportName": "Kuwait Int'l",
                  "terminal": "",
                  "cityCode": "KWI",
                  "cityName": "Kuwait City",
                  "countryCode": "KW",
                  "countryName": "Kuwait"
                }
              },
              "duration": 110,
              "stopOver": false,
              "stopPoint": "",
              "stopPointArrivalTime": "2019-02-02T21:45:00",
              "stopPointDepartureTime": "2019-02-02T20:55:00",
              "remark": null,
              "isETicketEligible": true,
              "flightStatus": "Confirmed"
            },
            {
              "baggage": null,
              "cabinBaggage": null,
              "tripIndicator": 2,
              "airline": {
                "airlineCode": "KU",
                "airlineName": "Kuwait Airways Corporation",
                "flightNumber": "383",
                "fareClass": "V",
                "image": "airline_logos/KU.png"
              },
              "origin": {
                "airport": {
                  "airportCode": "KWI",
                  "airportName": "Kuwait Int'l",
                  "terminal": "",
                  "cityCode": "KWI",
                  "cityName": "Kuwait City",
                  "countryCode": "KW",
                  "countryName": "Kuwait"
                },
                "depTime": "2019-02-02T22:30:00"
              },
              "destination": {
                "airport": {
                  "airportCode": "DEL",
                  "airportName": "Indira Gandhi Airport",
                  "terminal": "",
                  "cityCode": "DEL",
                  "cityName": "Delhi",
                  "countryCode": "IN",
                  "countryName": "India"
                }
              },
              "duration": 230,
              "stopOver": false,
              "stopPoint": "",
              "stopPointArrivalTime": "2019-02-03T04:50:00",
              "stopPointDepartureTime": "2019-02-02T22:30:00",
              "remark": null,
              "isETicketEligible": true,
              "flightStatus": "Confirmed",
              "accumulatedDuration": 385
            }
          ]
        ],
        "fareDetails": [
          {
            "currency": "INR",
            "passengerType": 1,
            "passengerCount": 2,
            "baseFare": 26000,
            "tax": 8588,
            "fuelCharge": 0,
            "AdditionalTxnFeeOfrd": 0,
            "AdditionalTxnFeePub": 0,
            "PGCharge": 0
          },
          {
            "currency": "INR",
            "passengerType": 2,
            "passengerCount": 2,
            "baseFare": 19500,
            "tax": 8264,
            "fuelCharge": 0,
            "AdditionalTxnFeeOfrd": 0,
            "AdditionalTxnFeePub": 0,
            "PGCharge": 0
          },
          {
            "currency": "INR",
            "passengerType": 3,
            "passengerCount": 1,
            "baseFare": 2600,
            "tax": 1162,
            "fuelCharge": 0,
            "AdditionalTxnFeeOfrd": 0,
            "AdditionalTxnFeePub": 0,
            "PGCharge": 0
          }
        ],
        "fareRules": [
          {
            "airline": "KU",
            "fareCode": "V",
            "fareRuleDetail": "",
            "fareRestriction": ""
          },
          {
            "airline": "KU",
            "fareCode": "V",
            "fareRuleDetail": "",
            "fareRestriction": ""
          },
          {
            "airline": "KU",
            "fareCode": "V",
            "fareRuleDetail": "",
            "fareRestriction": ""
          },
          {
            "airline": "KU",
            "fareCode": "V",
            "fareRuleDetail": "",
            "fareRestriction": ""
          }
        ],
        "fare": {
          "currency": "INR",
          "baseFare": 48100,
          "tax": 18014,
          "fuelCharge": 0,
          "markupFare": 60896.82,
          "otherCharges": 0,
          "discount": 5217.18,
          "commissionEarned": 910,
          "optionalCharge": 0,
          "serviceFee": 0,
          "totalBaggageCharges": 0,
          "totalMealCharges": 0,
          "totalSeatCharges": 0,
          "enukeMarkup": 0,
          "enukeFare": 60896.82,
          "agencyMarKup": 0,
          "agencyFare": 60896.82,
          "offeredFare": 60896.82,
          "publishedFare": 66158.8,
          "websiteFare": null
        }
      },
    ]
  ]

  render() {
    let { prices, selectedPrice, datas } = this.state;
    // let dict = this.props.flightSearchParams;
    return (
      <View style={styles.container}>
        <Spinner
          style={{ flex: 1 }}
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={{ color: colors.colorWhite }}
        />
        <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} />
        <View style={{
          marginTop: Platform.OS == "ios" ? (getStatusBarHeight(false) + 5) : 0,
          flex: 1,
          backgroundColor: colors.colorWhite
        }}>
          <View style={styles.header}>
            <TouchableOpacity style={styles.headerBackButton} onPress={() => NavigationServices.goBack()}>
              <Image source={Images.imgback} style={{ width: 15, height: 15, resizeMode: "contain" }}></Image>
              <Text style={styles.backBtnText}> Back</Text>
            </TouchableOpacity>
            <View style={styles.headerContent}>
              <View style={styles.headerContentRow}>
                <Text style={styles.headerStnName}>New Delhi</Text>
                <Image source={Images.imgPlane} style={styles.headerContentIcon}></Image>
                <Text style={styles.headerStnName}>Mumbai</Text>
              </View>
              <View style={styles.headerContentRow}>
                <Text style={styles.headerContentRowText}>6 Dec</Text>
                <View style={styles.headerRowDevider}></View>
                <Text style={styles.headerContentRowText}>Economy</Text>
                <View style={styles.headerRowDevider}></View>
                <Text style={styles.headerContentRowText}>1 Traveller</Text>
              </View>
            </View>
            <TouchableOpacity style={styles.headerButton}>
              <Image source={Images.imgfilter} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
            </TouchableOpacity>
          </View>
          <View style={styles.tabs}>
            <TouchableOpacity style={styles.tab}>
              <Text style={styles.tabText}>Departure</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.tab, {
              borderLeftWidth: 1,
              borderRightWidth: 1,
              borderColor: "lightgray"
            }]}>
              <Text style={styles.tabText}>Duration</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.tab, {
              flexDirection: "row"
            }]}>
              <Text style={styles.tabText}>Price</Text>
              <Image source={Images.imgdownarrow} style={{
                width: 20, height: 15, resizeMode: "contain"
              }}></Image>
            </TouchableOpacity>
          </View>
          <View style={styles.mainView}>
            <ScrollView showsVerticalScrollIndicator={false}>
              {
                datas.map((data, index) => (
                  <View style={styles.card}>
                    <View style={styles.cardHeader}>
                      <View style={styles.cardHeaderCol1}>
                        <View style={styles.cardheaderFlightView}>
                          <Image source={require("../../../Assets/images/jet.png")}
                            style={styles.cardheaderFlightImg}></Image>
                          <Text style={[styles.price, { color: colors.colorBlack }]}>Jet Airways 304</Text>
                        </View>
                        <View style={styles.cardHeaderAmenties}>
                          <Image source={Images.imgfood} style={styles.flightAmenityImg}></Image>
                          <Image source={Images.imgflightinfo} style={styles.flightAmenityImg}></Image>
                          <Image source={Images.imgseat} style={styles.flightAmenityImg}></Image>
                        </View>
                      </View>
                      <View style={styles.cardHeaderCol2}>
                        <TouchableOpacity style={styles.bookButton}>
                          <Text style={styles.bookBtnText}>BOOK NOW</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View style={styles.cardContent}>
                      <View style={styles.cardContentCol1}>
                        <View style={{ width: "20%" }}>
                          <Text style={styles.estimate}>20:15</Text>
                          <Text style={styles.stnName}>DEL</Text>
                        </View>
                        <View style={{ width: "60%", alignItems: "center" }}>
                          <Text style={styles.estimate}>2h 15m</Text>
                          <View style={styles.dotView}>
                            <Image source={Images.imgbluedot} style={{ width: "10%" }}></Image>
                            <Dash style={{
                              width: "75%",
                              height: 1,
                            }} dashColor={colors.gray} dashLength={3}
                            />
                            {/* <Text style={{ width: "80%", overflow: "hidden" }}>-----------------------------</Text> */}
                            <Image source={Images.imgbluedot} style={{ width: "10%" }}></Image>
                          </View>
                          <Text style={styles.estimate}>Nonstop</Text>
                        </View>
                        <View style={{ width: "20%", alignItems: "flex-end" }}>
                          <Text style={styles.estimate}>20:15</Text>
                          <Text style={styles.stnName}>DEL</Text>
                        </View>
                      </View>
                      <View style={styles.cardContentCol2}>
                        <Text style={styles.priceText}>
                          <Image source={Images.imgrupeeblue}
                            style={styles.priceImg}></Image>
                          3237
                          </Text>
                      </View>
                    </View>
                  </View>
                ))
              }
              <View style={{ height: 200 }}></View>
            </ScrollView>

          </View>
          <View style={styles.pricesView}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              {
                prices.map((price, index) => (
                  <TouchableOpacity style={[styles.priceView, {
                    borderTopColor: index == selectedPrice ? colors.colorBlue : "transparent"
                  }]} onPress={() => this.setState({ selectedPrice: index })}>
                    <View style={styles.priceBtn}>
                      <Text style={[styles.price, {
                        color: index == selectedPrice ? colors.colorBlue : colors.colorBlack
                      }]}>Wed, 04 Dec</Text>
                      <Text style={[styles.price, {
                        marginTop: 5,
                        color: index == selectedPrice ? colors.colorBlue : colors.colorBlack,
                        flexDirection: "row",
                      }]}>
                        <Image source={index == selectedPrice ? Images.imgrupeeblue : Images.imgrupeebrown}
                          style={{
                            width: 12, height: 12, resizeMode: "contain"
                          }}></Image>
                        3237
                      </Text>
                    </View>
                    <View style={[styles.priceDevider, {
                      display: prices[index + 1] ? "flex" : "none"
                    }]}></View>
                  </TouchableOpacity>
                ))
              }
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    flightSearchParams: state.FlightsReducer.flightSearchParams,
    isLoading: state.isLoadingReducer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveFlightItem: (data) => dispatch(saveFlightItem(data)),
    searchFlightAction: (data) => dispatch(searchFlightAction(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FlightList)