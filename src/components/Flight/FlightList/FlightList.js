import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList,
  Image,
  TouchableHighlight,
  Dimensions,
  Platform,
  TouchableOpacity,
  Modal,
  DeviceEventEmitter
} from "react-native";

import Images from "../../../Helper Classes/Images";
import NavigationServices from '../../../Helper Classes/NavigationServices'
import APIManager from "../../../Helper Classes/APIManager/APIManagerUpdated";
import Spinner from "react-native-loading-spinner-overlay";
import FlightFilters from "../FlightFilters";
import { connect } from "react-redux";
import { styles } from "./Styles1";
import styles1 from "./Styles";
import moment from "moment";
import { baseUrl, imageBaseUrl } from "../../../Helper Classes/APIManager/APIConstants";
import { saveFlightItem } from "../../../Redux/Actions/Actions";
import { ifIphoneX, getStatusBarHeight } from "react-native-iphone-x-helper";

import { getFlightList } from "../../../Helper Classes/APIManager/ApiProvider";
import { searchFlightAction } from '../../../Redux/Actions';
import StringConstants from '../../../Helper Classes/StringConstants';
import { SafeAreaView } from "react-navigation";
import { colors } from "../../../Helper Classes/Colors";
import Header from "../../../Helper Classes/FlighListHeader";
import Dash from "react-native-dash";
import CardView from "react-native-cardview";
import Fonts from "../../../Helper Classes/Fonts";


class FlightList extends Component {
  constructor(props) {
    super(props);

    this.state = {

      data: [],
      flightListData: [],
      filteresFlightData: [],
      completeResponseData: [],
      loading: false,
      selectedIndex: 0,
      expendingIndex: -1,
      spinner: false,
      traceId: "",
      modelVisible: false,
      isFilterActive: false,

      //for selecting two way depart or arrival
      selectedOriginIndex: 0,
      selectedOriginFlightIndex: 0,
      selectedDestinationFlightIndex: 0,
      selectedFilterIndex: 0,

      //for soting of flights
      selectedSort: 3,
      sortArrIncr: false,
      sortDepIncr: false,
      sortPriceIncr: true,
      sortDurIncr: false,
      sortAirlineIncr: false,

      twoWayTotalPrice: null,
      destinationFlightItem: null,
      originFlightItem: null,
      uniqueAirlines: [],
      filterForReturn: false,
      filterForDep: false,
      filterDataForTwoWayDep: null,
      filterDataForTwoWayRet: null,
      isRoundTrip: false,
      // sortDep

      isInterNational: false,

      selectedFiltersDepart: {
        timeFilter: [],
        stopageFilter: [],
        refundable: false,
        freeMeal: false,
        airlines: [],
        timeFilter2: []
      },

      selectedFiltersArr: {
        timeFilter: [],
        stopageFilter: [],
        refundable: false,
        freeMeal: false,
        airlines: [],
        timeFilter2: []
      }

    };

    this.depAirlines = [];
    this.arrvialAirlines = [];
    this.internationalAirlines = [];
    flatListref = null;

    this._renderListdate = this._renderListdate.bind(this);
    // this._renderFlightListItem = this._renderFlightListItem.bind(this);
    this.getFlightsList = this.getFlightsList.bind(this);
    this.onFilterSelection = this.onFilterSelection.bind(this);
    this._renderFlightSingleWayListItem = this._renderFlightSingleWayListItem.bind(
      this
    );
    this._renderSagmentView = this._renderSagmentView.bind(this);
    this._renderCommonListItem = this._renderCommonListItem.bind(this);
    this._renderInterNationalFlightListItem = this._renderInterNationalFlightListItem.bind(
      this
    );
    this.getBackgroundColorForItem = this.getBackgroundColorForItem.bind(this);
    this._renderBookNowBtn = this._renderBookNowBtn.bind(this);
    this.updateFlightListFromApi = this.updateFlightListFromApi.bind(this)
  }
  // props data from flight search page
  // let dict = {
  //   origin: this.state.sourceDict.airport_code,
  //   destination: this.state.destinationDict.airport_code,
  //   departure_date: dateFormat(this.state.departDate,'yyyy-mm-dd'),
  //   return_date: (this.state.returndate == null) ? null:dateFormat(this.state.returndate,'yyyy-mm-dd'),
  //   FlightCabinClass: this.state.selectedClassIndex,
  //   ADT: this.state.numberOfAdults,
  //   CHD: this.state.numberOfChilds,
  //   INF: this.state.numberOfInfant,
  //   non_stop: this.state.isNonStop
  // };

  //------------------------- Components life cycle methods ---------------

  twoWaydummy = [
    [
      {
        "index": "OB1",
        "origin": "DEL",
        "destination": "BOM",
        "isLCC": true,
        "airlineCode": "6E",
        "airlineName": "Indigo",
        "isRefundable": true,
        "source": "TBO",
        "direction": 0,
        "tripType": "OB1",
        "flightSegment": [
          [
            {
              "baggage": "40 KG",
              "cabinBaggage": " 7 KG",
              "tripIndicator": 1,
              "airline": {
                "airlineCode": "6E",
                "airlineName": "Indigo",
                "flightNumber": "129",
                "fareClass": "O",
                "image": "airline_logos/6E.png"
              },
              "seatAvailable": 3,
              "origin": {
                "airport": {
                  "airportCode": "DEL",
                  "airportName": "Indira Gandhi Airport",
                  "terminal": "1",
                  "cityCode": "DEL",
                  "cityName": "Delhi",
                  "countryCode": "IN",
                  "countryName": "India"
                },
                "depTime": "2019-04-10T17:30:00"
              },
              "destination": {
                "airport": {
                  "airportCode": "BOM",
                  "airportName": "Mumbai",
                  "terminal": "",
                  "cityCode": "BOM",
                  "cityName": "Mumbai",
                  "countryCode": "IN",
                  "countryName": "India"
                }
              },
              "duration": 130,
              "stopOver": false,
              "stopPoint": "",
              "stopPointArrivalTime": "2019-04-10T19:40:00",
              "stopPointDepartureTime": "2019-04-10T17:30:00",
              "remark": null,
              "isETicketEligible": true,
              "flightStatus": "Confirmed"
            }
          ]
        ],
        "fareDetails": [
          {
            "currency": "INR",
            "passengerType": 1,
            "passengerCount": 1,
            "baseFare": 2819,
            "tax": 910,
            "fuelCharge": 400,
            "AdditionalTxnFeeOfrd": 0,
            "AdditionalTxnFeePub": 0,
            "PGCharge": 0
          }
        ],
        "fareRules": [
          {
            "airline": "6E",
            "fareCode": "O0IP",
            "fareRuleDetail": "",
            "fareRestriction": ""
          }
        ],
        "fare": {
          "currency": "INR",
          "baseFare": 2819,
          "tax": 910,
          "fuelCharge": 400,
          "markupFare": 3783.23,
          "otherCharges": 54.23000000000002,
          "discount": 0,
          "commissionEarned": 65.96,
          "optionalCharge": 0,
          "serviceFee": 0,
          "totalBaggageCharges": 0,
          "totalMealCharges": 0,
          "totalSeatCharges": 0,
          "enukeMarkup": 0,
          "enukeFare": 3783.23,
          "agencyMarKup": 0,
          "agencyFare": 3783.23,
          "offeredFare": 3783.23,
          "publishedFare": 3965,
          "websiteFare": null
        }
      },
    ],
    [
      {
        "index": "IB1",
        "origin": "BOM",
        "destination": "DEL",
        "isLCC": true,
        "airlineCode": "6E",
        "airlineName": "Indigo",
        "isRefundable": true,
        "source": "TBO",
        "direction": 1,
        "tripType": "IB1",
        "flightSegment": [
          [
            {
              "baggage": "46 KG",
              "cabinBaggage": " 7 KG",
              "tripIndicator": 1,
              "airline": {
                "airlineCode": "6E",
                "airlineName": "Indigo",
                "flightNumber": "176",
                "fareClass": "L",
                "image": "airline_logos/6E.png"
              },
              "seatAvailable": 2,
              "origin": {
                "airport": {
                  "airportCode": "BOM",
                  "airportName": "Mumbai",
                  "terminal": "1",
                  "cityCode": "BOM",
                  "cityName": "Mumbai",
                  "countryCode": "IN",
                  "countryName": "India"
                },
                "depTime": "2019-04-11T13:35:00"
              },
              "destination": {
                "airport": {
                  "airportCode": "DEL",
                  "airportName": "Indira Gandhi Airport",
                  "terminal": "1",
                  "cityCode": "DEL",
                  "cityName": "Delhi",
                  "countryCode": "IN",
                  "countryName": "India"
                }
              },
              "duration": 130,
              "stopOver": false,
              "stopPoint": "",
              "stopPointArrivalTime": "2019-04-11T15:45:00",
              "stopPointDepartureTime": "2019-04-11T13:35:00",
              "remark": null,
              "isETicketEligible": true,
              "flightStatus": "Confirmed"
            }
          ]
        ],
        "fareDetails": [
          {
            "currency": "INR",
            "passengerType": 1,
            "passengerCount": 1,
            "baseFare": 2280,
            "tax": 935,
            "fuelCharge": 400,
            "AdditionalTxnFeeOfrd": 0,
            "AdditionalTxnFeePub": 0,
            "PGCharge": 0
          }
        ],
        "fareRules": [
          {
            "airline": "6E",
            "fareCode": "L0IP",
            "fareRuleDetail": "",
            "fareRestriction": ""
          }
        ],
        "fare": {
          "currency": "INR",
          "baseFare": 2280,
          "tax": 935,
          "fuelCharge": 400,
          "markupFare": 3303.99,
          "otherCharges": 88.98999999999978,
          "discount": 0,
          "commissionEarned": 53.35,
          "optionalCharge": 0,
          "serviceFee": 0,
          "totalBaggageCharges": 0,
          "totalMealCharges": 0,
          "totalSeatCharges": 0,
          "enukeMarkup": 0,
          "enukeFare": 3303.99,
          "agencyMarKup": 0,
          "agencyFare": 3303.99,
          "offeredFare": 3303.99,
          "publishedFare": 3451,
          "websiteFare": null
        }
      }
    ]
  ]

  dummyData = [
    {
      "index": "OB53",
      "origin": "DEL",
      "destination": "BOM",
      "isLCC": true,
      "airlineCode": "SG",
      "airlineName": "SpiceJet",
      "isRefundable": true,
      "source": "TBO",
      "direction": 0,
      "tripType": "OB53",
      "flightSegment": [
        [
          {
            "baggage": "15 KG",
            "cabinBaggage": "7 KG",
            "tripIndicator": 1,
            "airline": {
              "airlineCode": "SG",
              "airlineName": "SpiceJet",
              "flightNumber": "169",
              "fareClass": "V",
              "image": "/var/www/travel_portal/htdocs/public/airline_logos/SG.png"
            },
            "seatAvailable": 15,
            "origin": {
              "airport": {
                "airportCode": "DEL",
                "airportName": "Indira Gandhi Airport",
                "terminal": "1D",
                "cityCode": "DEL",
                "cityName": "Delhi",
                "countryCode": "IN",
                "countryName": "India"
              },
              "depTime": "2019-04-05T19:45:00"
            },
            "destination": {
              "airport": {
                "airportCode": "BOM",
                "airportName": "Mumbai",
                "terminal": "1",
                "cityCode": "BOM",
                "cityName": "Mumbai",
                "countryCode": "IN",
                "countryName": "India"
              }
            },
            "duration": 140,
            "stopOver": false,
            "stopPoint": "",
            "stopPointArrivalTime": "2019-04-05T22:05:00",
            "stopPointDepartureTime": "2019-04-05T19:45:00",
            "remark": null,
            "isETicketEligible": true,
            "flightStatus": "Confirmed"
          }
        ]
      ],
      "fareDetails": [
        {
          "currency": "INR",
          "passengerType": 1,
          "passengerCount": 1,
          "baseFare": 2500,
          "tax": 528,
          "fuelCharge": 0,
          "AdditionalTxnFeeOfrd": 0,
          "AdditionalTxnFeePub": 0,
          "PGCharge": 0
        }
      ],
      "fareRules": [
        {
          "airline": "SG",
          "fareCode": "VSAVER",
          "fareRuleDetail": "",
          "fareRestriction": ""
        }
      ],
      "fare": {
        "currency": "INR",
        "baseFare": 2500,
        "tax": 528,
        "fuelCharge": 0,
        "markupFare": 2883.83,
        "otherCharges": 0,
        "discount": 144.17000000000007,
        "commissionEarned": 63.75,
        "optionalCharge": 0,
        "serviceFee": 0,
        "totalBaggageCharges": 0,
        "totalMealCharges": 0,
        "totalSeatCharges": 0,
        "enukeMarkup": 0,
        "enukeFare": 2883.83,
        "agencyMarKup": 0,
        "agencyFare": 2883.83,
        "offeredFare": 2883.83,
        "publishedFare": 3029.71,
        "websiteFare": null
      }
    },
    {
      "index": "OB54",
      "origin": "DEL",
      "destination": "BOM",
      "isLCC": true,
      "airlineCode": "SG",
      "airlineName": "SpiceJet",
      "isRefundable": true,
      "source": "TBO",
      "direction": 0,
      "tripType": "OB54",
      "flightSegment": [
        [
          {
            "baggage": "15 KG",
            "cabinBaggage": "7 KG",
            "tripIndicator": 1,
            "airline": {
              "airlineCode": "SG",
              "airlineName": "SpiceJet",
              "flightNumber": "159",
              "fareClass": "V",
              "image": "/var/www/travel_portal/htdocs/public/airline_logos/SG.png"
            },
            "seatAvailable": 15,
            "origin": {
              "airport": {
                "airportCode": "DEL",
                "airportName": "Indira Gandhi Airport",
                "terminal": "1D",
                "cityCode": "DEL",
                "cityName": "Delhi",
                "countryCode": "IN",
                "countryName": "India"
              },
              "depTime": "2019-04-05T21:35:00"
            },
            "destination": {
              "airport": {
                "airportCode": "BOM",
                "airportName": "Mumbai",
                "terminal": "1",
                "cityCode": "BOM",
                "cityName": "Mumbai",
                "countryCode": "IN",
                "countryName": "India"
              }
            },
            "duration": 144,
            "stopOver": false,
            "stopPoint": "",
            "stopPointArrivalTime": "2019-04-05T23:59:00",
            "stopPointDepartureTime": "2019-04-05T21:35:00",
            "remark": null,
            "isETicketEligible": true,
            "flightStatus": "Confirmed"
          },
          {
            "baggage": "15 KG",
            "cabinBaggage": "7 KG",
            "tripIndicator": 1,
            "airline": {
              "airlineCode": "SG",
              "airlineName": "SpiceJet",
              "flightNumber": "159",
              "fareClass": "V",
              "image": "/var/www/travel_portal/htdocs/public/airline_logos/SG.png"
            },
            "seatAvailable": 15,
            "origin": {
              "airport": {
                "airportCode": "DEL",
                "airportName": "Indira Gandhi Airport",
                "terminal": "1D",
                "cityCode": "DEL",
                "cityName": "Delhi",
                "countryCode": "IN",
                "countryName": "India"
              },
              "depTime": "2019-04-05T21:35:00"
            },
            "destination": {
              "airport": {
                "airportCode": "BOM",
                "airportName": "Mumbai",
                "terminal": "1",
                "cityCode": "BOM",
                "cityName": "Mumbai",
                "countryCode": "IN",
                "countryName": "India"
              }
            },
            "duration": 144,
            "stopOver": false,
            "stopPoint": "",
            "stopPointArrivalTime": "2019-04-05T23:59:00",
            "stopPointDepartureTime": "2019-04-05T21:35:00",
            "remark": null,
            "isETicketEligible": true,
            "flightStatus": "Confirmed"
          },
          {
            "baggage": "15 KG",
            "cabinBaggage": "7 KG",
            "tripIndicator": 1,
            "airline": {
              "airlineCode": "SG",
              "airlineName": "SpiceJet",
              "flightNumber": "159",
              "fareClass": "V",
              "image": "/var/www/travel_portal/htdocs/public/airline_logos/SG.png"
            },
            "seatAvailable": 15,
            "origin": {
              "airport": {
                "airportCode": "DEL",
                "airportName": "Indira Gandhi Airport",
                "terminal": "1D",
                "cityCode": "DEL",
                "cityName": "Delhi",
                "countryCode": "IN",
                "countryName": "India"
              },
              "depTime": "2019-04-05T21:35:00"
            },
            "destination": {
              "airport": {
                "airportCode": "BOM",
                "airportName": "Mumbai",
                "terminal": "1",
                "cityCode": "BOM",
                "cityName": "Mumbai",
                "countryCode": "IN",
                "countryName": "India"
              }
            },
            "duration": 144,
            "stopOver": false,
            "stopPoint": "",
            "stopPointArrivalTime": "2019-04-05T23:59:00",
            "stopPointDepartureTime": "2019-04-05T21:35:00",
            "remark": null,
            "isETicketEligible": true,
            "flightStatus": "Confirmed"
          }
        ]
      ],
      "fareDetails": [
        {
          "currency": "INR",
          "passengerType": 1,
          "passengerCount": 1,
          "baseFare": 2500,
          "tax": 528,
          "fuelCharge": 0,
          "AdditionalTxnFeeOfrd": 0,
          "AdditionalTxnFeePub": 0,
          "PGCharge": 0
        }
      ],
      "fareRules": [
        {
          "airline": "SG",
          "fareCode": "VSAVER",
          "fareRuleDetail": "",
          "fareRestriction": ""
        }
      ],
      "fare": {
        "currency": "INR",
        "baseFare": 2500,
        "tax": 528,
        "fuelCharge": 0,
        "markupFare": 2883.83,
        "otherCharges": 0,
        "discount": 144.17000000000007,
        "commissionEarned": 63.75,
        "optionalCharge": 0,
        "serviceFee": 0,
        "totalBaggageCharges": 0,
        "totalMealCharges": 0,
        "totalSeatCharges": 0,
        "enukeMarkup": 0,
        "enukeFare": 2883.83,
        "agencyMarKup": 0,
        "agencyFare": 2883.83,
        "offeredFare": 2883.83,
        "publishedFare": 3029.71,
        "websiteFare": null
      }
    }]

  internationalDummyData = [
    [
      {
        "index": "OB84",
        "origin": "KWI",
        "destination": "DXB",
        "isLCC": true,
        "airlineCode": "KU",
        "airlineName": "Kuwait Airways Corporation",
        "isRefundable": true,
        "source": "TBO",
        "direction": 0,
        "tripType": "OB84",
        "flightSegment": [
          [
            {
              "baggage": null,
              "cabinBaggage": null,
              "tripIndicator": 1,
              "airline": {
                "airlineCode": "KU",
                "airlineName": "Kuwait Airways Corporation",
                "flightNumber": "384",
                "fareClass": "V",
                "image": "airline_logos/KU.png"
              },
              "origin": {
                "airport": {
                  "airportCode": "DEL",
                  "airportName": "Indira Gandhi Airport",
                  "terminal": "",
                  "cityCode": "DEL",
                  "cityName": "Delhi",
                  "countryCode": "IN",
                  "countryName": "India"
                },
                "depTime": "2019-01-28T05:50:00"
              },
              "destination": {
                "airport": {
                  "airportCode": "KWI",
                  "airportName": "Kuwait Int'l",
                  "terminal": "",
                  "cityCode": "KWI",
                  "cityName": "Kuwait City",
                  "countryCode": "KW",
                  "countryName": "Kuwait"
                }
              },
              "duration": 280,
              "stopOver": false,
              "stopPoint": "",
              "stopPointArrivalTime": "2019-01-28T08:00:00",
              "stopPointDepartureTime": "2019-01-28T05:50:00",
              "remark": null,
              "isETicketEligible": true,
              "flightStatus": "Confirmed"
            },
            {
              "baggage": null,
              "cabinBaggage": null,
              "tripIndicator": 1,
              "airline": {
                "airlineCode": "KU",
                "airlineName": "Kuwait Airways Corporation",
                "flightNumber": "673",
                "fareClass": "V",
                "image": "airline_logos/KU.png"
              },
              "origin": {
                "airport": {
                  "airportCode": "KWI",
                  "airportName": "Kuwait Int'l",
                  "terminal": "",
                  "cityCode": "KWI",
                  "cityName": "Kuwait City",
                  "countryCode": "KW",
                  "countryName": "Kuwait"
                },
                "depTime": "2019-01-28T15:40:00"
              },
              "destination": {
                "airport": {
                  "airportCode": "DXB",
                  "airportName": "Dubai",
                  "terminal": "",
                  "cityCode": "DXB",
                  "cityName": "Dubai",
                  "countryCode": "AE",
                  "countryName": "United Arab Emirates"
                }
              },
              "duration": 115,
              "stopOver": false,
              "stopPoint": "",
              "stopPointArrivalTime": "2019-01-28T18:35:00",
              "stopPointDepartureTime": "2019-01-28T15:40:00",
              "remark": null,
              "isETicketEligible": true,
              "flightStatus": "Confirmed",
              "accumulatedDuration": 855
            }
          ],
          [
            {
              "baggage": null,
              "cabinBaggage": null,
              "tripIndicator": 2,
              "airline": {
                "airlineCode": "KU",
                "airlineName": "Kuwait Airways Corporation",
                "flightNumber": "676",
                "fareClass": "V",
                "image": "airline_logos/KU.png"
              },
              "origin": {
                "airport": {
                  "airportCode": "DXB",
                  "airportName": "Dubai",
                  "terminal": "",
                  "cityCode": "DXB",
                  "cityName": "Dubai",
                  "countryCode": "AE",
                  "countryName": "United Arab Emirates"
                },
                "depTime": "2019-02-02T20:55:00"
              },
              "destination": {
                "airport": {
                  "airportCode": "KWI",
                  "airportName": "Kuwait Int'l",
                  "terminal": "",
                  "cityCode": "KWI",
                  "cityName": "Kuwait City",
                  "countryCode": "KW",
                  "countryName": "Kuwait"
                }
              },
              "duration": 110,
              "stopOver": false,
              "stopPoint": "",
              "stopPointArrivalTime": "2019-02-02T21:45:00",
              "stopPointDepartureTime": "2019-02-02T20:55:00",
              "remark": null,
              "isETicketEligible": true,
              "flightStatus": "Confirmed"
            },
            {
              "baggage": null,
              "cabinBaggage": null,
              "tripIndicator": 2,
              "airline": {
                "airlineCode": "KU",
                "airlineName": "Kuwait Airways Corporation",
                "flightNumber": "383",
                "fareClass": "V",
                "image": "airline_logos/KU.png"
              },
              "origin": {
                "airport": {
                  "airportCode": "KWI",
                  "airportName": "Kuwait Int'l",
                  "terminal": "",
                  "cityCode": "KWI",
                  "cityName": "Kuwait City",
                  "countryCode": "KW",
                  "countryName": "Kuwait"
                },
                "depTime": "2019-02-02T22:30:00"
              },
              "destination": {
                "airport": {
                  "airportCode": "DEL",
                  "airportName": "Indira Gandhi Airport",
                  "terminal": "",
                  "cityCode": "DEL",
                  "cityName": "Delhi",
                  "countryCode": "IN",
                  "countryName": "India"
                }
              },
              "duration": 230,
              "stopOver": false,
              "stopPoint": "",
              "stopPointArrivalTime": "2019-02-03T04:50:00",
              "stopPointDepartureTime": "2019-02-02T22:30:00",
              "remark": null,
              "isETicketEligible": true,
              "flightStatus": "Confirmed",
              "accumulatedDuration": 385
            }
          ]
        ],
        "fareDetails": [
          {
            "currency": "INR",
            "passengerType": 1,
            "passengerCount": 2,
            "baseFare": 26000,
            "tax": 8588,
            "fuelCharge": 0,
            "AdditionalTxnFeeOfrd": 0,
            "AdditionalTxnFeePub": 0,
            "PGCharge": 0
          },
          {
            "currency": "INR",
            "passengerType": 2,
            "passengerCount": 2,
            "baseFare": 19500,
            "tax": 8264,
            "fuelCharge": 0,
            "AdditionalTxnFeeOfrd": 0,
            "AdditionalTxnFeePub": 0,
            "PGCharge": 0
          },
          {
            "currency": "INR",
            "passengerType": 3,
            "passengerCount": 1,
            "baseFare": 2600,
            "tax": 1162,
            "fuelCharge": 0,
            "AdditionalTxnFeeOfrd": 0,
            "AdditionalTxnFeePub": 0,
            "PGCharge": 0
          }
        ],
        "fareRules": [
          {
            "airline": "KU",
            "fareCode": "V",
            "fareRuleDetail": "",
            "fareRestriction": ""
          },
          {
            "airline": "KU",
            "fareCode": "V",
            "fareRuleDetail": "",
            "fareRestriction": ""
          },
          {
            "airline": "KU",
            "fareCode": "V",
            "fareRuleDetail": "",
            "fareRestriction": ""
          },
          {
            "airline": "KU",
            "fareCode": "V",
            "fareRuleDetail": "",
            "fareRestriction": ""
          }
        ],
        "fare": {
          "currency": "INR",
          "baseFare": 48100,
          "tax": 18014,
          "fuelCharge": 0,
          "markupFare": 60896.82,
          "otherCharges": 0,
          "discount": 5217.18,
          "commissionEarned": 910,
          "optionalCharge": 0,
          "serviceFee": 0,
          "totalBaggageCharges": 0,
          "totalMealCharges": 0,
          "totalSeatCharges": 0,
          "enukeMarkup": 0,
          "enukeFare": 60896.82,
          "agencyMarKup": 0,
          "agencyFare": 60896.82,
          "offeredFare": 60896.82,
          "publishedFare": 66158.8,
          "websiteFare": null
        }
      },
    ]
  ]

  componentDidMount() {
    this.getFlightsList();
    //this.getFlightsCalendar();
    DeviceEventEmitter.addListener(StringConstants.FLIGH_LIST_EVENT, this.updateFlightListFromApi)

  }

  updateFlightListFromApi(response) {
    console.log("getFlightListSaga");
    this.setState({ traceId: response.others.traceId });
    if (response.results.length == 2) {
      this.setState({
        isRoundTrip: true,
        completeResponseData: response.results,
        flightListData: response.results[0],
        loading: !this.state.loading,
        originFlightItem: response.results[0][0],
        destinationFlightItem: response.results[1][0],
        twoWayTotalPrice: response.results[0][0].fare.offeredFare + response.results[1][0].fare.offeredFare
      }, () => { this.setUniqueAirlines() });
    } else {
      if (response.results[0][0].flightSegment.length === 2) {
        this.setState({
          isInterNational: true
        })
      }
      this.setState({
        completeResponseData: response.results,
        flightListData: response.results[0],
        loading: !this.state.loading,
      }, () => { this.setUniqueAirlines() });
    }
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.FLIGH_LIST_EVENT, this.updateFlightListFromApi)
  }

  //------------ On Filter Selection -------------

  minutesOfDay(m) {
    return m.minutes() + m.hours() * 60;
  }

  calulateTwoWayPrice() {
    let item1 = this.state.originFlightItem;
    let item2 = this.state.destinationFlightItem;
    let fare = item1.fare.offeredFare + item2.fare.offeredFare;
    fare = Math.ceil(fare);
    this.setState({ twoWayTotalPrice: fare });
  }

  scrollToitemindex = () => {
    if (this.state.selectedOriginIndex == 1) {
      if (this.state.selectedDestinationFlightIndex !== -1) {
        this.flatListref.scrollToIndex({ animated: true, index: this.state.selectedDestinationFlightIndex });
      }
    } else {
      if (this.state.selectedOriginFlightIndex !== -1) {
        this.flatListref.scrollToIndex({ animated: true, index: this.state.selectedOriginFlightIndex })
      }
    }
    dummy = [{
      "index": "OB52",
      "origin": "DEL",
      "destination": "BOM",
      "isLCC": true,
      "airlineCode": "SG",
      "airlineName": "SpiceJet",
      "isRefundable": true,
      "source": "TBO",
      "direction": 0,
      "tripType": "OB52",
      "flightSegment": [
        [
          {
            "baggage": "15 KG",
            "cabinBaggage": "7 KG",
            "tripIndicator": 1,
            "airline": {
              "airlineCode": "SG",
              "airlineName": "SpiceJet",
              "flightNumber": "9169",
              "fareClass": "V"
            },
            "seatAvailable": 5,
            "origin": {
              "airport": {
                "airportCode": "DEL",
                "airportName": "Indira Gandhi Airport",
                "terminal": "1D",
                "cityCode": "DEL",
                "cityName": "Delhi",
                "countryCode": "IN",
                "countryName": "India"
              },
              "depTime": "2019-04-05T12:00:00"
            },
            "destination": {
              "airport": {
                "airportCode": "BOM",
                "airportName": "Mumbai",
                "terminal": "1",
                "cityCode": "BOM",
                "cityName": "Mumbai",
                "countryCode": "IN",
                "countryName": "India"
              }
            },
            "duration": 95,
            "stopOver": false,
            "stopPoint": "",
            "stopPointArrivalTime": "2019-04-05T13:35:00",
            "stopPointDepartureTime": "2019-04-05T12:00:00",
            "remark": null,
            "isETicketEligible": true,
            "flightStatus": "Confirmed"
          }
        ]
      ],
      "fareDetails": [
        {
          "currency": "INR",
          "passengerType": 1,
          "passengerCount": 1,
          "baseFare": 2500,
          "tax": 528,
          "fuelCharge": 0,
          "AdditionalTxnFeeOfrd": 0,
          "AdditionalTxnFeePub": 0,
          "PGCharge": 0
        }
      ],
      "fareRules": [
        {
          "airline": "SG",
          "fareCode": "VSAVER",
          "fareRuleDetail": "",
          "fareRestriction": ""
        }
      ],
      "fare": {
        "currency": "INR",
        "baseFare": 2500,
        "tax": 528,
        "fuelCharge": 0,
        "markupFare": 2883.8,
        "otherCharges": 0,
        "discount": 144.19999999999982,
        "commissionEarned": 63.75,
        "optionalCharge": 0,
        "serviceFee": 0,
        "totalBaggageCharges": 0,
        "totalMealCharges": 0,
        "totalSeatCharges": 0,
        "enukeMarkup": 0,
        "enukeFare": 2883.8,
        "agencyMarKup": 0,
        "agencyFare": 2883.8,
        "offeredFare": 2883.8,
        "publishedFare": 3029.68,
        "websiteFare": null
      }
    },]
    //-------------- API Calling Methods --------------------
  }

  onFilterSelection(dict) {
    console.log("on filter seletion");
    if (this.state.isRoundTrip) {
      if (this.state.selectedOriginIndex == 0) {
        this.setState({
          modelVisible: false,
          selectedFiltersDepart: dict
        });
        let timeFilters = dict.timeFilter;
        let stopageFilter = dict.stopageFilter;
        let isRefundable = dict.refundable;
        let isFreeMeal = dict.freeMeal;
        let airLines = dict.airlines;
        let filterDate = this.state.completeResponseData[0][0].flightSegment[0][0].stopPointDepartureTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        console.log(dict);
        let filterData = this.state.completeResponseData[0];

        if (timeFilters.length > 0 && timeFilters.length < 4) {
          timeFilters.forEach((item, index) => {
            let timeArr = String(item).split(" - ");
            //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
            let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
            let finaltime2 = filterDate;
            if (timeArr[1] == "24:00") {
              finaltime2 = finaltime2 + "T" + "23:59" + ":59";
            } else {
              finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
            }
            console.log("time is  " + finaltime1);
            let deptTime1 = finaltime1;
            let depTime2 = finaltime2;
            // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
            let arr = this.state.completeResponseData[0].filter(data => {
              let flightDeptTime = data.flightSegment[0][0].stopPointDepartureTime;
              if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
                console.log("true")
                return true;
              } else {
                return false;
              }
            });
            console.log(arr);
            if (index == 0) {
              filterData = arr;
            } else {
              console.log("arr length in second time " + arr.length);
              filterData = filterData.concat(arr);
            }
            console.log("filter data after time filter " + filterData.length);
          });
        }
        if (stopageFilter.length > 0 && stopageFilter.length < 3) {
          stopageFilter.forEach((item, index) => {
            let arr = filterData.filter(data => {
              if (item == 0) {
                return data.flightSegment[0].length == 1;
              } else if (item == 1) {
                return data.flightSegment[0].length == 2;
              } else {
                return data.flightSegment[0].length > 2;
              }
            });
            if (index == 0) {
              filterData = arr;
            } else {
              filterData = filterData.concat(arr);
            }
          });
          console.log("filter data after stopage filter " + filterData.length);
        }
        if (isRefundable) {
          filterData = filterData.filter(data => {
            return data.isRefundable == true;
          });
          console.log("filter data after refundable filter " + filterData.length);
        }
        if (isFreeMeal) {
          filterData = filterData.filter(data => {
            return data.fare.totalMealCharges == 0;
          });
          console.log("filter data after free meal filter " + filterData.length);
        }
        if (airLines.length > 0) {
          let arr2 = [];
          airLines.forEach((airlineIndex, index) => {
            let arr = filterData.filter((flightItem) => {
              if (flightItem.flightSegment[0][0].airline.airlineCode === this.depAirlines[airlineIndex].airlineCode) {
                return true;
              } else {
                return false;
              }
            })
            if (index == 0) {
              arr2 = arr;
            } else {
              console.log("arr length in second time " + arr.length);
              arr2 = arr2.concat(arr);
            }
          })
          filterData = arr2;
        }
        console.log("total data in list " + this.state.flightListData.length);
        console.log(timeFilters.length);
        console.log(filterData);
        console.log(filterData.length);
        if (timeFilters.length == 0 && stopageFilter.length == 0 && !isRefundable && !isFreeMeal && airLines.length == 0) {
          if (!this.state.filterForReturn) {
            this.setState({
              isFilterActive: false,
              filterForDep: false,
              loading: !this.state.loading
            });
          } else {
            this.setState({
              filterForDep: false,
              loading: !this.state.loading
            });
          }
        } else {
          this.setState({
            filterDataForTwoWayDep: filterData,
            filteresFlightData: filterData,
            selectedOriginFlightIndex: filterData.length > 0 ? 0 : -1,
            isFilterActive: true,
            filterForDep: true,
            loading: !this.state.loading
          });
        }
      }
      else {
        this.setState({
          modelVisible: false,
          selectedFiltersArr: dict
        });
        let timeFilters1 = dict.timeFilter;
        let stopageFilter = dict.stopageFilter;
        let isRefundable = dict.refundable;
        let isFreeMeal = dict.freeMeal;
        let airLines = dict.airlines;
        const lastItem = this.state.completeResponseData[1][0].flightSegment[0].length - 1;
        let filterDate = this.state.completeResponseData[1][0].flightSegment[0][0].stopPointDepartureTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        console.log(dict);
        let filterData = this.state.completeResponseData[1];
        if (timeFilters1.length > 0 && timeFilters1.length < 4) {
          timeFilters1.forEach((item, index) => {
            let timeArr = String(item).split(" - ");
            //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
            let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
            let finaltime2 = filterDate;
            if (timeArr[1] == "24:00") {
              finaltime2 = finaltime2 + "T" + "23:59" + ":59";
            } else {
              finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
            }
            console.log("time is  " + finaltime1);
            let deptTime1 = finaltime1;
            let depTime2 = finaltime2;
            // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
            let arr = this.state.completeResponseData[1].filter(data => {
              let lastItem = data.flightSegment[0].length - 1;
              let flightDeptTime = data.flightSegment[0][lastItem].stopPointDepartureTime;
              if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
                console.log("true")
                return true;
              } else {
                return false;
              }
            });
            console.log(arr);
            if (index == 0) {
              filterData = arr;
            } else {
              console.log("arr length in second time " + arr.length);
              filterData = filterData.concat(arr);
            }
            console.log("filter data after time filter " + filterData.length);
          });
        }
        if (stopageFilter.length > 0 && stopageFilter.length < 3) {
          stopageFilter.forEach((item, index) => {
            let arr = filterData.filter(data => {
              if (item == 0) {
                return data.flightSegment[0].length == 1;
              } else if (item == 1) {
                return data.flightSegment[0].length == 2;
              } else {
                return data.flightSegment[0].length > 2;
              }
            });
            if (index == 0) {
              filterData = arr;
            } else {
              filterData = filterData.concat(arr);
            }
          });
          console.log("filter data after stopage filter " + filterData.length);
        }
        if (isRefundable) {
          filterData = filterData.filter(data => {
            return data.isRefundable == true;
          });
          console.log("filter data after refundable filter " + filterData.length);
        }
        if (isFreeMeal) {
          filterData = filterData.filter(data => {
            return data.fare.totalMealCharges == 0;
          });
          console.log("filter data after free meal filter " + filterData.length);
        }
        if (airLines.length > 0) {
          let arr2 = [];
          airLines.forEach((airlineIndex, index) => {
            let arr = filterData.filter((flightItem) => {
              if (flightItem.flightSegment[0][lastItem].airline.airlineCode === this.arrvialAirlines[airlineIndex].airlineCode) {
                return true;
              } else {
                return false;
              }
            })
            if (index == 0) {
              arr2 = arr
            } else {
              arr2 = arr2.concat(arr)
            }
            // if (index == 0) {
            //   filterData = arr;
            // } else {
            //   console.log("arr length in second time " + arr.length);
            //   filterData = filterData.concat(arr);
            // }
          })
          filterData = arr2;
        }
        console.log("total data in list " + this.state.flightListData.length);
        console.log(timeFilters1.length);
        console.log(filterData);
        console.log(filterData.length);
        if (timeFilters1.length == 0 && stopageFilter.length == 0 && !isRefundable && !isFreeMeal && airLines.length == 0) {
          if (!this.state.filterForDep) {
            this.setState({
              isFilterActive: false,
              filterForReturn: false,
              loading: !this.state.loading
            });
          } else {
            this.setState({
              filterForReturn: false,
              loading: !this.state.loading
            });
          }
        } else {
          this.setState({
            filteresFlightData: filterData,
            filterDataForTwoWayRet: filterData,
            isFilterActive: true,
            filterForReturn: true,
            selectedDestinationFlightIndex: filterData.length > 0 ? 0 : -1,
            loading: !this.state.loading
          });
        }
      }
    }
    else if (this.state.isInterNational) {
      this.setState({
        modelVisible: false,
        selectedFiltersDepart: dict
      });
      let timeFilters = dict.timeFilter;
      let timeFilters2 = dict.timeFilter2;
      let stopageFilter = dict.stopageFilter;
      let isRefundable = dict.refundable;
      let isFreeMeal = dict.freeMeal;
      let airLines = dict.airlines;
      console.log(dict);
      let filterData = this.state.completeResponseData[0];
      if (((timeFilters.length > 0 && timeFilters.length < 4)) && (timeFilters2.length > 0 && timeFilters2.length < 4)) {
        let filterDate = this.state.completeResponseData[0][0].flightSegment[0][0].stopPointDepartureTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        timeFilters.forEach((item, index) => {
          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);
          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = this.state.completeResponseData[0].filter(data => {
            let flightDeptTime = data.flightSegment[0][0].stopPointDepartureTime;
            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }
          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }
          console.log("filter data after time filter " + filterData.length);
        });
        filterDate = this.state.completeResponseData[0][0].flightSegment[1][0].stopPointDepartureTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        timeFilters2.forEach((item, index) => {
          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);
          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = filterData.filter(data => {
            let flightDeptTime = data.flightSegment[1][0].stopPointDepartureTime;
            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }
          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }
          console.log("filter data after time filter " + filterData.length);
        });
      }
      else if (timeFilters.length > 0 && timeFilters.length < 4) {
        let filterDate = this.state.completeResponseData[0][0].flightSegment[0][0].stopPointDepartureTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        timeFilters.forEach((item, index) => {
          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);
          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = filterData.filter(data => {
            let flightDeptTime = data.flightSegment[0][0].stopPointDepartureTime;
            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }
          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }
          console.log("filter data after time filter " + filterData.length);
        });
      }
      else if (timeFilters2.length > 0 && timeFilters2.length < 4) {
        let filterDate = this.state.completeResponseData[0][0].flightSegment[1][0].stopPointDepartureTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        timeFilters2.forEach((item, index) => {
          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);
          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = this.state.completeResponseData[0].filter(data => {
            let flightDeptTime = data.flightSegment[1][0].stopPointDepartureTime;
            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }
          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }
          console.log("filter data after time filter " + filterData.length);
        });
      }
      if (stopageFilter.length > 0 && stopageFilter.length < 3) {
        stopageFilter.forEach((item, index) => {
          let arr = filterData.filter(data => {
            if (item == 0) {
              if ((data.flightSegment[0].length == 1) && (data.flightSegment[1].length == 1)) {
                return true;
              }
            } else if (item == 1) {
              if ((data.flightSegment[0].length == 2) && (data.flightSegment[1].length == 2)) {
                return true;
              }
            } else {
              if ((data.flightSegment[0].length > 2) && (data.flightSegment[1].length > 2)) {
                return true;
              }
            }
          });
          if (index == 0) {
            filterData = arr;
          } else {
            filterData = filterData.concat(arr);
          }
        });
        console.log("filter data after stopage filter " + filterData.length);
      }
      if (isRefundable) {
        filterData = filterData.filter(data => {
          return data.isRefundable == true;
        });
        console.log("filter data after refundable filter " + filterData.length);
      }
      if (isFreeMeal) {
        filterData = filterData.filter(data => {
          return data.fare.totalMealCharges == 0;
        });
        console.log("filter data after free meal filter " + filterData.length);
      }
      if (airLines.length > 0) {
        let arr2 = [];
        airLines.forEach((airlineIndex, index) => {
          let arr = filterData.filter((flightItem) => {
            if (flightItem.flightSegment[0][0].airline.airlineCode === this.internationalAirlines[airlineIndex].airlineCode) {
              return true;
            } else {
              return false;
            }
          })
          if (index == 0) {
            arr2 = arr
          } else {
            arr2 = arr2.concat(arr)
          }
          // if (index == 0) {
          //   filterData = arr;
          // } else {
          //   console.log("arr length in second time " + arr.length);
          //   filterData = filterData.concat(arr);
          // }
        })
        filterData = arr2;
      }
      console.log("total data in list " + this.state.flightListData.length);
      console.log(timeFilters.length);
      console.log(filterData);
      console.log(filterData.length);
      if (timeFilters.length == 0 && timeFilters2.length == 0 && stopageFilter.length == 0 && !isRefundable && !isFreeMeal && airLines.length == 0) {
        this.setState({
          isFilterActive: false,
          loading: !this.state.loading
        });
      } else {
        this.setState({
          filteresFlightData: filterData,
          isFilterActive: true,
          selectedDestinationFlightIndex: filterData.length > 0 ? 0 : -1,
          loading: !this.state.loading
        });
      }
    }
    else {
      this.setState({
        modelVisible: false,
        selectedFiltersDepart: dict
      });

      let timeFilters = dict.timeFilter;
      let stopageFilter = dict.stopageFilter;
      let isRefundable = dict.refundable;
      let isFreeMeal = dict.freeMeal;
      let airLines = dict.airlines;
      console.log("FLIGHT_LIST_DATA:" + JSON.stringify(this.state.flightListData[0]))
      let filterDate = this.state.flightListData[0].flightSegment[0][0].stopPointDepartureTime;
      if (filterDate) {
        filterDate = filterDate.split("T")[0];
      }
      console.log(dict);
      let filterData = this.state.flightListData;

      if (timeFilters.length > 0 && timeFilters.length < 4) {
        timeFilters.forEach((item, index) => {
          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);

          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = this.state.flightListData.filter(data => {
            let flightDeptTime = data.flightSegment[0][0].stopPointDepartureTime;
            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }
          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }
          console.log("filter data after time filter " + filterData.length);
        });
      }
      if (stopageFilter.length > 0 && stopageFilter.length < 3) {
        let filterArr = [];
        stopageFilter.forEach((item, index) => {
          let arr = filterData.filter(data => {
            if (item == 0) {
              return data.flightSegment[0].length == 1;
            } else if (item == 1) {
              return data.flightSegment[0].length == 2;
            } else {
              return data.flightSegment[0].length > 2;
            }
          });

          if (index == 0) {
            filterArr = arr;
          } else {
            filterArr = filterArr.concat(arr);
          }
        });
        filterData = filterArr;
        console.log("filter data after stopage filter " + filterData.length);
      }
      if (isRefundable) {
        filterData = filterData.filter(data => {
          return data.isRefundable == true;
        });
        console.log("filter data after refundable filter " + filterData.length);
      }
      if (isFreeMeal) {
        filterData = filterData.filter(data => {
          return data.fare.totalMealCharges == 0;
        });
        console.log("filter data after free meal filter " + filterData.length);
      }
      if (airLines.length > 0) {
        let arr2 = []
        airLines.forEach((airlineIndex, index) => {
          let arr = filterData.filter((flightItem) => {
            if (flightItem.flightSegment[0][0].airline.airlineCode === this.depAirlines[airlineIndex].airlineCode) {
              return true;
            } else {
              return false;
            }
          })
          if (index == 0) {
            arr2 = arr
          } else {
            arr2 = arr2.concat(arr)
          }
        })
        filterData = arr2;
      }
      console.log("total data in list " + this.state.flightListData.length);
      console.log(timeFilters.length);
      console.clear();
      console.log(JSON.stringify(filterData));
      console.log(filterData.length);
      if (timeFilters.length == 0 && stopageFilter.length == 0 && !isRefundable && !isFreeMeal && airLines.length == 0) {
        this.setState({
          isFilterActive: false,
          loading: !this.state.loading
        });
      } else {
        this.setState({
          filteresFlightData: filterData,
          isFilterActive: true,
          selectedOriginFlightIndex: 0,
          loading: !this.state.loading
        });
      }
    }
  }

  setUniqueAirlines() {
    let exp;
    const map = new Map();
    const map2 = new Map();
    console.log("set uniquee")
    if (this.props.flightSearchParams.return_date == null) {
      //for single way airlines filter
      exp = 1;
    } else if (this.props.flightSearchParams.return_date != null && this.state.completeResponseData.length == 2) {
      //for two way domestic airlines filter
      exp = 2;
    } else {
      //for Internatinal flights
      exp = 3;
    }
    console.log(exp)
    switch (exp) {
      case 1: {
        console.log("in case first");
        const response = this.state.completeResponseData[0];
        for (const item of response) {
          if (!map.has(item.flightSegment[0][0].airline.airlineCode)) {
            map.set(item.flightSegment[0][0].airline.airlineCode, true)
            this.depAirlines.push({
              airlineCode: item.flightSegment[0][0].airline.airlineCode,
              airlineName: item.flightSegment[0][0].airline.airlineName,
              airlineImage: item.flightSegment[0][0].airline.image
            })
          }
        }
      }
        break;
      case 2:
        {
          const departData = this.state.completeResponseData[0];
          const arrivalData = this.state.completeResponseData[1];
          for (const item of departData) {
            if (!map.has(item.flightSegment[0][0].airline.airlineCode)) {
              map.set(item.flightSegment[0][0].airline.airlineCode, true)
              this.depAirlines.push({
                airlineCode: item.flightSegment[0][0].airline.airlineCode,
                airlineName: item.flightSegment[0][0].airline.airlineName,
                airlineImage: item.flightSegment[0][0].airline.image
              })
            }
          }
          for (const item of arrivalData) {
            if (!map2.has(item.flightSegment[0][0].airline.airlineCode)) {
              map2.set(item.flightSegment[0][0].airline.airlineCode, true)
              this.arrvialAirlines.push({
                airlineCode: item.flightSegment[0][0].airline.airlineCode,
                airlineName: item.flightSegment[0][0].airline.airlineName,
                airlineImage: item.flightSegment[0][0].airline.image
              })
            }
          }

        }
        break;
      case 3:
        {
          const response = this.state.completeResponseData[0];
          for (const item of response) {
            if (!map.has(item.flightSegment[0][0].airline.airlineCode)) {
              map.set(item.flightSegment[0][0].airline.airlineCode, true)
              this.internationalAirlines.push({
                airlineCode: item.flightSegment[0][0].airline.airlineCode,
                airlineName: item.flightSegment[0][0].airline.airlineName,
                airlineImage: item.flightSegment[0][0].airline.image
              })
            }
          }
        }
        break;
    }

  }

  getFlightsList() {
    let param = this.props.flightSearchParams;
    // this.setState({ spinner: true });
    console.log('parameters for flights list   ' + JSON.stringify(param));
    this.props.searchFlightAction(param);
  }

  getFlightsCalendar() {
    let dict = this.props.flightSearchParams;
    let param = {
      origin: dict.origin,
      destination: dict.destination,
      departure_date: dict.departure_date
    };

    this.setState({ spinner: true });
    APIManager.getFlightsCalender(param, (success, response) => {
      this.setState({ spinner: false });
      this.getFlightsList();
      if (success) {
        let data = response.object;
        let sortData = data.sort(function compare(a, b) {
          let dateA = Date.parse(moment(a.departs_at).format("YYYY-MM-DD"))
          let dateB = Date.parse(moment(b.departs_at).format("YYYY-MM-DD"))
          return dateA - dateB;
        });
        this.setState({ data: sortData, loading: !this.state.loading });
      } else {
        console.log("Got error in calendar  " + JSON.stringify(response));
        // setTimeout(() => {
        //   alert(response.message);
        // }, 100);
      }
    });
  }

  compareTodayWithdate(date) {
    // let now = dateFormat(new Date(), "yyyy-mm-dd");
    let now = moment(new Date()).format("YYYY-MM-DD");
    let dateCompare = moment(date).format("YYYY-MM-DD");

    if (now == dateCompare) {
      return "Today";
    } else {
      return moment(date).format("DD MMM");
    }
  }

  arrivalSort(flightData) {
    console.log("calling arr sort and flightData is");
    console.log(flightData)
    if (this.state.sortArrIncr) {
      console.log("increment")
      flightData.sort((a, b) => {
        let alastflightIndex = a.flightSegment[0].length - 1;
        let blastflightIndex = b.flightSegment[0].length - 1;
        let aDate = new Date(a.flightSegment[0][alastflightIndex].stopPointArrivalTime);
        let bDate = new Date(b.flightSegment[0][blastflightIndex].stopPointArrivalTime);
        if (aDate < bDate) {
          return -1;
        } else if (aDate > bDate) {
          return 1;
        } else {
          return 0;
        }
      })

    } else {

      console.log("decrement")
      flightData.sort((a, b) => {
        let alastflightIndex = a.flightSegment[0].length - 1;
        let blastflightIndex = b.flightSegment[0].length - 1;
        let aDate = new Date(a.flightSegment[0][alastflightIndex].stopPointArrivalTime);
        let bDate = new Date(b.flightSegment[0][blastflightIndex].stopPointArrivalTime);
        if (bDate < aDate) {
          return -1;
        } else if (bDate > aDate) {
          return 1;
        } else {
          return 0;
        }
      })

    }

    console.log("sorted flight Data is ")
    console.log(flightData);
    console.log("returning flight Data");
    return flightData;

  }

  priceSort(flightData) {
    console.log("calling flightSort sort and flightData is");
    console.log(flightData)
    if (this.state.sortPriceIncr) {
      flightData.sort((a, b) => {
        // let aFare = new Date(a.fare.offeredFare);
        // let bDate = new Date(b.fare.offeredFare);
        if (a.fare.offeredFare < b.fare.offeredFare) {
          return -1;
        } else if (a.fare.offeredFare > b.fare.offeredFare) {
          return 1;
        } else {
          return 0;
        }
      })
    } else {
      flightData.sort((a, b) => {
        // let aDate = new Date(a.flightSegment[0][0].origin.depTime);
        // let bDate = new Date(b.flightSegment[0][0].origin.depTime);
        if (a.fare.offeredFare > b.fare.offeredFare) {
          return -1;
        } else if (a.fare.offeredFare < b.fare.offeredFare) {
          return 1;
        } else {
          return 0;
        }
      })
    }
    console.log("sorted flight Data is ")
    console.log(flightData);
    console.log("returning flight Data");
    return flightData;
  }

  durationSort(flightData) {
    console.log("calling duration sort and flightData is");
    console.log(flightData)
    if (this.state.sortDurIncr) {
      flightData.sort((a, b) => {
        let atotalDuration = 0;
        for (i = 0; i < a.flightSegment[0].length; i++) {
          let item = a.flightSegment[0][i];
          atotalDuration = atotalDuration + item.duration;
        }
        let btotalDuration = 0;
        for (i = 0; i < b.flightSegment[0].length; i++) {
          let item = b.flightSegment[0][i];
          btotalDuration = btotalDuration + item.duration;
        }
        if (atotalDuration < btotalDuration) {
          return -1;
        } else if (atotalDuration > btotalDuration) {
          return 1;
        } else {
          return 0;
        }
      })
    } else {
      flightData.sort((a, b) => {
        let atotalDuration = 0;
        for (i = 0; i < a.flightSegment[0].length; i++) {
          let item = a.flightSegment[0][i];
          atotalDuration = atotalDuration + item.duration;
        }
        let btotalDuration = 0;
        for (i = 0; i < b.flightSegment[0].length; i++) {
          let item = b.flightSegment[0][i];
          btotalDuration = btotalDuration + item.duration;
        }
        if (atotalDuration < btotalDuration) {
          return 1;
        } else if (atotalDuration > btotalDuration) {
          return -1;
        } else {
          return 0;
        }
      })
    }
    console.log("sorted flight Data is ")
    console.log(flightData);
    console.log("returning flight Data");
    return flightData;
  }

  depSort(flightData) {
    console.log("calling dep sort and flightData is");
    console.log(flightData)
    if (this.state.sortDepIncr) {
      flightData.sort((a, b) => {
        let aDate = new Date(a.flightSegment[0][0].origin.depTime);
        let bDate = new Date(b.flightSegment[0][0].origin.depTime);
        if (aDate < bDate) {
          return -1;
        } else if (aDate > bDate) {
          return 1;
        } else {
          return 0;
        }
      })
    } else {
      flightData.sort((a, b) => {
        let aDate = new Date(a.flightSegment[0][0].origin.depTime);
        let bDate = new Date(b.flightSegment[0][0].origin.depTime);
        if (bDate < aDate) {
          return -1;
        } else if (bDate > aDate) {
          return 1;
        } else {
          return 0;
        }
      })
    }
    console.log("sorted flight Data is ")
    console.log(flightData);
    console.log("returning flight Data");
    return flightData;
  }

  airlineNameSort(flightData) {
    console.log("calling dep sort and flightData is");
    console.log(flightData)
    if (this.state.sortAirlineIncr) {
      flightData.sort((a, b) => {

        if (a.airlineName < b.airlineName) {
          return -1;
        } else if (a.airlineName > a.airlineName) {
          return 1;
        } else {
          return 0;
        }
      })
    } else {

      flightData.sort((a, b) => {
        if (b.airlineName < a.airlineName) {
          return -1;
        } else if (b.airlineName > a.airlineName) {
          return 1;
        } else {
          return 0;
        }
      })
    }
    return flightData;
  }

  sortFlights() {
    let flightData = [];
    if (this.state.isFilterActive) {
      flightData = this.state.filteresFlightData;
      switch (this.state.selectedSort) {
        case 0:
          // for departure sort    
          flightData = this.depSort(flightData);
          break;
        case 1:
          // for Arrival sort
          flightData = this.arrivalSort(flightData);
          break;
        case 2:
          // for duration sort{}
          {
            flightData = this.durationSort(flightData);
            break;
          }
        case 3:
          // for price sort
          flightData = this.priceSort(flightData);
          break;
        case 4:
          {
            flightData = this.airlineNameSort(flightData);
            break;
          }
      }
      this.setState({
        filteresFlightData: flightData,
        loading: !this.state.loading,
      })
    } else {
      flightData = this.state.flightListData;
      switch (this.state.selectedSort) {
        case 0:
          // for dep sort
          flightData = this.depSort(flightData);
          break;
        case 1:
          // for Arrival sort
          flightData = this.arrivalSort(flightData);
          break;
        case 2:
          // for duration sort{}
          {
            flightData = this.durationSort(flightData);
            break;
          }
        case 3:
          // for price sort
          flightData = this.priceSort(flightData);
          break;
        case 4: {
          flightData = this.airlineNameSort(flightData);
          break;
        }
      }
      this.setState({
        flightListData: flightData,
        loading: !this.state.loading,
      })
    }
  }

  _renderListdate({ item, index }) {
    return (
      <View style={styles.styleDateListItemSeperView}>
        <Text style={{ fontSize: 12, color: "gray" }}>
          {this.compareTodayWithdate(item.departs_at)}
        </Text>
        <View
          style={[
            styles.styleListDotView,
            {
              backgroundColor:
                index === this.state.selectedIndex
                  ? "rgba(25,89,189,1)"
                  : "gray"
            }
          ]}
        />
        <Text style={{ fontSize: 12, color: "gray", marginTop: 8 }}>
          {item.currency} {item.price_per_adult}
        </Text>
      </View>
    );
  }

  calculateArrivalAndDepartureTime(item) {
    let data = item.flightSegment[0];

    // let deptTime = dateFormat(data[0].stopPointDepartureTime,'hh:mm')
    // let arrTime = dateFormat(data[0].stopPointArrivalTime,'hh:mm')
    // return deptTime + '-' + arrTime

    // console.log('calculated item is  '+ JSON.stringify(data[data.lenght-1].stopPointArrivalTime))
    // console.log('calculated items depart time is  '+ JSON.stringify(data[0].stopPointDepartureTime))
    if (data.lenght == 1) {
      // let deptTime = moment(data[0].stopPointDepartureTime).format("HH:mm");
      // let arrTime = moment(data[0].stopPointArrivalTime).format("HH:mm");
      let deptTime = moment(data[0].stopPointDepartureTime).format("HH:mm");
      let arrTime = moment(data[0].stopPointArrivalTime).format("HH:mm");
      console.log(
        "depart Time is " + moment(data[0].stopPointDepartureTime).format("HH:mm")
      );
      console.log(
        "Arrival Time is " + moment(data[0].stopPointArrivalTime).format("HH:mm")
      );

      let depTime = String(data[0].stopPointDepartureTime).split("T");
      let arTime = String(data[0].stopPointArrivalTime).split("T");

      return (
        depTime[1].substring(0, depTime[1].length - 3) +
        "-" +
        arTime[1].substring(0, arTime[1].length - 3)
      );
    } else {
      let deptTime = moment(data[0].stopPointDepartureTime).format("HH:mm");
      let arrTime = moment(
        data[data.length - 1].stopPointArrivalTime).format("HH:mm");

      let depTime = String(data[0].stopPointDepartureTime).split("T");
      let arTime = String(data[data.length - 1].stopPointArrivalTime).split(
        "T"
      );

      return (
        depTime[1].substring(0, depTime[1].length - 3) +
        "-" +
        arTime[1].substring(0, arTime[1].length - 3)
      );
      // return deptTime + '-' + arrTime
    }
  }

  calculateDurationTime(data) {
    let totalDuration = 0;
    for (i = 0; i < data.length; i++) {
      let item = data[i];
      totalDuration = totalDuration + item.duration;
    }
    return this.convertMinsIntoHandM(totalDuration);
  }

  convertMinsIntoHandM(n) {
    var num = n;
    var hours = num / 60;
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + "h " + rminutes + "m";
  }

  returnNonStopOrByOrigin(item) {

    let viacode;
    if (item.length == 1) {
      return "Non-Stop";
    } else {
      item.forEach((item, index) => {
        if (index != 0) {
          if (index == 1) {
            viacode = "Via " + item.origin.airport.airportCode
          } else {
            viacode = viacode + "-" + item.origin.airport.airportCode
          }

        }
      });
      return viacode;
    }

  }

  calculateNavBarHeight() {
    return (
      (Platform.OS == "ios" ? getStatusBarHeight(true) : 0) +
      (Platform.OS == "ios" ? 44 : 52)
    );
  }

  getBackgroundColorForItem(index) {
    if (this.state.completeResponseData.length == 1) {
      return colors.colorWhite;
    }
    if (this.state.selectedOriginIndex == 0) {
      let color = this.state.selectedOriginFlightIndex == index
        ? "rgb(229,239,249)"
        : colors.colorWhite;
      console.log(color)
      return color
    }
    return this.state.selectedDestinationFlightIndex == index
      ? "rgb(229,239,249)"
      : colors.colorWhite;
  }

  _renderFlightSingleWayListItem({ item, index }) {
    let dict = this.props.flightSearchParams;
    return (
      <CardView cardElevation={5} cardMaxElevation={5} cornerRadius={5}
        style={{
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: .4,
          shadowRadius: 1,
          elevation: 1,
          marginBottom: (dict.return_date && this.state.completeResponseData.length == 2) ? 15 : 0
        }}>
        <TouchableOpacity key={index} style={[styles1.card, {
          backgroundColor: this.getBackgroundColorForItem(index)
        }]} onPress={() => {
          if (dict.return_date && this.state.completeResponseData.length == 2) {
            if (this.state.selectedOriginIndex == 0) {
              requestAnimationFrame(() => {
                this.setState({
                  selectedOriginFlightIndex: index,
                  loading: !this.state.loading,
                  originFlightItem: item
                }, () => { this.calulateTwoWayPrice() });
              })
            } else {
              requestAnimationFrame(() => {
                this.setState({
                  selectedDestinationFlightIndex: index,
                  loading: !this.state.loading,
                  destinationFlightItem: item
                }, () => { this.calulateTwoWayPrice() });

              })
            }
          }
          else {
            let finalArr = []
            finalArr.push(item)
            let dictPassenger = {
              adult: dict.ADT,
              child: dict.CHD,
              infant: dict.INF
            };
            let data = {
              flightdata: finalArr,
              traceid: this.state.traceId,
              flightType: 0,
              isRoundTrip: this.state.isRoundTrip,
              isInterNational: this.state.isInterNational,
              isNavigateFromReviewPage: false
            }
            this.props.saveFlightItem(data);
            // alert("fare is :--" + Math.ceil(item.fare.offeredFare));
            NavigationServices.navigate('FlightDetails', {
              flightdata: finalArr,
              traceid: this.state.traceId,
              passengers: dictPassenger,
              flightType: 0,
              origin: dict.origin,
              destination: dict.destination,
              isRoundTrip: this.state.isRoundTrip,
              isInterNational: this.state.isInterNational,
              isNavigateFromReviewPage: false,
              totalFare: Math.ceil(item.fare.offeredFare),
              currency: item.fare.currency
            });
          }
        }}>
          {this._renderCommonListItem(item, index, 0, true)}
        </TouchableOpacity>
      </CardView>
    );
  }

  _renderInterNationalFlightListItem({ item, index }) {
    let dict = this.props.flightSearchParams;
    return (
      <TouchableOpacity style={styles1.doubleCard} onPress={() => {
        let finalArr = [item]
        let dictPassenger = {
          adult: dict.ADT,
          child: dict.CHD,
          infant: dict.INF
        };
        let data = {
          flightdata: finalArr,
          traceid: this.state.traceId,

          flightType: 1,

          isRoundTrip: this.state.isRoundTrip,
          isInterNational: this.state.isInterNational,
          isNavigateFromReviewPage: false,
        }
        this.props.saveFlightItem(data);
        // alert("fare is :--" + Math.ceil(item.fare.offeredFare));
        NavigationServices.navigate('FlightDetails', {
          flightdata: finalArr,
          traceid: this.state.traceId,
          passengers: dictPassenger,
          flightType: 1,
          origin: dict.origin,
          destination: dict.destination,
          isRoundTrip: this.state.isRoundTrip,
          isInterNational: this.state.isInterNational,
          isNavigateFromReviewPage: false,
          totalFare: Math.ceil(item.fare.offeredFare),
          currency: item.fare.currency
        });
      }}
      >
        <View style={{ flexDirection: "row", flex: 1 }}>
          <View style={{
            width: "80%",
            height: "100%",
            borderRightWidth: 1,
            borderRightColor: colors.lightgrey,
          }}>
            {this._renderCommonListItem(item, index, 0, false)}
            <View
              style={{ height: 1, backgroundColor: colors.lightgrey }}>
            </View>
            {this._renderCommonListItem(item, index, 1, false)}
          </View>
          <View style={styles1.cardContentCol2}>
            <View style={{ backgroundColor: colors.colorBlue, width: 75, elevation: 6, paddingTop: 5, paddingBottom: 5, borderRadius: 5, alignItems: "center" }}>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
                <Text style={{ color: colors.colorWhite, fontFamily: Fonts.bold, }}>
                  {/* ₹ */}
                  {item.fare.currency}
                </Text>
                <Text style={{ color: colors.colorWhite, marginLeft: 3, textAlign: "right", fontFamily: Fonts.bold }}>
                  {Math.ceil(item.fare.offeredFare)}
                </Text>
              </View>
              <Text style={{ color: colors.colorWhite, textAlign: "center", fontSize: 10, fontFamily: Fonts.bold }}>BOOK</Text>
            </View>
            <View style={{ width: 55, justifyContent: "flex-end", alignItems: "center" }}>
              <Text numberOfLines={2} style={[styles.listItemGreyText, { marginTop: 5, alignSelf: "center", textAlign: "center", fontSize: 9 }]}>
                {item.isRefundable ? "Refundable" : "Non Refundable"}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  _renderCommonListItem(item, index, sagmentIndex, isNational) {
    const len = item.flightSegment[sagmentIndex].length - 1;
    const { tripType } = this.props.flightSearchParams;

    return (
      <View>
        <View style={[styles1.cardHeader, { backgroundColor: isNational ? colors.gray : colors.colorWhite }]}>
          <View style={[styles1.cardHeaderCol1, { width: isNational ? "80%" : "100%" }]}>
            <View style={styles1.cardheaderFlightView}>
              <Image source={{ uri: imageBaseUrl + item.flightSegment[sagmentIndex][0].airline.image }}
                style={styles1.cardheaderFlightImg}></Image>
              <Text style={[styles1.price, { color: colors.colorBlack }]}>{item.airlineName + " | "}{item.airlineCode + "-" + item.flightSegment[0][0].airline.flightNumber}</Text>
            </View>
            <View style={styles1.cardHeaderAmenties}>
              {/* <Image source={Images.imgfood} style={styles1.flightAmenityImg}></Image>
              <Image source={Images.imgflightinfo} style={styles1.flightAmenityImg}></Image> */}
              {
                item.flightSegment[0][0].seatAvailable ?
                  <Image source={Images.imgseat} style={styles1.flightAmenityImg}></Image> : null
              }
            </View>
          </View>
          {isNational && <View style={[styles1.cardHeaderCol2, { width: "20%" }]}>
            <Text style={styles1.priceText}>
              <Text style={{ color: colors.colorBlue }}>{item.fare.currency + " "}</Text>
              {/* <Image source={Images.imgrupeeblue} style={styles1.priceImg}></Image> */}
              {Math.ceil(item.fare.offeredFare)}
            </Text>
          </View>}
        </View>
        <View style={styles1.cardContent}>
          <View style={[styles1.cardContentCol1, { width: "100%", paddingRight: 7 }]}>
            <View style={{ width: isNational ? "15%" : "20%" }}>
              <Text style={styles1.estimate}>{moment(item.flightSegment[sagmentIndex][0].origin.depTime).format("HH:mm")}</Text>
              <Text style={styles1.stnName}>{item.flightSegment[sagmentIndex][0].origin.airport.airportCode}</Text>
            </View>
            {this.renderDashView(item.flightSegment[sagmentIndex], isNational)}
            <View style={{ width: isNational ? "15%" : "20%", alignItems: "flex-end" }}>
              <Text style={styles1.estimate}>{moment(item.flightSegment[sagmentIndex][0].destination.arrTime).format("HH:mm")}</Text>
              <Text style={styles1.stnName}>{item.flightSegment[sagmentIndex][0].destination.airport.airportCode}</Text>
            </View>
          </View>
        </View>
      </View>
    )
  }

  renderDashView = (item, isNational) => {
    if (isNational) {
      return (
        <View style={{
          width: "70%",
          height: 15,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}>
          <View style={{
            width: "45%",
            alignItems: "center"
            // backgroundColor: "blue"
          }}>
            <Text style={styles1.stnName}>{this.returnNonStopOrByOrigin(item)}</Text>
            <View style={styles1.dotView}>
              <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
              <Dash style={{
                width: "75%",
                height: 1,
              }} dashColor={colors.lightgrey} dashLength={5}
              />
            </View>
          </View>
          <View style={{
            width: "10%",
            height: 20,
            marginTop: 15
          }}>
            <Image source={Images.imgplaneblue} style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
          </View>
          <View style={{
            width: "45%",
            alignItems: "center"
          }}>
            <Text style={styles1.stnName}>{this.calculateDurationTime(item)}</Text>
            <View style={styles1.dotView}>
              <Dash style={{
                width: "75%",
                height: 1,
              }} dashColor={colors.lightgrey} dashLength={5}
              />
              <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
            </View>
          </View>
        </View>
      )
    } else {
      return (
        <View style={{ width: "60%", alignItems: "center" }}>
          <Text style={styles1.stnName}>{this.calculateDurationTime(item)}</Text>
          <View style={styles1.dotView}>
            <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
            <Dash style={{
              width: "75%",
              height: 1,
            }} dashColor={colors.lightgrey} dashLength={5}
            />
            <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
          </View>
          <Text style={styles1.stnName}>{this.returnNonStopOrByOrigin(item)}</Text>
        </View>
      )
    }
  }

  _renderSagmentView() {
    let dict = this.props.flightSearchParams;
    if (dict.return_date && this.state.completeResponseData.length == 2) {
      return (
        <View style={[styles.styleSagmentVIew, { height: 60 }]}>
          <View style={styles.styleSagementButtonsView}>
            <TouchableHighlight
              style={[
                styles.styleSagmentButton,
                {
                  backgroundColor:
                    this.state.selectedOriginIndex == 0
                      ? colors.colorBlue
                      : "transparent"
                }
              ]}
              underlayColor="transparent"
              onPress={() => {
                if (this.state.isFilterActive) {
                  if (this.state.filterForDep) {
                    this.setState({
                      selectedSort: 3,
                      sortPriceIncr: true,
                      filteresFlightData: this.state.filterDataForTwoWayDep,
                      selectedOriginIndex: 0,
                      loading: !this.state.loading
                    }, () => { this.scrollToitemindex() });
                  } else {
                    this.setState({
                      selectedSort: 3,
                      sortPriceIncr: true,
                      filteresFlightData: this.state.completeResponseData[0],
                      selectedOriginIndex: 0,
                      loading: !this.state.loading
                    }, () => { this.scrollToitemindex() });
                  }
                } else {
                  this.setState({
                    selectedSort: 3,
                    sortPriceIncr: true,
                    flightListData: this.state.completeResponseData[0],
                    selectedOriginIndex: 0,
                    loading: !this.state.loading
                  }, () => { this.scrollToitemindex() });
                }
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  color: this.state.selectedOriginIndex == 0 ? colors.colorWhite : colors.colorBlue,
                  fontFamily: Fonts.semiBold
                }}
              >
                {dict.origin + " - " + dict.destination}
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              style={[
                styles.styleSagmentButton,
                {
                  backgroundColor:
                    this.state.selectedOriginIndex == 1
                      ? colors.colorBlue
                      : "transparent"
                }
              ]}
              underlayColor="transparent"
              onPress={() => {
                if (this.state.isFilterActive) {
                  if (this.state.filterForReturn) {
                    this.setState({
                      selectedSort: 3,
                      sortPriceIncr: true,
                      filteresFlightData: this.state.filterDataForTwoWayRet,
                      selectedOriginIndex: 1,
                      loading: !this.state.loading
                    }, () => { this.scrollToitemindex() });
                  } else {
                    this.setState({
                      selectedSort: 3,
                      sortPriceIncr: true,
                      filteresFlightData: this.state.completeResponseData[1],
                      selectedOriginIndex: 1,
                      loading: !this.state.loading
                    }, () => { this.scrollToitemindex() });
                  }
                } else {
                  this.setState({
                    selectedSort: 3,
                    sortPriceIncr: true,
                    flightListData: this.state.completeResponseData[1],
                    selectedOriginIndex: 1,
                    loading: !this.state.loading
                  }, () => { this.scrollToitemindex() });
                }
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  color:
                    this.state.selectedOriginIndex == 1
                      ? colors.colorWhite
                      : colors.colorBlue,
                  fontFamily: Fonts.semiBold
                }}
              >
                {dict.destination + " - " + dict.origin}
              </Text>
            </TouchableHighlight>
          </View>
        </View>
      );
    }
    return null;
  }

  _renderBookNowBtn() {
    let dict = this.props.flightSearchParams;
    if (dict.return_date && this.state.completeResponseData.length == 2) {
      let arr1 = this.state.completeResponseData[0];
      let dict1 = arr1[this.state.selectedOriginFlightIndex]
      return (
        <View style={styles1.bottomVIew}>
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 1.5 }}>
              <Text numberOfLines={1} style={{
                fontSize: 11,
                color: colors.colorBlue, fontFamily: Fonts.medium
              }}> Your Selection  </Text>
              <View style={{ flexDirection: "row", marginLeft: 5 }}>
                <Image
                  style={styles.selectedflighticon}
                  source={{ uri: "http://travel.yiipro.com/" + this.state.originFlightItem.flightSegment[0][0].airline.image }}
                />
                <Image
                  style={[styles.selectedflighticon, { marginLeft: 15 }]}
                  source={{ uri: "http://travel.yiipro.com/" + this.state.destinationFlightItem.flightSegment[0][0].airline.image }}
                />
              </View>
            </View>
            <View style={{ flex: 1.5 }}>
              <Text style={{ fontFamily: Fonts.bold, color: colors.colorWhite, fontSize: 16, marginTop: 10 }}>
                {/*₹*/} {dict1.fare.currency + " "} {Math.ceil(this.state.twoWayTotalPrice)}
              </Text>
            </View>

            <View style={styles.BookbtnStyle}>
              <TouchableOpacity
                onPress={() => {
                  let arr1 = this.state.completeResponseData[0]
                  let arr2 = this.state.completeResponseData[1]
                  let dict1 = arr1[this.state.selectedOriginFlightIndex]
                  let dict2 = arr2[this.state.selectedDestinationFlightIndex]
                  let finalArr = [dict1, dict2]
                  let dictPassenger = {
                    adult: dict.ADT,
                    child: dict.CHD,
                    infant: dict.INF
                  };
                  let data = {
                    flightdata: finalArr,
                    traceid: this.state.traceId,
                    flightType: 1,
                    isRoundTrip: this.state.isRoundTrip,
                    isInterNational: this.state.isInterNational,
                    isNavigateFromReviewPage: false,
                  }
                  this.props.saveFlightItem(data);
                  // alert("fare is :--" + Math.ceil(this.state.twoWayTotalPrice));
                  NavigationServices.navigate('FlightDetails', {
                    flightdata: finalArr,
                    traceid: this.state.traceId,
                    passengers: dictPassenger,
                    flightType: 1,
                    origin: dict.origin,
                    destination: dict.destination,
                    isRoundTrip: this.state.isRoundTrip,
                    isInterNational: this.state.isInterNational,
                    isNavigateFromReviewPage: false,
                    totalFare: Math.ceil(this.state.twoWayTotalPrice),
                    currency: dict1.fare.currency
                  });
                }}>
                <Text style={{ fontSize: 14, color: colors.colorBlue, fontFamily: Fonts.bold }}>
                  CONTINUE
                </Text>
              </TouchableOpacity>
            </View>
          </View>

        </View>
      );
    }
    return <View />;
  }

  //departure view with 5 tab Airline Departure arrival duration and price 
  //based on which sorting will be performed 
  _renderdepartureView() {
    return (
      <View style={styles1.tabs}>
        <TouchableOpacity style={[styles1.tab, { display: "none" }]}
          onPress={() => {
            this.setState({
              sortAirlineIncr: !this.state.sortAirlineIncr,
              selectedSort: 4,
              spinner: true
            }, () => {
              setTimeout(() => { this.setState({ spinner: false }) }, 1000)
            })
            this.sortFlights()
          }}>
          <View
            style={{ flexDirection: "row", alignItems: "center" }}>
            <Text
              style={{
                fontSize: 12,
                color: (this.state.selectedSort == 4) ? "rgba(25,89,189,1)" : "grey",
                fontFamily: Fonts.semiBold
              }}
            >
              AirLines
        </Text>
            {this.state.selectedSort == 4
              ?
              <Image style={{ height: 12, width: 5, marginLeft: 5 }}
                source={
                  this.state.sortAirlineIncr
                    ? Images.imgsortingdec
                    : Images.imgsortinginc}
              />
              :
              null}
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          underlayColor={"transparent"}
          style={styles1.tab}
          onPress={() => {
            // requestAnimationFrame(()=>{
            console.log("departure click");
            this.setState({
              sortDepIncr: !this.state.sortDepIncr,
              selectedSort: 0,
              spinner: true
            }, () => {
              this.sortFlights()
              setTimeout(() => { this.setState({ spinner: false }) }, 1000)
            })
            // });

          }}
        >
          <Text style={styles1.tabText}>Departure</Text>
          {this.state.selectedSort == 0
            ?
            <Image style={{ height: 12, width: 5, marginLeft: 5 }}
              source={
                this.state.sortDepIncr
                  ? Images.imgsortingdec
                  : Images.imgsortinginc}
            />
            :
            null}
        </TouchableOpacity>

        <TouchableOpacity style={[styles1.tab, {
          borderLeftWidth: 1,
          borderRightWidth: 1,
          borderColor: "lightgray"
        }]} onPress={() => {
          this.setState({
            sortDurIncr: !this.state.sortDurIncr,
            spinner: true,
            selectedSort: 2
          }, () => {
            this.sortFlights();
            setTimeout(() => { this.setState({ spinner: false }) }, 1000)
          })
        }}>
          <Text style={styles1.tabText}>Duration</Text>
          {this.state.selectedSort == 2
            ?
            <Image style={{ height: 12, width: 5, marginLeft: 5 }}
              source={
                this.state.sortDurIncr
                  ? Images.imgsortingdec
                  : Images.imgsortinginc}
            />
            :
            null}
        </TouchableOpacity>

        <TouchableOpacity style={[styles1.tab, { display: "none" }]}
          underlayColor={"transparent"}
          onPress={() => {
            console.log("arrival click");
            this.setState({
              sortArrIncr: !this.state.sortArrIncr,
              selectedSort: 1,
              spinner: true
            }, () => {
              this.sortFlights()
              setTimeout(() => { this.setState({ spinner: false }) }, 1000)
            })
          }} >
          <View
            style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={{
              fontSize: 12,
              color: (this.state.selectedSort == 1) ? "rgba(25,89,189,1)" : "grey"
              ,
              fontFamily: "Montserrat-SemiBold"
            }}>
              Arrival
        </Text>
            {this.state.selectedSort == 1
              ?
              <Image style={{ height: 12, width: 5, marginLeft: 5 }}
                source={
                  this.state.sortArrIncr
                    ? Images.imgsortingdec
                    : Images.imgsortinginc}
              />
              :
              null}
          </View>

        </TouchableOpacity>

        <TouchableOpacity style={styles1.tab}
          onPress={() => {
            this.setState({
              selectedSort: 3,
              spinner: true,
              sortPriceIncr: !this.state.sortPriceIncr,
            }, () => {
              this.sortFlights();
              setTimeout(() => { this.setState({ spinner: false }) }, 1000)
            })
          }}>
          <Text style={styles1.tabText}>Price</Text>
          {this.state.selectedSort == 3
            ?
            <Image style={{ height: 12, width: 5, marginLeft: 5 }}
              source={
                this.state.sortPriceIncr
                  ? Images.imgsortingdec
                  : Images.imgsortinginc}
            />
            :
            null}
        </TouchableOpacity>
      </View>
    )
  }

  _renderItemSeperator = () => (
    <View style={{
      marginLeft: 5,
      marginRight: 5,
      height: 12,
      backgroundColor: colors.colorWhite
    }} />
  )

  filterFlight = () => {
    this.setState({ modelVisible: true });
  }

  render() {

    const { destination, origin, return_date, tripType } = this.props.flightSearchParams;

    let originCity
    let airlines
    if (this.state.isRoundTrip && this.state.selectedOriginIndex == 1) {
      originCity = destination;
      airlines = this.arrvialAirlines
    } else if (this.state.isRoundTrip && this.state.selectedOriginIndex == 0) {
      originCity = origin;
      airlines = this.depAirlines
    } else if (this.state.isInterNational) {
      originCity = origin;
      airlines = this.internationalAirlines
    } else {
      originCity = origin;
      airlines = this.depAirlines
    }
    // let dict = this.props.flightSearchParams;
    return (

      <SafeAreaView style={{ flex: 1, backgroundColor: colors.colorBlue }}>
        <Spinner
          style={{ flex: 1 }}
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={{ color: "white" }}
        />
        <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} />
        {/* {this._renderNavigationBar()} */}

        <View style={styles1.container}>
          <Header
            fireEvent={this.filterFlight}
            data={this.props.flightSearchParams}
            bookingType={tripType}></Header>
          {this._renderSagmentView()}
          {this._renderdepartureView()}


          <View style={styles1.mainView}>
            <FlatList
              showsVerticalScrollIndicator={false}
              ref={(ref) => { this.flatListref = ref }}
              data={
                this.state.isFilterActive
                  ? this.state.filteresFlightData
                  : this.state.flightListData
              }
              extraData={this.state.loading}
              renderItem={
                (return_date && this.state.completeResponseData.length == 2) ||
                  return_date == null
                  ? this._renderFlightSingleWayListItem
                  : this._renderInterNationalFlightListItem
              }
              ItemSeparatorComponent={return_date == null ? this._renderItemSeperator : null}
              keyExtractor={(item, index) => index.toString()}
            />

            {
              (return_date && this.state.completeResponseData.length == 2) ?
                <View style={{ height: 50 }}></View> : null
            }
            {this._renderBookNowBtn()}
          </View>
        </View>
        {/* </ScrollView> */}
        <Modal
          transparent={false}
          animationType={"slide"}
          visible={this.state.modelVisible}>
          <View
            style={{ flex: 1 }}>
            <FlightFilters
              originCity={originCity}
              destCity={destination}
              airlines={airlines}
              isInterNational={this.state.isInterNational}
              onModelHide={() => {
                this.setState({
                  modelVisible: false
                });
              }}
              modelVisible={this.state.modelVisible}
              slectedFilters={this.state.isRoundTrip && this.state.selectedOriginIndex == 1 ? this.state.selectedFiltersArr : this.state.selectedFiltersDepart}
              onFilterSelection={filterDict => {
                this.onFilterSelection(filterDict);
              }}
            />
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}


const mapStateToProps = state => {
  return {
    flightSearchParams: state.FlightsReducer.flightSearchParams,
    isLoading: state.isLoadingReducer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveFlightItem: (data) => dispatch(saveFlightItem(data)),
    searchFlightAction: (data) => dispatch(searchFlightAction(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FlightList)