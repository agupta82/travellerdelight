import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Modal,
  DeviceEventEmitter,
  TouchableHighlight
} from "react-native";
import { commonstyle } from "../../Helper Classes/Commonstyle";

export default class SelectTravellers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numberOfAdults: this.props.numberOfAdults,
      numberOfChilds: this.props.numberOfChilds,
      numberOfInfant: this.props.numberOfInfant
    };

    this._renderAdultsView = this._renderAdultsView.bind(this);
    this._renderChildrenView = this._renderChildrenView.bind(this);
    this._renderInfantView = this._renderInfantView.bind(this);
  }

  _renderAdultsView() {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          margin: 15
        }}
      >
        <View style={styles.styleAdultView}>
          <Text style={[commonstyle.blackText,{  fontSize: 13 }]}>Adults</Text>
          <Text style={[commonstyle.greyText,{fontSize: 13, marginTop: 5 }]}>
            Above 12 yrs
          </Text>
        </View>

        <View style={styles.styleIncrementOrDecrView}>


        <TouchableHighlight
            style={[styles.stylePlusOrMinusBtn, { marginLeft: 5 }]}
            underlayColor="transparent"
            onPress={() => {
              let numberOfAdults = this.state.numberOfAdults;
              if (numberOfAdults > 0) {
                this.setState({
                  numberOfAdults: numberOfAdults - 1
                });
              }
            }}
          >
            <Text style={{ fontSize: 25, color: "black" }}>-</Text>
          </TouchableHighlight>

       

          <View style={styles.styleNumbersView}>
            <Text style={{ fontSize: 12, color: "white",fontFamily:"Montserrat-SemiBold" }}>
              {this.state.numberOfAdults}
            </Text>
          </View>

       

          <TouchableHighlight
            style={[styles.stylePlusOrMinusBtn, { marginRight: 5 }]}
            underlayColor="transparent"
            onPress={() => {
              this.setState({
                numberOfAdults: this.state.numberOfAdults + 1
              });
              this.forceUpdate();
            }}
          >
            <Text style={{ fontSize: 20, color: "black" }}>+</Text>
          </TouchableHighlight>

        </View>
      </View>
    );
  }

  _renderChildrenView() {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          margin: 15
        }}
      >
        <View style={styles.styleAdultView}>
          <Text style={[commonstyle.blackText,{  fontSize: 13 }]}>Children</Text>
          <Text style={[commonstyle.greyText,{  fontSize: 13, marginTop: 5 }]}>
            2 - 12 yrs
          </Text>
        </View>

        <View style={styles.styleIncrementOrDecrView}>
       


        <TouchableHighlight
            style={[styles.stylePlusOrMinusBtn, { marginLeft: 5 }]}
            underlayColor="transparent"
            onPress={() => {
              let numberOfChilds = this.state.numberOfChilds;
              if (numberOfChilds > 0) {
                this.setState({
                  numberOfChilds: numberOfChilds - 1
                });
              }
            }}
          >
            <Text style={{ fontSize: 25, color: "black" }}>-</Text>
          </TouchableHighlight>



          <View style={styles.styleNumbersView}>
            <Text style={{ fontSize: 12, color: "white" ,fontFamily:"Montserrat-SemiBold"}}>
              {this.state.numberOfChilds}
            </Text>
          </View>


          <TouchableHighlight
            style={[styles.stylePlusOrMinusBtn, { marginRight: 5 }]}
            underlayColor="transparent"
            onPress={() => {
              this.setState({
                numberOfChilds: this.state.numberOfChilds + 1
              });
              this.forceUpdate();
            }}
          >
            <Text style={{ fontSize: 20, color: "black" }}>+</Text>
          </TouchableHighlight>


        </View>
      </View>
    );
  }

  _renderInfantView() {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          margin: 15
        }}
      >
        <View style={styles.styleAdultView}>
          <Text style={[commonstyle.blackText,{ fontSize: 13 }]}>Infant</Text>
          <Text style={[commonstyle.greyText,{ color: "gray", fontSize: 13, marginTop: 5 }]}>
            Below 2 yrs
          </Text>
        </View>

        <View style={styles.styleIncrementOrDecrView}>
         
        <TouchableHighlight
            style={[styles.stylePlusOrMinusBtn, { marginLeft: 5 }]}
            underlayColor="transparent"
            onPress={() => {
              let numberOfInfant = this.state.numberOfInfant;
              if (numberOfInfant > 0) {
                this.setState({
                  numberOfInfant: numberOfInfant - 1
                });
              }
            }}
          >
            <Text style={{ fontSize: 25, color: "black" }}>-</Text>
          </TouchableHighlight>

          <View style={styles.styleNumbersView}>
            <Text style={{ fontSize: 12, color: "white",fontFamily:"Montserrat-SemiBold" }}>
              {this.state.numberOfInfant}
            </Text>
          </View>

        

          <TouchableHighlight
            style={[styles.stylePlusOrMinusBtn, { marginRight: 5 }]}
            underlayColor="transparent"
            onPress={() => {
              this.setState({
                numberOfInfant: this.state.numberOfInfant + 1
              });
              this.forceUpdate();
            }}
          >
            <Text style={{ fontSize: 20, color: "black" }}>+</Text>
          </TouchableHighlight>


        </View>
      </View>
    );
  }

  validateAdults(adultNum, childNum, infantNum) {
    if (adultNum == 0) {
      alert("Please add atleast 1 adult.");
      return;
    }

    let dict = {
      adultsNum: adultNum,
      childNum: childNum,
      infantNum: infantNum
    };

    DeviceEventEmitter.emit("TravellerSelected", dict);
  }

  render() {
    return (
      <View>
        {/* <Modal
          animationType="slide"
          transparent={true}
          visible={this.props.selectTravellersModel}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}
        > */}
          <View style={styles.container}>
            <View style={styles.stylePopUpView}>
              <View style={styles.styleTitleView}>
                <Text
                  style={[commonstyle.blackSemiBold,{ fontSize: 18, }]}
                >
                  Select Travellers
                </Text>
              </View>

              {this._renderAdultsView()}

              {this._renderChildrenView()}

              {this._renderInfantView()}

              <View style={styles.styleBottomBtnsView}>
               
                <TouchableHighlight 
                style={styles.styleCancelOrDoneBtnView}
                underlayColor='transparent'
                onPress={()=>{
                    DeviceEventEmitter.emit('TravellerSelected',null)
                }}
                >
                  <Text style={[commonstyle.greySemiBold,{ fontSize: 14 }]}>
                    Cancel
                  </Text>
                </TouchableHighlight>
              
                <TouchableHighlight
                  style={[styles.styleCancelOrDoneBtnView]}
                  underlayColor="transparent"
                  onPress={() => {
                    this.validateAdults(
                      this.state.numberOfAdults,
                      this.state.numberOfChilds,
                      this.state.numberOfInfant
                    );
                  }}
                >
                  <Text style={{ color: "rgba(25,89,189,1)", fontSize: 14,fontFamily:"Montserrat-SemiBold" }}>Done</Text>
                </TouchableHighlight>

              </View>
            </View>
          </View>
        {/* </Modal> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(0,0,0,0.5)",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  stylePopUpView: {
    width: "80%",
    backgroundColor: "white",
    borderRadius: 15
  },
  styleTitleView: {
    margin: 15,
    height: 40,
    justifyContent: "center"
  },
  styleAdultView: {
    width: "60%",
    justifyContent: "center"
  },
  styleIncrementOrDecrView: {
    flexDirection: "row",
    justifyContent: "center",
    width: "30%"
  },
  stylePlusOrMinusBtn: {
    justifyContent: "center",
    alignItems: "center",
    height: 30,
    width: 30
  },
  styleNumbersView: {
    height: 30,
    width: 30,
    borderRadius: 15,
    backgroundColor: "rgba(25,89,189,1)",
    justifyContent: "center",
    alignItems: "center"
  },
  styleBottomBtnsView: {
    flexDirection: "row",
    justifyContent: "flex-end",
    margin: 15,
    height: 35,
    paddingRight: 10
  },
  styleCancelOrDoneBtnView: {
    height: "100%",
    width: 60,
    justifyContent: "center",
    alignItems: "center"
  }
});
