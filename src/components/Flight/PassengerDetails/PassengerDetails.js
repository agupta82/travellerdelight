import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList,
  Image,
  TouchableHighlight,
  Dimensions,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import Images from "../../../Helper Classes/Images";
import Moment from 'moment';
import NavigationBar from "../../../Helper Classes/NavigationBar";
import NavigationServices from './../../../Helper Classes/NavigationServices'
import CustomTextInput from "../../../Helper Classes/Custom TextFiel/CustomTextInput";
import DateTimePicker from "react-native-modal-datetime-picker";
import ActionSheet from "react-native-actionsheet";
import Spinner from "react-native-loading-spinner-overlay";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import BaggageOrMealModal from "../BaggageOrMealModal";
import { Button } from "../../../Helper Classes/Button";
import { colors } from "../../../Helper Classes/Colors";
import { commonstyle } from "../../../Helper Classes/Commonstyle";
import Header from "../../../Helper Classes/CommonHeader";
import { SafeAreaView } from "react-navigation";
import Fonts from "../../../Helper Classes/Fonts";
import { styles } from "./Styles";
import moment from "moment"
import { connect } from "react-redux";
import Flightadata from "../../../json/FlightData";

const screenSize = Dimensions.get("window");

const passengerType = {
  Adult: "Adult",
  child: "Children",
  Infant: "Infant"
};

class PassengerDetails extends Component {
  constructor(props) {
    super(props);
    this.ActionSheet = null;
    this.state = {
      dateSlection: true,
      passengersArray: [],
      adultDict: {
        passenger_type: passengerType.Adult,
        title: "",
        fName: "",
        mName: "",
        lName: "",
        dob: "",
        nationality: "",
        // gender: "",
        dep_beg: "",
        dep_meal: ""
      },
      childDict: {
        passenger_type: passengerType.child,
        title: "",
        fName: "",
        mName: "",
        lName: "",
        dob: "",
        nationality: "",
        // gender: "",
        dep_beg: "",
        dep_meal: ""
      },
      infantDict: {
        passenger_type: passengerType.Infant,
        title: "",
        fName: "",
        mName: "",
        lName: "",
        dob: "",
        nationality: "",
        //   gender: ""
      },

      phoneNumber: "9837389939",
      email: "test123@gmail.com",
      // addressDict: {
      //   addLine1: "",
      //   addLine2: "",
      //   city: "",
      //   countryCode: "",
      //   postalCode: ""
      // },
      titlesArr: ["Mr.", "Mrs.", "Ms.", "Cancel"],
      loading: false,
      isDateTimePickerVisible: false,
      selectedIndexForDob: -1,
      genderIndex: -1,
      spinner: false,
      fareQuotes: [],
      baggageOrMealModal: false,
      isForBaggage: true,
      baggageData: [],
      mealData: [],
      isForDept: true,
      flightType: null,
      origin: null,
      destination: null,
      isRoundTrip: null,
      isInterNational: null,
      fareDetails: null,
      traceId: null,
      passengers: null,
      flightData: null,
    };

    this._renderAdultOrChildView = this._renderAdultOrChildView.bind(this);
    this._renderListItem = this._renderListItem.bind(this);
    this.calculatePassengers = this.calculatePassengers.bind(this);
    this._renderContactDetailsView = this._renderContactDetailsView.bind(this);
    // this._renderAddressDetailsView = this._renderAddressDetailsView.bind(this);
    this.validateEntryFields = this.validateEntryFields.bind(this);
    // this.APIFlightBooking = this.APIFlightBooking.bind(this);
  }

  //----------------- Component Life cycle Methods ----------

  componentDidMount() {

    let { navigation } = this.props;
    // let flightData = Flightadata;
    // let passengers = {
    //   adult: 2,
    //   child: 2,
    //   infant: 1
    // };
    let flightData = navigation.getParam('flightdata', null);
    let passengers = navigation.getParam('passengers', null);
    let traceId = navigation.getParam('traceid', null);
    let flightType = navigation.getParam('flightType', null);
    let origin = navigation.getParam('origin', null);
    let destination = navigation.getParam('destination', null);
    let isRoundTrip = navigation.getParam('isRoundTrip', null);
    let isInterNational = navigation.getParam('isInterNational', null);
    //console.log("DDDeTa",JSON.stringify(flightData))
    this.setState({
      fareDetails: flightData.fare,
      traceId: traceId,
      loading: !this.state.loading,
      passengers: passengers,
      flightData: flightData,
      flightType: flightType,
      origin: origin,
      destination: destination,
      isRoundTrip: isRoundTrip,
      isInterNational: isInterNational
    }, () => {
      this.calculatePassengers(passengers.adult, passengers.child, passengers.infant);
    });
  }

  showActionSheet(index) {
    this.setState({
      selectedIndexForDob: index
    });
    this.ActionSheet.show();
  }

  //-------------- Date Picker Methods ----------

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    console.log("A date has been picked: ", date.dateString);
    let arr = this.state.passengersArray;
    let dict = arr[this.state.selectedIndexForDob];
    //dict.dob = dateFormat(date, "yyyy-mm-dd") + 'T00:00:00';
    dict.dob = moment(date).format("YYYY-MM-DD") + 'T00:00:00';
    arr[this.state.selectedIndexForDob] = dict;
    this.setState({
      passengersArray: arr,
      loading: !this.state.loading
    })
    this._hideDateTimePicker();
  };

  //------------ Custom Methods -----------

  calculatePassengers(numOfAdult, numOfChild, numOfInfant) {
    let arr = [];
    if (numOfAdult > 0) {
      for (i = 0; i < numOfAdult; i++) {
        let dict = {
          passenger_type: passengerType.Adult,
          title: "",
          fName: "",
          mName: "",
          lName: "",
          dob: "",
          nationality: "",
          // gender: "",
          dep_beg: "",
          dep_meal: ""
        };
        if (this.state.flightType == 1) {
          dict["ret_beg"] = "";
          dict["ret_meal"] = "";
        }
        arr.push(dict);
      }
    }
    if (numOfChild > 0) {
      for (i = 0; i < numOfChild; i++) {
        let dict = {
          passenger_type: passengerType.child,
          title: "",
          fName: "",
          mName: "",
          lName: "",
          dob: "",
          nationality: "",
          dep_beg: "",
          dep_meal: ""
        };
        if (this.state.flightType == 1) {
          dict["ret_beg"] = "";
          dict["ret_meal"] = "";
        }
        arr.push(dict);
      }
    }
    if (numOfInfant > 0) {
      for (i = 0; i < numOfInfant; i++) {
        arr.push(this.state.infantDict);
      }
    }
    console.log("Passenger Details  " + JSON.stringify(arr));
    this.setState({
      passengersArray: arr,
      loading: !this.state.loading
    });
  }

  onChangeText(index, text, key) {
    let dict = this.state.passengersArray[index];
    dict[key] = text;
    let arr = this.state.passengersArray;
    arr[index] = dict;
    this.setState({
      passengersArray: arr,
      loading: !this.state.loading
    });
  }

  isEmailValid(text) {
    // console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      // console.log("Email is Not Correct");
      return false;
    } else {
      // console.log("Email is Correct");
      return true;
    }
  }

  validateEntryFields() {
    let arr = this.state.passengersArray;
    let isDetailsValid = true;
    for (index in arr) {
      let item = arr[index];
      if (item.title.trim().length == 0) {
        alert("Please Select Title.");
        isDetailsValid = false;
        return false;
      }
      if (String(item.fName).trim().length == 0) {
        alert("Please Enter First Name.");
        isDetailsValid = false;
        return false;
      }
      if (String(item.lName).trim().length == 0) {
        alert("Please Enter Last Name.");
        isDetailsValid = false;
        return false;
      }
      if (String(item.dob).trim().length == 0) {
        alert("Please Select DOB.");
        isDetailsValid = false;
        return false;
      }

      if (item.passenger_type === "Adult") {
        let age = moment(new Date()).diff(item.dob, 'years');
        if (age <= 12) {
          alert("Adult's age must be greater than 12 years");
          console.warn(age);
          isDetailsValid = false;
          return false;
        }
      }
      if (item.passenger_type === "Children") {
        let age = moment(new Date()).diff(item.dob, 'years');
        if (age > 12 || age < 2) {
          alert("Childs age must be between 2 to 12 years");
          console.warn(age);
          isDetailsValid = false;
          return false;
        }
      }
      if (item.passenger_type === "Infant") {
        let age = moment(new Date()).diff(item.dob, 'years');
        if (age > 2) {
          alert("Infant's age must be Below 2 years");
          console.warn(age);
          isDetailsValid = false;
          return false;
        }
      }
    }

    if (!isDetailsValid) {
      return;
    }

    if (this.state.email.trim().length == 0) {
      alert("Please enter an email.");
      return;
    }

    if (!this.isEmailValid(this.state.email)) {
      alert("Please enter an valid email address.");
      return;
    }

    if (this.state.phoneNumber.trim().length == 0) {
      alert("Please enter phone number.");
      return;
    }
    if (this.state.phoneNumber.trim().length < 10) {
      alert("Please enter valid phone number");
      return;
    }

    NavigationServices.navigate('ReviewFlightBooking', {
      flightdata: this.state.flightData,
      traceid: this.state.traceId,
      passengers: this.state.passengersArray,
      flightType: this.state.flightType,
      email: this.state.email,
      phoneNumber: this.state.phoneNumber,
      origin: this.state.origin,
      destination: this.state.destination,
      isRoundTrip: this.state.isRoundTrip,
      isInterNational: this.state.isInterNational,
    });
  }

  //---------------- API Calling Methids -------------

  // APIConfirmFare(flights) {
  //   let param = {
  //     type: "TBO",
  //     flight: flights,
  //     traceId: this.state.traceId
  //   };

  //   APIManager.confireFareAPI(param, (success, response) => {
  //     if (!success) {
  //       console.log("error" + response);
  //       setTimeout(() => {
  //         alert(response);
  //       }, 100);
  //     } else {
  //       console.log("Fare API " + response);
  //     }
  //   });
  // }

  // APIConfirmQuote(flights) {
  //   let param = {
  //     type: "TBO",
  //     flight: flights,
  //     traceId: this.state.traceId
  //   };

  //   console.log("param for Quotes" + JSON.stringify(param));

  //   this.setState({ spinner: true });

  //   APIManager.confireQuote(param, (success, response) => {
  //     this.setState({ spinner: false });
  //     if (success) {
  //       console.log("Quote API Response" + JSON.stringify(response));
  //       this.setState({
  //         fareQuotes: response
  //       });
  //     } else {
  //       console.log("error" + response);
  //       setTimeout(() => {
  //         Alert.alert("Error", response, [
  //           { text: "OK", onPress: () => Actions.FlightSearch() }
  //         ]);
  //       }, 100);
  //     }
  //   });
  // }

  // APIFlightBooking(indexes) {
  //   let adtDict = this.state.passengersArray[0];
  //   //let addDict = this.state.addressDict;

  //   let finalDict = {};

  //   if (this.props.flightType == 0) {
  //     let fareQuote = this.state.fareQuotes[0].OB.farequote;
  //     // fareQuote["ResultIndex"] = indexes[0];
  //     finalDict["OB"] = {
  //       Status: fareQuote.IsLCC,
  //       Baggage: [],
  //       MealDynamic: [],
  //       FareQuote: fareQuote
  //     };
  //     finalDict["IB"] = {
  //       Status: false,
  //       Baggage: "",
  //       MealDynamic: "",
  //       FareQuote: ""
  //     };
  //   } else {
  //     let OBfareQuote = this.state.fareQuotes[0].OB.farequote;
  //     let IBfareQuote = this.state.fareQuotes[1].IB.farequote;

  //     // OBfareQuote["ResultIndex"] = indexes[0];
  //     // IBfareQuote["ResultIndex"] = indexes[1];
  //     finalDict["OB"] = {
  //       Status: OBfareQuote.IsLCC,
  //       Baggage: [],
  //       MealDynamic: [],
  //       FareQuote: OBfareQuote
  //     };
  //     finalDict["IB"] = {
  //       Status: IBfareQuote.IsLCC,
  //       Baggage: [],
  //       MealDynamic: [],
  //       FareQuote: IBfareQuote
  //     };
  //   }


  //   let param1 = {
  //     email: this.state.email,
  //     mobile: this.state.phoneNumber,
  //     ADT: [
  //       {
  //         title: String(adtDict.title).substring(0, adtDict.title.length - 1),
  //         fname: adtDict.fName,
  //         mname: adtDict.mName,
  //         lname: adtDict.lName,
  //         dob: adtDict.dob,
  //         dep_baggage: "0",
  //         ret_baggage: "0",
  //         dep_meal: "0",
  //         ret_meal: "0",
  //         passport_no: "",
  //         passport_expiry: "",
  //         passportRqd: false
  //       }
  //     ],
  //     CHD: [],
  //     INF: [],
  //     // address: {
  //     //   AddressLine1: addDict.addLine1,
  //     //   AddressLine2: addDict.addLine2,
  //     //   CityName: addDict.city,
  //     //   CountryCode: addDict.countryCode,
  //     //   PostalCode: addDict.postalCode
  //     // },
  //     attributes: {
  //       traceId: this.state.traceId,
  //       index: indexes
  //     },
  //     type: "TBO",
  //     finalData: finalDict,
  //     final_booking_amount: 3250.21
  //   };

  //   console.log(
  //     "Api calling Flight Bookingn Request Param  : " + JSON.stringify(param1)
  //   );

  //   this.setState({ spinner: true });

  //   APIManager.flightBooking(param1, (success, response) => {
  //     this.setState({ spinner: false });

  //     if (success) {
  //       console.log("booked successfully " + JSON.stringify(response));
  //       setTimeout(() => {
  //         Actions.BookingConfirmation({
  //           status: 1,
  //           bookingId: response.booking_id
  //         });
  //       }, 100);
  //     } else {
  //       console.log("error" + response);
  //       setTimeout(() => {
  //         Actions.BookingConfirmation({ status: 0, failedError: response });
  //       }, 100);
  //     }
  //   });
  // }

  //MARK:------------- Modal Switching Method ----------

  setModalVisiable() {
    this.setState({
      baggageOrMealModal: !this.state.baggageOrMealModal
    });
  }


  onModalDonePress(dict) {
    this.setModalVisiable();
    console.log("Selectd item in popup " + JSON.stringify(dict));
  }

  //------------ Render Adult or Child View ----------

  _renderAdultOrChildView(item, index) {
    return (
      <View style={{ padding: 15 }}>
        <CustomTextInput
          placeholder="Title"
          value={item.title}
          tintColor={colors.lightgrey}
          isEditable={false}
          onChangeText={text => { }}
          onResponderStart={() => this.showActionSheet(index)}
        />

        <CustomTextInput
          placeholder="First Name"
          value={item.fName}
          tintColor={colors.lightgrey}
          isEditable={true}
          autoCorrect={false}
          onChangeText={text => this.onChangeText(index, text, "fName")}
        />

        <CustomTextInput
          placeholder="Middle Name"
          value={item.mName}
          tintColor={colors.lightgrey}
          autoCorrect={false}
          isEditable={true}
          onChangeText={text => this.onChangeText(index, text, "mName")}
        />

        <CustomTextInput
          placeholder="Last Name"
          value={item.lName}
          tintColor={colors.lightgrey}
          isEditable={true}
          autoCorrect={false}
          onChangeText={text => this.onChangeText(index, text, "lName")}
        />

        <TouchableOpacity activeOpacity={.8} style={{
          width: "100%",
          alignItems: "center",
          flexDirection: "row",
          paddingLeft: 5,
          paddingBottom: 10,
          borderBottomColor: colors.lightgrey,
          borderBottomWidth: 1
        }} onPress={() => {
          if (this.state.dateSlection) {
            this.setState({
              selectedIndexForDob: index,
              isDateTimePickerVisible: true
            });
          } else {
            this.setState({
              selectedIndexForDob: index,
              isDateTimePickerVisible: true,
              dateSlection: false
            }
            );
          }
        }} >
          <View style={{ width: "100%", flexDirection: "row", justifyContent: "space-between" }}>
            <Text style={{ marginLeft: 5, color: colors.colorBlack, fontFamily: Fonts.medium }}>
              {item.dob === "" ? "DOB" : Moment(item.dob).format("YYYY-MM-DD")}
            </Text>
            <Image style={{ height: 20, width: 20 }} source={Images.imgblackcalender} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  //---------------- Render title View -----------------


  _renderTitleView(title, image) {
    return (
      <View style={styles.cardheader}>
        <Image source={image ? image : Images.imgpayment} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
        <Text style={styles.cardheaderText}>{title}</Text>
      </View>
    );
  }

  //--------- Render Baggage View --------------

  _renderBaggageView(item, index, type) {
    return (
      <View style={styles.styleBaggageContainerView}>
        <TouchableOpacity style={styles.dropButton}
        // onPress={
        //   () => {
        //   if (type == "Departure") {
        //     let ssr = this.state.fareQuotes[0].OB.ssr;
        //     if (!ssr || ssr.ResponseStatus == 2) {
        //       alert(ssr ? ssr.Error.ErrorMessage : "No baggage available");
        //       return;
        //     }
        //     this.setState({
        //       baggageData: ssr.Baggage[0],
        //       isForBaggage: true,
        //       isForDept: true
        //     });
        //   } else {
        //     let ssr = this.state.fareQuotes[1].IB.ssr;
        //     if (!ssr || ssr.ResponseStatus == 2) {
        //       alert(ssr ? ssr.Error.ErrorMessage : "No baggage available");
        //       return;
        //     }
        //     this.setState({
        //       baggageData: ssr.Baggage[0],
        //       isForBaggage: true,
        //       isForDept: false
        //     });
        //   }
        //   this.setModalVisiable();
        // }

        // }
        >
          {/* <View style={styles.styleBaggageDropView}>
              <Text style={[commonstyle.blackText, { fontSize: 9 }]}>0 Kg</Text>

              <Image
                style={{ height: 10, width: 10 }}
                source={Images.imgarrow}
                resizeMode="contain"
              />
            </View> */}
          <Text style={styles.styleBaggageText}>{type} Baggage</Text>
          <Image source={Images.imgbottom}></Image>
        </TouchableOpacity>

        {/* <View style={{ width: "45%", height: "100%" }}> */}
        <TouchableOpacity style={styles.dropButton}
        // onPress={
        //   () => {
        //   if (type == "Departure") {
        //     let ssr = this.state.fareQuotes[0].OB.ssr;
        //     if (!ssr || ssr.ResponseStatus == 2) {
        //       alert(ssr ? ssr.Error.ErrorMessage : "No meal available");
        //       return;
        //     }
        //     this.setState({
        //       mealData: ssr.MealDynamic[0],
        //       isForBaggage: false,
        //       isForDept: true
        //     });
        //   } else {
        //     let ssr = this.state.fareQuotes[1].IB.ssr;
        //     if (!ssr || ssr.ResponseStatus == 2) {
        //       alert(ssr ? ssr.Error.ErrorMessage : "No meal available");
        //       return;
        //     }
        //     this.setState({
        //       mealData: ssr.MealDynamic[0],
        //       isForBaggage: false,
        //       isForDept: false
        //     });
        //   }
        //   this.setModalVisiable();
        // }
        // }

        >
          {/* <View style={styles.styleBaggageDropView}>
              <Text style={[commonstyle.blackText, { fontSize: 9 }]}>No Meal</Text>

              <Image
                style={{ height: 10, width: 10 }}
                source={Images.imgarrow}
                resizeMode="contain"
              />
            </View> */}
          <Text style={styles.styleBaggageText}>{type} Meal</Text>
          <Image source={Images.imgbottom}></Image>
        </TouchableOpacity>
        {/* </View> */}
      </View>
    );
  }

  //---------------- Render Contact Details View -----------

  _renderContactDetailsView() {
    return (

      <View style={styles.itemBackground}>
        {this._renderTitleView("Contact Details", Images.imgcontact)}
        <View style={{ padding: 15 }}>
          <CustomTextInput
            placeholder="Email"
            value={this.state.email}
            tintColor={colors.lightgrey}
            isEditable={true}
            autoCapitalize={"none"}
            autoCorrect={false}
            icon={Images.imgmail}
            contentType="postalCode"
            onChangeText={text => {
              this.setState({
                email: text
              });
            }}
          />

          <CustomTextInput
            placeholder="Phone Number"
            isEditable={true}
            value={this.state.phoneNumber}
            tintColor={colors.lightgrey}
            keyboardType="number-pad"
            contentType="telephoneNumber"
            maxLength={10}
            icon={Images.imgphone}
            onChangeText={text => {
              this.setState({
                phoneNumber: text
              });
            }}
          />
        </View>
      </View>

    );
  }

  //------------ List Item Render Method -------------

  _renderListItem({ item, index }) {
    if (item.passenger_type == passengerType.Infant) {
      return (
        <View style={[styles.itemBackground, { margin: 10 }]}>
          {this._renderTitleView(item.passenger_type, Images.imgpassender)}

          {this._renderAdultOrChildView(item, index)}
        </View>
      );
    }
    return (
      <View style={[styles.itemBackground, { margin: 10 }]}>
        {this._renderTitleView(item.passenger_type, Images.imgpassender)}

        {this._renderAdultOrChildView(item, index)}

        {this._renderBaggageView(item, index, "Departure")}
        {this.state.flightType == 1 ? (
          this._renderBaggageView(item, index, "Return")
        ) : (
            <View />
          )}
      </View>
    );
  }

  //-------- Render Fare Amount View ----------

  _renderFareAmountView(title, fare) {
    return (
      <View style={styles.styleFareAmountView}>
        <Text
          style={{ fontSize: 14, color: "rgb(0,114,198)", fontFamily: "Montserrat-SemiBold" }}
        >
          {title}
        </Text>
        <Text style={[commonstyle.blackText, { fontSize: 14 }]}>{fare}</Text>
      </View>
    );
  }

  //-------------- Render Method --------------------------

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Spinner
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={{ color: colors.colorWhite }}
        />
        <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} />
        <Header title="Traveller Details" />
        <ScrollView contentContainerStyle={styles.mainContainer}>

          <FlatList data={this.state.passengersArray}
            extraData={this.state.loading}
            renderItem={this._renderListItem}
            keyExtractor={(item, index) => index.toString()}
          />

          {this._renderContactDetailsView()}

          {/* {this._renderAddressDetailsView()} */}

          {/* {this._renderFareDetailsView()} */}


          <DateTimePicker
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this._handleDatePicked}
            maximumDate={new Date()}
            onCancel={this._hideDateTimePicker}
          />

          <ActionSheet
            ref={o => (this.ActionSheet = o)}
            title={"Select Title"}
            options={this.state.titlesArr}
            cancelButtonIndex={3}
            //   destructiveButtonIndex={1}
            onPress={index => {
              if (index != 3) {
                let arr = this.state.passengersArray;
                let dict = arr[this.state.selectedIndexForDob];
                dict.title = this.state.titlesArr[index];
                arr[this.state.selectedIndexForDob] = dict;
                this.setState({
                  passengersArray: arr,
                  loading: !this.state.loading
                });
              }
            }}
          />

        </ScrollView>

        <BaggageOrMealModal
          baggageOrMealModal={this.state.baggageOrMealModal}
          baggageData={this.state.baggageData}
          mealData={this.state.mealData}
          isForBaggage={this.state.isForBaggage}
          type={this.state.isForBaggage ? "Baggage" : "Meal"}
          onDonePress={dict => this.onModalDonePress(dict)}
          onCancelPress={() => this.setModalVisiable()}
        />
        <Button
          height={45}
          borderRadius={7}
          onPress={() => { this.validateEntryFields() }}
          text={"Continue"}
        />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    flightSearchParams: state.FlightsReducer.flightSearchParams,
  }
}

export default connect(mapStateToProps, null)(PassengerDetails)