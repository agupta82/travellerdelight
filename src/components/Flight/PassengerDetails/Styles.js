import { StyleSheet } from "react-native"
import { colors } from "../../../Helper Classes/Colors";
import Fonts from "../../../Helper Classes/Fonts";

export const styles = StyleSheet.create({
  dropButton: {
    height: 35, width: "49%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    // backgroundColor: "red",
    paddingLeft: 10, marginBottom: 20,
    paddingRight: 10,
    borderBottomColor: colors.lightgrey,
    borderBottomWidth: 1
  },
  itemBackground: {
    backgroundColor: colors.colorWhite,
    margin: 12,
    marginBottom: 15,
    borderRadius: 10,
    elevation: 5,
    padding: 0,
    shadowColor: colors.colorBlack,
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: 2 },
    shadowRadius: 5,
    // height: 200
    // overflow: "hidden"
  },
  cardheader: {
    height: 40, width: "100%",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "rgb(245,245,245)",
    padding: 10
  },
  cardheaderText: {
    color: colors.colorBlack,
    fontFamily: Fonts.semiBold,
    fontSize: 14, marginLeft: 12
  },
  container: {
    flex: 1,
    backgroundColor: colors.colorBlue,
  },
  mainContainer: {
    // flex: 1,
    backgroundColor: colors.colorWhite,
  },
  styleAdultOrChildViewContainer: {
    width: "100%",
    padding: 15,
    shadowColor: "grey",
    shadowOpacity: 0.5,
    shadowOffset: { width: 1, height: 1 },
    shadowRadius: 5,
    backgroundColor: "white",
    borderRadius: 7,
    marginBottom: 10,
    elevation: 6
  },
  styleDobAndNationalityContainer: {
    flexDirection: "row",
    height: 55,
    width: "100%",
    justifyContent: "space-between"
  },
  styleGenderContainerView: {
    flexDirection: "row",
    height: 40,
    alignItems: "center",
    padding: 5
  },
  styleBaggageContainerView: {
    padding: 10,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  styleGenderView: {
    marginLeft: 15,
    flexDirection: "row",
    alignItems: "center"
  },
  styleGenderMarkBtn: {
    height: 30,
    width: 30,
    justifyContent: "center",
    alignItems: "center"
  },
  styleTitleViewContainer: {
    height: 30,
    width: "100%",
    justifyContent: "center",
    marginLeft: 5
  },
  styleBaggageText: {
    height: 20,
    marginTop: 5,
    fontSize: 14,
    color: colors.colorBlack,
    fontFamily: Fonts.medium
  },
  styleBaggageDropView: {
    height: "100%",
    width: "100%",
    borderRadius: 5,
    borderColor: "grey",
    borderWidth: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 5,
    paddingRight: 5
  },
  styleFareAmountView: {
    height: 30,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 5,
    paddingRight: 5
  }
});
