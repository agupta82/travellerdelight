import {StyleSheet,Dimensions,Platform} from "react-native";
import { colors } from "../../../Helper Classes/Colors";

export const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "rgba(244,248,254,1)"
    },
    styleSearchView: {
      
      marginLeft: 10,
      marginRight: 10,
      marginBottom: 30,
      borderRadius: 10,
      backgroundColor: "white",
      marginTop: 5,
      shadowColor: "black",
      shadowOpacity: 0.3,
      shadowOffset: { width: 5, height: 2 },
      shadowRadius: 5,
      elevation: 10
    },
    styleAirportNameSuperView: {
      width: "100%",
      flexDirection: "row",
      // justifyContent: "space-between",
      alignItems: "center"
    },
    styleNonStopFlightView: {
      height: 20,
      flexDirection: "row",
      alignItems: "center",
      marginBottom: 30
    },
    styleSearchFlightBtnView: 
      {
      margin:10,
      width:"92%",
      height:50,padding :10,
      backgroundColor:colors.colorBlue,
      borderRadius:7,
      justifyContent:"center",
      alignItems:"center",
      elevation:2,
      shadowColor: "black",
      shadowOpacity: 0.3,
      shadowOffset: { width: 1, height: 1 },
      shadowRadius: 3,
    
    }
  });