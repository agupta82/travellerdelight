
import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity, ScrollView,
  StatusBar,
  Image,
  Dimensions, DeviceEventEmitter
} from "react-native";

import Fonts from '../../../Helper Classes/Fonts';
import Spinner from 'react-native-loading-spinner-overlay';
import Images from "../../../Helper Classes/Images";
//import HotelBookingDetails from "./Hotel/HotelBookingDetail/HotelBookingDetails";
import { connect } from "react-redux";
import { colors } from "../../../Helper Classes/Colors";
import SelectAirPortModel from "../../../Modals/SelectAirport/SelectAirPortModal";
import CalendarModal from "../../../Modals/Calendar/CalendarModal";
import SelectTravellerModal from "../../../Modals/SelectTraveller/SelectTravellerModal";
import { saveFlightSearchParams } from "../../../Redux/Actions/Actions";
import styles from "./Styles";
import NavigationServices from "../../../Helper Classes/NavigationServices";
import SelectFlightClass from "../SelectFlightClass";
import StringConstants from "../../../Helper Classes/StringConstants";
import moment from 'moment';
import CardView from 'react-native-cardview';
import SubmitButton from "../../../Helper Classes/SubmitButton";

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

class FlightSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cities: [{
        id: 1
      }],
      tripType: 1,
      selectedStopage: 1,
      openedCalendar: false,
      openedTravellerModal: false,
      openedFlightClass: false,
      oneWayActive: true,
      // departDate: new Date(),
      isForHotel: false,
      forReturnDate: false,
      //old keys
      // departDate: (new Date()).setDate((new Date).getDate() + 10),
      departDate: new Date(),
      returnDate: null,
      oneWayActive: true,
      sourceDict: {
        airport_name: "Delhi Indira Gandhi Intl",
        country_code: "IN",
        country_name: "INDIA",
        city_name: "Delhi",
        airport_code: "DEL"
      },
      destinationDict: {
        airport_name: "Chhatrapati Shivaji",
        country_code: "IN",
        country_name: "INDIA",
        city_name: "Mumbai",
        airport_code: "BOM"
      },
      selectTravellersModel: false,
      selectClassModel: false,
      numberOfAdults: 1,
      numberOfChilds: 0,
      numberOfInfant: 0,
      selectedClassIndex: 1,
      selectedClassName: 'All',
      disabled: false,
      isNonStop: true
    }
  }
  //-----------------Components Life cycle Methods ---------------

  componentDidMount() {

    console.log("flight search")
    DeviceEventEmitter.addListener(StringConstants.AIRPORT_SELECT, data => {
      if (data.isSource) {
        this.setState({ sourceDict: data });
      } else {
        this.setState({ destinationDict: data });
      }
    });

    DeviceEventEmitter.addListener(StringConstants.DATE_SELECT, dict => {
      if (dict.returnDate && !this.state.oneWayActive) {
        this.setState({
          departDate: dict.departDate,
          returnDate: dict.returnDate,
          oneWayActive: false
        });
      } else {
        this.setState({
          departDate: dict.departDate,
          returnDate: null,
          oneWayActive: true
        });
      }

    });

    DeviceEventEmitter.addListener(StringConstants.TRAVELLER_SELECT, dict => {
      if (dict) {
        this.setState({
          numberOfAdults: dict.adultsNum,
          numberOfChilds: dict.childNum,
          numberOfInfant: dict.infantNum
        })
      }
    });

    DeviceEventEmitter.addListener(StringConstants.TRAVELLER_CLASS, dict => {
      if (dict) {
        this.setState({
          selectedClassIndex: dict.selectedIndex,
          selectedClassName: dict.selectedClass
        })
      }
    });

    let airportsData = this.props.airPortListData;
    if (airportsData && airportsData.length != 0) {
      let srcArr = airportsData.filter(data => {
        return data.airport_code == "DEL";
      });
      let destArr = airportsData.filter(data => {
        return data.airport_code == "BOM";
      });
      this.setState({
        sourceDict: srcArr[0],
        destinationDict: destArr[0]
      });
    }
  }

  closeFlightClass = () => {
    this.setState({ openedFlightClass: false });
  }

  closeAirportModal = () => {
    this.setState({ openedAirport: false });
  }

  closeCalendarModal = () => {
    this.setState({ openedCalendar: false });
  }

  closeTravellerModal = () => {
    this.setState({ openedTravellerModal: false });
  }

  getDate(date) {
    //return dateFormat(date, "ddd, mmm dS");
    return moment(date).format("DD MMM");
  }

  returnNumbersOfTravellersString() {
    let numAdults = this.state.numberOfAdults
    let numChild = this.state.numberOfChilds
    let numInfant = this.state.numberOfInfant
    let str = numAdults + ' Adult'

    if (numChild > 0) {
      str = str + ',' + numChild + ' Child'
    }

    if (numInfant > 0) {
      str = str + ',' + numInfant + ' Infant'
    }

    return str
  }

  tripTypeSlectionFunctio(status) {
    if (status == false) {
      let depart_date = new Date(this.state.departDate);
      let return_Date = new Date(this.state.departDate);
      return_Date.setDate(depart_date.getDate() + 1)
      this.setState({ oneWayActive: status, returnDate: return_Date })
    }
    else {
      this.setState({ oneWayActive: status, returnDate: null })
    }
  }

  swithOrigins = () => {
    let { sourceDict, destinationDict } = this.state;
    let a = Object.assign({}, sourceDict);
    let b = Object.assign({}, destinationDict);
    let tempDict = Object.assign({}, a);
    a = Object.assign({}, b);
    b = Object.assign({}, tempDict);
    this.setState({
      sourceDict: a,
      destinationDict: b
    })
  }

  renderSingleCityCardView = () => {
    let { sourceDict, destinationDict, tripType, selectedClassName, returnDate, isNonStop, selectedClassIndex,
      departDate, oneWayActive, numberOfAdults, numberOfChilds, numberOfInfant } = this.state;
    return (
      <View style={{ paddingBottom: 50, width: "100%", alignItems: "center" }}>
        <CardView style={styles.card}>
          <View style={styles.cardRow}>
            <TouchableOpacity style={styles.cardBtn} onPress={() => {
              // this.setState({ openedAirport: true })
              NavigationServices.navigate('SearchAirport', { isSource: true, forFlightSearch: true });
            }}>
              <Text style={styles.cardText}>From</Text>
              <Text style={styles.stnCode}>{sourceDict.airport_code}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconButton} onPress={() => {
              this.swithOrigins();
            }}>
              <Image source={Images.imgswitchflight}
                style={styles.icon}></Image>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.cardBtn, { paddingLeft: 30 }]} onPress={() => {
              NavigationServices.navigate('SearchAirport', { isSource: false, forFlightSearch: true });
            }}>
              <Text style={styles.cardText}>To</Text>
              <Text style={styles.stnCode}>{destinationDict.airport_code}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.border}></View>
          <View style={styles.cardRow}>
            <TouchableOpacity style={styles.cardBtn} onPress={() => {
              requestAnimationFrame(() => {
                NavigationServices.navigate('Calender', {
                  isForHotel: false,
                  isOneWayActive: oneWayActive,
                  departDate,
                  forReturnDate: false
                })
              })
            }}>
              <Text style={styles.cardText}>Departure</Text>
              <Text style={[styles.stnCode, {
                fontFamily: Fonts.medium
              }]}>{this.getDate(departDate)}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconButton} disabled={true}>
              <Image source={Images.imgCalender}
                style={styles.icon}></Image>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.cardBtn, { paddingLeft: 10 }]} onPress={() => {
              requestAnimationFrame(() => {
                NavigationServices.navigate('Calender', {
                  isForHotel: false,
                  departDate: this.state.departDate,
                  isOneWayActive: oneWayActive,
                  forReturnDate: true,
                  returnDate: this.state.returnDate
                })
              })
            }} disabled={oneWayActive}>
              <Text style={styles.cardText}>Return</Text>
              {
                returnDate != null ?
                  <Text style={[styles.stnCode, {
                    fontFamily: Fonts.medium
                  }]}>{this.getDate(returnDate)}</Text> :
                  <Text style={styles.selection}>Select Date</Text>
              }
            </TouchableOpacity>
          </View>
          <View style={styles.border}></View>
          <View style={styles.cardRow}>
            <TouchableOpacity style={styles.cardBtn} onPress={() => this.setState({ openedTravellerModal: true })}>
              <Text style={styles.cardText}>Passengers</Text>
              <Text style={[styles.stnCode, {
                fontFamily: Fonts.medium
              }]}>{numberOfAdults + numberOfChilds + numberOfInfant}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconButton}>
              <Image source={Images.imguser}
                style={styles.icon}></Image>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.cardBtn, { paddingLeft: 10 }]} onPress={() => this.setState({ openedFlightClass: true })}>
              <Text style={styles.cardText}>Class</Text>
              <Text style={styles.selection}>{selectedClassName == "All" ? "Select Class" : selectedClassName}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.border}></View>
          <View style={styles.classContainer}>
            <TouchableOpacity style={[styles.classButton, {
              backgroundColor: isNonStop ? colors.colorBlue : colors.lightBlue,
            }]} onPress={() => this.setState({ isNonStop: !isNonStop })}>
              <Text style={[styles.classBtnText, {
                color: isNonStop ? colors.colorWhite : colors.colorBlack
              }]}>NONSTOP</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.classButton, {
              marginHorizontal: 13,
              backgroundColor: !isNonStop ? colors.colorBlue : colors.lightBlue,
            }]} onPress={() => this.setState({ isNonStop: !isNonStop })}>
              <Text style={[styles.classBtnText, {
                color: !isNonStop ? colors.colorWhite : colors.colorBlack
              }]}>MULTISTOP</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity style={[styles.classButton, {
              backgroundColor: selectedStopage == 3 ? colors.colorBlue : colors.lightBlue,
            }]} onPress={() => this.setState({ selectedStopage: 3 })}>
              <Text style={[styles.classBtnText, {
                color: selectedStopage == 3 ? colors.colorWhite : colors.colorBlack
              }]}>First Class</Text>
            </TouchableOpacity> */}
          </View>
        </CardView >
        <SubmitButton marginTop={-25} onPress={() => {
          requestAnimationFrame(() => {
            let dict = {
              origin: sourceDict.airport_code,
              destination: destinationDict.airport_code,
              departure_date: moment(this.state.departDate).format('YYYY-MM-DD'),
              return_date: (returnDate == null) ? null : moment(returnDate).format('YYYY-MM-DD'),
              FlightCabinClass: selectedClassIndex,
              ADT: numberOfAdults,
              CHD: numberOfChilds,
              INF: numberOfInfant,
              non_stop: isNonStop,
              tripType
            };
            this.props.saveFlightSearchParams(dict);
            sourceDict.airport_code == destinationDict.airport_code ? alert("Arrival and destination cannot be the same") : NavigationServices.navigate('FlightList', { tripType });
          });
        }} />
      </View>
    )
  }

  renderMultiCityCardView = () => {
    let { selectedStopage, cities } = this.state;
    return (
      <View style={{ paddingBottom: 50, width: "100%", alignItems: "center" }}>
        <CardView style={styles.multiCardView}>
          {
            cities.map((city, index) => (
              <View style={{ width: "100%" }} key={city.id}>
                <View style={[styles.cardRow]}>
                  <TouchableOpacity style={styles.miltiCitycardbtn} onPress={() => this.setState({ openedAirport: true })}>
                    <Text style={styles.cardText}>From</Text>
                    <Text style={styles.stnCode}>DEL</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.iconButton}>
                    <Image source={Images.imgswitchflight}
                      style={styles.icon}></Image>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.miltiCitycardbtn} onPress={() => this.setState({ openedAirport: true })}>
                    <Text style={styles.cardText}>To</Text>
                    <Text style={styles.stnCode}>BOM</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.iconButton}>
                    <Image source={Images.imgCalender}
                      style={styles.icon}></Image>
                  </TouchableOpacity>
                  <TouchableOpacity style={[styles.miltiCitycardbtn, { width: 70 }]} onPress={() => this.setState({ openedAirport: true })}>
                    <Text style={styles.cardText}>Date</Text>
                    <Text style={styles.stnCode}>10 Dec</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.border}></View>
              </View>
            ))
          }
          <TouchableOpacity style={styles.addCityBtn} onPress={() => {
            let cities = this.state.cities;
            cities.push({ id: cities.length + 1 });
            this.setState({ cities });
          }}>
            <Text style={styles.addCityText}>+ <Text> ADD CITY</Text></Text>
          </TouchableOpacity>
          <View style={styles.border}></View>
          <View style={styles.cardRow}>
            <TouchableOpacity style={styles.cardBtn} onPress={() => this.setState({ openedTravellerModal: true })}>
              <Text style={styles.cardText}>Passengers</Text>
              <Text style={[styles.stnCode, {
                fontFamily: Fonts.medium
              }]}>2</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconButton}>
              <Image source={Images.imguser}
                style={styles.icon}></Image>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.cardBtn, { paddingLeft: 10 }]} onPress={() => this.setState({ openedFlightClass: true })}>
              <Text style={styles.cardText}>Class</Text>
              <Text style={styles.selection}>Select Class</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.border}></View>
          <View style={styles.classContainer}>
            <TouchableOpacity style={[styles.classButton, {
              backgroundColor: selectedStopage == 1 ? colors.colorBlue : colors.lightBlue,
            }]} onPress={() => this.setState({ selectedStopage: 1 })}>
              <Text style={[styles.classBtnText, {
                color: selectedStopage == 1 ? colors.colorWhite : colors.colorBlack
              }]}>Economy</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.classButton, {
              marginHorizontal: 13,
              backgroundColor: selectedStopage == 2 ? colors.colorBlue : colors.lightBlue,
            }]} onPress={() => this.setState({ selectedStopage: 2 })}>
              <Text style={[styles.classBtnText, {
                color: selectedStopage == 2 ? colors.colorWhite : colors.colorBlack
              }]}>Buisness</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.classButton, {
              backgroundColor: selectedStopage == 3 ? colors.colorBlue : colors.lightBlue,
            }]} onPress={() => this.setState({ selectedStopage: 3 })}>
              <Text style={[styles.classBtnText, {
                color: selectedStopage == 3 ? colors.colorWhite : colors.colorBlack
              }]}>First Class</Text>
            </TouchableOpacity>
          </View>
        </CardView>
        <SubmitButton marginTop={-25} onPress={() => {
          alert("Comming soon..."); return
          requestAnimationFrame(() => {
            let dict = {
              origin: sourceDict.airport_code,
              destination: destinationDict.airport_code,
              departure_date: moment(this.state.departDate).format('YYYY-MM-DD'),
              return_date: (returnDate == null) ? null : moment(returnDate).format('YYYY-MM-DD'),
              FlightCabinClass: selectedClassIndex,
              ADT: numberOfAdults,
              CHD: numberOfChilds,
              INF: numberOfInfant,
              non_stop: isNonStop,
              tripType
            };
            this.props.saveFlightSearchParams(dict);
            sourceDict.airport_code == destinationDict.airport_code ? alert("Arrival and destination cannot be the same") : NavigationServices.navigate('FlightList', { tripType });
          });
        }} />
      </View>
    )
  }

  render() {
    let { tripType, selectedClassIndex, openedTravellerModal, isSource, forFlightSearch,
      departDate, openedCalendar, isForHotel, forReturnDate, openedFlightClass } = this.state;
    let { isLoading } = this.props;
    return (
      <View style={styles.container}>
        <SelectFlightClass modalVisible={openedFlightClass}
          closeModal={this.closeFlightClass}
          selectedClassIndex={selectedClassIndex} />

        <SelectTravellerModal modalVisible={openedTravellerModal}
          closeModal={this.closeTravellerModal}
          numberOfAdults={this.state.numberOfAdults}
          numberOfChilds={this.state.numberOfChilds}
          numberOfInfant={this.state.numberOfInfant} />

        <Spinner visible={isLoading}
          textContent={"Loading..."}
          textStyle={{ color: 'white' }}
        />
        <StatusBar barStyle="light-content" />
        <Image source={Images.imghomebg}
          style={{
            height: screenHeight / 2.5,
            width: screenWidth,
            resizeMode: "stretch"
          }}></Image >
        <View style={styles.mainContainer}>
          <View style={styles.header}>
            <TouchableOpacity style={styles.headerBackButton} onPress={() => NavigationServices.goBack()}>
              <Image source={Images.imgback} style={{ width: 15, height: 15, resizeMode: "contain" }}></Image>
              <Text style={{
                color: colors.colorWhite,
                fontFamily: Fonts.semiBold
              }}> Back</Text>
            </TouchableOpacity>
            <Text style={styles.headerTitle}>Search Flight</Text>
            <TouchableOpacity style={styles.headerButton}>
              <Image source={Images.imgnotifications} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
            </TouchableOpacity>
          </View>
          <View style={styles.tabs}>
            <TouchableOpacity style={[styles.tabButton, {
              backgroundColor: tripType == 1 ? colors.colorGold : colors.colorWhite,
            }]} onPress={() => {
              this.setState({ tripType: 1, oneWayActive: true });
              this.tripTypeSlectionFunctio(true)
            }}>
              <Text style={[styles.tabText, {
                color: tripType == 1 ? colors.colorWhite : colors.colorBlack
              }]}>One Way</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.tabButton, {
              backgroundColor: tripType == 2 ? colors.colorGold : colors.colorWhite,
              borderLeftWidth: 1, borderRightWidth: 1, borderColor: "lightgrey"
            }]} onPress={() => {
              this.setState({ tripType: 2, oneWayActive: false });
              this.tripTypeSlectionFunctio(false)
            }}>
              <Text style={[styles.tabText, {
                color: tripType == 2 ? colors.colorWhite : colors.colorBlack
              }]}>Round Trip</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.tabButton, {
              backgroundColor: tripType == 3 ? colors.colorGold : colors.colorWhite,
            }]} onPress={() => this.setState({ tripType: 3, oneWayActive: false })}>
              <Text style={[styles.tabText, {
                color: tripType == 3 ? colors.colorWhite : colors.colorBlack
              }]}>Multi City</Text>
            </TouchableOpacity>
          </View>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1 }}>
            {
              tripType == 3 ?
                this.renderMultiCityCardView() :
                this.renderSingleCityCardView()
            }
            <View style={{ height: 200 }}></View>
          </ScrollView>
        </View>
      </View >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
    userData: state.userDataReducer,
    isLogin: state.isLoginReducer,
    userToken: state.userTokenReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveFlightSearchParams: (data) => dispatch(saveFlightSearchParams(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FlightSearch)
