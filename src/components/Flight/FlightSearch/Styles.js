import { StyleSheet, Dimensions, Platform } from "react-native";
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import { colors } from "../../../Helper Classes/Colors";

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default Styles = StyleSheet.create({
  addCityText: {
    color: colors.colorGold,
    fontFamily: Fonts.bold,
    fontSize: 14
  },
  addCityBtn: {
    height: 30, width: screenWidth,
    marginTop: -10,
    justifyContent: "center",
    alignItems: "center"
  },
  classBtnText: {
    fontFamily: Fonts.medium,
    fontSize: 14, letterSpacing: .5,
  },
  submitBtn: {
    width: 60,
    backgroundColor: "red",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    elevation: 5,
    shadowColor: colors.colorBlack,
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: 2 },
    shadowRadius: 5,
  },
  submitContainer: {
    width: 60,
    height: 60,
    alignItems: "center",
    justifyContent: "center",
    // marginTop: 20,
    position: "absolute",
    bottom: -20,
    zIndex: 99
  },
  classButton: {
    backgroundColor: colors.colorBlue,
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },
  classContainer: {
    width: "100%",
    height: 60,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around"
  },
  border: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgrey",
    marginTop: 10, marginBottom: 20
  },
  selection: {
    fontFamily: Fonts.regular,
    fontSize: 12,
    color: colors.colorBlack,
    lineHeight: 25
  },
  miltiCitycardbtn: {
    height: "100%",
    minWidth: 50,
  },
  cardBtn: {
    height: "100%",
    minWidth: 90,
    // backgroundColor: "red"
  },
  icon: {
    width: 30, height: 30,
    resizeMode: "contain",
    marginTop: 7
  },
  iconButton: {
    width: 50,
    height: "100%",
    paddingTop: 5,
    alignItems: "center",
  },
  stnCode: {
    fontFamily: Fonts.bold,
    fontSize: 20,
    color: colors.colorBlue,
    lineHeight: 25
  },
  cardText: {
    fontFamily: Fonts.medium,
    fontSize: 14,
    color: colors.colorBlack,
    lineHeight: 25
  },
  cardRow: {
    width: "100%",
    height: 70,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  multiCardView: {
    width: screenWidth - 24,
    // height: 430,
    backgroundColor: colors.colorWhite,
    borderRadius: 10,
    // overflow: "hidden",
    marginTop: 25,
    padding: 15,
    paddingBottom: 35,
    borderColor: "lightgrey",
    borderWidth: 1,
    alignItems: "center",
    elevation: 5,
    shadowColor: colors.colorBlack,
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: 2 },
    shadowRadius: 5,
  },
  card: {
    width: screenWidth - 30,
    height: 430,
    backgroundColor: colors.colorWhite,
    borderRadius: 10,
    // overflow: "hidden",
    marginTop: 25,
    padding: 15,
    borderColor: colors.lightgrey,
    borderWidth: 1,
    alignItems: "center",
    elevation: 5,
    shadowColor: colors.colorBlack,
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: 2 },
    shadowRadius: 5,
  },
  tabs: {
    height: 40,
    width: screenWidth - 24,
    borderRadius: 10,
    flexDirection: "row",
    overflow: "hidden",
    borderWidth: .5,
    borderColor: "lightgrey",
    marginTop: 20,
    elevation: 5,
    shadowColor: colors.colorBlack,
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: 2 },
    shadowRadius: 5,
  },
  tabButton: {
    height: "100%",
    width: (screenWidth - 24) / 3,
    alignItems: "center",
    justifyContent: "center"
  },
  tabText: {
    color: colors.colorBlack,
    fontSize: 14,
    fontFamily: Fonts.medium
  },
  headerBackButton: {
    width: 70,
    // backgroundColor: "red",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    height: 45,
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  mainContainer: {
    position: "absolute",
    top: Platform.OS == "ios" ? (getStatusBarHeight() + 5) : 0,
    width: screenWidth,
    height: screenHeight,
    flex: 1,
    left: 0,
    alignItems: "center",
    // backgroundColor: "red"
  },
  headerButton: {
    height: "100%",
    width: 50,
    // backgroundColor: "blue",
    justifyContent: "center",
    alignItems: "center"
  },
  headerTitle: {
    color: colors.colorWhite,
    fontSize: 16,
    // fontWeight: "600"
    fontFamily: Fonts.bold
  },
  container: {
    flex: 1,
    //backgroundColor: "rgba(244,248,254,1)"
    backgroundColor: "#FDFDFD"
  },
});