import React, { Component } from "react";
import { Text, View, StatusBar, Image, TouchableHighlight, ScrollView, DeviceEventEmitter, SafeAreaView, Modal, TouchableOpacity } from "react-native";
import NavigationServices from '../../../Helper Classes/NavigationServices'
import Images from "../../../Helper Classes/Images";
import Dash from "react-native-dash";
import NavigationBar from "../../../Helper Classes/NavigationBar";
import Constants from "../../../Helper Classes/Constants";
import SelectTraveller from '../SelectTraveller'
import SelectFlightClass from '../SelectFlightClass'
import { colors } from "../../../Helper Classes/Colors";
import { commonstyle } from "../../../Helper Classes/Commonstyle";
import { connect } from "react-redux"
import { saveFlightSearchParams } from "../../../Redux/Actions/Actions";
import { styles } from "./Styles1"

import moment from 'moment';

class FlightSearch1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      departDate: new Date(),
      oneWayActive: true,
      sourceDict: {
        airport_name: "Delhi Indira Gandhi Intl",
        country_code: "IN",
        country_name: "INDIA",
        city_name: "Delhi",
        airport_code: "DEL"
      },
      destinationDict: {
        airport_name: "Chhatrapati Shivaji",
        country_code: "IN",
        country_name: "INDIA",
        city_name: "Mumbai",
        airport_code: "BOM"
      },
      returnDate: null,
      selectTravellersModel: false,
      selectClassModel: false,
      numberOfAdults: 1,
      numberOfChilds: 0,
      numberOfInfant: 0,
      selectedClassIndex: 1,
      selectedClassName: 'All',
      disabled: false,
      isNonStop: false
    };

    this._renderDepartAndReturnView = this._renderDepartAndReturnView.bind(
      this
    );
    this._renderFlightPlateforms = this._renderFlightPlateforms.bind(this);
    this._renderTravellerDetailsAndClassView = this._renderTravellerDetailsAndClassView.bind(
      this
    );
    this.returnNumbersOfTravellersString = this.returnNumbersOfTravellersString.bind(this)
  }

  //------------------------Components Life cycle methods -------------------

  componentDidMount() {

    console.log("inflightsearcg")
    DeviceEventEmitter.addListener("airportSelect", data => {
      if (data.isSource) {
        this.setState({ sourceDict: data });
      } else {
        this.setState({ destinationDict: data });
      }
    });

    DeviceEventEmitter.addListener("dateSelect", dict => {
      if (dict.returnDate != null) {
        this.setState({
          departDate: dict.departDate,
          returndate: dict.returnDate,
          oneWayActive: false
        });
        //Actions.refresh();
      } else {
        this.setState({
          departDate: dict.departDate,
          returndate: null,
          oneWayActive: true
        });
        //Actions.refresh();
      }

    });

    DeviceEventEmitter.addListener("TravellerSelected", dict => {
      this.setModalVisible(false, true)
      if (dict) {
        this.setState({
          numberOfAdults: dict.adultsNum,
          numberOfChilds: dict.childNum,
          numberOfInfant: dict.infantNum
        })
      }
    });

    DeviceEventEmitter.addListener("TravellerClass", dict => {
      this.setModalVisible(false, false)
      if (dict) {
        this.setState({
          selectedClassIndex: dict.selectedIndex,
          selectedClassName: dict.selectedClass
        })
      }
    });



    let airportsData = this.props.airPortListData;
    if (airportsData && airportsData.length != 0) {
      let srcArr = airportsData.filter(data => {
        return data.airport_code == "DEL";
      });
      let destArr = airportsData.filter(data => {
        return data.airport_code == "BOM";
      });
      this.setState({
        sourceDict: srcArr[0],
        destinationDict: destArr[0]
      });
    }

    // Actions.refresh({onBack:()=>Actions.Login()})

  }

  getDate(date) {
    //return dateFormat(date, "ddd, mmm dS");
    return moment(date).format("ddd, MMM Do");
  }

  disabledMultipleTap() {

    this.setState({
      disabled: true,
    });

    setTimeout(() => {
      this.setState({
        disabled: false,
      });
    }, 3000)

  }

  _renderFlightPlateforms() {
    return (
      <View style={styles.styleAirportNameSuperView}>
        <TouchableHighlight
          underlayColor="transparent"
          onPress={() => {
            NavigationServices.navigate('SearchAirport', { isSource: true, forFlightSearch: true })
          }}
        >
          <View style={{ margin: 15, flex: 1 }}>
            <Text style={[commonstyle.greyBold, { fontSize: 13 }]}>
              From
            </Text>

            <Text
              style={[commonstyle.blackSemiBold, {
                fontSize: 16,
                marginTop: 5
              }]}
            >
              {this.state.sourceDict.airport_code}
            </Text>

            <Text style={[commonstyle.blackText, { fontSize: 10, marginTop: 5 }]}>
              {this.state.sourceDict.city_name}
            </Text>
          </View>
        </TouchableHighlight>

        <Image
          style={{ flex: 1 }}
          source={Images.imgsearchflight}
          resizeMode="contain"
        />

        <TouchableHighlight
          underlayColor="transparent"
          onPress={() => {
            NavigationServices.navigate('SearchAirport', { isSource: false, forFlightSearch: true })
          }}
        >
          <View style={{ margin: 15, flex: 1 }}>
            <Text
              style={[commonstyle.greyBold, {
                fontSize: 13,
                textAlign: "right"
              }]}
            >
              To
            </Text>

            <Text
              style={[commonstyle.blackSemiBold, {
                fontSize: 16,
                marginTop: 5,
                textAlign: "right"
              }]}
            >
              {this.state.destinationDict.airport_code}
            </Text>

            <Text
              style={[commonstyle.blackText, {
                fontSize: 10,

                marginTop: 5,
                textAlign: "right"
              }]}
            >
              {this.state.destinationDict.city_name}
            </Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }

  _renderDepartAndReturnView() {
    return (
      <View
        style={[
          styles.styleAirportNameSuperView,
          { marginTop: 10, justifyContent: "space-between" }
        ]}
      >
        <TouchableHighlight
          style={{ margin: 15, flex: 1 }}
          disabled={this.state.disabled}
          underlayColor="transparent"
          onPress={() => {
            this.disabledMultipleTap();
            requestAnimationFrame(() => {
              NavigationServices.navigate('Calender', { isForHotel: false, isOneWayActive: this.state.oneWayActive, departDate: this.state.departDate, forReturnDate: false })
            })

          }}
        >
          <View>
            <Text style={[commonstyle.greySemiBold, { fontSize: 13 }]}>
              Depart
            </Text>

            <Text
              style={[commonstyle.blackSemiBold, {
                fontSize: 15,
                marginTop: 5,

              }]}
            >
              {this.getDate(this.state.departDate)}
            </Text>
          </View>
        </TouchableHighlight>

        <Dash
          style={{
            height: "98.5%",
            width: 1,
            flexDirection: "column"
          }}
          dashColor="gray"
          dashLength={8}
        />

        <TouchableHighlight style={{ margin: 15, flex: 1 }}
          underlayColor={"transparent"}
          disabled={this.state.disabled}
          onPress={() => {
            this.disabledMultipleTap()
            requestAnimationFrame(() => {
              NavigationServices.navigate('Calender', { isForHotel: false, departDate: this.state.departDate, isOneWayActive: this.state.oneWayActive, forReturnDate: true, returnDate: this.state.returndate })
            })
          }}>
          <View >
            <Text
              style={[commonstyle.greySemiBold, {
                fontSize: 13,
                textAlign: "right"
              }]}
            >
              Return
            </Text>
            {this.state.oneWayActive ?
              <Text
                style={{
                  fontSize: 10,
                  color: "rgba(25,89,189,1)",
                  fontFamily: "Montserrat-Bold",
                  marginTop: 5,
                  textAlign: "right"
                }}
              >
                Book Round Trip to Save extra
            </Text> : <Text
                style={[commonstyle.blackSemiBold, {
                  fontSize: 15,
                  marginTop: 5,

                  textAlign: "right"
                }]}
              >
                {this.getDate(this.state.returndate)}
              </Text>}
          </View>
        </TouchableHighlight>
      </View>
    );
  }

  setModalVisible(visible, isTraveller) {
    console.log("in set modal")
    if (isTraveller) {
      this.setState({ selectTravellersModel: visible });
    }
    else {
      this.setState({ selectClassModel: visible });
    }

  }

  tripTypeSlectionFunctio(status) {
    if (status == false) {
      let depart_date = new Date(this.state.departDate);
      let return_Date = new Date(this.state.departDate);
      return_Date.setDate(depart_date.getDate() + 1)
      this.setState({ oneWayActive: status, returndate: return_Date })
    }
    else {
      this.setState({ oneWayActive: status, returndate: null })
    }
  }

  returnNumbersOfTravellersString() {
    let numAdults = this.state.numberOfAdults
    let numChild = this.state.numberOfChilds
    let numInfant = this.state.numberOfInfant
    let str = numAdults + ' Adult'

    if (numChild > 0) {
      str = str + ',' + numChild + ' Child'
    }

    if (numInfant > 0) {
      str = str + ',' + numInfant + ' Infant'
    }

    return str
  }

  _renderTravellerDetailsAndClassView() {
    return (
      <View
        style={[
          styles.styleAirportNameSuperView,
          { marginTop: 10, marginBottom: 20, justifyContent: "space-between" }
        ]}
      >
        <TouchableHighlight
          style={{ margin: 15, flex: 1 }}
          underlayColor='transparent'
          onPress={() => {
            this.setModalVisible(true, true)
          }}
        >
          <View >
            <Text style={[commonstyle.greySemiBold, { fontSize: 13 }]}>
              Travellers
            </Text>

            <Text
              style={[commonstyle.blackSemiBold, {
                fontSize: 15,

                marginTop: 5
              }]}
            >
              {this.returnNumbersOfTravellersString()}
            </Text>
          </View>
        </TouchableHighlight>

        <Dash
          style={{
            height: "95.5%",
            width: 1,
            flexDirection: "column",

          }}
          dashColor="gray"
          dashLength={8}
        />

        <TouchableHighlight
          style={{ margin: 15, flex: 1 }}
          underlayColor='transparent'
          onPress={() => {
            this.setModalVisible(true, false)
          }}
        >
          <View style={{ flex: 1, alignItems: "flex-end" }}>
            <Text
              style={[commonstyle.greySemiBold, {
                fontSize: 13,

                textAlign: "right"
              }]}
            >
              Class
            </Text>

            <Text
              style={[commonstyle.blackSemiBold, {
                fontSize: 14,
                marginTop: 5,
                textAlign: "right"
              }]}
            >
              {this.state.selectedClassName}
            </Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }

  renderOneWayRoundTrip() {
    return (
      <View style={{
        flexDirection: 'row', elevation: 6, height: 42, alignSelf: "center", width: '80%', backgroundColor: 'white', borderRadius: 20, position: "absolute", bottom: 30,
        shadowColor: "black",
        shadowOpacity: 0.3,
        shadowOffset: { width: 1, height: 1 },
        shadowRadius: 3
      }}>
        <View style={{ width: '50%' }}>
          {this.state.oneWayActive ?
            <TouchableOpacity style={{ width: '100%', height: '100%', backgroundColor: 'rgba(25,89,189,1)', justifyContent: 'center', alignItems: 'center', borderRadius: 20 }}  >
              <Text style={{ color: 'white', fontSize: 14, fontFamily: "Montserrat-SemiBold" }}>One Way</Text>
            </TouchableOpacity> :
            <TouchableOpacity style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}
              onPress={() => { this.tripTypeSlectionFunctio(true) }}>
              <Text style={{ color: colors.colorBlue, fontSize: 14, fontFamily: "Montserrat-SemiBold" }}>One Way</Text>
            </TouchableOpacity>
          }
        </View>
        <View style={{ width: '50%' }}>
          {this.state.oneWayActive ?
            <TouchableOpacity style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }} onPress={() => { this.tripTypeSlectionFunctio(false) }}>
              <Text style={{ color: colors.colorBlue, fontSize: 14, fontFamily: "Montserrat-SemiBold" }}>Round Trip</Text>
            </TouchableOpacity> :
            <TouchableOpacity style={{ width: '100%', height: '100%', backgroundColor: 'rgba(25,89,189,1)', justifyContent: 'center', alignItems: 'center', borderRadius: 20 }} onPress={() => { }}>
              <Text style={{ color: 'white', fontSize: 14, fontFamily: "Montserrat-SemiBold" }}>Round Trip</Text>
            </TouchableOpacity>
          }
        </View>
      </View>
    )
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="light-content" />
        <NavigationBar title="Search" showBack={true} />
        <ScrollView>
          <View style={{ height: 150, width: "100%" }}>
            <Image
              style={{ height: 150, width: "100%", position: "absolute" }}
              source={Images.imgworldmap}
              resizeMode="cover"
            />
            {this.renderOneWayRoundTrip()}
          </View>

          <View style={styles.styleSearchView}>
            {this._renderFlightPlateforms()}

            <Dash
              style={{
                height: 1,
                width: "98.5%",
                marginTop: 10,
                marginLeft: 4
              }}
              dashColor="gray"
              dashLength={8}
            />

            {this._renderDepartAndReturnView()}

            <Dash
              style={{
                height: 1,
                width: "98.5%",
                marginTop: 10,
                marginLeft: 4
              }}
              dashColor="gray"
              dashLength={8}
            />

            {this._renderTravellerDetailsAndClassView()}

            <View style={styles.styleNonStopFlightView}>
              <TouchableHighlight
                style={{ width: 25, height: 25, marginLeft: 10 }}
                underlayColor={"transparent"}
                onPress={() => { this.setState({ isNonStop: !this.state.isNonStop }) }}
              >
                <Image
                  style={{ width: 20, height: 20, marginTop: 2.5 }}
                  source={this.state.isNonStop ? Images.imgcheck : Images.imguncheck}
                  resizeMode="contain"
                />
              </TouchableHighlight>
              <Text style={[commonstyle.blackText, { fontSize: 14, marginLeft: 10 }]}>
                Non Stop Fight Only
              </Text>
            </View>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.selectTravellersModel}>
              < SelectTraveller
                numberOfAdults={this.state.numberOfAdults}
                numberOfChilds={this.state.numberOfChilds}
                numberOfInfant={this.state.numberOfInfant} />
            </Modal>
            <Modal

              animationType="slide"
              transparent={true}
              visible={this.state.selectClassModel}>

              <SelectFlightClass selectedClassIndex={this.state.selectedClassIndex} />
            </Modal>

          </View>
        </ScrollView>
        {/* <TouchableHighlight
          style={styles.styleSearchFlightBtnView}
          underlayColor="transparent"
          disabled={this.state.disabled}
          onPressIn={() => { */}
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          {/* <Button */}
          <TouchableOpacity
            style={styles.styleSearchFlightBtnView}
            height={50}
            disabled={this.state.disabled}
            onPress={() => {

              this.disabledMultipleTap();

              requestAnimationFrame(() => {

                let dict = {
                  origin: this.state.sourceDict.airport_code,
                  destination: this.state.destinationDict.airport_code,
                  departure_date: moment(this.state.departDate).format('YYYY-MM-DD'),
                  return_date: (this.state.returndate == null) ? null : moment(this.state.returndate).format('YYYY-MM-DD'),
                  FlightCabinClass: this.state.selectedClassIndex,
                  ADT: this.state.numberOfAdults,
                  CHD: this.state.numberOfChilds,
                  INF: this.state.numberOfInfant,
                  non_stop: this.state.isNonStop
                };
                this.props.saveFlightSearchParams(dict);
                this.state.sourceDict.airport_code == this.state.destinationDict.airport_code ? alert("Arrival and destination cannot be the same") : NavigationServices.navigate('FlightList');


                // Actions.ReviewFlightBooking();
              });

            }}
          >
            <Text style={{ color: colors.colorWhite, fontFamily: "Montserrat-SemiBold", fontSize: 16 }}>
              Search Flight
          </Text>
          </TouchableOpacity>
        </View>

        {/* <Text style={{ fontSize: 16, fontWeight: "700", color: "white", fontFamily: "Montserrat-Regular" }}>
            Search Flight
              </Text>
        </TouchableHighlight> */}

      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoadingReducer,
    airPortListData: state.airPortListDataReducer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveFlightSearchParams: (data) => dispatch(saveFlightSearchParams(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FlightSearch1)