import React, { Component } from "react";
import {
  Animated,
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList,
  Image,
  TouchableHighlight,
  ScrollView
} from "react-native";
import NavigationBar from "../Helper Classes/NavigationBar";
import Images from "../Helper Classes/Images";

export default class MakePayment extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  _renderWalletView() {
    return (
      <View style={styles.styleTotalPriceSuperView}>
        <View style={styles.styleTotalPriceView}>
          <Text style={{ fontSize: 16, color: "black", fontWeight: "600" }}>
            Wallet
          </Text>
          <Text style={{ fontSize: 10, color: "gray" }}>
            current balance is $152
          </Text>
        </View>

        <View
          style={{
            width: "30%",
            justifyContent: "center",
            alignItems: "flex-end"
          }}
        >
          <TouchableHighlight>
            <Image
              style={{ height: 25, width: 25 }}
              source={Images.imgnextArrow}
              resizeMode="center"
            />
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  _renderCardView(title) {
    return (
      <View style={styles.styleTotalPriceSuperView}>
        <View style={styles.styleCardImageView}>
          <Image
            style={{ height: 40, width: 40 }}
            source={Images.imgCard}
            resizeMode="contain"
          />
        </View>

        <View
          style={{
            width: "70%",
            height: "100%",
            alignItems: "center",
            flexDirection: "row",
            justifyContent: 'space-between'
          }}
        >
          <Text style={{ fontSize: 15, color: "black", fontWeight: "600" }}>
            {title}
          </Text>

          <TouchableHighlight>
            <Image
              style={{ height: 25, width: 25 }}
              source={Images.imgnextArrow}
              resizeMode="center"
            />
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />
        <NavigationBar title="Make Payment" showBack={true} />
        <ScrollView>
          <View style={styles.styleTotalPriceSuperView}>
            <View style={styles.styleTotalPriceView}>
              <Text style={{ fontSize: 16, color: "black", fontWeight: "600" }}>
                Total Price
              </Text>
              <Text style={{ fontSize: 10, color: "gray" }}>
                (Convienience fee of Rs300 is addedd)
              </Text>
            </View>

            <View
              style={{
                width: "25%",
                justifyContent: "center",
                alignItems: "flex-end"
              }}
            >
              <Text style={{ fontSize: 14, color: "black", fontWeight: "500" }}>
                $152
              </Text>
            </View>
          </View>

          <View style={{ margin: 20 }}>
            <Text style={{ fontSize: 15, color: "gray", fontWeight: "500" }}>
              Select Paymet Option
            </Text>

            {this._renderWalletView()}

            {this._renderCardView("Credit Card")}

            {this._renderCardView("Debit Card")}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(244,248,254,1)"
  },
  styleTotalPriceSuperView: {
    height: 60,
    width: "100%",
    marginTop: 15,
    flexDirection: "row",
    backgroundColor: "white"
  },
  styleTotalPriceView: {
    marginLeft: 20,
    width: "60%",
    height: "100%",
    justifyContent: "center"
    // alignItems:'center'
  },
  styleCardImageView: {
    width: "20%",
    height: "100%",
    justifyContent: "center",
    marginLeft: 20
  }
});
