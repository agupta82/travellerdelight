export default data = [
  {
    "index": "OB53",
    "origin": "DEL",
    "destination": "BOM",
    "isLCC": true,
    "airlineCode": "SG",
    "airlineName": "SpiceJet",
    "isRefundable": true,
    "source": "TBO",
    "direction": 0,
    "tripType": "OB53",
    "flightSegment": [
      [
        {
          "baggage": "15 KG",
          "cabinBaggage": "7 KG",
          "tripIndicator": 1,
          "airline": {
            "airlineCode": "SG",
            "airlineName": "SpiceJet",
            "flightNumber": "169",
            "fareClass": "V",
            "image": "/var/www/travel_portal/htdocs/public/airline_logos/SG.png"
          },
          "seatAvailable": 15,
          "origin": {
            "airport": {
              "airportCode": "DEL",
              "airportName": "Indira Gandhi Airport",
              "terminal": "1D",
              "cityCode": "DEL",
              "cityName": "Delhi",
              "countryCode": "IN",
              "countryName": "India"
            },
            "depTime": "2019-04-05T19:45:00"
          },
          "destination": {
            "airport": {
              "airportCode": "BOM",
              "airportName": "Mumbai",
              "terminal": "1",
              "cityCode": "BOM",
              "cityName": "Mumbai",
              "countryCode": "IN",
              "countryName": "India"
            }
          },
          "duration": 140,
          "stopOver": false,
          "stopPoint": "",
          "stopPointArrivalTime": "2019-04-05T22:05:00",
          "stopPointDepartureTime": "2019-04-05T19:45:00",
          "remark": null,
          "isETicketEligible": true,
          "flightStatus": "Confirmed"
        }
      ]
    ],
    "fareDetails": [
      {
        "currency": "INR",
        "passengerType": 1,
        "passengerCount": 1,
        "baseFare": 2500,
        "tax": 528,
        "fuelCharge": 0,
        "AdditionalTxnFeeOfrd": 0,
        "AdditionalTxnFeePub": 0,
        "PGCharge": 0
      }
    ],
    "fareRules": [
      {
        "airline": "SG",
        "fareCode": "VSAVER",
        "fareRuleDetail": "",
        "fareRestriction": ""
      }
    ],
    "fare": {
      "currency": "INR",
      "baseFare": 2500,
      "tax": 528,
      "fuelCharge": 0,
      "markupFare": 2883.83,
      "otherCharges": 0,
      "discount": 144.17000000000007,
      "commissionEarned": 63.75,
      "optionalCharge": 0,
      "serviceFee": 0,
      "totalBaggageCharges": 0,
      "totalMealCharges": 0,
      "totalSeatCharges": 0,
      "enukeMarkup": 0,
      "enukeFare": 2883.83,
      "agencyMarKup": 0,
      "agencyFare": 2883.83,
      "offeredFare": 2883.83,
      "publishedFare": 3029.71,
      "websiteFare": null
    }
  },
  {
    "index": "OB54",
    "origin": "DEL",
    "destination": "BOM",
    "isLCC": true,
    "airlineCode": "SG",
    "airlineName": "SpiceJet",
    "isRefundable": true,
    "source": "TBO",
    "direction": 0,
    "tripType": "OB54",
    "flightSegment": [
      [
        {
          "baggage": "15 KG",
          "cabinBaggage": "7 KG",
          "tripIndicator": 1,
          "airline": {
            "airlineCode": "SG",
            "airlineName": "SpiceJet",
            "flightNumber": "159",
            "fareClass": "V",
            "image": "/var/www/travel_portal/htdocs/public/airline_logos/SG.png"
          },
          "seatAvailable": 15,
          "origin": {
            "airport": {
              "airportCode": "DEL",
              "airportName": "Indira Gandhi Airport",
              "terminal": "1D",
              "cityCode": "DEL",
              "cityName": "Delhi",
              "countryCode": "IN",
              "countryName": "India"
            },
            "depTime": "2019-04-05T21:35:00"
          },
          "destination": {
            "airport": {
              "airportCode": "BOM",
              "airportName": "Mumbai",
              "terminal": "1",
              "cityCode": "BOM",
              "cityName": "Mumbai",
              "countryCode": "IN",
              "countryName": "India"
            }
          },
          "duration": 144,
          "stopOver": false,
          "stopPoint": "",
          "stopPointArrivalTime": "2019-04-05T23:59:00",
          "stopPointDepartureTime": "2019-04-05T21:35:00",
          "remark": null,
          "isETicketEligible": true,
          "flightStatus": "Confirmed"
        },
        {
          "baggage": "15 KG",
          "cabinBaggage": "7 KG",
          "tripIndicator": 1,
          "airline": {
            "airlineCode": "SG",
            "airlineName": "SpiceJet",
            "flightNumber": "159",
            "fareClass": "V",
            "image": "/var/www/travel_portal/htdocs/public/airline_logos/SG.png"
          },
          "seatAvailable": 15,
          "origin": {
            "airport": {
              "airportCode": "DEL",
              "airportName": "Indira Gandhi Airport",
              "terminal": "1D",
              "cityCode": "DEL",
              "cityName": "Delhi",
              "countryCode": "IN",
              "countryName": "India"
            },
            "depTime": "2019-04-05T21:35:00"
          },
          "destination": {
            "airport": {
              "airportCode": "BOM",
              "airportName": "Mumbai",
              "terminal": "1",
              "cityCode": "BOM",
              "cityName": "Mumbai",
              "countryCode": "IN",
              "countryName": "India"
            }
          },
          "duration": 144,
          "stopOver": false,
          "stopPoint": "",
          "stopPointArrivalTime": "2019-04-05T23:59:00",
          "stopPointDepartureTime": "2019-04-05T21:35:00",
          "remark": null,
          "isETicketEligible": true,
          "flightStatus": "Confirmed"
        },
        {
          "baggage": "15 KG",
          "cabinBaggage": "7 KG",
          "tripIndicator": 1,
          "airline": {
            "airlineCode": "SG",
            "airlineName": "SpiceJet",
            "flightNumber": "159",
            "fareClass": "V",
            "image": "/var/www/travel_portal/htdocs/public/airline_logos/SG.png"
          },
          "seatAvailable": 15,
          "origin": {
            "airport": {
              "airportCode": "DEL",
              "airportName": "Indira Gandhi Airport",
              "terminal": "1D",
              "cityCode": "DEL",
              "cityName": "Delhi",
              "countryCode": "IN",
              "countryName": "India"
            },
            "depTime": "2019-04-05T21:35:00"
          },
          "destination": {
            "airport": {
              "airportCode": "BOM",
              "airportName": "Mumbai",
              "terminal": "1",
              "cityCode": "BOM",
              "cityName": "Mumbai",
              "countryCode": "IN",
              "countryName": "India"
            }
          },
          "duration": 144,
          "stopOver": false,
          "stopPoint": "",
          "stopPointArrivalTime": "2019-04-05T23:59:00",
          "stopPointDepartureTime": "2019-04-05T21:35:00",
          "remark": null,
          "isETicketEligible": true,
          "flightStatus": "Confirmed"
        }
      ]
    ],
    "fareDetails": [
      {
        "currency": "INR",
        "passengerType": 1,
        "passengerCount": 1,
        "baseFare": 2500,
        "tax": 528,
        "fuelCharge": 0,
        "AdditionalTxnFeeOfrd": 0,
        "AdditionalTxnFeePub": 0,
        "PGCharge": 0
      }
    ],
    "fareRules": [
      {
        "airline": "SG",
        "fareCode": "VSAVER",
        "fareRuleDetail": "",
        "fareRestriction": ""
      }
    ],
    "fare": {
      "currency": "INR",
      "baseFare": 2500,
      "tax": 528,
      "fuelCharge": 0,
      "markupFare": 2883.83,
      "otherCharges": 0,
      "discount": 144.17000000000007,
      "commissionEarned": 63.75,
      "optionalCharge": 0,
      "serviceFee": 0,
      "totalBaggageCharges": 0,
      "totalMealCharges": 0,
      "totalSeatCharges": 0,
      "enukeMarkup": 0,
      "enukeFare": 2883.83,
      "agencyMarKup": 0,
      "agencyFare": 2883.83,
      "offeredFare": 2883.83,
      "publishedFare": 3029.71,
      "websiteFare": null
    }
  }];