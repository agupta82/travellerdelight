export default hotel = { "hotel": { "id": "1415067", "index": "46", "name": "Zostel Delhi", "type": "3 Hotel", "star": 3, "description": "<p>HeadLine : In New Delhi (Paharganj)</p><p>Location : With a stay at Zostel Delhi in New Delhi (Paharganj), you ll be within a 5-minute drive of Chandni Chowk and Jama Masjid.  This hotel is 2.3 mi (3.7 km) from Red Fort and 3 mi (4.8 km) from India Gate.</p><p>Rooms : Make yourself at home in one of the 39 air-conditioned guestrooms. Complimentary wireless Internet access keeps you connected, and cable programming is available for your entertainment. Private bathrooms with showers feature complimentary toiletries and hair dryers. Conveniences include separate sitting areas and ceiling fans, and housekeeping is provided daily.</p><p>Dining : Take advantage of the hotel s room service (during limited hours). Continental breakfasts are available for a fee.</p><p>CheckIn Instructions : Extra-person charges may apply and vary depending on property policy. <br />Government-issued photo identification and a credit card, debit card, or cash deposit are required at check-in for incidental charges. <br />Special requests are subject to availability upon check-in and may incur additional charges. Special requests cannot be guaranteed.  <ul><li>No onsite parking is available. </li>Please note that cultural norms and guest policies may differ by country and by property. The policies listed are provided by the property. </ul></p>&nbsp;<br/><b>Disclaimer notification: Amenities are subject to availability and may be chargeable as per the hotel policy.</b>&nbsp; <br />", "photo": null, "images": ["https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzHnLpxgUoJaLSq8bf6H9XIsKNMob0wW+c=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzV+YAHV/BMvU7OzkhOG5kk21qFssWCPxA=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnxjIaty2cZ8Tb7pSnHmolFbQOV1L3BRI50=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnxu50jzh8AQsob5+NL8O6OyJ0aaXUTEHvk=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnwN6Z57y1xGbO4E1rgbjDd3bK49SvtJnAM=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnxPIn638r+urhfLz8MTu1KYqckhG6bpwDI=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnwN6Z57y1xGbHHdN1gq6DA+8kT6nsXdJ2U=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzHnLpxgUoJaP9vrzvPDsPVFq8SxprofOg=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzHnLpxgUoJaOI+IAFtw2r3M1tZtUpYUjs=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzD/iYZ47sjpKC1WPKmqlEsJ7zZnJE5Q0w=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfny/dKwohAWUnSqxYMIaCIoVl/zQDcSnpq8=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnxPIn638r+urp3a7OfWqUCRRKN6zlhvKVU=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnyfvEHJNueyIJxqJasl9W7lCtbz3ji6gC0=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnyfvEHJNueyIHUsx6U8aLD9yDeyhSV7mGU=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzV+YAHV/BMvZwRcYn2y37cnsRBXMIJ0bo=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzHnLpxgUoJaLilwsEJIQ3ZjxHuYUPwtgw=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfny/dKwohAWUnV+ImeFTsDQF1mC0DmBaukU=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfny/dKwohAWUnTXrMm6idGXmEAMkaUZQxA0=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzHnLpxgUoJaFojIBjK2nipykJWxiHGOsc=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnxjIaty2cZ8TYXACzWXGJuzIunIxGVbjQ0=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzD/iYZ47sjpCpRw/M6noMrhsHBCf9BnTc=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnxPIn638r+urnxy6Kt3uyGtTq+qgnL2y8o=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnxLJONBPhzFlgptWFiJwbGQGmmQJdvg0jw=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzV+YAHV/BMvd9lC77KSP5DvfdZpIK3aCU=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzD/iYZ47sjpNvFiFnLmFSs5qX1LzczfPg=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzD/iYZ47sjpPUQB48lmKNFJ558t/Qtyoo=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfny/dKwohAWUnRJWW+yRGOM2ESzOAbiqmE8=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzTdLqhkbdadDWFR+O762FTVVzfj8iRa/Y=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfny/dKwohAWUnYHne1o2AgkVumBzqNWNqPA=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnwN6Z57y1xGbHrWjI244/2aKHGGf0GMY9A=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnxLJONBPhzFlhEerJYVp0uMXNPWdZUzATs=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzV+YAHV/BMvZBdVU+SnRO2tuFteVFEzrk=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnx2ssfa104M3fEK+q3yHqUln1EcTjnsNog=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnxu50jzh8AQsjhCrDbFh6Y6ll7VKUQsagw=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnyfvEHJNueyIJ7xF4Ok9vGVReVAjjnQkLc=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzD/iYZ47sjpK+0AoGPZo9WAnEvpZI3Zsg=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnxPIn638r+urtQNSV4Um7H8b566a4Jm0po=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnxjIaty2cZ8TTv++OKvv2SI4/AJsgRX/R8=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnzhQTZNM8gvYdVcF8lSjK5iKelKZAKPuhk=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnwd935MrTgjf9yPVXm1Whg3vNJbL8gFLHc=", "https://api.tbotechnology.in/imageresource.aspx?img=FbrGPTrju5e5v0qrAGTD8pPBsj8/wYA5WgddmFh/ywMQ4k4M5e2I0zJVI5ODLDXuqkQ9i5jCfnxu50jzh8AQssTSC4ZuEDbSyMj0k3Tnv+k="], "latitude": "28.645748", "longitude": "77.216806", "amenities": [{ "Key": "1) ", "Value": "Distances are displayed to the nearest 0.1 mile and kilometer. <br /> <p>Ramakrishna Mission - 1.5 km / 0.9 mi <br /> Palika Bazaar - 1.8 km / 1.1 mi <br /> Jama Masjid - 2 km / 1.2 mi <br /> Kasturba Gandhi Marg - 2.1 km / 1.3 mi <br /> Gole Market - 2.2 km / 1.4 mi <br /> Ajmal Khan Road - 2.3 km / 1.4 mi <br /> Lady Hardinge Medical College - 2.4 km / 1.5 mi <br /> Jantar Mantar - 2.5 km / 1.5 mi <br /> Gurudwara Sis Ganj Temple - 2.7 km / 1.7 mi <br /> Chandni Chowk - 2.7 km / 1.7 mi <br /> Laxminarayan Temple - 2.8 km / 1.7 mi <br /> Red Fort - 3 km / 1.9 mi <br /> Gurudwara Bangla Sahib - 3 km / 1.9 mi <br /> Sir Ganga Ram Hospital - 3.7 km / 2.3 mi <br /> Raj Ghat - 3.8 km / 2.3 mi <br /> </p><p>The preferred airport for Zostel Delhi is Delhi (DEL-Indira Gandhi Intl.) - 21.1 km / 13.1 mi </p>" }], "facilities": ["Express check-in", "Library", "Luggage storage", "Elevator/lift", "Rooftop terrace", "Breakfast available (surcharge)", "Concierge services", "Designated smoking areas", "24-hour front desk", "Business center", "Laundry facilities", "Tours/ticket assistance", "Accessible bathroom", "One meeting room", "Dry cleaning/laundry service", "Free WiFi", "Arcade/game room", "In-room accessibility", "Airport transportation (surcharge)", "Free wired Internet", "Supervised childcare/activities (free)", "Free newspapers in lobby"], "policy": null, "instructions": null, "address": "5 Arakashan Road Near New Delhi Railway Station New Delhi Delhi 110005, Opposite New Delhi Railway Station110005 New Delhi, New Delhi, 110005, India, India", "pin": "110005", "city": "", "state": "", "country": "India", "phone": "91-022-48962267", "services": null }, "rooms": { "roomlist": [{ "ChildCount": 0, "RequireAllPaxDetails": false, "RoomId": 0, "RoomStatus": 0, "RoomIndex": 1, "RoomTypeCode": "6362029|6bfcbc98-25af-e45b-121d-b09293afd4d8|1^^1^^168954||6bfcbc98-25af-e45b-121d-b09293afd4d8~!:~1~!:~1", "RoomDescription": "", "RoomTypeName": "1 Person In 6-Bed Dormitory - Female Only", "RatePlanCode": "168954||6bfcbc98-25af-e45b-121d-b09293afd4d8", "RatePlan": 13, "InfoSource": "FixedCombination", "SequenceNo": "TH~~1415067~1", "DayRates": [{ "Amount": 507.66, "Date": "2019-12-26T00:00:00" }], "IsPerStay": false, "SupplierPrice": null, "Price": { "CurrencyCode": "INR", "RoomPrice": 507.66, "Tax": 0, "ExtraGuestCharge": 0, "ChildCharge": 0, "OtherCharges": 5.06, "Discount": 0, "PublishedPrice": 522.84, "PublishedPriceRoundedOff": 523, "OfferedPrice": 512.72, "OfferedPriceRoundedOff": 513, "AgentCommission": 10.12, "AgentMarkUp": 0, "ServiceTax": 0.72, "TDS": 4.34, "ServiceCharge": 0, "TotalGSTAmount": 0.72, "GST": { "CGSTAmount": 0, "CGSTRate": 0, "CessAmount": 0, "CessRate": 0, "IGSTAmount": 0.72, "IGSTRate": 18, "SGSTAmount": 0, "SGSTRate": 0, "TaxableAmount": 5 }, "EnukeFare": 512.72, "AgencyFare": 512.72, "AgencyMarKup": 0, "OthrCharges": 5.060000000000002 }, "RoomPromotion": "", "Amenities": [], "Amenity": [], "SmokingPreference": "NoPreference", "BedTypes": [], "HotelSupplements": [], "LastCancellationDate": "2019-12-25T23:59:59", "CancellationPolicies": [{ "Charge": 100, "ChargeType": 2, "Currency": "INR", "FromDate": "2019-12-26T00:00:00", "ToDate": "2019-12-27T23:59:59" }], "CancellationPolicy": "1 Person In 6-Bed Dormitory - Female Only#^#100.00% of total amount will be charged, If cancelled between 26-Dec-2019 00:00:00 and 27-Dec-2019 23:59:59.|#!#", "Inclusion": [], "IsPassportMandatory": false, "IsPANMandatory": true }, { "ChildCount": 0, "RequireAllPaxDetails": false, "RoomId": 0, "RoomStatus": 0, "RoomIndex": 2, "RoomTypeCode": "3639582|b3d20391-148a-4e49-41ed-55c8fc3e7017|1^^1^^168954||b3d20391-148a-4e49-41ed-55c8fc3e7017~!:~2~!:~1", "RoomDescription": "", "RoomTypeName": "Standard Room", "RatePlanCode": "168954||b3d20391-148a-4e49-41ed-55c8fc3e7017", "RatePlan": 13, "InfoSource": "FixedCombination", "SequenceNo": "TH~~1415067~2", "DayRates": [{ "Amount": 1532.69, "Date": "2019-12-26T00:00:00" }], "IsPerStay": false, "SupplierPrice": null, "Price": { "CurrencyCode": "INR", "RoomPrice": 1532.69, "Tax": 184.38, "ExtraGuestCharge": 0, "ChildCharge": 0, "OtherCharges": 5.06, "Discount": 0, "PublishedPrice": 1732.26, "PublishedPriceRoundedOff": 1732, "OfferedPrice": 1722.14, "OfferedPriceRoundedOff": 1722, "AgentCommission": 10.12, "AgentMarkUp": 0, "ServiceTax": 0.72, "TDS": 4.34, "ServiceCharge": 0, "TotalGSTAmount": 0.72, "GST": { "CGSTAmount": 0, "CGSTRate": 0, "CessAmount": 0, "CessRate": 0, "IGSTAmount": 0.72, "IGSTRate": 18, "SGSTAmount": 0, "SGSTRate": 0, "TaxableAmount": 5 }, "EnukeFare": 1722.14, "AgencyFare": 1722.14, "AgencyMarKup": 0, "OthrCharges": 5.07000000000005 }, "RoomPromotion": "", "Amenities": [], "Amenity": [], "SmokingPreference": "NoPreference", "BedTypes": [], "HotelSupplements": [], "LastCancellationDate": "2019-12-25T23:59:59", "CancellationPolicies": [{ "Charge": 100, "ChargeType": 2, "Currency": "INR", "FromDate": "2019-12-26T00:00:00", "ToDate": "2019-12-27T23:59:59" }], "CancellationPolicy": "Standard Room#^#100.00% of total amount will be charged, If cancelled between 26-Dec-2019 00:00:00 and 27-Dec-2019 23:59:59.|#!#", "Inclusion": [], "IsPassportMandatory": false, "IsPANMandatory": true }, { "ChildCount": 0, "RequireAllPaxDetails": false, "RoomId": 0, "RoomStatus": 0, "RoomIndex": 3, "RoomTypeCode": "3639584|b1fbac22-fb79-3948-9eab-dcd1d5f8dc1f|1^^1^^168954||b1fbac22-fb79-3948-9eab-dcd1d5f8dc1f~!:~3~!:~1", "RoomDescription": "", "RoomTypeName": "Deluxe Room", "RatePlanCode": "168954||b1fbac22-fb79-3948-9eab-dcd1d5f8dc1f", "RatePlan": 13, "InfoSource": "FixedCombination", "SequenceNo": "TH~~1415067~3", "DayRates": [{ "Amount": 1918.16, "Date": "2019-12-26T00:00:00" }], "IsPerStay": false, "SupplierPrice": null, "Price": { "CurrencyCode": "INR", "RoomPrice": 1918.16, "Tax": 230.66, "ExtraGuestCharge": 0, "ChildCharge": 0, "OtherCharges": 5.06, "Discount": 0, "PublishedPrice": 2164.01, "PublishedPriceRoundedOff": 2164, "OfferedPrice": 2153.89, "OfferedPriceRoundedOff": 2154, "AgentCommission": 10.12, "AgentMarkUp": 0, "ServiceTax": 0.72, "TDS": 4.34, "ServiceCharge": 0, "TotalGSTAmount": 0.72, "GST": { "CGSTAmount": 0, "CGSTRate": 0, "CessAmount": 0, "CessRate": 0, "IGSTAmount": 0.72, "IGSTRate": 18, "SGSTAmount": 0, "SGSTRate": 0, "TaxableAmount": 5 }, "EnukeFare": 2153.89, "AgencyFare": 2153.89, "AgencyMarKup": 0, "OthrCharges": 5.069999999999794 }, "RoomPromotion": "", "Amenities": [], "Amenity": [], "SmokingPreference": "NoPreference", "BedTypes": [], "HotelSupplements": [], "LastCancellationDate": "2019-12-25T23:59:59", "CancellationPolicies": [{ "Charge": 100, "ChargeType": 2, "Currency": "INR", "FromDate": "2019-12-26T00:00:00", "ToDate": "2019-12-27T23:59:59" }], "CancellationPolicy": "Deluxe Room#^#100.00% of total amount will be charged, If cancelled between 26-Dec-2019 00:00:00 and 27-Dec-2019 23:59:59.|#!#", "Inclusion": [], "IsPassportMandatory": false, "IsPANMandatory": true }, { "ChildCount": 0, "RequireAllPaxDetails": false, "RoomId": 0, "RoomStatus": 0, "RoomIndex": 4, "RoomTypeCode": "3639586|c1a07938-6c6e-aace-2229-148ca4ee7aa7|1^^1^^168954||c1a07938-6c6e-aace-2229-148ca4ee7aa7~!:~4~!:~1", "RoomDescription": "", "RoomTypeName": "Designer Suite", "RatePlanCode": "168954||c1a07938-6c6e-aace-2229-148ca4ee7aa7", "RatePlan": 13, "InfoSource": "FixedCombination", "SequenceNo": "TH~~1415067~4", "DayRates": [{ "Amount": 2552.15, "Date": "2019-12-26T00:00:00" }], "IsPerStay": false, "SupplierPrice": null, "Price": { "CurrencyCode": "INR", "RoomPrice": 2552.15, "Tax": 307.31, "ExtraGuestCharge": 0, "ChildCharge": 0, "OtherCharges": 5.06, "Discount": 0, "PublishedPrice": 2874.65, "PublishedPriceRoundedOff": 2875, "OfferedPrice": 2864.52, "OfferedPriceRoundedOff": 2865, "AgentCommission": 10.12, "AgentMarkUp": 0, "ServiceTax": 0.72, "TDS": 4.34, "ServiceCharge": 0, "TotalGSTAmount": 0.72, "GST": { "CGSTAmount": 0, "CGSTRate": 0, "CessAmount": 0, "CessRate": 0, "IGSTAmount": 0.72, "IGSTRate": 18, "SGSTAmount": 0, "SGSTRate": 0, "TaxableAmount": 5 }, "EnukeFare": 2864.52, "AgencyFare": 2864.52, "AgencyMarKup": 0, "OthrCharges": 5.059999999999889 }, "RoomPromotion": "", "Amenities": [], "Amenity": [], "SmokingPreference": "NoPreference", "BedTypes": [], "HotelSupplements": [], "LastCancellationDate": "2019-12-25T23:59:59", "CancellationPolicies": [{ "Charge": 100, "ChargeType": 2, "Currency": "INR", "FromDate": "2019-12-26T00:00:00", "ToDate": "2019-12-27T23:59:59" }], "CancellationPolicy": "Designer Suite#^#100.00% of total amount will be charged, If cancelled between 26-Dec-2019 00:00:00 and 27-Dec-2019 23:59:59.|#!#", "Inclusion": [], "IsPassportMandatory": false, "IsPANMandatory": true }], "combination": { "InfoSource": "FixedCombination", "IsPolicyPerStay": false, "RoomCombination": [{ "RoomIndex": [1] }, { "RoomIndex": [2] }, { "RoomIndex": [3] }, { "RoomIndex": [4] }] } } }

 //{
//   "hotel": {
//     "id": "00000608",
//     "name": "Maidens Hotel",
//     "type": "5 Hotel",
//     "star": 5,
//     "description": "<br /><b>Property Description:</b> <br />&nbsp;Maiden's Hotel is situated in the heart of Old Delhi a property proudly presented by Trident. It is adorned with 55 exquisite rooms & a conference room equipped with audio visual facilities. Guests can savor some fine dining options at the hotel which include, The Curzon Room that serves European & Indian cuisine, The Garden Terrace which is an informal coffee shop & Cavalry Bar serving an array of cocktails. Guests can stay fit by availing recreational options like the outdoor pool & fitness center&nbsp; <br />",
//     "photo": null,
//     "images": [
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1498204604/Hotel/New Delhi/00000608/1632822-Overview_ylZWGI.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079766/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Restaurant.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079736/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Board_room.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079738/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Deluxe_suite.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079746/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Heritage_room.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079756/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Lutyens_suite.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079758/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Luxury_suite.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079770/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Swimming_pool.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079762/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Pool_side.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079734/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Board_room_1.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079756/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Lutyens_suite_1.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079767/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Suite_room.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079763/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Premier_room.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079745/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Overview.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079752/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Lawns.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079740/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Dine.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1440079751/Domestic Hotels/Hotels_New Delhi/Maidens Hotel/Lobby.jpg",
//       "https://imgcld.yatra.com/ytimages/image/upload/t_hotel_mobileactualimage/v1393485693/Garden Sitting Area.jpg"
//     ],
//     "latitude": "28.674448",
//     "longitude": "77.2262",
//     "amenities": [
//       {
//         "Key": "1) ",
//         "Value": "Metro Station (Civil Lines) - 0.5 kms"
//       },
//       {
//         "Key": "2) ",
//         "Value": "Jama Masjid - 3.0 kms"
//       },
//       {
//         "Key": "3) ",
//         "Value": "Red Fort - 3.0 kms"
//       },
//       {
//         "Key": "4) ",
//         "Value": "New Delhi Railway Station - 4.0 kms"
//       },
//       {
//         "Key": "5) ",
//         "Value": "Mahatma Gandhi Memorial (Raj Ghat) - 6.0 kms"
//       },
//       {
//         "Key": "6) ",
//         "Value": "Trade Fair Grounds (Pragati Maidan) - 6.0 kms"
//       },
//       {
//         "Key": "7) ",
//         "Value": "Connaught Place - 6.0 kms"
//       },
//       {
//         "Key": "8) ",
//         "Value": "Delhi Zoo (Purana Qila) - 8.0 kms"
//       },
//       {
//         "Key": "9) ",
//         "Value": "National Museum - 9.0 kms"
//       },
//       {
//         "Key": "10) ",
//         "Value": "India Gate - 10.0 kms"
//       },
//       {
//         "Key": "11) ",
//         "Value": "Jantar Mantar - 10.0 kms"
//       },
//       {
//         "Key": "12) ",
//         "Value": "Humayun's  Tomb - 11.0 kms"
//       },
//       {
//         "Key": "13) ",
//         "Value": "Nehru Memorial Museum & Planetorium - 11.0 kms"
//       },
//       {
//         "Key": "14) ",
//         "Value": "Akshardham Temple - 11.0 kms"
//       },
//       {
//         "Key": "15) ",
//         "Value": "Lotus Temple - 13.0 kms"
//       },
//       {
//         "Key": "16) ",
//         "Value": "Qutab Minar - 18.0 kms"
//       },
//       {
//         "Key": "17) ",
//         "Value": "International Airport - 24.0 kms"
//       },
//       {
//         "Key": "18) ",
//         "Value": "Domestic Airport - 26.0 kms"
//       }
//     ],
//     "facilities": [
//       "Central Air Conditioning",
//       "Banquet facilities",
//       "Bar",
//       "Suitable for children",
//       "Beauty Services",
//       "Newspaper",
//       "Breakfast Services",
//       "Full Services Health Spa",
//       "Swimming pool",
//       "Smoking Rooms",
//       "Fitness Centre",
//       "Lounge",
//       "Front desk",
//       "Cell phone rental",
//       "Coffee shop or cafe",
//       "Internet Access",
//       "WIFI",
//       "Medical services",
//       "Paid WiFi",
//       "Parking",
//       "Gym",
//       "Restaurant",
//       "Room service",
//       "Safe Deposit Box"
//     ],
//     "policy": "Guest must be over 18 years of age to check-in to this hotel.| As per Government regulations, it is mandatory for all guests above 18 years of age to carry a valid photo identity card & address proof at the time of check-in. Please note that failure to abide by this can result with the hotel denying a check-in. Hotels normally do not provide any refund for such cancellations.||Default Check In Time :14:00:00|Default Check Out Time :12:00:00| Failure to check-in to the hotel, will attract the full cost of stay or penalty as per the hotel cancellation policy.|Hotels charge a compulsory Gala Dinner Supplement during Christmas, New Year's eve or other special events and festivals like Diwali or Dusshera. These additional  charge are not included in the booking amount and will be collected directly at the hotel.|There might be seasonal variation in hotel tariff rates during Peak days, for example URS period in Ajmer or Lord Jagannath Rath Yatra in Puri, the room tariff differences if any will have to be borne and paid by the customer directly at the hotel, if the booking stay period falls during such dates.| All additional charges other than the room charges and inclusions as mentioned in the booking voucher are to be borne and paid separately during check-out. Please make sure that you are aware of all such charges that may comes as extras. Some of them can be WiFi costs, Mini Bar, Laundry Expenses, Telephone calls, Room Service, Snacks etc.|Some hotels may have policies that do not allow unmarried / unrelated couples or certain foreign nationalities to check-in without the correct documentation. No refund will be applicable in case the hotel denies check-in under such circumstances. If you have any doubts on this, do call us for any assistance.|Any changes or booking modifications are subject to availability and charges may apply as per the hotel policies.|",
//     "instructions": null,
//     "address": "7, Sham Nath Marg Civil Lines , New Delhi, India",
//     "pin": "110054",
//     "city": "",
//     "state": "",
//     "country": "India",
//     "phone": null,
//     "services": null,
//     "index": "204"
//   },
//   "rooms": {
//     "roomlist": [
//       {
//         "id": 1,
//         "name": "Luxury Suite",
//         "enukeFare": 2362.5,
//         "agencyFare": 2362.5,
//         "enukeMarkup": 0,
//         "agencyMarKup": 0,
//         "baseFare": 712.5,
//         "tax": 210,
//         "otherCharge": 1440,
//         "OfferedPrice": 2362.5,
//         "price": 2362.5,
//         "amenities": [
//           "Breakfast, ",
//           "15% Discount on Food and Beverages, 15% Discount on Laundry, 15% Discount on Telecom, Complimentary Internet Access (In Guest Rooms), ,, "
//         ],
//         "inclusion": [
//           "Breakfast, ",
//           "15% Discount on Food and Beverages, 15% Discount on Laundry, 15% Discount on Telecom, Complimentary Internet Access (In Guest Rooms), ,, "
//         ],
//         "cancellationPolicies": [
//           {
//             "Charge": 100,
//             "ChargeType": 2,
//             "Currency": "GBP",
//             "FromDate": "2019-06-11T00:00:00",
//             "ToDate": "2019-06-12T23:59:59"
//           }
//         ],
//         "cancellationPolicy": "Luxury Suite#^#100.00% of total amount will be charged, If cancelled between 11-Jun-2019 00:00:00 and 12-Jun-2019 23:59:59.|#!#",
//         "roomDetail": {
//           "ChildCount": 0,
//           "RequireAllPaxDetails": false,
//           "RoomId": 0,
//           "RoomStatus": 0,
//           "RoomIndex": 1,
//           "RoomTypeCode": "0000033514|0000129329|1",
//           "RoomDescription": "Luxury Suite",
//           "RoomTypeName": "Luxury Suite",
//           "RatePlanCode": "0000129329",
//           "RatePlan": 13,
//           "RatePlanName": "Luxury Suite",
//           "InfoSource": "FixedCombination",
//           "SequenceNo": "DS~~00000608~1",
//           "DayRates": [
//             {
//               "Amount": 712.5,
//               "Date": "2019-06-11T00:00:00"
//             }
//           ],
//           "SupplierPrice": null,
//           "Price": {
//             "CurrencyCode": "GBP",
//             "RoomPrice": 712.5,
//             "Tax": 210,
//             "ExtraGuestCharge": 0,
//             "ChildCharge": 0,
//             "OtherCharges": 1440,
//             "Discount": 0,
//             "PublishedPrice": 2400,
//             "PublishedPriceRoundedOff": 2400,
//             "OfferedPrice": 2362.5,
//             "OfferedPriceRoundedOff": 2362.5,
//             "AgentCommission": 37.5,
//             "AgentMarkUp": 0,
//             "ServiceTax": 259.2,
//             "TDS": 15,
//             "ServiceCharge": 0,
//             "TotalGSTAmount": 259.2,
//             "GST": {
//               "CGSTAmount": 0,
//               "CGSTRate": 0,
//               "CessAmount": 0,
//               "CessRate": 0,
//               "IGSTAmount": 259.2,
//               "IGSTRate": 18,
//               "SGSTAmount": 0,
//               "SGSTRate": 0,
//               "TaxableAmount": 1440
//             }
//           },
//           "RoomPromotion": "",
//           "Amenities": [
//             "Breakfast, ",
//             "15% Discount on Food and Beverages, 15% Discount on Laundry, 15% Discount on Telecom, Complimentary Internet Access (In Guest Rooms), ,, "
//           ],
//           "Amenity": [
//             "Minibar",
//             "Attached Bathroom",
//             "Handicap accessible rooms",
//             "Central Air Conditioning",
//             "Toothbrush/Razor on request",
//             "Temperature Control",
//             "Global Direct Dial",
//             "Smoking rooms",
//             "Television-Room",
//             "Wakeup-calls",
//             "Wheelchair accessibility-Room",
//             "window opens",
//             "Premium TV channels",
//             "Refrigerator-Room",
//             "Rollaway beds",
//             "Separate sitting area",
//             "Air conditioning-Room",
//             "Bathrobes",
//             "Bathtub only",
//             "Cable/satellite TV",
//             "Ceiling fan",
//             "Child care(in room,surcharge)",
//             "Complimentary newspaper",
//             "Complimentary toiletries",
//             "Direct-dial phone",
//             "In-room safe",
//             "Internet access-dial-up",
//             "Cribs available",
//             "Designer toiletries"
//           ],
//           "SmokingPreference": "NoPreference",
//           "BedTypes": [],
//           "HotelSupplements": [],
//           "LastCancellationDate": "2019-06-10T23:59:59",
//           "CancellationPolicies": [
//             {
//               "Charge": 100,
//               "ChargeType": 2,
//               "Currency": "GBP",
//               "FromDate": "2019-06-11T00:00:00",
//               "ToDate": "2019-06-12T23:59:59"
//             }
//           ],
//           "CancellationPolicy": "Luxury Suite#^#100.00% of total amount will be charged, If cancelled between 11-Jun-2019 00:00:00 and 12-Jun-2019 23:59:59.|#!#",
//           "Inclusion": [
//             "Breakfast, ",
//             "15% Discount on Food and Beverages, 15% Discount on Laundry, 15% Discount on Telecom, Complimentary Internet Access (In Guest Rooms), ,, "
//           ]
//         }
//       },
//       {
//         "id": 2,
//         "name": "Deluxe Suite",
//         "enukeFare": 2520,
//         "agencyFare": 2520,
//         "enukeMarkup": 0,
//         "agencyMarKup": 0,
//         "baseFare": 760,
//         "tax": 224,
//         "otherCharge": 1536,
//         "OfferedPrice": 2520,
//         "price": 2520,
//         "amenities": [
//           "Breakfast, ",
//           "Complimentary Internet Access (In Guest Rooms), ,, "
//         ],
//         "inclusion": [
//           "Breakfast, ",
//           "Complimentary Internet Access (In Guest Rooms), ,, "
//         ],
//         "cancellationPolicies": [
//           {
//             "Charge": 100,
//             "ChargeType": 2,
//             "Currency": "GBP",
//             "FromDate": "2019-06-11T00:00:00",
//             "ToDate": "2019-06-12T23:59:59"
//           }
//         ],
//         "cancellationPolicy": "Deluxe Suite#^#100.00% of total amount will be charged, If cancelled between 11-Jun-2019 00:00:00 and 12-Jun-2019 23:59:59.|#!#",
//         "roomDetail": {
//           "ChildCount": 0,
//           "RequireAllPaxDetails": false,
//           "RoomId": 0,
//           "RoomStatus": 0,
//           "RoomIndex": 2,
//           "RoomTypeCode": "0000024972|0000092193|1",
//           "RoomDescription": "Deluxe Suite",
//           "RoomTypeName": "Deluxe Suite",
//           "RatePlanCode": "0000092193",
//           "RatePlan": 13,
//           "RatePlanName": "Deluxe Suite",
//           "InfoSource": "FixedCombination",
//           "SequenceNo": "DS~~00000608~2",
//           "DayRates": [
//             {
//               "Amount": 760,
//               "Date": "2019-06-11T00:00:00"
//             }
//           ],
//           "SupplierPrice": null,
//           "Price": {
//             "CurrencyCode": "GBP",
//             "RoomPrice": 760,
//             "Tax": 224,
//             "ExtraGuestCharge": 0,
//             "ChildCharge": 0,
//             "OtherCharges": 1536,
//             "Discount": 0,
//             "PublishedPrice": 2560,
//             "PublishedPriceRoundedOff": 2560,
//             "OfferedPrice": 2520,
//             "OfferedPriceRoundedOff": 2520,
//             "AgentCommission": 40,
//             "AgentMarkUp": 0,
//             "ServiceTax": 276.48,
//             "TDS": 16,
//             "ServiceCharge": 0,
//             "TotalGSTAmount": 276.48,
//             "GST": {
//               "CGSTAmount": 0,
//               "CGSTRate": 0,
//               "CessAmount": 0,
//               "CessRate": 0,
//               "IGSTAmount": 276.48,
//               "IGSTRate": 18,
//               "SGSTAmount": 0,
//               "SGSTRate": 0,
//               "TaxableAmount": 1536
//             }
//           },
//           "RoomPromotion": "",
//           "Amenities": [
//             "Breakfast, ",
//             "Complimentary Internet Access (In Guest Rooms), ,, "
//           ],
//           "Amenity": [
//             "Minibar",
//             "Attached Bathroom",
//             "Handicap accessible rooms",
//             "Central Air Conditioning",
//             "Toothbrush/Razor on request",
//             "Temperature Control",
//             "Global Direct Dial",
//             "Smoking rooms",
//             "Television-Room",
//             "Wakeup-calls",
//             "Wheelchair accessibility-Room",
//             "window opens",
//             "Premium TV channels",
//             "Refrigerator-Room",
//             "Rollaway beds",
//             "Separate sitting area",
//             "Air conditioning-Room",
//             "Bathrobes",
//             "Bathtub only",
//             "Cable/satellite TV",
//             "Ceiling fan",
//             "Child care(in room,surcharge)",
//             "Complimentary newspaper",
//             "Complimentary toiletries",
//             "Direct-dial phone",
//             "In-room safe",
//             "Internet access-dial-up",
//             "Cribs available",
//             "Designer toiletries"
//           ],
//           "SmokingPreference": "NoPreference",
//           "BedTypes": [],
//           "HotelSupplements": [],
//           "LastCancellationDate": "2019-06-10T23:59:59",
//           "CancellationPolicies": [
//             {
//               "Charge": 100,
//               "ChargeType": 2,
//               "Currency": "GBP",
//               "FromDate": "2019-06-11T00:00:00",
//               "ToDate": "2019-06-12T23:59:59"
//             }
//           ],
//           "CancellationPolicy": "Deluxe Suite#^#100.00% of total amount will be charged, If cancelled between 11-Jun-2019 00:00:00 and 12-Jun-2019 23:59:59.|#!#",
//           "Inclusion": [
//             "Breakfast, ",
//             "Complimentary Internet Access (In Guest Rooms), ,, "
//           ]
//         }
//       }
//     ],
//     "combination": {
//       "InfoSource": "FixedCombination",
//       "IsPolicyPerStay": false,
//       "RoomCombination": [
//         {
//           "RoomIndex": [
//             1
//           ]
//         },
//         {
//           "RoomIndex": [
//             2
//           ]
//         }
//       ]
//     }
//   }
// }