export default Titles = {
  adult: [
    { "id": 1, "value": "Mr." },
    { "id": 2, "value": "Mrs." },
    { "id": 3, "value": "Ms." }
  ],
  child: [
    { "id": 1, "value": "Master" },
    { "id": 2, "value": "Miss" },
  ]
}