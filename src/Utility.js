import Images from "./Helper Classes/Images";

export function convertMinsIntoHandM(n) {
  var num = n;
  var hours = num / 60;
  var rhours = Math.floor(hours);
  var minutes = (hours - rhours) * 60;
  var rminutes = Math.round(minutes);
  return rhours + "h " + rminutes + "m";
}

export function MinutesToHours(n) {
  var num = parseFloat(n);
  var hours = num / 60;
  var rhours = Math.floor(hours);
  var minutes = (hours - rhours) * 60;
  var rminutes = Math.round(minutes);
  return rhours + "h " + rminutes + "m";
}

export function filterAmenities(amenities, active) {

  const allAmenities = [
    { "icon": active ? Images.breakfast_blue : Images.breakfast_black, "amenity": "Breakfast" },
    { "icon": active ? Images.room_service_blue : Images.room_service_black, "amenity": "Room Service" },
    { "icon": active ? Images.wifi_blue : Images.wifi_black, "amenity": "Free Wifi" },
    { "icon": active ? Images.spa_blue : Images.spa_black, "amenity": "Spa" },
    { "icon": active ? Images.parking_blue : Images.parking_black, "amenity": "Free Parking" },
    { "icon": active ? Images.air_conditioner_blue : Images.air_conditioner_black, "amenity": "Air Conditioner" },
    { "icon": active ? Images.power_backup_blue : Images.power_backup_black, "amenity": "Power Backup" },
    { "icon": active ? Images.swimming_blue : Images.swimming_black, "amenity": "Swimming Pool" },
  ]
  let newAmenities = [];
  for (var i = 0; i < amenities.length; i++) {
    let oldAmenity = amenities[i].toLowerCase();
    for (j = 0; j < allAmenities.length; j++) {
      if (allAmenities[j].amenity.toLowerCase().includes(oldAmenity)) {
        newAmenities.push(allAmenities[j])
        break;
      }
    }
  }
  // alert(JSON.stringify(newAmenities))
  return newAmenities;
}