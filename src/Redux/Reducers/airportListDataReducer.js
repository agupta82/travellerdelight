import ActionTypes from '../Actions/actionType'

export const airportListDataReducer = (state = [], action) => {
  switch (action.type) {
    case ActionTypes.AIRPORT_LIST_DATA:
      return action.payload
    default:
      return state
  }
}