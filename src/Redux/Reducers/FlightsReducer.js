import {SAVE_FLIGHT_SEARCH_PARAMS,SAVE_FLIGHT_ITEM} from "./../Actions/ActionTypes";


const initialState = {
flightSearchParams:null,
flightItem:null
}

export const FlightsReducer = ( state = initialState,action)=>{
    switch(action.type){
        case SAVE_FLIGHT_SEARCH_PARAMS:
            return{
                ...state,
                flightSearchParams:action.payload
            }
        case SAVE_FLIGHT_ITEM:
            return{
                ...state,
                flightItem:action.payload
            } 
        default:
            return state;   
    }
  }