import { SAVE_HOTELLIST_DATA, SELECTED_HOTEL, Add_HOTEL_SEARCH_PARAMS, SAVE_SELECTED_ROOM_DATA, SAVE_SELECTED_HOTEL_PARAMS } from "../Actions/ActionTypes";

const initialState = {

  selectedHotelItem: null,
  hotelSearchParam: null,
  selectedRoomData: null
};
export const HotelListreducer = (state = initialState, action) => {
  switch (action.type) {

    // case SAVE_HOTELLIST_DATA:
    //   return {
    //     ...state,
    //     HotellistData: action.payload
    //   };

    case SELECTED_HOTEL:
      return {
        ...state,
        selectedHotelItem: action.payload
      }

    case Add_HOTEL_SEARCH_PARAMS:
      return {
        ...state,
        hotelSearchParam: action.payload
      }

    case SAVE_SELECTED_ROOM_DATA:
      return {
        ...state,
        selectedRoomData: action.payload
      }

    //HotelScreen2
    // case SAVE_SELECTED_HOTEL_PARAMS:  
    // return{
    //        ...state,
    //  }

    //   case DECREMENT:
    //     return {
    //       ...state,
    //       counter: Number(state.counter) - Number(action.payload)
    //     };

    default:
      return state;
  }
};