import ActionTypes from '../Actions/actionType'

export const dailyTokenReducer = (state = null, action) => {
  switch (action.type) {
      case ActionTypes.DAILY_TOKEN:
          return action.payload
      default:
          return state
  }
}