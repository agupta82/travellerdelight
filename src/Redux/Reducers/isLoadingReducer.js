import ActionType from "../Actions/actionType";

export const isLoadingReducer = (state = false, action) => {
	
	switch (action.type) {
		case ActionType.IS_LOADING:
			console.log("LOADING_ACTION:"+action.payload)
			return action.payload
		default:
			console.log("LOADING_ACTION_DEFAULT:"+state)
			return state
	}
}