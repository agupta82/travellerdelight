import ActionTypes from '../Actions/actionType'

export const agencyIdReducer = (state = null, action) => {
  switch (action.type) {
      case ActionTypes.AGENCY_ID:
          return action.payload
      default:
          return state
  }
}