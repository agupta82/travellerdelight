import ActionTypes from './actionType'

export const airportListDataAction = (airportListData) =>{
    return{
        type:ActionTypes.AIRPORT_LIST_DATA,
        payload:airportListData
    };
}