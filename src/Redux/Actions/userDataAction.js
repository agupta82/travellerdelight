import ActionTypes from './actionType'

export const userDataAction = (userData) =>{
    return{
        type:ActionTypes.USER_DATA,
        payload:userData
    };
}

export const isLoginAction = (isLogin) =>{
  return{
      type:ActionTypes.IS_LOGIN,
      payload:isLogin
  };
}