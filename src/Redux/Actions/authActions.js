import ActionType from './actionType'


export const doLoginAction = (data) => {
  // console.log("setting user token" + JSON.stringify(token));
  return {
    type: ActionType.DO_LOGIN_SAGA,
    payload: data,
  };
}

export const doLogoutAction = () => {
  return {
    type: ActionType.DO_LOGOUT_SAGA,
  };
}

export const doRegisterAction = (data) => {
  return {
    type: ActionType.DO_REGISTER_SAGA,
    payload: data,
  };
}

export const forgotPasswordAction = (data) => {
  return {
    type: ActionType.FORGOT_PASSWORD_SAGA,
    payload: data,
  };
}

export const updateUserProfileAction = (data, props) => {
  return {
    type: ActionType.UPDATE_USER_PROFILE_SAGA,
    payload: data,
    props: props,
  };
}

export const facebookLoginAction = (data) => {
  return {
    type: ActionType.FACEBOOK_LOGIN_SAGA,
    payload: data,
  };
}

export const googleLoginAction = (data) => {
  return {
    type: ActionType.GOOGLE_LOGIN_SAGA,
    payload: data,
  };
}