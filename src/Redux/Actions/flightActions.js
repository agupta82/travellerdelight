import ActionType from './actionType'


export const getAirportListAction = () => {
    return {
		type:ActionType.AIRPORT_LIST_SAGA,        
	};
}

export const getFlightRuleAction = (data) => {
    return {
		type:ActionType.FLIGHT_RULE,     
		payload:data   
	};
}

export const bookFlightAction = (data) => {
    return {
		type:ActionType.BOOK_FLIGHT_SAGA,     
		payload:data   
	};
}

export const confirmQuoteAction = (data) => {
    return {
		type:ActionType.CONFIRM_QUOTE_SAGA,     
		payload:data   
	};
}

export const searchFlightAction = item => {
    return{
        type:ActionType.SEARCH_FLIGHT_SAGA,
        payload:item
    }
}

export const flightBookedListAction = ()=>{
    return{
        type:ActionType.FLIGHT_BOOKED_LIST_SAGA,
    }
}

export const getFlightBookingDetailsAction = (data) => {
    return {
        type:ActionType.FLIGHT_BOOKING_DETAILS_SAGA,
        payload:data
	};
}