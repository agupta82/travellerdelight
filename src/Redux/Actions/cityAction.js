import ActionType from './actionType'


export const searchCityAction = (data) => {
    return {
		type:ActionType.SEARCH_CITY_SAGA,
        payload:data
	};
}