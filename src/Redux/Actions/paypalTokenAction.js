import ActionType from './actionType'


export const paypalTokenAction = (payload) => {
  return {
    type: ActionType.PAYPAL_TOKEN_SAGA,
    payload: payload
  };
}