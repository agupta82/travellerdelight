import ActionTypes from './actionType'

export const dailyTokenAction = (dailyToken) =>{
    return{
        type:ActionTypes.DAILY_TOKEN,
        payload:dailyToken
    };
}