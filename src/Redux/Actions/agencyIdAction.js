import ActionTypes from './actionType'


export const agencyIdAction = (agencyId) =>{
    return{
        type:ActionTypes.AGENCY_ID,
        payload:agencyId
    };
}