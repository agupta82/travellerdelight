import ActionType from './actionType'


export const getHotelTicketListAction = (props) => {
  return {
    type: ActionType.HOTEL_TICKET_LIST,
    props: props,
  };
}


export const getHotelListAction = (data) => {
  return {
    type: ActionType.HOTEL_LIST,
    payload: data
  };
}

export const getHotelDetailsAction = (data) => {
  return {
    type: ActionType.HOTEL_DETAILS_SAGA,
    payload: data
  };
}

export const getHotelBookingDetailsAction = (data) => {
  return {
    type: ActionType.HOTEL_BOOKING_DETAILS_SAGA,
    payload: data
  };
}

export const bookHotelAction = (data, nav) => {
  return {
    type: ActionType.BOOK_HOTEL_SAGA,
    payload: data,
    nav: nav
  };
}
