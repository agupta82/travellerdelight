import ActionTypes from './actionType'

export const userTokenAction = (userToken) =>{
    return{
        type:ActionTypes.USER_TOKEN,
        payload:userToken
    };
}