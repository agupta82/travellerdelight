import ActionType from '../Actions/actionType'

export const setLoadingAction = item => {
    return{
        type:ActionType.IS_LOADING,
        payload:item
    }
}