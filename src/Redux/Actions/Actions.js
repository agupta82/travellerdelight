import {SAVE_HOTELLIST_DATA,
    SELECTED_HOTEL,
    Add_HOTEL_SEARCH_PARAMS,
    SAVE_SELECTED_ROOM_DATA,
    SAVE_FLIGHT_SEARCH_PARAMS,
    SAVE_SELECTED_HOTEL_PARAMS,
    SAVE_FLIGHT_ITEM,
} 
    from "./ActionTypes"

// export const saveHotelList = hotelList => {
//     return {
//         type: SAVE_HOTELLIST_DATA,
//         payload: hotelList
//       };
// }

export const selectedHotel = hotelData => {
    return {
        type : SELECTED_HOTEL,
        payload : hotelData
    }
}

export const saveHotelSearchParams = params =>{
    return {
        type: Add_HOTEL_SEARCH_PARAMS,
        payload : params
    }
}

export const saveSelectedRoomData = roomData =>{
    return{
        type:SAVE_SELECTED_ROOM_DATA,
        payload :roomData  
    }
}


// export const saveSelectedHotelParams = params =>{
//     return{
//         type : SAVE_SELECTED_HOTEL_PARAMS,
//         payload : params
//     }
// }


//Flights Actions
export const saveFlightSearchParams = searchParams => {
    return{
        type:SAVE_FLIGHT_SEARCH_PARAMS,
        payload:searchParams
    }
}

export const saveFlightItem = item => {
    return{
        type:SAVE_FLIGHT_ITEM,
        payload:item
    }
}





// export const saveSelectedHotel = data => {
//     return{
//         type:
//     }
// }