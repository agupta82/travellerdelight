import { takeLatest, call, fork, put } from 'redux-saga/effects';
import {
  DeviceEventEmitter,
  Alert
} from 'react-native'
import ActionType from '../Actions/actionType'
import { getHotelBookedListApi, getHotelListApi, getHotelBookingDetailsApi, getHotelDetailsApi, bookHotelApi } from "../../Helper Classes/APIManager/ApiProvider"
import StringConstants from '../../Helper Classes/StringConstants';
import { NavigationActions, StackActions } from 'react-navigation';
import NavigationServices from '../../Helper Classes/NavigationServices';
function* getHotelBookedList(props) {
  console.log("HOTEL_BOOK_LIST_PROPS:" + JSON.stringify(props))
  // yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(getHotelBookedListApi);
    console.log("HOTEL_BOOK_LIST_RES:" + JSON.stringify(response))
    // getErrorFromJson(error,isCatchSection)

    if (response && response.code && response.code.code && response.code.code == 200) {

      yield put({ type: ActionType.IS_LOADING, payload: false });
      DeviceEventEmitter.emit(StringConstants.HOTEL_BOOKING_LIST_EVENT, response.data)

    } else if (response && response.code && response.code.code && response.code.code == 500) {
      yield put({ type: ActionType.IS_LOADING, payload: false });
      setTimeout(() => {
        Alert.alert(
          "Alert",
          "Please Login Again",
          [{ text: "Ok", onPress: () => { DeviceEventEmitter.emit(StringConstants.SESSION_EXPIRE_EVENT) } }],
          { cancelable: false }
        )
      }, 100)
    } else {
      yield put({ type: ActionType.IS_LOADING, payload: false });
      setTimeout(() => {
        alert(StringConstants.ERROR_OCCURED)
      }, 100);

    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* getHotelList({ type, payload }) {
  console.log("HOTEL_LIST_REQ:" + JSON.stringify(payload))
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(getHotelListApi, payload);
    console.log("HOTEL_LIST_RES:" + JSON.stringify(response))
    // getErrorFromJson(error,isCatchSection)
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (response && response.results) {
      DeviceEventEmitter.emit(StringConstants.HOTEL_LIST_EVENT, response)
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}


function* getHotelBookingDetails({ type, payload }) {
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(getHotelBookingDetailsApi, payload);
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (response) {
      DeviceEventEmitter.emit(StringConstants.HOTEL_BOOKING_DETAILS_EVENT, response)
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* getHotelDetails({ type, payload }) {
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(getHotelDetailsApi, payload);
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (response) {
      console.log(JSON.stringify(response));
      DeviceEventEmitter.emit(StringConstants.HOTEL_DETAILS_EVENT, response)
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* bookHotel({ type, payload, nav }) {
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(bookHotelApi, payload);

    console.log("BOOK_HOTEL_RES:" + JSON.stringify(response))
    if (response && response.status && response.status == "confirmed") {

      yield put({ type: ActionType.IS_LOADING, payload: false });
      setTimeout(() => {
        let routeName = "BookingSuccess"
        let params = {
          "data": {
            bookingId: response.booking_id,
            checkInDate: payload.check_in,
            checkOutDate: payload.check_out,
            transactionId: payload.orderID
          },
          isHotel: true
        }
        NavigationServices.navigate(routeName, params)
      }, 100);
    } else {
      yield put({ type: ActionType.IS_LOADING, payload: false });
      NavigationServices.goBack();
      if (response && response.message) {
        setTimeout(() => {
          alert(response.message);
        }, 100);
      }
    }
  }
  catch (error) {
    NavigationServices.goBack();
    console.log(JSON.stringify(error));
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

// Watcher: watch hotel request
export default function* watchHotel() {
  // Take Last Action Only
  yield takeLatest(ActionType.HOTEL_TICKET_LIST, getHotelBookedList);
  yield takeLatest(ActionType.HOTEL_LIST, getHotelList);
  yield takeLatest(ActionType.HOTEL_BOOKING_DETAILS_SAGA, getHotelBookingDetails);
  yield takeLatest(ActionType.HOTEL_DETAILS_SAGA, getHotelDetails);
  yield takeLatest(ActionType.BOOK_HOTEL_SAGA, bookHotel);
};