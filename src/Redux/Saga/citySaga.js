import { takeLatest, call, fork, put } from 'redux-saga/effects';
import {
  DeviceEventEmitter
} from 'react-native'
import ActionType from '../Actions/actionType'
import { getCitiesApi } from "../../Helper Classes/APIManager/ApiProvider"
import StringConstants from '../../Helper Classes/StringConstants';
import NavigationServices from '../../Helper Classes/NavigationServices';

function* searchCity({ type, payload }) {
//   yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(getCitiesApi, payload);
    if (response) {
        yield put({ type: ActionType.IS_LOADING, payload: false });
        if(response.data){
            DeviceEventEmitter.emit(StringConstants.SEARCH_CITY_EVENT,response.data)
        }
    } else {
        yield put({ type: ActionType.IS_LOADING, payload: false });
        if (response && response.message) {
            setTimeout(() => {
                alert(response.message);
            }, 100);
        }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
  }
};


// Watcher: watch city request
export default function* watchCity() {
  // Take Last Action Only
  yield takeLatest(ActionType.SEARCH_CITY_SAGA, searchCity);
};