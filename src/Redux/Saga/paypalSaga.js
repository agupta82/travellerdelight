import { takeLatest, call, fork, put } from 'redux-saga/effects';
import {
  DeviceEventEmitter,
  Alert,
} from 'react-native'
import ActionType from '../Actions/actionType'
import { getPaypalTokenApi, getApprovalUrlApi, } from "../../Helper Classes/APIManager/PaypalApiProvider"
import StringConstants from '../../Helper Classes/StringConstants';
import NavigationServices from '../../Helper Classes/NavigationServices';

function* getPaypalToken({ type, payload }) {

  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(getPaypalTokenApi);
    console.log('TOKEN_RES:' + JSON.stringify(response));

    if (response && response.access_token) {
      yield put({ type: ActionType.IS_LOADING, payload: false });

      let accessToken = response.access_token;
      DeviceEventEmitter.emit(StringConstants.PAYPAL_TOKEN, accessToken)

    } else {
      console.log("Error getting Token" + JSON.stringify(response))
      yield put({ type: ActionType.IS_LOADING, payload: false });
      NavigationServices.goBack()
      setTimeout(() => { alert("Error getting Token") }, 1000)
    }

  }
  catch (error) {
    NavigationServices.goBack()
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    } else if (error) {
      setTimeout(() => {
        alert(JSON.stringify(error));
      }, 100);
    }
  }
};

function* getPaypalUrl({ type, payload }) {
  console.log("responseGetApprovalUrlApi:" + JSON.stringify(payload))
  try {
    let response = yield call(getApprovalUrlApi, payload);
    yield put({ type: ActionType.IS_LOADING, payload: true });

    //console.log("responseGetApprovalUrlApi:" + JSON.stringify(response))

    if (response && response.id && response.links) {

      console.log("URlS" + JSON.stringify(response))
      yield put({ type: ActionType.IS_LOADING, payload: false });

      const { id, links } = response
      const approvalUrl = links.find(response => response.rel == "approval_url")
      console.log("URL:" + JSON.stringify(approvalUrl))
      DeviceEventEmitter.emit(StringConstants.PAYPAL_URL, {
        paymentId: id,
        approvalUrl: approvalUrl.href,
        accessToken: payload.accessToken,
      })
    } else {
      NavigationServices.goBack()
      yield put({ type: ActionType.IS_LOADING, payload: false });
      setTimeout(() => { alert("Error getting Approval") }, 1000)
    }

  }
  catch (error) {
    console.log(error);
    NavigationServices.goBack()
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    } else if (error) {
      setTimeout(() => {
        alert(JSON.stringify(error));
      }, 100);
    }
  }
};



// Watcher: watch auth request
export default function* watchPaypal() {
  // Take Last Action Only
  yield takeLatest(ActionType.PAYPAL_TOKEN_SAGA, getPaypalToken);
  yield takeLatest(ActionType.PAYPAL_URL_SAGA, getPaypalUrl);
};