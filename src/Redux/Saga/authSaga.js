import { takeLatest, call, fork, put } from 'redux-saga/effects';
import {
  DeviceEventEmitter,
  Alert,
} from 'react-native'
import ActionType from '../Actions/actionType'
import { loginUser, logoutUser, registerUserApi, forgotPasswordApi, updateUserProfileApi,facebookLoginApi,googleLoginApi } from "../../Helper Classes/APIManager/ApiProvider"
import StringConstants from '../../Helper Classes/StringConstants';
import NavigationServices from '../../Helper Classes/NavigationServices';

function* doLogin({ type, payload }) {
  console.log("email is:-" + payload.email);
  console.log("password is:-" + payload.password);
  let { email, password } = payload;
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(loginUser, { email, password });
    if (response.code.code == 200) {
      if (response != null && response.data != null && response.data.token != null && response.data.user != null) {
        console.log("Login Success")
        yield put({ type: ActionType.IS_LOGIN, payload: true });
        yield put({ type: ActionType.USER_TOKEN, payload: response.data.token });
        yield put({ type: ActionType.USER_DATA, payload: response.data.user });
        yield put({ type: ActionType.IS_LOADING, payload: false });
        console.log("dataUser:", JSON.stringify(response.data))
        DeviceEventEmitter.emit(StringConstants.IS_LOGINEVENT, true)

        NavigationServices.replace("Profile")
      } else {
        console.log("Data not found" + JSON.stringify(response))
        yield put({ type: ActionType.IS_LOADING, payload: false });
        setTimeout(() => { alert("User data not found") }, 1000)
      }
    } else {
      console.log("login failed" + JSON.stringify(response));
      setTimeout(() => { alert("Login failed " + response.code.description) }, 1000);
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
};

function* doLogout() {
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(logoutUser);
    if (response && response.code && response.code.code == 200) {
      yield put({ type: ActionType.IS_LOGIN, payload: false });
      yield put({ type: ActionType.USER_TOKEN, payload: null });
      yield put({ type: ActionType.USER_DATA, payload: null });
      yield put({ type: ActionType.IS_LOADING, payload: false });
      NavigationServices.replace("Authentication")
      // DeviceEventEmitter.emit(StringConstants.IS_LOGOUTEVENT, true)
    } else {
      console.log("login failed" + JSON.stringify(response));
      yield put({ type: ActionType.IS_LOADING, payload: false });
      // DeviceEventEmitter.emit(StringConstants.IS_LOGOUTEVENT, false)
      if (response && response.code && response.code.description) {
        setTimeout(() => { alert("Logout failed " + response.code.description) }, 1000);
      }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    //  DeviceEventEmitter.emit(StringConstants.IS_LOGOUTEVENT, false)
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* doRegisterUser({ type, payload }) {
  console.log("REGISTER_REQ:" + JSON.stringify(payload))
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(registerUserApi, payload);

    // getErrorFromJson(error,isCatchSection)
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (response && response.data) {
      console.log("REGISTER_RES:" + JSON.stringify(response))
      setTimeout(() => {
        alert(JSON.stringify(response.data.message));
      }, 100);
      DeviceEventEmitter.emit(StringConstants.REGISTER_EVENT, response.data)
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }

  }
}

function* forgotPassword({ type, payload }) {
  console.log("FORGOT_PASSWORD_REQ:" + JSON.stringify(payload))
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(forgotPasswordApi, payload);

    // getErrorFromJson(error,isCatchSection)
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (response && response.data && response.data === "RESET_LINK_SENT") {
      let msg = "Reset password link has been sent to your server"
      setTimeout(() => {
        Alert.alert('Alert', msg, [
          { text: 'OK', onPress: () => NavigationServices.replace('Login') }
        ])
      }, 100)

    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }

  }
}

function* updateUserProfile({ type, payload }) {
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(updateUserProfileApi, payload);
    console.log('Update_RES  ' +response)
    // getErrorFromJson(error,isCatchSection)
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (response && response.data) {

      yield put({ type: ActionType.USER_DATA, payload: response.data });

      console.log('Update API Response  ' + JSON.stringify(response))
      setTimeout(() => {
        Alert.alert('Alert', 'Profile updated successfully.', [{
          text: 'Ok', onPress: () => {
            //props.updateSuccess()
            NavigationServices.goBack()
          }
        }])
      }, 100);
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        Alert.alert('Alert', error.message, [{
          text: 'Ok', onPress: () => {
            NavigationServices.goBack();
          }
        }])
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        Alert.alert('Alert', error.code.description, [{
          text: 'Ok', onPress: () => {
            NavigationServices.goBack();
          }
        }])
      }, 100);
    }

  }
}

function* facebookLogin({ type, payload }) {
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(facebookLoginApi, payload);
    if (response.code.code == 200) {
      if (response != null && response.data != null && response.data.token != null && response.data.user != null) {
        console.log("Login Success")
        yield put({ type: ActionType.IS_LOGIN, payload: true });
        yield put({ type: ActionType.USER_TOKEN, payload: response.data.token });
        yield put({ type: ActionType.USER_DATA, payload: response.data.user });
        yield put({ type: ActionType.IS_LOADING, payload: false });
        console.log("dataUser:", JSON.stringify(response.data))
        DeviceEventEmitter.emit(StringConstants.IS_LOGINEVENT, true)

        NavigationServices.replace("Profile")
      } else {
        console.log("Data not found" + JSON.stringify(response))
        yield put({ type: ActionType.IS_LOADING, payload: false });
        setTimeout(() => { alert("User data not found") }, 1000)
      }
    } else {
      console.log("login failed" + JSON.stringify(response));
      setTimeout(() => { alert("Login failed " + response.code.description) }, 1000);
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
};

// Watcher: watch auth request
export default function* watchAuth() {
  // Take Last Action Only
  yield takeLatest(ActionType.DO_LOGIN_SAGA, doLogin);
  yield takeLatest(ActionType.DO_LOGOUT_SAGA, doLogout);
  yield takeLatest(ActionType.DO_REGISTER_SAGA, doRegisterUser);
  yield takeLatest(ActionType.FORGOT_PASSWORD_SAGA, forgotPassword);
  yield takeLatest(ActionType.UPDATE_USER_PROFILE_SAGA, updateUserProfile);
  yield takeLatest(ActionType.FACEBOOK_LOGIN_SAGA, facebookLogin);
};