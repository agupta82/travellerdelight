import { all, fork, spawn } from 'redux-saga/effects';
import watchAuth from "./authSaga";
import watchFlight from "./flightSaga";
import watchHotel from "./hotelSaga";
import watchCity from "./citySaga";
import watchPaypal from './paypalSaga'

export function* rootSaga() {
  yield all([
    fork(watchAuth),
    fork(watchFlight),
    fork(watchHotel),
    fork(watchCity),
    fork(watchPaypal)
  ]);
}