import { takeLatest, call, fork, put } from 'redux-saga/effects';
import { Alert, DeviceEventEmitter } from 'react-native'
import ActionType from '../Actions/actionType'
import { getAirPortList, getFlightRuleApi, bookFlightApi, confireQuoteApi, getFlightList, getFlightsBookedListApi, getFlightBookingDetailsApi } from "../../Helper Classes/APIManager/ApiProvider"
import StringConstants from '../../Helper Classes/StringConstants';
import { flightBookedListAction } from '../Actions';
import NavigationServices from '../../Helper Classes/NavigationServices';

function* getAirpotList() {
  console.log("AIRPORT_REQ:")
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(getAirPortList);
    console.log("AIRPORT_RES:" + JSON.stringify(response))

    if (response && response.code && response.code.code == 200) {
      let data = response.data;
      yield put({ type: ActionType.AIRPORT_LIST_DATA, payload: data });
      yield put({ type: ActionType.IS_LOADING, payload: false });

      //this event is only for SearchAirport page
      //DeviceEventEmitter.emit(StringConstants.SEARCH_AIRPORT_EVENT, data)

    } else {
      yield put({ type: ActionType.IS_LOADING, payload: false });
      if (response && response.message) {
        setTimeout(() => {
          alert(response.message);
        }, 100);
      }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* getFlightRule({ type, payload }) {
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(getFlightRuleApi, payload);
    console.log("AIRPORT_RES:" + JSON.stringify(response))

    if (response) {
      yield put({ type: ActionType.IS_LOADING, payload: false });
      DeviceEventEmitter.emit(StringConstants.FLIGHT_RULE_EVENT, response)
    } else {
      yield put({ type: ActionType.IS_LOADING, payload: false });
      if (response && response.message) {
        setTimeout(() => {
          alert(response.message);
        }, 100);
      }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* bookFlight({ type, payload }) {
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    console.clear();
    console.log("data is:=" + JSON.stringify(payload));
    let response = yield call(bookFlightApi, payload);

    if (response && response.success) {
      console.log("BOOK_FLIGHT_RES:" + JSON.stringify(response))

      yield put({ type: ActionType.IS_LOADING, payload: false });
      setTimeout(() => {
        NavigationServices.navigate('BookingSuccess', {
          status: 1,
          bookingId: response.booking_id,
          data: response
        })
      }, 100);
    } else {
      console.log("failed data is:=" + JSON.stringify(response));
      yield put({ type: ActionType.IS_LOADING, payload: false });
      if (response && response.message) {
        setTimeout(() => {
          Alert.alert(
            "Failed",
            response.message,
            [{
              text: "Ok", onPress: () => {
                NavigationServices.goBack();
                // DeviceEventEmitter.emit(StringConstants.SESSION_EXPIRE_EVENT) 
              }
            }],
            { cancelable: false }
          )
          // alert(response.message);
        }, 100);
      }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* confirmQuote({ type, payload }) {

  console.log("Payload:" + JSON.stringify(payload))
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(confireQuoteApi, payload);
    console.log("QUOTE_RES:" + JSON.stringify(response))
    if (response) {

      yield put({ type: ActionType.IS_LOADING, payload: false });
      DeviceEventEmitter.emit(StringConstants.CONFIRM_QUOTE_EVENT, response)
    } else {
      yield put({ type: ActionType.IS_LOADING, payload: false });
      if (response && response.message) {
        setTimeout(() => {
          alert(response.message);
        }, 100);
      }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* searchFlight({ type, payload }) {

  console.log("FLIGHT_LIST_payload" + JSON.stringify(payload))
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(getFlightList, payload);
    yield put({ type: ActionType.IS_LOADING, payload: false });

    if (response) {
      DeviceEventEmitter.emit(StringConstants.FLIGH_LIST_EVENT, response)
    }

  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
};

function* getFlightBookedList() {
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(getFlightsBookedListApi);

    if (response && response.code && response.code.code && response.code.code == 200) {

      yield put({ type: ActionType.IS_LOADING, payload: false });
      DeviceEventEmitter.emit(StringConstants.FLIGHT_BOOKED_LIST_EVENT, response.data)

    } else if (response && response.code && response.code.code && response.code.code == 500) {
      yield put({ type: ActionType.IS_LOADING, payload: false });
      setTimeout(() => {
        Alert.alert(
          "Alert",
          "Please Login Again",
          [{ text: "Ok", onPress: () => { DeviceEventEmitter.emit(StringConstants.SESSION_EXPIRE_EVENT) } }],
          { cancelable: false }
        )
      }, 100)
    } else {
      yield put({ type: ActionType.IS_LOADING, payload: false });
      setTimeout(() => {
        alert(StringConstants.ERROR_OCCURED)
      }, 100);

    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* getFlightBookingDetails({ type, payload }) {
  yield put({ type: ActionType.IS_LOADING, payload: true });
  try {
    let response = yield call(getFlightBookingDetailsApi, payload);
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (response && response.data && response.data.bookings) {
      DeviceEventEmitter.emit(StringConstants.FLIGHT_BOOKING_DETAILS_EVENT, response.data.bookings)
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

// Watcher: watch flight request
export default function* watchFlight() {
  // Take Last Action Only
  yield takeLatest(ActionType.AIRPORT_LIST_SAGA, getAirpotList);
  yield takeLatest(ActionType.FLIGHT_RULE, getFlightRule);
  yield takeLatest(ActionType.BOOK_FLIGHT_SAGA, bookFlight);
  yield takeLatest(ActionType.CONFIRM_QUOTE_SAGA, confirmQuote);
  yield takeLatest(ActionType.SEARCH_FLIGHT_SAGA, searchFlight);
  yield takeLatest(ActionType.FLIGHT_BOOKED_LIST_SAGA, getFlightBookedList);
  yield takeLatest(ActionType.FLIGHT_BOOKING_DETAILS_SAGA, getFlightBookingDetails);

};