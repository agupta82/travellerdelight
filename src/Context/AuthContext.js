import React,{Component} from "react";
const AuthContext = React.createContext();

class AuthProvider extends Component{
    constructor(props){
        super(props)
        this.state={
            isLoggedin:false
        }
    }

    logIn = () => {
        this.setState({
          isLoggedIn: true
        });
      };
    
      logOut = () => {
        this.setState({
          isLoggedIn: false
        });
      };

    render(){
        return(
     <AuthContext.Provider
        value={{
          ...this.state,
          logIn: this.logIn,
          logOut: this.logOut
          }}
        >
        {this.props.children}
      </AuthContext.Provider>
        )
    }
}
const AuthCounsumer = AuthContext.Consumer;

export { AuthProvider,AuthCounsumer }
