import { Dimensions, Platform, View, TouchableOpacity, Text, Image, } from "react-native";
import React from "react";
import { StyleSheet } from "react-native";
import Images from "./Images";
import Fonts from "./Fonts";
import { colors } from "./Colors";
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import NavigationServices from "./NavigationServices";

export default class CommonHeader extends React.Component {
  constructor(props) {
    super(props);
  }

  onBackClick = () => {
    NavigationServices.goBack();
  }

  renderBackButton() {
    return (
      <TouchableOpacity activeOpacity={0.8}
        style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}
        onPress={() => this.onBackClick()}>
        <Image source={Images.imgback} style={{ width: 15, height: 15, resizeMode: "contain" }}></Image>
        <Text style={styles.backBtnText}> Back</Text>
      </TouchableOpacity>
    )
  }

  renderToolbar() {
    return (
      <View style={this.props.isTransparent ? styles.transparentToolbar : styles.toolbar}>
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          paddingHorizontal: 10
        }}>
          <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
            {this.props.noBack ? null : this.renderBackButton()}
          </View>
          <View style={{ width: '70%', alignItems: 'center' }}>
            {this.props.titleView ? this.props.titleView() :
              <Text style={styles.headerStnName} >{this.props.title}</Text>}
          </View>
          <View style={{ width: '15%' }}>
            {this.props.rightImg ?
              <TouchableOpacity style={styles.headerButton} onPress={() => this.props.fireEvent()}>
                <Image source={this.props.rightImg ? this.props.rightImg : null} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
              </TouchableOpacity> : null
            }
          </View>
        </View>
        {/* <View style={{ flexDirection: 'row',  width: '10%' }}>
                <TouchableOpacity activeOpacity={0.8}
                >
                    <Image source={Images.ic_close_white}
                        style={{ marginRight: 15, height: 25, width: 25 }}
                    ></Image>
                </TouchableOpacity>
            </View> */}
      </View >
    )
  }

  render() {
    let { fireEvent } = this.props;
    return (
      this.renderToolbar()
      // <View style={styles.header}>
      //   <TouchableOpacity style={styles.headerBackButton} onPress={() => NavigationServices.goBack()}>
      //     <Image source={Images.imgback} style={{ width: 15, height: 15, resizeMode: "contain" }}></Image>
      //     <Text style={styles.backBtnText}> Back</Text>
      //   </TouchableOpacity>
      //   <View style={styles.headerContent}>
      //     <Text style={styles.headerStnName}>{this.props.title}</Text>

      //   </View>
      //   {this.props.fireEvent ?
      //     <TouchableOpacity style={styles.headerButton} onPress={() => fireEvent()}>
      //       <Image source={Images.imgfilter} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
      //     </TouchableOpacity> : <View style={styles.headerButton}>
      //     </View>}
      // </View>
    )
  }
}

const styles = StyleSheet.create({
  toolbar: {
    height: (Platform.OS === 'ios') ? 44 : 56,
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    backgroundColor: colors.colorBlue,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 3,
    shadowRadius: 0,
  },
  transparentToolbar: {
    height: (Platform.OS === 'ios') ? 44 : 56,
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
  },
  headerRowDevider: {
    height: 12, width: 1.5,
    backgroundColor: colors.colorWhite,
    marginLeft: 7,
    marginRight: 7,
    marginTop: 5
  },
  headerContentRowText: {
    color: colors.colorWhite,
    fontSize: 12,
    fontFamily: Fonts.medium,
    lineHeight: 20
  },
  headerContentIcon: {
    width: 20, height: 20,
    resizeMode: "contain",
    marginLeft: 10,
    marginRight: 10
  },
  headerStnName: {
    color: colors.colorWhite,
    fontSize: 17,
    fontFamily: Fonts.semiBold
  },
  headerContentRow: { width: "100%", flexDirection: "row", justifyContent: "center" },
  headerContent: {
    height: "100%",
    alignItems: 'center',
    width: screenWidth - 120,
    padding: 8,
    justifyContent: "center"
  },
  backBtnText: {
    color: colors.colorWhite,
    fontFamily: Fonts.semiBold
  },
  container: {
    flex: 1,
    backgroundColor: "rgba(244,248,254,1)"
  },
  header: {
    height: 70,
    width: screenWidth,
    backgroundColor: colors.colorBlue,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 12,
    paddingRight: 12
  },
  headerBackButton: {
    width: 60,
    // backgroundColor: "red",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  headerButton: {
    height: "100%",
    width: 40,
    // backgroundColor: "blue",
    justifyContent: "center",
    alignItems: "flex-end"
  },
})