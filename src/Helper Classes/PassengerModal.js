import React, { Component } from "react";
import { FlatList, View, Text, Image, ScrollView, TouchableHighlight } from "react-native";

import Images from "../Helper Classes/Images";
import moment from "moment"
import { colors } from "./Colors";

const _renderItemSeparator = () => (
  <View style={{ height: 1, width: "100%", marginTop: 5, marginBottom: 5, backgroundColor: "#D3D3D3" }} />
)

export class PassengersModal extends Component {

  renderHeaderComponent = () =>
    (
      <View style={{ flexDirection: "row", marginBottom: 10 }}>
        <Text style={{ alignSelf: "center", flex: 1, textAlign: "center" }}>
          Passenger
      </Text>
        <TouchableHighlight
          underlayColor={"transparent"}
          onPress={this.props.editPassengerHandler}>
          <Image
            style={{ height: 20, width: 20, }}
            source={Images.imgpencilfilled}
          />
        </TouchableHighlight>
      </View>
    )





  renderPassengerItem = (item) => {
    console.warn(item)
    // return (

    //   <View style={{ flex: 1 }}>


    //     <View style={{ flexDirection: "row", width: 300, height: 40, alignItems: "center" }}>
    //       <Image
    //         style={{ height: 15, width: 15 }}
    //         source={Images.imgusericon} />
    //       <Text style={{ marginLeft: 10, fontFamily: "Montserrat-Medium", flex: 1, flexDirection: "row" }}>
    //         {item.title} {item.fName} {item.mName} {item.lName}
    //       </Text>
    //       <Text style={{ fontSize: 12, fontFamily: "Montserrat-Medium", color: "grey" }}>
    //         {item.passenger_type} ({moment().diff(item.dob, 'years')})
    //   </Text>
    //       {/* <TouchableHighlight
    //   style = {{padding:5}}
    //   underlayColor = {"transparent"}
    //   onPress = {()=>this.setState({ isModalVisible: true })}>
    //   <Text style ={{color:colors.colorBlue,fontFamily:"Montserrat-Medium",fontSize:12}}>
    //     {item.item.passenger_type} */}
    //       {/* </Text> */}
    //       {/* </TouchableHighlight> */}
    //     </View>

    //   </View>
    // )
  }
  
  render() {

    // console.warn(this.props.passengers)



    return (
      <View
        style={{ backgroundColor: "rgba(0,0,0,0.5)", flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ScrollView style={{ padding: 15, marginTop: 50 }}>

          <FlatList
            style={{ backgroundColor: "white", paddingLeft: 20, paddingRight: 20, borderRadius: 5, padding: 10, marginBottom: 20 }}
            data={this.props.passengers}
            ListHeaderComponent={this.renderHeaderComponent}
            renderItem={this.renderPassengerItem}
            keyExtractor={(item, index) => { item, index }}
            ItemSeparatorComponent={_renderItemSeparator}
          />
          <View
            style={{ margin: 10, justifyContent: "center", width: "100%", alignItems: "center" }}>
            <TouchableHighlight
              underlayColor={"transparent"}
              style={{
                backgroundColor: colors.colorWhite, borderRadius: 5, borderColor: colors.colorBlue, justifyContent: "center", alignItems: "center", borderWidth: 1

              }}
              onPress={this.props.modalCloseHandler}>
              <Text
                style={{ color: colors.colorBlue, fontFamily: "Montserrat-Bold", marginTop: 10, marginBottom: 10, marginLeft: 20, marginRight: 20 }}>
                Close
              </Text>
            </TouchableHighlight>

          </View>
        </ScrollView>
      </View>
    )
  }
}
