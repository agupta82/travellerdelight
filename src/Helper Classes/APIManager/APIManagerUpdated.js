import React from "react";
import {  Alert } from "react-native";
import AsyncStorage from '@react-native-community/async-storage'
import Constants from "../Constants";
import StringConstants from "../StringConstants";
import APIConstants from "./APIConstants";
import moment from "moment";
const PAGE = "Api Manager"

export default class APIManager extends React.Component {
  
  //MARK: --------- Getting Data from userDefault ----------------

  static getDataFromAsync(key, completion) {
    AsyncStorage.getItem(key, (error, result) => {
      if (error == null) {
        console.log("The result is " + result);
        completion(result);
      } else {
        completion(null);
      }
    });
  }
  
  static removeItemfromAsync(key){
    AsyncStorage.removeItem(key,error =>{
      if(error){
        console.log("error clearing " +key)
      }else{
        console.log(key + " item cleared")
      }
     }
    )
  }

  // static saveValueToAsync(key,val){
  //   AsyncStorage.setItem(key, val ,error => {
  //     if(error){
  //       console.log("error storing" + key + error)
  //     }else{
  //       console.log(key + "stored successfully")
  //     }
  //   }
  //   )
  // }

  //MARK: ------------- Storing Data into PREFERENCE ---------------

  static saveJsonString(key, jsonString) {
    AsyncStorage.setItem(key, jsonString, error => {
      if (error) {
        // alert((result == null) ? 'found nil' : 'done')
        console.log("error storing " + Key + error);
      } else {
        console.log(key + " stored successfully");
      }
    });
  }

//MARK:----------- Getting error from Error Response --------------

static getErrorFromJson(error,isCatchSection){
    if(isCatchSection)
    {
        return error.message
    }
    return error.code.description
}


  //MARK: ------------ Comman method to call API  ------------------

  static fetchData(urlString, methodType, body, successBlock, failureBlock) {
    if (Constants.dailyToken == null) {
      this.getToken(token => {
        if (token == null) {
            let dict = {
                code:{
                    code:-1001,
                    text:'Internet Connection Error',
                    description:'Unable to get Token.'
                }
            }
          failureBlock(dict,false);
          return;
        }

        console.log("Current token number is " + token);
        Constants.dailyToken = token;
        let header = {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization":'Bearer '+Constants.userToken,
          token: token,
          agency_Id:Constants.agencyId
         };

        this.fetchDataAfterGettingToken(
          urlString,
          methodType,
          header,
          body,
          response => {
            successBlock(response);
          },
          (error,isCatchSection) => {
            failureBlock(error,isCatchSection);
          }
        );
      });
    } else {

      let token = Constants.dailyToken;

      console.log("Current token number is " + token);
      let header = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization":'Bearer '+Constants.userToken,
        token: token,
        agency_Id:Constants.agencyId
      };

      this.fetchDataAfterGettingToken(
        urlString,
        methodType,
        header,
        body,
        response => {
          successBlock(response);
        },
        (error,isCatchSection) => {
          failureBlock(error,isCatchSection);
        }
      );
    }
  }

 static fetchDataAfterGettingToken(
    urlString,
    methodType,
    header,
    body,
    successBlock,
    failureBlock
  ) {
      console.log('Calling '+urlString)
    
   return fetch(urlString, {
      method: methodType,
      headers: header,
      body: methodType == "POST" ? JSON.stringify(body) : null
    })
      .then(function(response) {

        console.log(response.headers.get("Content-Type"));
        console.log(response.headers.get("Date"));
        console.log(response.status);
        console.log(response.statusText);
        console.log(response.type);
        console.log(response.url);

        response.json().then(function(data) {
          if (response.status !== 200) {
            console.log("Status Code: " + response.status);
            console.log("data is " + JSON.stringify(data));
            failureBlock(data, false);
            return;
          }
          // console.log(data.code.message);
          successBlock(data);
        });
      })
      .catch(error => {
        console.log("got an error" + JSON.stringify(error));
        failureBlock(error,true);
      });
  }

  //MARK:----------------  Get Flights Calender API--------------------

  static getFlightsCalender(param, completion) {
    console.log(
      "parameters of calendar " +
        JSON.stringify(param) +
        "and Url Is   " +
        APIConstants.getFlightsCalender
    );
    this.fetchData(
      APIConstants.getFlightsCalender,
      "POST",
      param,
      response => {
        if (response.code.code == 200) {
          completion(true, response.data);
        } else {
          completion(false, response.code.description);
        }
      },
      (error, isCatchSection) => {
        console.log("got error in getting calendars of flights " + error);
        completion(false, this.getErrorFromJson(error, isCatchSection));
      }
    );
  }

  //MARK: -------------- GET Daily Token API -------------------

  static getTokenAPI(completion) {
    let header = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    this.fetchDataAfterGettingToken(
      APIConstants.getFlightList,
      "GET",
      header,
      null,
      response => {
        let token = response.token;
        console.log("got Token " + token);
        let now = new Date();
        let dict = {
          date: moment(now).format("YYYY-MM-DD"),
          token: token
        };
        Constants.dailyToken = token;
        console.log("saving dictionary is  " + JSON.stringify(dict));
        this.saveJsonString("flightToken", JSON.stringify(dict));
        completion(token);
      },
      (error,isCatchSection) => {
        console.log(error);
        completion(null);
      }
    );
  }

  //MARK:--------------------- Check Token Is Available ---------------------

  static getToken(completion) {
    this.getDataFromAsync("flightToken", dictStr => {
      if (dictStr) {
        let dict = JSON.parse(dictStr);
        let date = Date.parse(dict.date);
        let now = Date.parse(moment(new Date()).format("YYYY-MM-DD"));

        console.log("current date is  " + now + "and saved date is  " + date);

        if (date == now) {
          console.log("not calling token API");
          completion(dict.token);
        } else {
          console.log("calling token API");
          this.getTokenAPI(token => {
            completion(token);
          });
        }
      } else {
        console.log("calling token API for first time");
        this.getTokenAPI(token => {
          completion(token);
        });
      }
    });
  }

  //MARK: ---------------- Get Airport List API ----------------

  static getAirPortList(completion) {

    console.log("Calling Airport List API");
    this.fetchData(
      APIConstants.getAirports,
      "GET",
      null,
      response => {
        console.log("AIRPORT_RES:"+JSON.stringify(response))
        if (response.code.code == 200) {
          let data = response.data;
          Constants.airPortListData = data;
          completion(true, "success");
        } else {
          completion(false, response.message);
        }
      },
      (error,isCatchSection) => {
        console.log(error);
        completion(false, error.message);
      }
    );
  }

  //MARK:----------------  Get Flights Calender API--------------------

  static getFlightsCalender(param, completion) {
    console.log(
      "parameters of calendar " +
        JSON.stringify(param) +
        "and Url Is   " +
        APIConstants.getFlightsCalender
    );
    this.fetchData(
      APIConstants.getFlightsCalender,
      "POST",
      param,
      response => {
        if (response.code.code == 200) {
          completion(true, response.data);
        } else {
          completion(false, response.code.description);
        }
      },
      (error,isCatchSection) => {
        console.log(
          "got error in getting calendars of flights " + error
        );
        completion(false, this.getErrorFromJson(error,isCatchSection));
      }
    );
  }


//MARK:------------------ Get Flight List API ----------------

//   static getFlightsList(body, successBlock, failureBlock) {

//     this.fetchData(
//         APIConstants.getFlightList,
//         "POST",
//         param,
//         response => {
//           successBlock(response)
//         },
//         (error,isCatchSection) => {
//           failureBlock(error);
//         }
//       );
//   }

// //MARK:---------------- Get Cities suggestion API ---------------

// static getCities(searchStr, completion) {
//     this.fetchData(
//       APIConstants.getCities + searchStr,
//       "GET",
//       null,
//       response => {
//         if (response.code.code == 200) {
//           completion(true, response.data);
//         } else {
//           completion(false, response.code.description);
//         }
//       },
//       (error,isCatchSection) => {
//         console.log(
//           "got error " + error
//         );
//         completion(false, this.getErrorFromJson(error,isCatchSection));
//       }
      
//     );
//   }


// //MARK:-------------- Get Hotels List API -----------------
// static getHotelsList(param, completion) {

//     this.fetchData(
//         APIConstants.getHotelsList,
//         "POST",
//         param,
//         response => {
//             completion(true,response)
//         },
//         (error,isCatchSection) => {
//             if(isCatchSection){
//                 completion(false,error.message)
//             }
//             else 
//             {
//                 completion(false,error.statusText)
//             }    
//         }
//       );
//   } 

//MARK: --------------------- Get Hotel Details ----------------

// static getHOtelDetails(dict,completion){

//     let strUrl = APIConstants.getHotelsList+'/'+dict.hotel.id+'?'+'rooms='+'1'+'&index='+dict.hotel.index+'&source='+dict.source+'&trace='+dict.sourceDetail.trace

//     this.fetchData(
//         strUrl,
//         "GET",
//         null,
//         response => {
//             completion(true,response)
//         },
//         (error,isCatchSection) => {
//             if(isCatchSection){
//                 completion(false,error.message)
//             }
//             else 
//             {
//                 completion(false,error.statusText)
//             }    
//         }
//       );

//   }

//MARK:---------------- Flight Booking API -----------------

// static flightBooking(param,completion){ 

//     this.fetchData(
//         APIConstants.flightbooking,
//         "POST",
//         param,
//         response => {
//             completion(true,response)
//         },
//         (error,isCatchSection) => {
//             if(isCatchSection){
//                 completion(false,error.message)
//             }
//             else 
//             {
//                 completion(false,JSON.stringify(error))
//             }    
//         }
//       );
// }

//MARK:----------------- Register User API ---------------

static registerUserApi(params,completion){
    console.log("callin register api")
    this.fetchData(
      APIConstants.register,
      "POST",
      params,
      response => {
        completion(true, response.data);
      },
      (error,isCatchSection) => {
        console.log(
          "got error " + error
        );
        completion(false, this.getErrorFromJson(error,isCatchSection));
      }
    );
  }

//MARK: -------------- Login User API ------------
// static loginUserApi(param,completion){
//   console.log("api calling")
//     this.fetchData(
//       APIConstants.login,"POST",param,
//       (response )=> {
//         if(response != null && response.data!=null
//            && response.data.token != null && 
//            response.data.user != null){
//             Constants.isLogin = true;
//             Constants.userData = response.data.user;
//             Constants.userToken = response.data.token;
//             this.saveJsonString(StringConstants.USER_DATA,
//               JSON.stringify(response.data)
//             )
//             this.saveJsonString(StringConstants.IS_LOGIN,"TRUE")
//             completion(true,response)
//            }else{
//              console.log("error" + JSON.stringify(response))
//            }
//       },(error,isCatchSection) => {
//         console.log(
//           "got error " + error
//         );
//         completion(false, this.getErrorFromJson(error,isCatchSection));
//       }
//     )
//   }

//MARK: -------------- Forgot Password API -----------
static forgotPasswordApi(param,completion){
    this.fetchData(
        APIConstants.forgotPassword,"POST",param,
      (response )=> {
        completion(true,response)
      },(error,isCatchSection) => {
        console.log(
          "got error " + error
        );
        completion(false, this.getErrorFromJson(error,isCatchSection));
      }
    )
  }

//MARK: -------------- GetUserData -----------
  static getUserData() {
    this.getDataFromAsync(StringConstants.IS_LOGIN,isLogin => {
      if(isLogin === "TRUE"){
        this.getDataFromAsync(StringConstants.USER_DATA, dictStr => {
          if (dictStr) {
            console.log("GET_USER_DATA:"+JSON.stringify(dictStr))
            let json = JSON.parse(dictStr);
            Constants.isLogin = true;
            Constants.userData = json.user;
            Constants.userToken = json.token;
          }
        });
      }else{
        Constants.isLogin = false;
      }
    });
  
    
  }

//MARK: -------------- GetHotelBookingList -----------
static getHotelBookedListApi(completion){
  this.fetchData(
    APIConstants.hotelBookedList,
    "GET",
     null,
    (response)=>{
      completion(true,response)
    },(error,isCatchSection)=>{
      console.log(
        "got error " + error
      );
      completion(false, this.getErrorFromJson(error,isCatchSection));
    }
  )
}

//MARK: -------------- Get list of booked flights -----------
static getFlightsBookedListApi(completion){
  this.fetchData(
    APIConstants.flightBookedList,
    "GET",
     null,
    (response)=>{
    completion(true,response)
    },
    (error,isCatchSection)=>{
      completion(false,this.getErrorFromJson(error,isCatchSection))
    }
  )
}

static logoutUser(completion){
  console.log("callin Logout User api");
  this.fetchData(
    APIConstants.logout,
    "GET",
    null,
    response => {
      console.log(response )
      Constants.isLogin = false
      Constants.userData = null;
      Constants.userToken = '';
      this.removeItemfromAsync(StringConstants.USER_DATA);
      this.saveJsonString(StringConstants.IS_LOGIN,"FALSE");
      completion(true, response);
    },
    (error, isCatchSection) => {
      console.log("got error " + error);
      completion(false, this.getErrorFromJson(error, isCatchSection));
    }
  );
 }

 //MARK: -------------- Get booked hotel details for particular id-----------
//  static getHotelBookingData(id,completion){
//   console.log(PAGE+ "Calling get hotel booking details")
//   this.fetchData(
//     APIConstants.hotelBookingDetails+id,
//     "GET",
//     null,
//     response => {
//       completion(true,response)
//     },(error,isCatchSection) =>{
//       console.log("got error " + error);
//       completion(false, this.getErrorFromJson(error, isCatchSection));
//     }
//   );
// }
 
 //MARK: -------------- Get booked flight details for particular id-----------
 static getBookedFlightData(id,completion){
  console.log(PAGE+ "Calling get flight booking details")
  this.fetchData(
    APIConstants.flightBookingDetails+id,
    "GET",
    null,
    response=>{
      completion(true,response)
    },
    (error,isCatchSection)=>{
      console.log(PAGE+" got error " + JSON.stringify(error))
      completion(false, this.getErrorFromJson(error, isCatchSection));
    }
  );
}

static getFlightRule(param,completion){
  console.log(PAGE+ "Calling get flight booking details")
  this.fetchData(
    APIConstants.getFlightRule,
    "POST",
    param,
    response=>{
      completion(true,response)
    },
    (error,isCatchSection)=>{
      console.log(PAGE+" got error " + JSON.stringify(error))
      completion(false, this.getErrorFromJson(error, isCatchSection));
    }
  );
}


//MARK:------------------- Update User Data -------------------
 
static updateUserProfile(param,completion){
  console.log("callin Update user profile api");
  this.fetchData(
    APIConstants.updateProfile,
    "POST",
    param,
    response => {
      let dict = {}
      dict['user'] = response.data
      dict['token'] = Constants.userToken

      this.saveJsonString(
        USERDEFAULTKEYS.loginUserData,
        JSON.stringify(dict)
      );
      
      Constants.userData = response.data;

      completion(true, response.data);
    },
    (error, isCatchSection) => {
      console.log("got error " + error);
      completion(false, this.getErrorFromJson(error, isCatchSection));
    }
  );
 }

  //MARK:---------------- Confirm Quote API ---------------

  static confireQuote(param, completion) {
    this.fetchData(
      APIConstants.confirmQuote,
      "POST",
      param,
      response => {
        completion(true, response);
      },
      (error, isCatchSection) => {
        if (isCatchSection) {
          completion(false, error.message);
        } else {
          completion(false, error.code.message);
        }
      }
    );
  }

  //MARK:---------------- Confirm Fare rule API ---------------

  static confireFareAPI(param, completion) {
    this.fetchData(
      APIConstants.confirmFare,
      "POST",
      param,
      response => {
        completion(true, response);
      },
      (error, isCatchSection) => {
        if (isCatchSection) {
          completion(false, error.message);
        } else {
          completion(false, error.code.text);
        }
      }
    );
  }


  //MARK:---------------- Flight Booking API -----------------

  static flightBooking(param, completion) {
    this.fetchData(
      APIConstants.flightbooking,
      "POST",
      param,
      response => {
        completion(true, response);
      },
      (error, isCatchSection) => {
        if (isCatchSection) {
          completion(false, error.message);
        } else {
          completion(false, error.code.error);
        }
      }
    );
  }

}
