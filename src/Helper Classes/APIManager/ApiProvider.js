import Constants from "../Constants";
import APIConstants from "./APIConstants";
import AsyncStorage from '@react-native-community/async-storage'
import { store } from './../../Redux/Store/Store'
import { dailyTokenAction } from './../../Redux/Actions/dailyTokenAction'
import moment from "moment";



async function callApi(urlString, header, body, methodType, isMultipart) {
  console.log("----------- Api request is----------- ");
  console.log("url string " + urlString);
  console.log("header " + JSON.stringify(header));
  console.log("body " + JSON.stringify(body));
  console.log("methodType " + methodType)



  return fetch(urlString, {
    method: methodType,
    headers: header,
    body: isMultipart ? body : methodType == "POST" ? JSON.stringify(body) : null
  })
    .then(response => {
      console.log("-----------Response is----------- ")
      console.log(response)
      if (response.status == 200) {
        return response.json()
      } else {
        throw new Error(" status code " + response.status)
      }
    }
    )
    .then((responseJson) => {
      return responseJson
    })
    .catch((error) => {
      throw error
    })
}

async function fetchApiData(urlString, body, methodType, isMultipart) {

  let userToken = store.getState().userTokenReducer;
  let dailyToken = store.getState().dailyTokenReducer;
  if (!dailyToken) {  //if token is null,0,"",undefined
    console.log("DAILY:");
    try {
      let token = await getDailyToken();
      // saveToken(token)
      let header = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": 'Bearer ' + userToken,
        token: token,
        agency_Id: 0
      }
      if (urlString.includes('node-api')) {
        console.log("HEADER_CHANGE")
        header = {
          "Accept": "application/json",
          "Content-Type": "application/json",
          token: token,
          agency_Id: 0
        }
      }
      if (urlString.includes('register')) {
        console.log("HEADER_CHANGE")
        header = {
          "Accept": "application/json",
          "Content-Type": "application/json",
          agency_Id: 0
        }
      }
      if (isMultipart) {
        header = {
          "Content-Type": "multipart/form-data",
          "Accept": "application/json",
          "Authorization": 'Bearer ' + userToken,
          token: dailyToken,
          agency_Id: 0
        }
      }
      return callApi(urlString, header, body, methodType, isMultipart)
    } catch (error) {
      throw new Error(error)
    }
  } else {
    console.log("NOT_DAILY:")
    //if token has some value
    let header = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": 'Bearer ' + userToken,
      token: dailyToken,
      agency_Id: 0
    }

    if (urlString.includes('node-api')) {
      console.log("HEADER_CHANGE")
      header = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        token: dailyToken,
        agency_Id: 0
      }
    }
    if (urlString.includes('register')) {
      console.log("HEADER_CHANGE")
      header = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        agency_Id: 0
      }
    }
    if (isMultipart) {
      header = {
        "Content-Type": "multipart/form-data",
        "Accept": "application/json",
        "Authorization": 'Bearer ' + userToken,
        token: dailyToken,
        agency_Id: 0
      }
    }

    return callApi(urlString, header, body, methodType, isMultipart)
  }
}

async function getDailyToken() {

  let header = {
    Accept: "application/json",
    "Content-Type": "application/json"
  };

  return fetch(
    APIConstants.getToken, {
    method: "GET",
    headers: header,
    body: null
  })
    .then((response) => response.json())
    .then(responseJson => {
      console.log("TOKEN:")
      let token = responseJson.token
      console.log("got Token " + token);
      let now = new Date();
      let dict = {
        date: moment(now).format("YY-MM-DD"),
        token: token
      };
      //Constants.dailyToken = token;
      store.dispatch(dailyTokenAction(token))
      // console.log("saving dictionary is  " + JSON.stringify(dict));
      //saveJsonString("flightToken", JSON.stringify(dict));
      return token
    })
    .catch((error) => {
      console.log("ERROR:" + JSON.stringify(error))
      throw new Error(error)
    })

}

export function getDataFromAsync(key, completion) {
  AsyncStorage.getItem(key, (error, result) => {
    if (error == null) {
      console.log("The result is " + result);
      completion(result);
    } else {
      completion(null);
    }
  });
}

export function removeItemfromAsync(key) {
  AsyncStorage.removeItem(key, error => {
    if (error) {
      console.log("error clearing " + key)
    } else {
      console.log(key + " item cleared")
    }
  }
  )
}

export function saveJsonString(key, jsonString) {
  AsyncStorage.setItem(key, jsonString, error => {
    if (error) {
      // alert((result == null) ? 'found nil' : 'done')
      console.log("error storing " + Key + error);
    } else {
      console.log(key + " stored successfully");
    }
  });
}

export function getErrorFromJson(error, isCatchSection) {
  if (isCatchSection) {
    return error.message
  }
  return error.code.description
}

export async function getFlightList(body) {

  return fetchApiData(APIConstants.getFlightList, body, "POST");

}

export async function loginUser(body) {
  console.log("----------Login User Api Call ------------------")
  return fetchApiData(APIConstants.login, body, "POST")
}

export async function logoutUser() {
  console.log("----------Logout User Api Call ------------------")
  return fetchApiData(APIConstants.logout, null, "GET")
}

// ---------------- Register User API ----------------
export async function registerUserApi(body) {
  console.log("----------Register User Api Call ------------------")
  return fetchApiData(APIConstants.register, body, "POST")
}

// ---------------- Forgot Password API ----------------
export async function forgotPasswordApi(body) {
  console.log("----------Forgot Password Api Call ------------------")
  return fetchApiData(APIConstants.forgotPassword, body, "POST")
}

// ---------------- Get Airport List API ----------------

export async function getAirPortList() {
  return fetchApiData(APIConstants.getAirports, null, "GET")
}

//MARK: -------------- GetHotelBookingList -----------
export async function getHotelBookedListApi() {
  return fetchApiData(APIConstants.hotelBookedList, null, "GET")
  // this.fetchData(
  //   APIConstants.hotelBookedList,
  //   "GET",
  //    null,
  //   (response)=>{
  //     completion(true,response)
  //   },(error,isCatchSection)=>{
  //     console.log(
  //       "got error " + error
  //     );
  //     completion(false, this.getErrorFromJson(error,isCatchSection));
  //   }
  // )
}


// export async function logoutUser() {
//     console.log("---------- Logout User Api Call ------------------")
//     return fetchApiData(APIConstants.logout, null, "GET")
//         .then(res => {

//             Constants.isLogin = false
//             Constants.userData = null;
//             Constants.userToken = '';
//             this.removeItemfromAsync(StringConstants.USER_DATA);
//             this.saveJsonString(StringConstants.IS_LOGIN, "FALSE");
//         }).catch(err => {
//             setTimeout(() => { alert("Error in logging out") })
//         })
// }

export async function getFlightRuleApi(param) {
  return fetchApiData(APIConstants.getFlightRule, param, "POST");
}

export async function confireQuoteApi(param) {
  return fetchApiData(APIConstants.confirmQuote, param, "POST")
  // .then( res => console.log(" console2 "+JSON.stringify(res)))
  // .catch( err => console.log(err));
}

export async function getHotelListApi(param) {
  return fetchApiData(APIConstants.getHotelsList, param, "POST")
}

export async function getHotelDetailsApi(param) {
  // let strUrl = APIConstants.getHotelsList + '/' + hoteldetailObj.id + '?' + 'rooms=' + '1' + '&index=' + hoteldetailObj.index + '&source=' + hoteldetailObj.source + '&trace=' + hoteldetailObj.traceId;
  // return fetchApiData(strUrl, null, "GET")
  return fetchApiData(APIConstants.getHotelDetail, param, "POST")

}

export function bookFlightApi(param) {
  return fetchApiData("http://travel.yiipro.com/node-api/v1/flight/book", param, "POST")
}

export function bookHotelApi(param) {
  return fetchApiData("http://travel.yiipro.com/node-api/v1/hotels/book", param, "POST")
}

export function getCitiesApi(searchStr) {
  return fetchApiData(APIConstants.getCities + searchStr, null, "GET");
}


export function blockRoomApi(params) {
  return fetchApiData(APIConstants.blockRoom, params, "POST");
}

export function getHotelBookingDetailsApi(id) {
  return fetchApiData(APIConstants.hotelBookingDetails + id, null, "GET");
}

export function updateUserProfileApi(params) {
  return fetchApiData(APIConstants.updateProfile, params, "POST", true);
}

//MARK: -------------- Get list of booked flights -----------
export async function getFlightsBookedListApi() {
  return fetchApiData(APIConstants.flightBookedList, null, "GET");
  // this.fetchData(
  //   APIConstants.flightBookedList,
  //   "GET",
  //    null,
  //   (response)=>{
  //   completion(true,response)
  //   },
  //   (error,isCatchSection)=>{
  //     completion(false,this.getErrorFromJson(error,isCatchSection))
  //   }
  // )
}

//MARK: -------------- Get booked flight details for particular id-----------
export async function getFlightBookingDetailsApi(id) {
  console.log("PAGE" + "Calling get flight booking details")
  return fetchApiData(APIConstants.flightBookingDetails + id, null, "GET");

  // this.fetchData(
  //   APIConstants.flightBookingDetails+id,
  //   "GET",
  //   null,
  //   response=>{
  //     completion(true,response)
  //   },
  //   (error,isCatchSection)=>{
  //     console.log(PAGE+" got error " + JSON.stringify(error))
  //     completion(false, this.getErrorFromJson(error, isCatchSection));
  //   }
  // );
}

export async function facebookLoginApi(body) {
  return fetchApiData(APIConstants.fbLogin, body, "POST")
}

export async function googleLoginApi(body) {
  return fetchApiData(APIConstants.googleLogin, body, "POST")
}