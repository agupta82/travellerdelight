
//export const baseUrl = 'http://travel.yiipro.com/api/';
//for development mode

// export const baseUrl = 'http://travel.yiipro.com/node-api/';

const serverUrl = "http://travellerdelight.com/node-api/v1/";
const baseUrl = "http://travel.yiipro.com/node-api/v1/"//"http://travellerdelight.com/node-api/v1/";
//const baseUrl = "http://travellerdelight.com/node-api/v1/";
//export const baseUrl = "http://myagency.yiipro.com/node-api/"
const baseUrl2 = "http://travel.yiipro.com/api/"
export const imageBaseUrl = "http://travel.yiipro.com/"
const APIConstants = {
  getAirports: 'http://travel.yiipro.com/api/airports', //done
  //getFlightList:'http://travellerdelight.com/node-api/',

  // getFlightList:'http://travel.yiipro.com/node-api/',
  getToken: baseUrl, //done
  getFlightList: baseUrl, //done
  //getFlightRule:'http://54.148.39.174:4872/flight/rules',
  getFlightRule: baseUrl + 'flight/rules', //done
  getFlightsCalender: baseUrl + 'flight/calander', // not used
  getCities: baseUrl2 + 'hotel-suggestion?keyword=', //done
  // getHotelsList:'http://54.148.39.174:4872/hotels',
  // getHotelsList:'http://travel.yiipro.com:4872/hotels',


  login: baseUrl2 + 'login', //done
  register: baseUrl2 + 'register', //done
  //flightbooking:'http://travel.yiipro.com:4872/flight/book',
  flightbooking: baseUrl + 'flight/book', //done
  forgotPassword: baseUrl2 + 'forgotpassword', //done
  //confirmFare:'http://54.148.39.174:4872/flight/rules',
  // confirmFare:'http://travel.yiipro.com/node-api/flight/rules',

  confirmFare: baseUrl + 'flight/rules', //not used

  // confirmQuote:'http://travel.yiipro.com/node-api/flight/quote',

  confirmQuote: baseUrl + 'flight/quote', //done


  //hotels Api
  getHotelsList: baseUrl + 'hotels', //done hotel detail pending (not working)
  blockRoom: baseUrl + 'hotels/room-block', //not used in project



  updateProfile: baseUrl2 + 'profile/update', //done, api not working
  logout: baseUrl2 + 'logout', //done
  hotelBookedList: baseUrl2 + 'hotel-ticket-list', //done
  flightBookedList: baseUrl2 + 'air-ticket-list', //done
  hotelBookingDetails: baseUrl + 'hotel-confirm/', //done
  flightBookingDetails: baseUrl + 'air-confirm/',// done, api not working
  getHotelDetail: baseUrl + 'hotel/detail',// pending
  fbLogin: baseUrl2 + 'fbLogin',
  googleLogin: baseUrl2 + 'googleLogin',
}


export default APIConstants;
