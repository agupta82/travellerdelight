import React, { Component } from "react";
import {DeviceEventEmitter} from "react-native"
import APIManager from "./APIManager/APIManagerUpdated"
import StringConstants from "./StringConstants"
export default class AuthManagement extends Component{
    constructor(props){
        super(props)
    }
    
    static APILogout(){
    console.log("in api logout")
    APIManager.logoutUser((success,response)=>{
      if(success)
      {
        console.log("logout success event emitted")
        DeviceEventEmitter.emit(StringConstants.IS_LOGOUTEVENT, true)
      }
      else 
      {
        DeviceEventEmitter.emit(StringConstants.IS_LOGOUTEVENT, false)
        console.log('Logout Error  '+JSON.stringify(response))
        setTimeout(() => {
          alert(response)
        }, 100);
      }
    })
  }
}
