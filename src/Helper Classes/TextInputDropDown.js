import React, { Component } from "react";
import {
  Animated,
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList,
  Image,
  TouchableHighlight,
  TextInput,
  ActivityIndicator
} from "react-native";
import APIManager from "../Helper Classes/APIManager/APIManagerUpdated";
import PropTypes from "prop-types";

export default class TextInputDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      isFocused: false,
      animating: false,
      data: [],
      loading: false,
      isEditing: false
    };

    this._renderListItem = this._renderListItem.bind(this);
  }

  //-------------- API Calling Methods ---------------

  getSearchData(searchStr) {
    this.setState({
      animating: true
    });
    APIManager.getCities(searchStr, (success, response) => {
      this.setState({
        animating: false
      });
      if (success) {
        this.setState({
          data: response,
          loading: !this.state.loading
        });
        // alert(JSON.stringify(response))
      } else {
        alert(response);
      }
    });
  }

  onChangeText(text) {
    this.setState({
      searchText: text,
      isEditing: true
    });

    if (text.trim().length != 0) {
      this.getSearchData(text);
    } else {
      this.setState({
        data: [],
        loading: !this.state.loading
      });
    }
  }

  _renderListItem({ item, index }) {
    return (
      <TouchableHighlight
        style={{
          height: 35,
          justifyContent: "center",
          backgroundColor: "white",
          borderBottomColor: "gray",
          borderBottomWidth: index == this.state.data.length - 1 ? 0 : 1
        }}
        underlayColor='transparent'
        onPress={()=>{
            this.setState({
                // isEditing:false,
                // data:[],
                searchText:item.name,
                // loading:!this.state.loading
            })
            alert(item.name)
            console.log('search Text is  '+item.name)
        }}
      >
        <Text style={{ marginLeft: 20, fontSize: 15, color: "black" }}>
          {item.name}
        </Text>
      </TouchableHighlight>
    );
  }

  render() {
    return (
      <View>
        <View style={styles.styleTextContainer}>
          <Text
            style={
              this.state.isFocused
                ? styles.styleSeletedTextLabel
                : styles.styleTextLabel
            }
          >
            {this.props.textLabelTitle || "Destination"}
          </Text>

          <TextInput
            style={styles.styleTextInput}
            placeholder=""
            value={this.state.searchText}
            onChangeText={text => this.onChangeText(text)}
            onFocus={() => {
              this.setState({
                isFocused: true
              });
            }}
            onEndEditing={() => {
              this.setState({
                data: [],
                animating: false,
                isEditing: false,
                loading: !this.state.loading
              });
              if (this.state.searchText.trim().length == 0) {
                this.setState({
                  isFocused: false
                });
              }
            }}
          />

          <ActivityIndicator
            style={{ alignSelf: "flex-end", position: "absolute", bottom: 10 }}
            size="small"
            color="gray"
            animating={this.state.animating}
          />

          <FlatList
            style={
              this.state.isEditing && this.state.data.length > 0
                ? styles.styleFlatListWithData
                : styles.styleFlatListNoData
            }
            data={this.state.data}
            extraData={this.state.loading}
            renderItem={this._renderListItem}
            keyExtractor={({ item, index }) => String(index)}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  styleTextContainer: {
    borderBottomColor: "black",
    borderBottomWidth: 1,
    justifyContent: "center",
    marginBottom: 10
  },
  styleSeletedTextLabel: {
    fontSize: 11,
    color: "gray",
    marginTop: 10,
    height: 15
  },
  styleTextLabel: {
    fontSize: 15,
    color: "gray",
    position: "absolute"
  },
  styleTextInput: {
    height: 40,
    width: "100%"
    //   backgroundColor:'red'
  },
  styleFlatListWithData: {
    // height: 100,
    width: "100%",
    opacity: 1,
    borderWidth: 1,
    borderColor: "gray"
    // position:'absolute',
    // top:65
  },
  styleFlatListNoData: {
    height: 0,
    opacity: 0
  }
});

// TextInputDropDown.defaultProps = {
//     textLabelTitle:'Destination'
//   };

//   TextInputDropDown.prototype = {
//    textLabelTitle:PropTypes.string.isRequired,

//   };
