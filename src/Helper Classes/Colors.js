export const colors = {
  // colorBlue: "rgb(0,114,208)",
  colorBlue: "#2a62fd",
  colorRed: "#eb2226",
  colorWhite: "#fff",
  headingTextColor: "black",
  colorBlack: "#000",
  colorGold: "#f9ba2f",
  lightBlue: "#dfe7fc",
  // gray: "rgb(231,234,237)",
  gray: "#f5f5f5",
  lightgrey: "lightgrey",
  backgroundColor: "rgb(246,245,246)",
  colorBlack62: "#626262",
  colorBlack21: "#212121"
}