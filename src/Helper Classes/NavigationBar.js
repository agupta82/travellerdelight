import React from "react";
import {
  View,
  Text,
  TouchableHighlight,
  Image,
} from "react-native";
import Images from "../Helper Classes/Images";
import { ifIphonex, getStatusBarHeight } from "react-native-iphone-x-helper";
import NavigationBarStyle from "../Helper Classes/NavigatonBarStyleSheet";
import PropTypes from "prop-types";
import NavigationServices from "./NavigationServices";

export default class NavigationBar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={NavigationBarStyle.styleNavBar}>
        <View style={NavigationBarStyle.styleNavBarSubView}>
          <TouchableHighlight
            style={[NavigationBarStyle.styleLeftBtn,{opacity:this.props.showBack ? 1:0}]}
            underlayColor="transparent"
            onPress={() => {
              NavigationServices.goBack();
            }}
          >
            <Image
              style={{ height: 25, width: 25 }}
              resizeMode="contain"
              source={Images.imgback}
            />
          </TouchableHighlight>

          <Text style={[NavigationBarStyle.styleNavTitle,{marginRight: this.props.showFiltertBtn ? 0:50}]}>
            {this.props.title}
          </Text>

          <View style={[NavigationBarStyle.styleNavBarRightView]}>
            <TouchableHighlight
              style={{
                width: this.props.showFiltertBtn ? 30 : 0,
                marginRight: this.props.showFiltertBtn ? 10 : 0,
                opacity: this.props.showFiltertBtn ? 1 : 0
              }}
              underlayColor="transparent"
              onPress={(this.props.onPress) ? this.props.onPress:()=>{
                
              }}
            >
              <View>
                <Image
                  style={{ height: 20, width: 20, marginLeft: 5 }}
                  source={Images.imgfilter}
                  resizeMode="contain"
                />
                {/* <View
                  style={{
                    height: 15,
                    width: 15,
                    borderRadius: 7.5,
                    backgroundColor: "red",
                    justifyContent: "center",
                    alignItems: "center",
                    alignSelf: "flex-end",
                    opacity: (this.props.count || 0) > 0 ? 1 : 0
                  }}
                  position="absolute"
                >
                  <Text style={{ color: "white", fontSize: 10 }}>
                    {this.props.count || 0}
                  </Text>
                </View> */}
              </View>
            </TouchableHighlight>

            <TouchableHighlight
              style={{
                width: this.props.showSearchBtn ? 30 : 0,
                marginRight: this.props.showSearchBtn ? 10 : 0,
                opacity: this.props.showSearchBtn ? 1 : 0
              }}
              underlayColor="transparent"
              onPress={() => {
                
              }}
            >
              <Image
                style={{ height: 30, width: 30 }}
                source={Images.ImgSearch}
                resizeMode="contain"
              />
            </TouchableHighlight>

            <TouchableHighlight
              style={{
                width: this.props.showAddBtn ? 30 : 0,
                marginRight: this.props.showAddBtn ? 10 : 0,
                opacity: this.props.showAddBtn ? 1 : 0
              }}
              underlayColor="transparent"
              onPress={() => {
                
              }}
            >
              <Image
                style={{ height: 30, width: 30 }}
                source={Images.ImgAddNavBar}
                resizeMode="contain"
              />
            </TouchableHighlight>
          </View>
        </View>
      </View>
    );
  }
}

NavigationBar.propTypes = {
  title: PropTypes.string,
  // count: PropTypes.int,
  showBack: PropTypes.bool,
  showRightButtons: PropTypes.bool,
  showFiltertBtn: PropTypes.bool,
  showSearchBtn: PropTypes.bool,
  showAddBtn: PropTypes.bool,
  onLeftBtnPress: () => {}
};

NavigationBar.defaultProps = {
  title: "Grocery",
  showBack: false,
  // count: 0,
  showRightButtons: true,
  onLeftBtnPress: () => {
   // Actions.drawerOpen();
  },
  showFiltertBtn: false,
  showSearchBtn: false,
  showAddBtn: false
};
