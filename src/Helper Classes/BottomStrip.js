import { Dimensions, View, TouchableOpacity, Text, Image, } from "react-native";
import React, { Component } from "react";
import { StyleSheet } from "react-native";
import Images from "./Images";
import Fonts from "./Fonts";
import { colors } from "./Colors";
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import NavigationServices from "./NavigationServices";

export default class BottomStrip extends Component {
  render() {
    let { fireEvent, price } = this.props;
    return (
      <View style={styles.bottomStrip}>
        <View style={styles.priceView}>
          {/* <Image source={Images.imgrupeewhite} style={styles.priceImg}></Image> */}
          <Text style={styles.priceText}>{price ? price : "3237"}</Text>
        </View>
        <TouchableOpacity style={styles.bottomBtn} onPress={() => fireEvent()}>
          <Text style={styles.bottomBtnTxt}>CONTINUE</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  bottomBtnTxt: {
    fontFamily: Fonts.medium,
    color: colors.colorBlue,
    fontSize: 12, letterSpacing: .5
  },
  bottomBtn: {
    backgroundColor: colors.colorWhite,
    height: 30,
    width: 110,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center"
  },
  priceText: {
    flexDirection: "row",
    color: colors.colorWhite,
    fontFamily: Fonts.bold,
    fontSize: 22,
    lineHeight: 30
  },
  priceImg: {
    width: 20, height: 20, resizeMode: "contain"
  },
  priceView: {
    width: "50%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center"
  },
  bottomStrip: {
    height: 50,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingRight: 14,
    paddingLeft: 14,
    backgroundColor: colors.colorBlue,
    position: "absolute",
    bottom: 0
  },
})