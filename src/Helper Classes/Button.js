import React from "react";
import { Text, TouchableOpacity } from "react-native";
import { colors } from "./Colors";
import Fonts from "./Fonts";
export const Button = ({ onPress, width, height, text, borderRadius, disabled }) => {
    // const {onPress, width, height, text ,borderRadius } = props 
    return (
        <TouchableOpacity
            disabled={disabled}
            style={{
                backgroundColor: colors.colorBlue,
                alignSelf: "center",
                width: width || "100%",
                height: height,
                borderRadius: borderRadius || 10,
                padding: 10,
                justifyContent: "center",
                alignItems: "center",
                elevation: 2,
                shadowOpacity: 0.3,
                shadowOffset: { width: 1, height: 1 },
                shadowRadius: 2,
            }}
            onPress={onPress}>
            <Text style={{
                color: colors.colorWhite,
                fontFamily: Fonts.bold,
                fontSize: 14, textTransform: "uppercase",
                letterSpacing: .8
            }}>
                {text}
            </Text>
        </TouchableOpacity>
    )
}

export const ThemeButton = ({ onPress, width, height, text, borderRadius, disabled }) => {
    // const {onPress, width, height, text ,borderRadius } = props 
    return (
        <TouchableOpacity
            disabled={disabled}
            style={{
                backgroundColor: colors.colorBlue,
                alignSelf: "center",
                width: width || "100%",
                height: height,
                borderRadius: borderRadius || 10,
                padding: 10,
                justifyContent: "center",
                alignItems: "center",
                elevation: 2,
                shadowOpacity: 0.3,
                shadowOffset: { width: 1, height: 1 },
                shadowRadius: 2,
            }}
            onPress={onPress}>
            <Text style={{ color: colors.colorWhite, fontFamily: Fonts.medium, fontSize: 14 }}>
                {text}
            </Text>
        </TouchableOpacity>
    )
}

export const ThemeLightButton = ({ onPress, width, height, text, borderRadius, disabled }) => {
    // const {onPress, width, height, text ,borderRadius } = props 
    return (
        <TouchableOpacity
            disabled={disabled}
            style={{
                backgroundColor: colors.colorBlue,
                alignSelf: "center",
                width: width || "100%",
                height: height,
                borderRadius: borderRadius || 10,
                padding: 10,
                justifyContent: "center",
                alignItems: "center",
                elevation: 2,
                shadowOpacity: 0.3,
                shadowOffset: { width: 1, height: 1 },
                shadowRadius: 2,
            }}
            onPress={onPress}>
            <Text style={{ color: colors.colorWhite, fontFamily: Fonts.medium, fontSize: 14 }}>
                {text}
            </Text>
        </TouchableOpacity>
    )
}
