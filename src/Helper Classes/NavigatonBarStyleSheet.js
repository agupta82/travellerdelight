import { StyleSheet, Dimensions, StatusBar, Platform } from "react-native";
import { ifIphoneX, getStatusBarHeight } from "react-native-iphone-x-helper";
const screenHeight = Dimensions.get("window").height;
const sch = Dimensions.get('screen').height

const screenWidth = Dimensions.get("window").width;

const NavigationBarStyleSheet = StyleSheet.create({
  styleNavBar: {
    height: ( (Platform.OS == 'ios')? 44:52),
    // height: ((Platform.OS == 'ios')? getStatusBarHeight(true):0) + ((Platform.OS == 'ios')? 44:52), 
    width: screenWidth,
    backgroundColor: 'transparent',
  
  },
  styleNavBarSubView: {
    height: ((Platform.OS == 'ios')? 44:52),
    width: "100%",
    // marginTop: ((Platform.OS == 'ios')? getStatusBarHeight(true):0),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems:'center',
    // backgroundColor:'green'
  },
  styleLeftBtn: {
    alignItems: "center",
    justifyContent: "center",
    width: 40,
    height: 40,
    marginLeft: 10
  },
  styleNavTitle: {
    fontSize: 18,
    fontFamily:"Montserrat-SemiBold",
    color: "black",
    alignSelf:'center'
  },
  styleNavBarRightView: {
    height: 35,
    flexDirection: "row",
    alignSelf: "flex-end",
    justifyContent: "flex-end"
  },
  styleTextFieldContainer:{
    width:'80%',
    height:'100%',
    
    justifyContent:'center',
    alignItems:'flex-start',
  },
 
  styleLocationContainer:{
    width: "95%",
    height: 40,
    flexDirection: "row",
    backgroundColor: "white",
    shadowColor:'grey',
    shadowOpacity:0.5,
    shadowOffset:{width:1,height:1},
    borderRadius:3,
    elevation:6
  }

});

export default NavigationBarStyleSheet;
