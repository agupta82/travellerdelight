import { Dimensions, View, TouchableOpacity, Text, Image, } from "react-native";
import React from "react";
import { StyleSheet } from "react-native";
import Images from "./Images";
import Fonts from "./Fonts";
import { colors } from "./Colors";
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import NavigationServices from "./NavigationServices";
import moment from 'moment';

export default class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        "All",
        "Economy",
        "Premium Economy",
        "Business",
        "Premium Business",
        "First"
      ],
    }
  }

  getDate(date) {
    //return dateFormat(date, "ddd, mmm dS");
    return moment(date).format("DD MMM");
  }

  getClassName(index) {
    return this.state.data[index - 1];
  }

  render() {
    let { fireEvent, data, bookingType } = this.props;
    return (
      <View style={styles.header}>
        <TouchableOpacity style={styles.headerBackButton} onPress={() => {
          // alert("Hello");
          NavigationServices.goBack();
        }}>
          <Image source={Images.imgback} style={{ width: 15, height: 15, resizeMode: "contain" }}></Image>
          <Text style={styles.backBtnText}> Back</Text>
        </TouchableOpacity>
        <View style={styles.headerContent}>
          <View style={styles.headerContentRow}>
            <Text style={styles.headerStnName}>{data.origin}</Text>
            <Image source={bookingType == 1 ? Images.imgPlane : Images.imgdualway} style={styles.headerContentIcon}></Image>
            <Text style={styles.headerStnName}>{data.destination}</Text>
          </View>
          <View style={styles.headerContentRow}>
            {
              bookingType == 1 ?
                <Text style={styles.headerContentRowText}>{this.getDate(data.departure_date)}</Text> :
                <Text style={styles.headerContentRowText}>{this.getDate(data.departure_date)} - {this.getDate(data.return_date)}</Text>
            }
            <View style={styles.headerRowDevider}></View>
            <Text style={styles.headerContentRowText}>{this.getClassName(data.FlightCabinClass)}</Text>
            <View style={styles.headerRowDevider}></View>
            <Text style={styles.headerContentRowText}>{data.ADT + data.CHD + data.INF} Traveller</Text>
          </View>
        </View>
        <TouchableOpacity style={styles.headerButton} onPress={() => fireEvent()}>
          <Image source={Images.imgfilter} style={{ width: 16, height: 16, resizeMode: "contain" }}></Image>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  headerRowDevider: {
    height: 12, width: 1.5,
    backgroundColor: colors.colorWhite,
    marginLeft: 7,
    marginRight: 7,
    marginTop: 5
  },
  headerContentRowText: {
    color: colors.colorWhite,
    fontSize: 12,
    fontFamily: Fonts.medium,
    lineHeight: 20
  },
  headerContentIcon: {
    width: 20, height: 20,
    resizeMode: "contain",
    marginLeft: 10,
    marginRight: 10
  },
  headerStnName: {
    color: colors.colorWhite,
    fontSize: 14,
    fontFamily: Fonts.semiBold
  },
  headerContentRow: { width: "100%", flexDirection: "row", justifyContent: "center" },
  headerContent: {
    height: "100%",
    width: screenWidth - 120,
    padding: 8,
    justifyContent: "center"
  },
  backBtnText: {
    color: colors.colorWhite,
    fontFamily: Fonts.semiBold
  },
  container: {
    flex: 1,
    backgroundColor: "rgba(244,248,254,1)"
  },
  header: {
    height: 55,
    width: screenWidth,
    backgroundColor: colors.colorBlue,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 12,
    paddingRight: 12
  },
  headerBackButton: {
    width: 60,
    // backgroundColor: "red",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  headerButton: {
    height: "100%",
    width: 40,
    // backgroundColor: "blue",
    justifyContent: "center",
    alignItems: "flex-end"
  },
})