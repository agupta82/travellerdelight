import React, { Component } from "react";
import {
  Animated,
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList,
  Image,
  TouchableHighlight,
  TextInput
} from "react-native";
import Images from "../Images";
import { colors } from "../Colors";

export default class CustomTextInput extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableHighlight
        style={this.props.style || { height: 55, width: "100%" }}
        underlayColor="transparent"
        onPress={() =>
          this.props.onResponderStart ? this.props.onResponderStart() : {}
        }
      >
        <View
          style={[
            styles.styleTextImageContainer,
            { borderBottomColor: this.props.tintColor || "gray" }
          ]}
        >
          {/* <View style={styles.styleImageContainer}>
            <Image
              style={{
                height: 20,
                width: 20,
                tintColor: this.props.tintColor || "gray"
              }}
              source={this.props.icon || Images.imgusericon}
              resizeMode="contain"
            />
          </View> */}

          <View style={styles.styleTextInputContainer}>
            <TextInput
              onTouchStart={() => {
                console.log("start touch");
              }}
              style={styles.styleTextField}
              placeholderTextColor={this.props.placeholderTextColor ? this.props.placeholderTextColor : colors.colorBlack}
              placeholder={this.props.placeholder}
              value={this.props.value}
              editable={this.props.isEditable}
              onChangeText={text => this.props.onChangeText(text)}
              onResponderStart={() =>
                this.props.onResponderStart ? this.props.onResponderStart() : {}
              }
              // onFocus={() => {
              //   console.log("focused");
              // }}
              keyboardType={this.props.keyboardType || "default"}
              maxLength={this.props.maxLength}
              enablesReturnKeyAutomatically={true}
              textContentType={this.props.contentType}
              autoCapitalize={this.props.autoCapitalize}
              autoCorrect={this.props.autoCorrect}
            />
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  styleTextImageContainer: {
    width: "100%",
    flexDirection: "row",
    borderBottomWidth: 1,
    height: "75%"
  },
  styleImageContainer: {
    width: 20,
    height: "100%",
    justifyContent: "center",
    marginLeft: 5
    // alignItems: "center",
    // backgroundColor:'green'
  },
  styleTextInputContainer: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
    marginLeft: 10
  },
  styleTextField: {
    height: "90%",
    width: "100%",
    color: 'black'
    // fontSize: 16
  }
});
